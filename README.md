# [Rzine.fr](https://rzine.fr/) [<img src="https://rzine.fr/img/Rzine_logo.png"  align="right" width="120"/>](http://rzine.fr/)

[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)

**Site web pour la diffusion et le partage de ressources sur la pratique de R en sciences humaines et sociales** : [https://rzine.fr/](https://rzine.fr/)

**Rzine** est un projet de la [Fédération de Recherche Collège international des sciences territoriales](https://cist.cnrs.fr/) (FR CIST 2007).

[Rzine.fr](https://rzine.fr/) est un [blogdown](https://github.com/rstudio/blogdown) construit à partir du template [Hugo](https://gohugo.io/) [Academic](https://themes.gohugo.io/academic/).



