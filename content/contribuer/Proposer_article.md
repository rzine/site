+++
title = "Proposer un article Rzine"
subtitle = ""
date = 2020-06-30
widget = "work_in_progress"
weight = 10 # Order that this section will appear.
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
+++

**Vous souhaitez valoriser une méthode de traitement ou d'analyse fait avec R dans un périmètre pluridisciplinaire ? Publiez un article Rzine !** 

Les articles Rzine offrent un espace de valorisation et de partage scientifique au moyen de la programmation lettrée, liant code R et écriture scientifique usuelle (dite "naturelle"). Ce sont des publications pédagogiques et didactiques, reproductibles et ouvertes, soumises à un processus de relecture par les pairs.



L’intégralité du processus de soumission d’un article Rzine est expliqué dans ce document :

<figure class="center">
  <a href="https://rzine-reviews.github.io/documentation/" target="_blank"><img src="/site/img/publish_rzine.png" width="100%"></a>
  
  <figcaption style="font-size:14px;"> « <a href="https://rzine-reviews.github.io/documentation/" target="_blank"><b>Publier un article Rzine</b></a> »</figcaption>

</figure>



En cas d’acceptation par le comité éditorial, le processus de relecture se déroule publiquement sur <a href="https://github.com/rzine-reviews" target="_blank">GitHub</a> afin de garantir la transparence des évaluations, et de faciliter le travail de co-construction de ce document computationnel avec l’auteur·e. Les éditeur·rices sont à votre disposition pour vous accompagner dans ce processus.

Contact des membres du comité éditorial en cas de question relative à la soumission d'un article : <a href="mailto:collection@rzine.fr" target="_blank"><b>collection[a]rzine.fr</b></a>.
