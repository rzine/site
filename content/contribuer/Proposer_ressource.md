+++
title = "Proposer une ressource"
subtitle = "Participer à l'ajout de ressources et d'initiatives"
date = 2021-01-18
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 5
+++


**Rzine est un site collaboratif. Vous pouvez proposer le référencement d'une ressource ou d'une initiative sur la pratique de R en SHS**.

**L'ajout de contenu est soumis au vote du** [**comité éditorial**](https://rzine.fr/a_propos/#people). **Ce vote, à la majorité, est guidé par les critères suivants :**

- Gratuité de la ressource, sans renvoi publicitaire évident
- Respect du règlement général sur la protection des données
- Rédaction dans un langage correct
- Dans le champ des méthodes applicables aux SHS
- Clarté et rigueur de la ressource
- Clarté et fonctionnement du code présenté
- Intérêt pour la communauté des utilisateurs de R en SHS
- Pérennité du mode de stockage
- Mise à disposition des données utilisées (très appréciée)
- Mise à disposition du code source (très appréciée)


**Vous pouvez soumettre une ressource ou une initiative via** <a href="http://enquetes-passages.huma-num.fr/index.php?r=survey/index&sid=996947&lang=fr" target="_blank">**ce formulaire**</a>. Il permet de renseigner les métadonnées essentielles associées à un contenu référencé. Une saisie partielle  de ces informations peut automatiquement entrainer le refus de son référencement par le comité éditorial.








