+++
# A "Meet the Team" section created with the People widget.
# This section displays people from `content/authors/` which belong to the `user_groups` below.

widget = "comite_editorial"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Comité éditorial Rzine"
subtitle = ""

[content]
  # Choose which groups/teams of users to display.
  #   Edit `user_groups` in each user's profile to add them to one or more of these groups.
  user_groups = ["Direction", "Édition", "Ont participé"]

[design]
  # Show user's social networking links? (true/false)
  show_social = true

  # Show user's interests? (true/false)
  show_interests = false

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.
  
  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  # image = "background.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  # text_color_light = true  
  
[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++



<style type='text/css'>

.section_title {
font-weight:bold;
font-size:22pt;
}

.people {
font-style:italic;
font-size:16pt;
}

</style>

<div class="section_title">Direction</div> 

<div class="people"><a href="../authors/jgravier/" target="_blank">Gravier Julie </a> - Chargée de recherche (UMR ThéMA, CNRS)</div>
<div class="people"><a href="../authors/mmaisonobe/" target="_blank">Maisonobe Marion</a> - Chargée de recherche (UMR Géographie-cités, CNRS)</div>
<div class="people"><a href="../authors/tgiraud/" target="_blank">Giraud Timothée</a> - Ingénieur de recherche en Sciences de l'information géographique (UAR RIATE, CNRS)</div>
<br>

<div class="section_title">Édition</div> 

<div class="people"><a href="../authors/lbeauguitte/" target="_blank">Beauguitte Laurent</a> - Chargé de recherche (UMR Géographie-cités, CNRS)</div>
<div class="people"><a href="../authors/mgentilhomme/" target="_blank">Gentilhomme Marion</a> - Ingénieure d'études en communication et édition(FR CIST, Université Paris 1 Panthéon-Sorbonne)</div>
<div class="people"><a href="../authors/rkrummeich/" target="_blank">Krummeich Raphaëlle</a> - Ingénieure de recherche en Humanités numériques (UMR IDEES, Université de Rouen Normandie)</div>
<div class="people"><a href="../authors/glecampion88/" target="_blank">Le Campion Grégoire</a> - Ingénieur d'études en Statistique (UMR PASSAGES, CNRS)</div>
<div class="people"><a href="../authors/emarveaux/" target="_blank">Marveaux Elina</a> - Ingénieure d'études en Sciences de l'information géographique (FR CIST, CNRS)</div>
<div class="people"><a href="../authors/hpecout/" target="_blank">Pecout Hugues</a> - Ingénieur d'études en Sciences de l'information géographique (UMR Géographie-cités, CNRS)</div>
<div class="people"><a href="../authors/splutniak/" target="_blank">Plutniak Sébastien</a> - Chargé de recherche (UMR CITERES, CNRS)</div>
<div class="people"><a href="../authors/srey/" target="_blank">Rey-Coyrehourcq Sébastien</a> - Ingénieur de recherche en Informatique (UMR IDEES, Université de Rouen Normandie)</div>
<div class="people"><a href="../authors/rysebaert/" target="_blank">Ysebaert Ronan</a> - Ingénieur d'études en Sciences de l'information géographique (UAR RIATE, Université Paris Cité)</div>


<br>

<div class="section_title">Ancien·nes membres</div> 

<div class="people"><a href="../authors/mletexier86/" target="_blank">Le Texier Marion</a> - Maîtresse de Conférences en Géographie (EA LAGAM, Université Paul Valéry - Montpellier 3)</div>
<div class="people"><a href="../authors/vjurie/" target="_blank">Jurie Violaine</a> - Ingénieure d'études en Sciences de l'information géographique (Office français de la biodiversité)</div>









