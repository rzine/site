+++
# Hero widget.
widget = "logo"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 60  # Order that this section will appear.

height = "100px"
title = "Logo"
# content = ""

# Hero image (optional). Enter filename of an image in the `static/img/` folder.
logo1 = "logo/cist-logo-bleu_sur_blanc.png"
logo2 = "logo/LOGO_CNRS_2019_RVB.png"
logo3 = "logo/logo-univ-rouen-normandie.png"

[design.background]
  text_color_light = true
+++


