+++
# A "Meet the Team" section created with the People widget.
# This section displays people from `content/authors/` which belong to the `user_groups` below.

widget = "evaluateurs"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Évaluateur·rices d'articles Rzine"
subtitle = ""

[content]
  # Choose which groups/teams of users to display.
  #   Edit `user_groups` in each user's profile to add them to one or more of these groups.
  user_groups = ["Direction", "Édition", "Ont participé"]

[design]
  # Show user's social networking links? (true/false)
  show_social = true

  # Show user's interests? (true/false)
  show_interests = false

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.
  
  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  # image = "background.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  # text_color_light = true  
  
[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++


<style type='text/css'>

.section_title {
font-weight:bold;
font-size:22pt;
}

.people {
font-style:italic;
font-size:16pt;
}

</style>

<div class="section_title">2024</div> 

<div class="people"><a href="../authors/lbeauguitte/" target="_blank">Laurent Beauguitte</a> - Chargé de recherche (UMR Géographie-cités, CNRS)</div>
<div class="people"><a href="../authors/pgourdon/" target="_blank">Paul Gourdon</a> - Ingénieur de recherche (UMR LATTS, CNRS)</div>
<div class="people"><a href="../authors/tfeuillet/" target="_blank">Feuillet Thierry</a> - Professeur de Géographie (UMR IDEES, Université de Rouen Normandie)</div>
<div class="people"><a href="../authors/hmathian/" target="_blank">Mathian Hélène</a> - Ingénieure de recherche en sciences de l'information géographique (UMR EVS, CNRS)</div>
<div class="people">Pons Armand - Doctorant (Laboratoire Image Ville Environnement, CNRS)</div>
<div class="people">Viry Matthieu - Ingénieur de recherche en développement logiciel (UAR RIATE, CNRS)</div>



<br>


<div class="section_title">2023</div> 

<div class="people"><a href="../authors/dmouralis/" target="_blank">Mouralis Damase</a> - Professeur de Géographie (UMR IDEES, Université de Rouen Normandie)</div>
<div class="people"><a href="../authors/mmaisonobe/" target="_blank">Pierson Julie</a> - Ingénieure d’études en sciences de l'information géographique (UMR LETG, CNRS)</div>

<br>

<div class="section_title">2021</div> 

<div class="people"><a href="/contribuer/#comite_editorial" target="_blank">Comité éditorial Rzine</a></div>




