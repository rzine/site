---
date: "2018-06-28T00:00:00+01:00"
header:
  caption: ""
  image: ""
share: false
title: Politique de confidentialité
---

**Rzine n'affiche aucune information personnelle sans autorisation**.
Tous les renseignements personnels récupérés sont directement fournis par les auteur·rices de publication dans un fichier Markdown. Les informations collectées sont :     

- *Nom*    
- *Prénom*    
- *Adresse électronique*    
- *Statut*    
- *Discipline*    
- *Employeur et unité de rattachement*     
- *Url de compte ou site personnel (Twitter, Git, Blog)*    



**Ces informations sont uniquement utilisées pour les finalités suivantes** :    

- *Affichage sur le site*    
- *Contact*    
- *Statistique*    


**Nous nous engageons à vous offrir un droit d'opposition et de retrait quant à vos renseignements personnels**. Le droit d'opposition s'entend comme étant la possibilité offerte aux auteur·rices de refuser que leurs renseignements personnels soient utilisés à certaines fins affichées ci-dessus. Le droit de retrait s'entend comme étant la possibilité offerte aux auteur·rices de demander à ce que leurs renseignements personnels ne soient plus affichés sur le site ou utilisés dans une liste de diffusion.

**Pour pouvoir exercer ces droits, contactez : contact@rzine.fr**   
Responsable de traitement : Hugues Pecout     

Sans réponse ou traitement de notre part, vous pouvez contacter le/la délégué·e à la protection des données à l’adresse suivante :
*DPD – 17 rue Notre Dame des Pauvres – 54519 – Vandoeuvre lès Nancy Cedex* - ***dpd.demandes@cnrs.fr***   

*Enfin, si vous estimez, après un délai raisonnable, que vos droits informatique et libertés ne sont pas respectés, vous avez la possibilité d’introduire une réclamation auprès de la [CNIL](https://www.cnil.fr/) en ligne ou par courrier postal*.

<br>   

#### Une instance Matomo pour la mesure de statistiques web   


**Ce site Web utilise Matomo**, un logiciel libre hébergé en interne, pour recueillir des statistiques anonymes sur l'utilisation de ce site.

Les données sont utilisées pour analyser le comportement des visiteurs du site afin d'identifier les pièges potentiels tels que les pages non trouvées, les problèmes d'indexation des moteurs de recherche et pour savoir quels contenus sont les plus appréciés. 

**Matomo traite uniquement les données suivantes** :

- Cookies   
- Adresse IP anonymisée (suppression des deux derniers octets)
- Localisation pseudo-anonyme de l'utilisateur (IP anonyme)
- Date et heure
- Titre de la page consultée
- URL de la page consultée
- URL de la page qui a été consultée avant la page actuelle
- Résolution de l'écran
- Heure dans le fuseau horaire local
- Fichiers sur lesquels on a cliqué et qui ont été téléchargés
- Liens cliqués vers un domaine extérieur
- Heure de génération des pages
- Pays, région, ville (basse résolution basée sur l'adresse IP)
- Langue principale du navigateur
- Agent utilisateur du navigateur
- Interactions avec les formulaires (mais pas le contenu).

