---
date: "2018-06-28T00:00:00+01:00"
header:
  caption: ""
  image: ""
share: false
title: Conditions d'utilisation
---

<div class="alert alert-danger" role="alert">
<b>Les ressources référencées ne répondent pas toutes aux mêmes conditions d'utilisation. Les auteur·rices sont libres de choisir la licence d'utilisation pour chacune de leur publication</b>.
</div>


**En revanche, l’intégralité des articles de la collection Rzine est soumise à la licence Creative Commons** [**CC BY-SA 4.0**](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), et ainsi complètement libre de droit. Cette licence vous autorise à :

- **Partager — copier**, distribuer et communiquer le  matériel par tous moyens et sous tous formats.    
- **Adapter — modifier**, transformer et créer à partir du matériel pour toutes utilisations, y compris commerciales.    


Selon les conditions suivantes :

- **Attribution**     
Vous devez créditer l'œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'offrant vous soutient ou soutient la façon dont vous avez utilisé son œuvre.

- **Partage dans les mêmes conditions**    
Dans le cas où vous effectuez une adaptation/modification, que vous transformez ou créez à partir du matériel composant l'œuvre originale, vous devez diffuser l'œuvre modifiée dans les mêmes conditions, c'est à dire avec la licence CC BY-SA 4.0.

- **Pas de restrictions complémentaires**      
Vous n'êtes pas autorisé·e à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'œuvre dans les conditions décrites par la licence.

