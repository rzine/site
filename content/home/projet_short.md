+++
title = "Le projet Rzine"
subtitle = "Encourager la production et favoriser la diffusion de documentation sur la pratique de R en SHS"
date = 2019-09-18
widget = "presentation_short"
weight = 10 # Order that this section will appear.
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false

hero_media = "Rzine_logo.png"
hero_media2 = "rzine_logo_bis.png"
+++


Rzine est un projet de la <a href="http://cist.cnrs.fr/" target="_blank">Fédération de Recherche CIST</a> démarré début 2018. Son objectif est de **contribuer à l'apprentissage et à la pratique de R en Sciences Humaines et Sociales, en favorisant le partage, la production et la diffusion de ressources**. Ce projet collaboratif et interdisciplinaire s'étend au-delà des équipes partenaires de la FR CIST et tente de répondre à un besoin commun à de nombreuses unités de recherche en Sciences Humaines et Sociales. La coordination du projet est assurée par un [**comité éditorial**](./contribuer/#comite_editorial) qui associe des membres de différents statuts, disciplines et unités de recherche. Actuellement, le projet est organisé en deux axes de travail :

**-  Le référencement maîtrisé de** [**ressources diverses**](./ressources/) sur la pratique de R en SHS.     
**-  La direction d'une** [**collection d'articles**](./articles_rzine/) soumis à comité de lecture, qui permet valoriser des traitements réalisés avec R.

Une présentation plus approfondie du projet Rzine est disponible sur [**cette page**](projet/rzine).






