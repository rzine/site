+++
# Hero widget.
widget = "welcome"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 5  # Order that this section will appear.

height = "450px"
title = "Rzine"
content = "Encourager la production et favoriser la diffusion de documentation sur la pratique du langage R en Sciences Humaines et Sociales"

# Hero image (optional). Enter filename of an image in the `static/img/` folder.
hero_media = "Rzine_logo.png"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "#4B79BD"
  
  # Background gradient.
  gradient_start = "#5B89C1"
  # gradient_start = "#B5A33F"
  # gradient_start = "#575757"
  # gradient_start = "#f3ac80"
  gradient_end = "#4B79BD"
  
  # Background image.
  #  image = "headers/header.jpg" 
  #  image_darken = 0.3  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = true

+++


