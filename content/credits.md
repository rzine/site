---
date: "2018-06-28T00:00:00+01:00"
header:
  caption: ""
  image: ""
share: false
title: Crédits    
---

<style type='text/css'>

img[src*='#left'] {
    float: left;
}
img[src*='#right'] {
    float: right;
}
img[src*='#center'] {
    display: block;
    margin: auto;
}

</style>

<div style="font-weight:bold;text-align:center;font-size:15pt">Sité édité par la FR CIST</div> 
<div style="font-style:italic;text-align:center;font-size:13pt;">Fédération de recherche <a href="http://cist.cnrs.fr/" target="_blank" rel="noopener">collège international des sciences territoriales</a> (FR 2007)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;">Université Paris Cité UFR GHES – Case 7001</div>
<div style="font-style:italic;text-align:center;font-size:13pt;">8 place Paul Ricoeur - 75205 Paris cedex 13</div>
<br>
<div style="font-weight:bold;text-align:center;font-size:15pt">Hébergeurs</div> 
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="https://www.univ-rouen.fr/" target="_blank">Université de Rouen Normandie</a> (serveur)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="https://www.cnrs.fr/fr/page-daccueil" target="_blank">CNRS</a> (forge & nom de domaine)</div> 
<br>

<div style="font-weight:bold;text-align:center;font-size:15pt">Administrateur</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/hpecout/" target="_blank">Hugues Pecout</a> (CNRS, UMR Géographie-cités)</div>
<div style="font-weight:bold;text-align:center;font-size:13pt">Co-administrateur·rices</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/tgiraud/" target="_blank">Timothée Giraud</a> (CNRS, UAR RIATE)</div> 
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/glecampion88/" target="_blank">Grégoire Le Campion</a> (CNRS, UMR PASSAGES)</div> 
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/emarveaux/" target="_blank">Elina Marveaux</a> (CNRS, FR CIST)</div> 
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/vjurie/" target="_blank">Violaine Jurie</a> (Office français de la biodiversité)</div> 
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/rysebaert/" target="_blank">Ronan Ysebaert</a> (Université Paris Cité, UAR RIATE)</div>
<br>

<div style="font-weight:bold;text-align:center;font-size:15pt">Architecture et maintenance</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/tgiraud/" target="_blank">Timothée Giraud</a> (CNRS, UAR RIATE)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/srey/" target="_blank">Sébastien Rey-Coyrehourcq</a> (Université de Rouen Normandie, UMR IDEES)</div>
<br>





<div style="font-weight:bold;text-align:center;font-size:15pt">Conception du site</div>
<div style="font-weight:bold;text-align:center;font-size:13pt">Rzine core team</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/lbeauguitte/" target="_blank">Laurent Beauguitte</a> (CNRS, UMR Géographie-cités)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/tgiraud/" target="_blank">Timothée Giraud</a> (CNRS, UAR RIATE)</div> 
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/mletexier86/" target="_blank">Marion Le Texier</a> (EA LAGAM, Université Paul Valéry - Montpellier 3)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/hpecout/" target="_blank">Hugues Pecout</a> (CNRS, UMR Géographie-cités)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/srey/" target="_blank">Sébastien Rey-Coyrehourcq</a> (Université de Rouen Normandie, UMR IDEES)</div>
<br>
<div style="font-weight:bold;text-align:center;font-size:15pt">Réalisation du site & logo</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/hpecout/" target="_blank">Hugues Pecout</a> (CNRS, UMR Géographie-cités)</div> 
<div style="font-style:italic;text-align:center;font-size:13pt">À partir du thème <a href="https://sourcethemes.com/academic/" target="_blank" rel="noopener">Academic</a> pour le générateur de sites <a href="https://gohugo.io" target="_blank" rel="noopener">Hugo</a></div> 
<br>

<div style="font-weight:bold;text-align:center;font-size:15pt">Avec la participation de</div>
<div style="font-style:italic;text-align:center;font-size:13pt;">Clément Billaux (Université de Rouen Normandie)</div> 
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/mgentilhomme/" target="_blank">Marion Gentilhomme</a> (Université Paris 1 Panthéon-Sorbonne, FR CIST)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;">Christine Hadrossek (CNRS, Direction de l'Information Scentifique et Technique)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/vjurie/" target="_blank">Violaine Jurie</a> (Office français de la biodiversité)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;"><a href="/authors/nlambert76/" target="_blank">Nicolas Lambert</a> (CNRS, UAR RIATE)</div>
<div style="font-style:italic;text-align:center;font-size:13pt;">Olivier Pissoat (CNRS, UMR PASSAGES)</div> 
<br>
 <div class="row">
   <div class="column" float="left" style="width:33%">
    <img src="/site/img/logo/cist-logo-bleu_sur_blanc.png" style="width:45%">
  </div>
  <div class="column" float="left" style="width:33%">
    <img src="/site/img/logo/LOGO_CNRS_2019_RVB.png"  style="width:40%">
  </div>
  <div class="column" float="left" style="width:33%;padding-top:20px;">
    <img src="/site/img/logo/logo-univ-rouen-normandie.png" style="width:67%">
  </div>
</div> 
