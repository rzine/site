---
authors:
- vjurie

name: Violaine Jurie
role: Ingénieure d’études en sciences de l’information géographique

organizations:
- name: Office français de la biodiversité
  url: "https://www.ofb.gouv.fr/"



superuser: false
user_groups:
- Édition
- Comité éditorial
---


