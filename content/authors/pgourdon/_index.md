---
authors:
- pgourdon

name: Paul Gourdon
role: Ingénieur de recherche en production, traitement et analyse de données
email: 'paul.gourdon@enpc.fr'

organizations:
- name: UMR LATTS
  url: "https://latts.fr/"
- name: CNRS 
  url: "http://www.cnrs.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'paul.gourdon@enpc.fr'
- icon: github
  icon_pack: fab
  link: https://github.com/pgourdongeo
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0009-0008-7303-8583

superuser: false
user_groups:
- Autres
---


