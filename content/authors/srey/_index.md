---
# = première lettre prenom + NOM == Nom du dossier !!!
authors:
- srey

# Prenom Nom (nom affiché)
name: Sébastien Rey-Coyrehourcq

# Statut
role: Research Engineer
# Trés courte bio
bio: Blablablm

# Email
email: "sebastien.rey-coyrehourcq@univ-rouen.fr"


# Laboratoire de rattachement
organizations:
- name: UMR IDEES
  url: "http://umr-idees.fr/"
- name: Université de Rouen Normandie
  
# Formation
# education:
#   courses:
#   - course: MASTER 2 Carthageo PRO
#     institution: Univesrité Paris 1
#     year: 2009
#   - course: ...
#     institution: ...
#     year: ...

# thématique de recherche
interests:
- Cobol
- Fortran
- VBA


# Social
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:sebastien.rey-coyrehourcq@univ-rouen.fr'
# - icon: twitter
#   icon_pack: fab
#   link: https://twitter.com/???
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.co.uk/citations?user=???
# - icon: github
#   icon_pack: fab
#   link: https://github.com/???

# Là il faut que je regarde a quoi ca peut nous servir...
superuser: false
user_groups:
- Autres
- Comité éditorial
---


