---
authors:
- msalmon

name: Maëlle Salmon
role: R(esearch) Software Engineer 
email: 'maelle.salmon@yahoo.se'


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:maelle.salmon@yahoo.se'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/ma_salmon
- icon: blog
  icon_pack: fa
  link: https://masalmon.eu/
- icon: github
  icon_pack: fab
  link: 'https://github.com/maelle'

superuser: false
user_groups:
- Autres
---


