---
# DISPLAY NAME
name: Martin chevalier

# USERNAME
authors: mchevalier

# STATUT
role:  Administrateur de l'INSEE

# EMPLOYEUR et/ou UNITE
organizations:
- name: INSEE
  url: "https://www.insee.fr/fr/accueil"

# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:martin.chevalier@insee.fr'

# Blog
- icon: blog
  icon_pack: fa
  link: https://teaching.slmc.fr

# Github
- icon: github
  icon_pack: fab
  link: https://github.com/martinchevalier

# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---
