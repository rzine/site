---
# DISPLAY NAME
name: Evangeline Reynolds

# USERNAME
authors: Ereynolds

# STATUT
role: Visiting Teaching Assistant Professor

# EMPLOYEUR et/ou UNITE
organizations:
- name: University of Denver, 
  url: "https://www.du.edu/"
- name: Korbel School of International Studies
  url: "https://korbel.du.edu/"

# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:evangeline.reynolds@du.edu'
  
# Google scholar  
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=VfkVW3sAAAAJ&hl=en
  
# Blog
- icon: blog
  icon_pack: fa
  link: https://evangelinereynolds.netlify.com/

# Github
- icon: github
  icon_pack: fab
  link: https://github.com/evamaerey
   
# Twitter  
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/EvaMaeRey

# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---
