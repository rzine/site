---
authors:
- splutniak
name: Sébastien Plutniak
role: Chargé de recherche
email: sebastien.plutniak@posteo.net
organizations:
- name: UMR CITERES
  url: 'http://citeres.univ-tours.fr/'
- name: CNRS
  url: 'https://www.cnrs.fr/fr/page-daccueil'
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:sebastien.plutniak@posteo.net'
- icon: github
  icon_pack: fab
  link: https://github.com/sebastien-plutniak
- icon: blog
  icon_pack: fa
  link: https://sebastien-plutniak.github.io/
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/sebplutniak
# - icon: youtube
#   icon_pack: fab
#   link: 
superuser: false
user_groups:
- Autres
- Comité éditorial
---

