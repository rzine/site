---
# = première lettre prenom + NOM == Nom du dossier !!!
authors:
- jlarmarange

# Prenom Nom (nom affiché)
name: Joseph Larmarange

# Statut
role: Chargé de recherche


# Email
email: 'joseph.larmarange@ceped.org'

# Laboratoire de rattachement
organizations:
- name: IRD
  url: "https://www.ird.fr/"
- name: Centre Population & Développement (UMR 196)
  url: "https://www.ceped.org/"


# Social
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:joseph.larmarange@ceped.org'
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=pQDKEIUAAAAJ&hl=en
- icon: github
  icon_pack: fab
  link: https://github.com/larmarange/
# - icon: twitter
#   icon_pack: fab
#   link: https://twitter.com/???

# PAS MODIFIER
superuser: false
user_groups:
- Autres
---




