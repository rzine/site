---
authors:
- lvaudor

name: Lise vaudor
role: Ingénieure de recherche  
email: 'lise.vaudor@ens-lyon.fr'

organizations:
- name: UMR 5600 Environnement Ville Société
  url: "http://umr5600.cnrs.fr/fr/accueil/"
- name: CNRS 
  url: "http://www.cnrs.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:lise.vaudor@ens-lyon.fr'
- icon: youtube
  icon_pack: fab
  link: https://www.youtube.com/channel/UC9QOCBVGHp4gnmcmkG1VbGg

superuser: false
user_groups:
- Autres
---


