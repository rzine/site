---
authors:
- faudard

name: Frédéric Audard
role: Maître de conférences
email: 'frederic.audard@univ-brest.fr'

organizations:
- name: LETG (UMR 6554)
  url: "https://letg.cnrs.fr"
- name: Université de Bretagne Occidentale 
  url: "https://www.univ-brest.fr"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:frederic.audard@univ-brest.fr'

superuser: false
user_groups:
- Publications Rzine
---


