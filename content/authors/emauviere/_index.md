---
authors:
- emauviere

name: Éric Mauvière
role: Statisticien - économiste
email: 'etienne.come@univ-eiffel.fr'

organizations:
- name: Président Icem7
  url: "https://www.icem7.fr"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:eric.mauviere@icem7.fr'
- icon: github
  icon_pack: fab
  link: https://github.com/icem7-open
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/ericMauviere


superuser: False
user_groups:
- Autres
---


