---
# = première lettre prenom + NOM == Nom du dossier !!!
authors:
- bgarnier

# Prenom Nom (nom affiché)
name: Bénédicte Garnier

# Statut
role: Ingénieure d’études 


# Email
email: 'garnier@ined.fr'


# Laboratoire de rattachement
organizations:
- name: INED
  url: "https://www.ined.fr/fr/"


# Social
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:garnier@ined.fr'
# - icon: twitter
#   icon_pack: fab
#   link: https://twitter.com/???
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.co.uk/citations?user=???
# - icon: github
#   icon_pack: fab
#   link: https://gitlab.huma-num.fr/hpecout

# PAS MODIFIER
superuser: false
user_groups:
- Autres
---


