---
authors:
- dbirre

name: Déborah Birre
role:  Doctorante en Géographie 
email: 'deborah.birre@gmail.com'


organizations:
- name: Université Sorbonne Paris Nord
  url: "https://www.univ-spn.fr/"



social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:deborah.birre@gmail.com'


superuser: false
user_groups:
- Publications Rzine
---


