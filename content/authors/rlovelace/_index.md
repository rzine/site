---
name: Robin Lovelace

authors: rlovelace

role: Associate Professor of Transport Data Science

organizations:
- name: Leeds Institute for Transport Studies (ITS)
  url: "https://environment.leeds.ac.uk/transport"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:R.Lovelace@leeds.ac.uk'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/robinlovelace  
- icon: blog
  icon_pack: fa
  link: https://www.robinlovelace.net/
- icon: github
  icon_pack: fab
  link: https://github.com/robinlovelace
   

superuser: false
user_groups: 
- Autres
---
