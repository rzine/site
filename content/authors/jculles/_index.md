---
authors: jculles

name: Jean-Clément Ullès

role: Doctorant en Géographie

organizations: 
- name: LAGAM
  url: "https://lagam.xyz/"
- name: Université Paul Valéry - Montpellier 3
  url : "https://www.univ-montp3.fr/"

superuser: false
user_groups:
- Publications Rzine
---

