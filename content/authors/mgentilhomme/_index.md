---
authors:
- mgentilhomme

name: Marion Gentilhomme
role: Ingénieure d’études en communication et édition
email: 'marion.gentilhomme@univ-paris1.fr'

organizations:
- name: FR CIST
  url: "http://cist.cnrs.fr/"
- name: Université Paris 1 Panthéon-Sorbonne
  url: "https://www.pantheonsorbonne.fr/accueil"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:marion.gentilhomme@univ-paris1.fr'


superuser: false
user_groups:
- Édition
- Comité éditorial
---


