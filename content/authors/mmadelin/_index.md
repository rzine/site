---
authors:
- mmadelin

name: Malika Madelin
role: Maîtresse de conférences
email: 'malika.madelin@univ-paris-diderot.fr'

organizations:
- name: PRODIG (UMR 8586)
  url: "https://www.prodig.cnrs.fr/"
- name: Université Paris Cité
  url: https://u-paris.fr/


social:
- icon: envelope
  icon_pack: fas
  link: 'malika.madelin@univ-paris-diderot.fr'
- icon: github
  icon_pack: fab
  link: https://github.com/mmadelin



superuser: false
user_groups:
- Autres
---


