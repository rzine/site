---
# DISPLAY NAME
name: Kim Antunez

# USERNAME
authors: kantuki

# STATUT
role: Administratrice de l'Insee en formation

# EMPLOYEUR et/ou UNITE
organizations:
- name: ENSAE
  url: "https://www.ensae.fr/en/"
- name: INSEE
  url: "https://www.insee.fr/fr/accueil"

# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:kim.antunez@ensae.fr'

# Blog
- icon: blog
  icon_pack: fa
  link: https://antuki.github.io/

# Github
- icon: github
  icon_pack: fab
  link: https://github.com/antuki
   
# Twitter  
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/antuki13

# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---
