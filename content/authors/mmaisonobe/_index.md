---
authors:
- mmaisonobe

name: Marion Maisonobe
role: Chargée de recherche au CNRS en géographie
email: 'Marion.MAISONOBE@cnrs.fr'

organizations:
- name: UMR Géographie-cités
  url: "https://www.parisgeo.cnrs.fr/"
- name: CNRS 
  url: "http://www.cnrs.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:Marion.MAISONOBE@cnrs.fr'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/GeoMaisonobe
- icon: blog
  icon_pack: fa
  link:  https://geoscimo.univ-tlse2.fr
- icon: github
  icon_pack: fab
  link: https://github.com/Marion-Mai
- icon: gitlab
  icon_pack: fab
  link: https://framagit.org/MarionMai


superuser: false
user_groups:
- Autres
- Direction
- Comité éditorial
---


