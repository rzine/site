---
# DISPLAY NAME
name: Olivier Meslin
authors: omeslin

role: Économiste & data scientist

organizations:
- name: SSP Lab
  url: "http://ssplab.lab.sspcloud.fr/index/"
- name: INSEE
  url: "https://www.insee.fr/fr/accueil"

social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/oliviermeslin

superuser: false
user_groups: 
- Autres
---
