---
authors:
- otheureaux

name: Olivier Theureaux
role: Ingénieur d’études en sciences de l’information géographique
email: 'olivier.theureaux@gmail.com'

organizations:
- name: CNRS 
  url: "http://www.cnrs.fr/"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:olivier.theureaux@gmail.com'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/OTheureaux

superuser: false
user_groups:
- Publications Rzine
---


