---
# DISPLAY NAME
name: Julien Barnier

# USERNAME
authors: jbarnier

# STATUT
role: Ingénieur d’études (production, traitement, analyse de données et enquêtes)


# Email
email: 'julien.barnier@cnrs.fr'


# EMPLOYEUR et/ou UNITE
organizations:
- name: UMR5283 Centre Max Weber
  url: "https://www.centre-max-weber.fr/"
#  Employeur :
- name: CNRS 
  url: "http://www.cnrs.fr/"


# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:julien.barnier@cnrs.fr'
  
# Github
- icon: github
  icon_pack: fab
  link: https://github.com/juba
   
# Twitter  
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/lapply

# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---
