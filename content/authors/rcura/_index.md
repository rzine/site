---
authors:
- rcura

name: Robin Cura
role: Maître de conférences en Géomatique
email: 'robin.cura@univ-paris1.fr'

organizations:
- name: UMR PRODIG
  url: "https://www.prodig.cnrs.fr/"
- name: Université Paris 1 Panthéon-Sorbonne
  url: "https://www.pantheonsorbonne.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'robin.cura@univ-paris1.fr'
- icon: blog
  icon_pack: fa
  link: http://robin.cura.info/
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.fr/citations?user=kjWBWV8AAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/RCura
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/RobinCura
  

superuser: false
user_groups:
- Autres
---


