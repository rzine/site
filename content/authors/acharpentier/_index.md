---
authors:
- acharpentier

name: Arthur Charpentier
role: Professeur
email: 'charpentier.arthur@uqam.ca'

organizations:
- name: Université du Québec à Montréal (UQAM)
  url: "https://uqam.ca/"
- name: Quantact
  url: "http://quantact.uqam.ca/"
- name: CREM UMR 6211 CNRS 
  url: "https://crem.univ-rennes1.fr/"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:charpentier.arthur@uqam.ca'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/freakonometrics
- icon: blog
  icon_pack: fa
  link: https://freakonometrics.github.io/index.html
- icon: youtube
  icon_pack: fab
  link: https://www.youtube.com/channel/UCv3Y_a-KbK6DGtIwWOm_0yg/videos
- icon: github
  icon_pack: fab
  link: https://github.com/freakonometrics



superuser: false
user_groups:
- Autres
---


