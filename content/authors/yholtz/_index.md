---
authors:
- yholtz

name: Yan Holtz
role: Data Analysis - Data visualization
email: 'yan.holtz.data@gmail.com'

organizations:
- name: Datadog
  url: "https://www.datadoghq.com/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:yan.holtz.data@gmail.com'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/R_Graph_Gallery
- icon: blog
  icon_pack: fa
  link: https://www.yan-holtz.com/index.html
- icon: github
  icon_pack: fab
  link: https://github.com/holtzy



superuser: false
user_groups:
- Autres
---


