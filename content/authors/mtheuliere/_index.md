---
# DISPLAY NAME
name: Maël Theulière

# USERNAME
authors: mtheuliere

# STATUT
role: Administrateur INSEE, Chef de division à la DREAL

# EMPLOYEUR et/ou UNITE
organizations:
- name: DREAL (pays de Loire)
  url: "http://www.pays-de-la-loire.developpement-durable.gouv.fr/"
- name: INSEE
  url: "https://www.insee.fr/fr/accueil"

# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:mael.theuliere@developpement-durable.gouv.fr'

# Github
- icon: github
  icon_pack: fab
  link: https://github.com/MaelTheuliere
   
# Twitter  
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/TheuliereMael

# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---
