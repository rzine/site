---
# DISPLAY NAME
name: Hélène Mathian

# USERNAME
authors: hmathian

# STATUT
role: Ingénieure de recherche

# EMPLOYEUR et/ou UNITE
organizations:
- name: UMR Environnement Ville Société
  url: "https://umr5600.cnrs.fr/fr/accueil/"
- name: CNRS
  url: "http://www.cnrs.fr/"

# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:helene.mathian@ens-lyon.fr'
  
# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---
