---
authors:
- clalanne

name: Christophe Lalanne
role: Ingénieur de recherche
email: 'christophe.lalanne@univ-paris-diderot.fr'

organizations:
- name: Laboratoire Interdisciplinaire des Energies de Demain (UMR 8236)
  url: "http://www.lied-pieri.univ-paris-diderot.fr/"
- name: Université Paris Cité 
  url: "https://u-paris.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:christophe.lalanne@univ-paris-diderot.fr'
- icon: github
  icon_pack: fab
  link: https://github.com/biostatsante

superuser: false
user_groups:
- Autres
---


