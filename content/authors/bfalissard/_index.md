---
authors:
- bfalissard

name: Bruno Falissard
role: Professeur
email: 'bruno.falissard@gmail.com'

organizations:
- name: Université Paris-saclay
  url: "https://www.universite-paris-saclay.fr/"
- name: Directeur du CESP (INSERM U1018)
  url: "https://cesp.inserm.fr/"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:bruno.falissard@gmail.com'
- icon: youtube
  icon_pack: fab
  link: https://www.youtube.com/user/brunofalissardconf


superuser: false
user_groups:
- Autres
---


