---
authors:
- jpierson

name: Julie Pierson
role: Ingénieure d’études en sciences de l’information géographique
email: 'julie.pierson@univ-brest.fr'

organizations:
- name: LETG (UMR 6554)
  url: "https://letg.cnrs.fr/"
- name: CNRS 
  url: "http://www.cnrs.fr/"



social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:julie.pierson@univ-brest.fr'


superuser: false
user_groups:
- Relecture
---


