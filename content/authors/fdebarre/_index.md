---
name: Florence Débarre
authors: fdebarre
role: Chargée de recherche
organizations:
- name: CNRS
  url: https://www.cnrs.fr/
- name: Institut d’écologie et des sciences de l’environnement de Paris (iEES Paris)
  url: https://iees-paris.fr/
social:
- icon: envelope
  icon_pack: fas
  link: mailto:florence.debarre@normalesup.org
- icon: blog
  icon_pack: fa
  link: http://www.normalesup.org/~fdebarre/
- icon: google-scholar
  icon_pack: ai
  link: http://scholar.google.com/citations?user=WkZgrYsAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/flodebarre
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/flodebarre
superuser: no
user_groups: 
- Autres
---
