---

name: Nicolas Lambert
authors: nlambert76
role: Ingénieur de recherche en sciences de l'information géographique

organizations:
- name: UMS RIATE
  url: "http://riate.cnrs.fr/"
- name: CNRS
  url: "http://www.cnrs.fr/"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:nicolas.lambert@cnrs.fr'
- icon: blog
  icon_pack: fa
  link: https://neocarto.hypotheses.org/author/neocarto
- icon: github
  icon_pack: fab
  link: https://github.com/neocarto 
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/neocartocnrs
- icon: youtube
  icon_pack: fab
  link: https://www.youtube.com/channel/UCoZLDHWvG1cp4h6IH4nIegQ
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0003-4976-6560


superuser: false
user_groups: 
- Publications Rzine
---
