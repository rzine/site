---
# Author_ID
authors:
- hcommenges

# Prenom Nom
name: Hadrien Commenges

# Statut
role: MCF en géographie

# Laboratoire de rattachement
organizations:
- name: UMR Géographie-cités
  url: "www.parisgeo.cnrs.fr/"
#  Employeur :
- name: Université Paris 1 Panthéon Sorbonne 
  url: https://www.pantheonsorbonne.fr/

### Social
social:
# Email
- icon: envelope
  icon_pack: fas
  link: 'mailto:hadrien.commenges@univ-paris1.fr'
# Git
- icon: github
  icon_pack: fab
  link: https://github.com/hcommenges

### NE PAS MODIFIER
superuser: false
user_groups:
- Autres
---


