---
authors:
- jhguay

name: Jean-Herman Guay
role: professeur de science politique
email: 'Jean-Herman.Guay@USherbrooke.ca'

organizations:
- name: Université de Sherbrooke
  url: "https://www.usherbrooke.ca/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:Jean-Herman.Guay@USherbrooke.ca'
- icon: blog
  icon_pack: fa
  link: https://perspective.usherbrooke.ca/

superuser: false
user_groups:
- Autres
---


