---
# METADONNEES OBLIGATOIRES #########
# authors = Code name de l'auteur
# Ex : Jeanne Dupont = jdupont
authors: lbeauguitte

# name = Prenom Nom (nom affiché)
name: Laurent Beauguitte

# role = Fonction + discipline
role: Chargé de recherche

# organizations = Unité de recherche et employeur
organizations:
# Unité de recherche
- name: UMR Géographie-cités
  url: "http://www.parisgeo.cnrs.fr/"
#  Employeur :
- name: CNRS

# ###################################


# METADONNEES FACULTATIVES ##########
# SI VOUS NE SOUHAITEZ PAS UTILISEZ CERTAINES DE CES METADONNES :
# PASSER LES LIGNES CONECRNEES EN COMMENTAIRE (#)

# bio = Phrase de présentation

  
# ###################################
social:

# Blog
- icon: blog
  icon_pack: fa
  link: https://esprad.hypotheses.org/cv-com-et-publis-novembre-2017


# METADONNEES PAR DEFAULT - DON'T TOUCH ##########
# Role de l'utilisateur
superuser: false

# Groupe d'utilisateur
user_groups:
- Autres
- Comité éditorial
---

