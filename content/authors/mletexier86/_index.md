---
# DISPLAY NAME
name: Marion Le Texier

# USERNAME
authors: mletexier86
# STATUT
role: Maîtresse de conférences en géographie

# EMPLOYEUR et/ou UNITE
organizations: 
- name: LAGAM
  url: "https://lagam.xyz/"
- name: Université Paul Valéry - Montpellier 3
  url : "https://www.univ-montp3.fr/"

# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:marion.le-texier@univ-rouen.fr'
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0003-3762-1874
# Twitter  
# - icon: twitter
#   icon_pack: fab
#   link: 
# Google scholar  
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?user=pB_1ldYAAAAJ&hl=fr&oi=ao
# Github  
# - icon: github
#   icon_pack: fab
#   link: https://github.com/MLTgeo
  

# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Ont participé
---
