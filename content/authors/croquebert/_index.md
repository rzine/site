---
# DISPLAY NAME
name: Corentin Roquebert

# USERNAME
authors: croquebert

# STATUT
role: Doctorant en sociologie

# EMPLOYEUR et/ou UNITE
organizations:
- name: Centre Max Weber (UMR 5283)
  url: "https://www.centre-max-weber.fr/"
- name: ENS Lyon
  url: "http://www.ens-lyon.fr/"

# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:corentin.roquebert@gmail.com'
  
# Twitter  
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/croquebert

# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---
