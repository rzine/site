---
authors:
- hpecout

name: Hugues Pecout
role: Ingénieur d’études en sciences de l’information géographique
email: 'hugues.pecout@cnrs.fr'

organizations:
- name: UMR Géographie-cités
  url: "http://www.parisgeo.cnrs.fr/"
- name: CNRS 
  url: "http://www.cnrs.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:hugues.pecout@cnrs.fr'
- icon: github
  icon_pack: fab
  link: https://github.com/HuguesPecout
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.huma-num.fr/hpecout
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-0246-0954


superuser: false
user_groups:
- Autres
- Édition
- Comité éditorial
---


