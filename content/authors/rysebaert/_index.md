---
authors: rysebaert
name: Ronan Ysebaert
role: Geomatics Engineer
email: "ronan.ysebaert@cnrs.fr"

organizations:
- name: UMS RIATE
  url: "http://riate.cnrs.fr/"
- name: Université Paris Cité
  url: https://u-paris.fr/

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:ronan.ysebaert@cnrs.fr'
- icon: github
  icon_pack: fab
  link: https://github.com/rysebaert
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-7344-5911

superuser: false
user_groups:
- Publications Rzine
- Édition
- Comité éditorial
---




