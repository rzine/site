---
authors: frebaudo
name: François Rebaudo

role: Ingénieur de recherche

# EMPLOYEUR et/ou UNITE
organizations:
- name: Laboratoire Evolution, Génomes, Comportement, Ecologie
  url: "http://www.egce.cnrs-gif.fr/?lang=fr"
- name: IRD
  url: "https://www.ird.fr/"


# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:francois.rebaudo@egce.cnrs-gif.fr'
  

# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---

