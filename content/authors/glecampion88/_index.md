---
# Fisrt and last name
name: Grégoire Le Campion

# username
authors: glecampion88

# statut
role: Ingénieur d'études CNRS

# Employer and unit
organizations:
- name: UMR Passages
  url: "https://www.passages.cnrs.fr/"


# Social and academic network
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:gregoire.lecampion@cnrs.fr'
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0001-7195-765X
# Twitter  
#- icon: twitter
#  icon_pack: fab
#  link:
# Google scholar  
#- icon: google-scholar
#  icon_pack: ai
#  link:
# Github  
#- icon: github
#  icon_pack: fab
#  link:

  
# Do not modify
superuser: false
user_groups: 
- Publications Rzine
- Édition
- Comité éditorial

---
