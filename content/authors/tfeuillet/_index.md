---
authors:
- tfeuillet

name: Thierry Feuillet
role:  Professeur de géographie
email: 'thierry.feuillet@unicaen.fr'

organizations:
- name: IDEES (UMR 6266)
  url: "https://umr-idees.fr/"
- name: Université de Caen-Normandie
  url: "https://www.unicaen.fr/"



social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:thierry.feuillet@unicaen.fr'


superuser: false
user_groups:
- Publications Rzine
---


