---
authors:
- brodrigues

name: Bruno Rodrigues
role: PhD in Economics
email: 'bruno@brodrigues.co'

organizations:
- name: Ministry of Higher Education and Research (Luxembourg)
  url: "https://mesr.gouvernement.lu/en.html"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:bruno@brodrigues.co'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/brodriguesco
- icon: blog
  icon_pack: fa
  link: https://www.brodrigues.co/
- icon: youtube
  icon_pack: fab
  link: https://www.youtube.com/c/BrunoRodrigues1988/featured
- icon: github
  icon_pack: fab
  link: https://github.com/b-rodrigues

superuser: false
user_groups:
- Autres
---


