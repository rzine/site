---
authors:
- dmouralis

name: Damase Mouralis
role: Professeur de Géographie
email: 'damase.mouralis@univ-rouen.fr'

organizations:
- name: IDEES (UMR 6266)
  url: "https://umr-idees.fr/"
- name: Université de Rouen Normandie 
  url: "https://www.univ-rouen.fr/"



social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:damase.mouralis@univ-rouen.fr'


superuser: false
user_groups:
- Relecture
---


