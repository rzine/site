---
authors:
- ecome

name: Etienne Côme
role: Chargé de recherche
email: 'etienne.come@univ-eiffel.fr'

organizations:
- name: Université Gustave Eiffel
  url: "https://www.ifsttar.fr/accueil/"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:etienne.come@univ-eiffel.fr'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/comeetie
- icon: blog
  icon_pack: fa
  link: https://www.comeetie.fr/
- icon: github
  icon_pack: fab
  link: https://github.com/comeetie

superuser: False
user_groups:
- Autres
---


