---
authors:
- ppassy

name: Paul Passy
role:  Maître de conférences en géographie
email: 'paul.passy@u-paris.fr'

organizations:
- name: PRODIG (UMR 8586)
  url: "https://www.prodig.cnrs.fr/"
- name: Université Paris Cité
  url: "https://u-paris.fr/"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:paul.passy@u-paris.fr'
- icon: blog
  icon_pack: fa
  link: https://briques-de-geomatique.readthedocs.io/fr/latest/

superuser: false
user_groups:
- Publications Rzine
---


