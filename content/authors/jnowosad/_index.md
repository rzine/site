---
name: Jakub Nowosad

authors: jnowosad

role: Assistant Professor

organizations:
- name: Institute of Geoecology and Geoinformation
  url: "https://igig.amu.edu.pl/en"
- name: Adam Mickiewicz University
  url: "https://amu.edu.pl/en/main-page"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:nowosad.jakub@gmail.com'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/jakub_nowosad  
- icon: blog
  icon_pack: fa
  link: https://nowosad.github.io/
- icon: github
  icon_pack: fab
  link: https://github.com/Nowosad
   

superuser: false
user_groups: 
- Autres
---
