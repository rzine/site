---
authors:
- rlamarcheperrin

name: Robin Lamarche Perrin
role: Chargé de Recherche
email: 'Robin.Lamarche-Perrin@lip6.fr'

organizations:
- name: CNRS 
  url: "https://www.cnrs.fr/fr/page-daccueil"
- name: Institut des systèmes complexes (îdf)
  url: "https://iscpif.fr"
- name: Laboratoire d’informatique de Paris 6
  url: "https://www.lip6.fr/"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:Robin.Lamarche-Perrin@lip6.fr'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/RLP_research
- icon: blog
  icon_pack: fa
  link: https://lodelo.art
- icon: github
  icon_pack: fab
  link: https://github.com/Lamarche-Perrin



superuser: false
user_groups:
- Autres
---


