---
authors:
- carmand

name: Cécile Armand
role: Post-doctorante ANR, Programme Access ERC, ENS Lyon 
email: 'cecile.armand@gmail.com'

social:
- icon: envelope
  icon_pack: fas
  link: 'cecile.armand@gmail.com'
- icon: github
  icon_pack: fab
  link: https://github.com/carmand03
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-9107-6443

superuser: false
user_groups:
- Publications Rzine
---


