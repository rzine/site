---
#  code_namePrenom Nom
authors:
- tgiraud
# Prenom Nom (nom affiché)
name: Timothée Giraud
# Statut
role: Ingénieur de recherche en sciences de l'information géographique
# Trés courte bio
bio: Expert R user & package author

# Email

# Laboratoire de rattachement
organizations:
- name: UAR RIATE
  url: "https://riate.cnrs.fr/"
#  Employeur :
- name: CNRS 
  url: "http://www.cnrs.fr/"


# Formation
#education:
#  courses:
#  - course: MASTER 2 Carthageo PRO
#    institution: Univesrité Paris 1
#    year: 2009
#  - course: ...
#    institution: ...
#    year: ...

# thématique de recherche
interests:
- Cartography
- Reproducible Research
- Geomatic


# Social
social:
- icon: mastodon
  icon_pack: fab
  link: https://fosstodon.org/@rcarto
- icon: blog
  icon_pack: fa
  link: https://rcarto.github.io/
- icon: github
  icon_pack: fab
  link: https://github.com/rCarto
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-1932-3323

# Là il faut que je regarde a quoi ca peut nous servir...
superuser: false
user_groups:
- Direction
- Comité éditorial
---


