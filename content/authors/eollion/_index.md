---
authors:
- eollion

name: Étienne Ollion 
role: Chargé de recherche
email: 'etienne.ollion@polytechnique.edu'

organizations:
- name: CNRS
  url: "https://www.cnrs.fr/"
- name: CREST
  url: "https://crest.science/"
- name: Ecole polytechnique
  url: "https://www.polytechnique.edu/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:etienne.ollion@polytechnique.edu'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/eollion
- icon: blog
  icon_pack: fa
  link: https://ollion.cnrs.fr/


superuser: false
user_groups:
- Autres
---


