---
authors:
- jgravier

name: Julie Gravier
role: Chargée de recherche
email: 'julie.gravier@cnrs.fr'

organizations:
- name: Laboratoire ThéMA (UMR 6049)
  url: "https://thema.univ-fcomte.fr/"
- name: Associée UMR Géographie-cités
  url: "https://geographie-cites.cnrs.fr/"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:julie.gravier@cnrs.fr'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/gravierjulie
- icon: github
  icon_pack: fab
  link: https://github.com/JGravier


superuser: false
user_groups:
- Direction
- Comité éditorial
---


