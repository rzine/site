---
authors:
- egallic

name: Ewen Gallic
role: Maître de conférences en économie
email: 'ewen.gallic@gmail.com'

organizations:
- name: École d’Économie d’Aix-Marseille
  url: "https://www.amse-aixmarseille.fr/fr"
- name:  Aix-Marseille Université
  url: "https://www.univ-amu.fr/fr"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:ewen.gallic@gmail.com'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/3wen?lang=fr
- icon: blog
  icon_pack: fa
  link: http://egallic.fr/
- icon: github
  icon_pack: fab
  link: https://github.com/3wen

superuser: false
user_groups:
- Autres
---


