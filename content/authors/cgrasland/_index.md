---
authors:
- cgrasland

name: Claude Grasland
role: Professeur de Géographie
email: 'claude.grasland@parisgeo.cnrs.fr'

organizations:
- name: FR CIST
  url: "http://cist.cnrs.fr/"
- name: UMR Géographie-cités
  url: "https://www.parisgeo.cnrs.fr/"
- name: Université Paris Cité
  url: https://u-paris.fr/

social:
- icon: envelope
  icon_pack: fas
  link: 'claude.grasland@parisgeo.cnrs.fr'
- icon: github
  icon_pack: fab
  link: https://github.com/ClaudeGrasland
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=oGq8BT4AAAAJ&hl=fr
- icon: blog
  icon_pack: fa
  link: https://toblerfirstlaw.net/fr/
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0003-1504-4965
- icon: researchgate
  icon_pack: fab
  link: https://www.researchgate.net/profile/Claude-Grasland

superuser: false
user_groups:
- Publications Rzine
---


