---
authors:
- mlethrosne

name: Murielle Lethrosne
role: Statisticienne 
email: 'murielle.lethrosne@developpement-durable.gouv.fr'

organizations:
- name: DREAL Centre-Val de Loire 
  url: "http://www.centre-val-de-loire.developpement-durable.gouv.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:murielle.lethrosne@developpement-durable.gouv.fr'
- icon: github
  icon_pack: fab
  link: https://github.com/MurielleL

superuser: false
user_groups:
- Autres
---


