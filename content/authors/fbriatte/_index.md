---
# DISPLAY NAME
name: François Briatte 

# USERNAME
authors: fbriatte

# STATUT
role: Maître assistant en science politique

# EMPLOYEUR et/ou UNITE
organizations:
- name: ESPOL
  url: "https://espol-lille.eu/"
- name: Université Catholique de Lille
  url: "https://www.univ-catholille.fr/"


# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:francois.briatte@sciencespo.fr'

# Blog
- icon: blog
  icon_pack: fa
  link: https://f.briatte.org/

# Google scholar  
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.fr/citations?hl=en&user=ykHPzKgAAAAJ

# Github
- icon: github
  icon_pack: fab
  link: https://github.com/briatte
   
# Twitter  
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/phnk  


# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---
