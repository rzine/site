---
authors:
- jboelaert

name: Julien Boelaert
role: Maître de conférences en science politique
email: 'julien.boelaert@univ-lille.fr'

organizations:
- name: CERAPS (UMR 8026)
  url: "https://ceraps.univ-lille.fr/"
- name: Université de Lille
  url: "https://www.univ-lille.fr/"


social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:julien.boelaert@univ-lille.fr'
- icon: github
  icon_pack: fab
  link: https://github.com/jboelaert


superuser: false
user_groups:
- Autres
---




