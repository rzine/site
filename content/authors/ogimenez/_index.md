---
authors:
- ogimenez

name: Olivier Gimenez
role: Directeur de recherche en statistiques, ecologie & sciences sociales
email: 'olivier.gimenez@cefe.cnrs.fr'

organizations:
- name: CEFE - UMR 5175
  url: "https://www.cefe.cnrs.fr/fr/"
- name: CNRS
  url: "https://www.cnrs.fr/fr/page-daccueil"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:olivier.gimenez@cefe.cnrs.fr'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/oaggimenez
- icon: blog
  icon_pack: fa
  link: https://oliviergimenez.github.io/
- icon: youtube
  icon_pack: fab
  link: https://www.youtube.com/channel/UCFtTq-4WwH0LIczQ__KFC1A/videos
- icon: figshare
  icon_pack: ai
  link: https://figshare.com/authors/Olivier_Gimenez/3914297
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.fr/citations?user=5NkQUA8AAAAJ&hl=fr
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/olivier-gimenez-545451115/
- icon: github
  icon_pack: fab
  link: https://github.com/oliviergimenez


superuser: false
user_groups:
- Autres
---


