---
authors:
- rkrummeich

name: Raphaëlle Krummeich
role: Ingénieure de recherche
email: 'raphaelle.krummeich@univ-rouen.fr'

organizations:
- name: Université de Rouen Normandie
  url: "https://www.univ-rouen.fr/"
- name: FED 3147 IRIHS
  url: "https://irihs.univ-rouen.fr/"



social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:raphaelle.krummeich@univ-rouen.fr'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/rkrummeich
- icon: blog
  icon_pack: fa
  link: https://transbordeur.hypotheses.org/
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.huma-num.fr/rkrummeich



superuser: false
user_groups:
- Édition
- Comité éditorial
---


