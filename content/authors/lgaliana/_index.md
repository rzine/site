---
authors:
- lgaliana

name: Lino Galiana
role: Administrateur INSEE

organizations:
- name: SSP Lab
  url: "http://ssplab.lab.sspcloud.fr/index/"
- name: INSEE
  url: "https://www.insee.fr/fr/accueil"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:lino.galiana@insee.fr'
- icon: blog
  icon_pack: fa
  link: https://linogaliana.netlify.app/
- icon: github
  icon_pack: fab
  link: https://github.com/linogaliana
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/linogaliana
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/linogaliana


superuser: false
user_groups:
- Autres
---


