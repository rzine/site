---
# DISPLAY NAME
name: Nicolas Robette

# USERNAME
authors: nrobette

# STATUT
role: Maître de conférences en sociologie

# EMPLOYEUR et/ou UNITE
organizations:
- name: CREST
  url: "http://crest.science/"
- name: INED
  url: "https://www.ined.fr/"

# SOCIAL/ACADEMIC NETWORKING
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:nicolas.robette@uvsq.fr'

# Blog
- icon: blog
  icon_pack: fa
  link: http://nicolas.robette.free.fr/

# Github
- icon: github
  icon_pack: fab
  link: https://github.com/nicolas-robette
   

# ###############
# NE PAS MODIFIER
superuser: false
user_groups: 
- Autres
---
