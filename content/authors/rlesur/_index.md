---
authors:
- rlesur

name: Romain Lesur
role: Administrateur INSEE 

organizations:
- name: Responsable du SSP Lab
  url: "http://ssplab.lab.sspcloud.fr/index/"
- name: INSEE
  url: "https://www.insee.fr/fr/accueil"

social:
- icon: github
  icon_pack: fab
  link: https://github.com/RLesur
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/RLesur

superuser: false
user_groups:
- Autres
---


