---
name: Anton Perdoncin

authors: aperdoncin

role: Post-doctorant en sociologie

organizations:
- name: EHESS
  url: "https://www.ehess.fr/fr"
- name: Lubartworld (ERC-CoG Project)
  url: "https://lubartworld.cnrs.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:anton.perdoncin@ehess.fr'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/AntonPerdoncin
- icon: blog
  icon_pack: fa
  link: https://lubartworld.cnrs.fr/en/anton.perdoncin/


superuser: false
user_groups: 
- Autres
---
