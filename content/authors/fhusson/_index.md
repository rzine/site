---
authors:
- fhusson

name: François Husson
role: Professor in Statistics
email: 'husson@agrocampus-ouest.fr'

organizations:
- name: Agrocampus Ouest
  url: "https://www.agrocampus-ouest.fr/"
- name: IRMAR (UMR 6625)
  url: "https://irmar.univ-rennes1.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:husson@agrocampus-ouest.fr'
- icon: blog
  icon_pack: fa
  link: https://husson.github.io/
- icon: youtube
  icon_pack: fab
  link: https://www.youtube.com/c/HussonFrancois/featured
- icon: github
  icon_pack: fab
  link: https://github.com/husson




superuser: false
user_groups:
- Autres
---


