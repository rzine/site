---
# = première lettre prenom + NOM == Nom du dossier !!!
authors:
- tlaurent

# Prenom Nom (nom affiché)
name: Thibault Laurent

# Statut
role: Ingénieur de Recherche en Statistique 


# Email
email: 'Thibault.Laurent@tse-fr.eu'


# Laboratoire de rattachement
organizations:
- name: CNRS
  url: "https://www.cnrs.fr/fr"
- name: UMR TSE-R
  url: "https://www.tse-fr.eu/fr/umr-tser"


# Social
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:Thibault.Laurent@tse-fr.eu'
- icon: blog
  icon_pack: fa
  link: http://www.thibault.laurent.free.fr/index.html


# - icon: github
#   icon_pack: fab
#   link: https://gitlab.huma-num.fr/hpecout

# PAS MODIFIER
superuser: false
user_groups:
- Autres
---


