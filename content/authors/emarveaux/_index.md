---
authors:
- emarveaux

name: Elina Marveaux
role: Ingénieure d’études en sciences de l’information géographique
email: 'elina.emx@gmail.com'

organizations:
- name: FR CIST
  url: "http://cist.cnrs.fr/"
- name: CNRS 
  url: "http://www.cnrs.fr/"

social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:elina.emx@gmail.com'
- icon: github
  icon_pack: fab
  link: https://github.com/ElinaMX
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.huma-num.fr/emarveaux
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0009-0000-8667-3019


superuser: false
user_groups:
- Édition
- Comité éditorial
---


