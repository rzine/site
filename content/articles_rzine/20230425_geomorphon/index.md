---
authors:
- otheureaux
- ppassy
- tfeuillet
- dbirre

# DATE de soumission à Rzine
date: "2023-04-28"

# TITRE
title: Caractérisation des formes du relief à l'échelle de bassins-versants
subtitle: Analyse quantitative des formes du relief via l'algorithme Geomorphons pour trois bassins-versants montagnards

# RESUME COURT - une phrase
summary: Cette fiche présente une méthode de caractérisation des formes du relief à l'échelle de bassins-versants qui repose sur l'algorithme *geomorphons* 


# TAGS
tags:
- raster
- terra
- tmap
- sf
- relief
- geomorphons
- MNT
- BD Topage
- MNT RGE 
- bassins-versants

publication_types: ["10"]

projects:
- rzine

sources:
- "8"

thematics: ["19","20","2"]


update :
- "0"

languages:
- "0"


# Link
url_source: '/docs/20230425_geomorphon/index.html'
url_code: 'https://github.com/rzine-reviews/fiche_rzine_geomorphon'
url_dataset: '/docs/20230425_geomorphon/data.zip'

links:
- name: HAL
  url: https://hal.science/RZINE/hal-04432286v1

doi: "10.48645/s8je-kz92"

# ###### NE PAS MODIFIER #####
featured: false
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Auteur·es** : [Olivier Theureaux](https://rzine.fr/authors/otheureaux/), [Paul Passy](https://rzine.fr/authors/ppassy/), [Thierry Feuillet](https://rzine.fr/authors/tfeuillet/), [Déborah Birre ](https://rzine.fr/authors/dbirre/)       
**Evaluateur·rice·s** :  [Julie Pierson](https://rzine.fr/authors/jpierson/), [Damase Mouralis](https://rzine.fr/authors/dmouralis/)    
**Editeur·rices** : [Grégoire Le Campion](https://rzine.fr/authors/glecampion88), [Ronan Ysebaert](https://rzine.fr/authors/rysebaert), [Marion Gentilhomme](https://rzine.fr/authors/mgentilhomme/)          


#### Résumé 

Cette fiche présente une méthode de caractérisation des formes du relief à l'échelle de bassins-versants. La méthode utilise l'algorithme *geomorphons* pour identifier les caractéristiques topographiques d'un terrain à partir de données issues d'un modèle numérique de terrain. Les résultats peuvent être utilisés pour cartographier et analyser les paysages à différentes échelles géographiques.


#### Citation

<div style="font-size:15px;">
Theureaux, O., Passy, P., Feuillet, T., & Birre, D. (2023). Caractérisation des formes du relief à l’échelle de bassins-versants. Rzine. https://doi.org/10.48645/s8je-kz92 
</div>

<p class="d-inline-flex" style="gap:12px;"><a href="https://doi.org/10.48645/s8je-kz92"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://zenodo.org/badge/DOI/10.48645/s8je-kz92.svg" alt="DOI:10.48645/s8je-kz92"></a><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg" alt="License: CC BY-SA 4.0"></a></p>





