---
authors:
- faudard
- jpierson
- glecampion88

# DATE de soumission à Rzine
date: "2024-09-25"

# TITRE
title: "La régression géographiquement pondérée : GWR"
subtitle: Comment prendre en compte l’effet local du spatial en statistique

# RESUME COURT - une phrase
summary: Cet article présente la réalisation d’une analyse de données à l’aide de la régression géographique pondérée ou GWR (Geographical Weighted Regression).


# TAGS
tags:
- régression géographiquement pondérée
- autocorrélation spatiale
- voisinage
- statistique spatiale

publication_types: ["10"]

projects:
- rzine

sources:
- "8"

thematics: ["19","6","2"]


update :
- "0"

languages:
- "0"


# Link
url_source: '/docs/20240930_gwr/index.html'
url_code: 'https://github.com/rzine-reviews/GWR_Rzine'
url_dataset: 'https://rzine.fr/docs/20240930_gwr/data.zip'

links:
- name: HAL
  url: https://hal.science/RZINE/halshs-04737454v1

doi: "10.48645/wk1m-hg05"

# ###### NE PAS MODIFIER #####
featured: false
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Auteur·es** : [Frédéric Audard](https://rzine.fr/authors/faudard/), [Grégoire Le Campion](https://rzine.fr/authors/glecampion88), [Julie Pierson](https://rzine.fr/authors/jpierson)     
**Evaluateurs** :  [Hélène Mathian](https://rzine.fr/authors/hmathian/), [Thierry Feuillet](https://rzine.fr/authors/tfeuillet/)    
**Editeur·rices** : [Ronan Ysebaert](https://rzine.fr/authors/rysebaert), [Marion Gentilhomme](https://rzine.fr/authors/mgentilhomme/)          


#### Résumé 

Cet article présente la réalisation d’une analyse de données à l’aide de la régression géographique pondérée ou GWR (Geographical Weighted Regression). La modélisation statistique dite “classique” présente des risques élevés lorsqu’on souhaite traiter des données spatiales. Des phénomènes de dépendance spatiale, des problèmes d’échelles d’application, des effets de contexte, ainsi qu’une forme d’hétérogénéité spatiale peuvent apparaître et compromettre les analyses effectuées, et engendrer des interprétations tronquées voire inexactes. Souvent ignorées, ces dimensions spatiales ne peuvent pas être considérées comme un simple aléa. 

L’objectif de cet article est de vous présenter des méthodes et leurs applications qui vous permettront d’étudier concrètement les effets des dimensions spatiales des données. Il s’agira d’une part de caractériser la structure spatiale de données attributaires liées à des entités spatiales ; et d’autre part de mesurer l’effet statistique de l’information spatiale dans un modèle de régression visant à déterminer les facteurs explicatifs d’un phénomène.

L’analyse présentée ici a pour objectif d’être reproductible : les données spatiales utilisées proviennent de la base ADMIN-EXPRESS de l’Institut national de l’information géographique et forestière (IGN) ; les données statistiques, quant à elles, ont été construites à partir de la base des notaires de France, complétées par des données de l’Institut national de la statistique et des études économiques (INSEE) pour les variables explicatives.

#### Citation

<div style="font-size:15px;">
Audard, F., Le Campion, G., & Pierson, J. (2024). La régression géographiquement pondérée : GWR. Rzine. https://doi.org/10.48645/wk1m-hg05
</div>

<p class="d-inline-flex" style="gap:12px;"><a href="https://doi.org/10.48645/wk1m-hg05"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://zenodo.org/badge/DOI/10.48645/wk1m-hg05.svg" alt="DOI:10.48645/wk1m-hg05"></a><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg" alt="License: CC BY-SA 4.0"></a></p>





