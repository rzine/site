---
# AUTHOR(S) 
authors:
- mletexier86

# DATE de soumission à Rzine
date: "2021-06-20"

# TITRE
title: Exploration spatio-temporelle d’objets géographiques ponctuels
subtitle: Un exemple appliqué à la base de données Mérimée

# RESUME COURT - une phrase
summary: Cette fiche rend compte d’une série de traitements permettant d’explorer la base de données MERIMEE qui consigne l’ensemble des bâtiments classés aux monuments historiques.


# TAGS
tags:
- exploration
- spatio-temporelles
- Mérimée
- merimee
- ponctuel
- lissage temporel
- carte 
- inventaire
- densité
- interactive
- animée
- prisme spatio-temporel
- ST-DBSCAN
- tydiverse
- ggmap
- plotly
- raster
- sf
- ggplot2
- pracma
- forecast
- cowplot
- leaflet
- osmdata 
- gganimate


publication_types: ["10"]


projects:
- rzine

sources:
- "8"


thematics: ["10","11","19","2"]


update :
- "0"

languages:
- "0"


# Link
url_source: '/docs/20200601_mletexier86_explo_spatiotemporel/index.html'
url_code: 'https://github.com/rzine-reviews/rzine_spatiotemp'
url_dataset: '/docs/20200601_mletexier86_explo_spatiotemporel/data.zip'

links:
- name: HAL
  url: https://hal.science/RZINE/hal-03433247v1

doi: "10.48645/vrmv-ja14"

# ###### NE PAS MODIFIER #####
featured: false
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Autrice** : [Marion Le Texier](https://rzine.fr/authors/mletexier86/)     
**Evaluateur.rice.s** : Comité éditorial Rzine  
**Editeur.rice.s** : [Marion Gentilhomme](https://rzine.fr/authors/mgentilhomme/), [Hugues Pecout](https://rzine.fr/authors/hpecout)     


#### Résumé 

Cette fiche rend compte d’une série de traitements permettant d’explorer la [base de données MERIMEE](https://www.culture.gouv.fr/Espace-documentation/Base-de-donnees-Culture/Merimee-une-base-de-donnees-du-patrimoine-monumental-francais-de-la-Prehistoire-a-nos-jours) qui consigne l’ensemble des bâtiments classés aux monuments historiques. Nous prenons l’exemple des fiches “Mérimée” du patrimoine architectural toulousain disponibles sur le site [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/base-merimee/).

#### Citation

<div style="font-size:15px;">
Le Texier M (2021). <i>"Exploration spatio-temporelle d’objets, géographiques ponctuels"</i>, doi: 10.48645/vrmv-ja14 , URL: https://rzine.fr/publication_rzine/20200601_mletexier86_explo_spatiotemporel/>.
</div>

<p class="d-inline-flex" style="gap:12px;"><a href="https://doi.org/10.48645/vrmv-ja14"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://zenodo.org/badge/DOI/10.48645/vrmv-ja14.svg" alt="DOI:10.48645/vrmv-ja14"></a><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg" alt="License: CC BY-SA 4.0"></a></p>





