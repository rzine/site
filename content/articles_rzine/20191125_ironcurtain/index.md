---
authors:
- nlambert76


date: "2021-10-24"


title: Le nouveau rideau de fer (exemple de carte en 2,5D)
summary: Ce document montre comment réaliser cette carte de discontinuités en 2,5D (fausse 3D) joliment mise en page entièrement dans le langage R



tags:
- cartographie
- PIB
- discontinutés
- rideau de fer
- europe
- inégalités
- sémiologie graphique
- carte


publication_types: ["10"]


thematics: ["2"]


update:
- "0"

projects:
- rzine

sources:
- "8"

languages:
- "0"

url_source: '/docs/20191125_ironcurtain/index.html'
url_code: 'https://github.com/rzine-reviews/ironcurtain'
url_dataset: '/docs/20191125_ironcurtain/data.zip'

doi: "10.48645/a4ra-yr11"

links:
- name: HAL
  url: https://hal.science/RZINE/hal-03514951v1

featured: false
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---



**Auteur** : [Nicolas Lambert](https://rzine.fr/authors/nlambert76/)  
**Evaluateur.rice.s** : Comité éditorial Rzine  
**Editeur.rice.s** : [Marion Gentilhomme](https://rzine.fr/authors/mgentilhomme/), [Hugues Pecout](https://rzine.fr/authors/hpecout)     


#### Résumé 

Cette fiche suit plusieurs objectifs. Tout d'abord, elle montre qu'il est possible de réaliser une belle carte d'édition joliment mise en page entièrement avec R, sans passer par un logiciel de DAO. Elle vise aussi à montrer comment, avec une projection cartographique adéquate, faire varier la hauteur des frontières pour exprimer les valeurs de discontinuités.

#### Citation

<div style="font-size:15px;">
Lambert N (2021). <i>"Réaliser une carte de discontinuités en 2,5D"</i>, doi: 10.48645/a4ra-yr11 , URL: https://rzine.fr/publication_rzine/20191125_ironcurtain/>.
</div>

<p class="d-inline-flex" style="gap:12px;"><a href="https://doi.org/10.48645/a4ra-yr11"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://zenodo.org/badge/DOI/10.48645/a4ra-yr11.svg" alt="DOI: 10.48645/a4ra-yr11"></a><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg" alt="License: CC BY-SA 4.0"></a></p>

