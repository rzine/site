---
authors:
- carmand

# DATE de publication
date: "2024-11-13"

# TITRE
title: "Uncovering Places in Two-Mode Networks"
subtitle: Using Structural Equivalence to Study Affiliation Networks

# RESUME COURT - une phrase
summary: This paper presents an effective approach for handling two-mode networks, utilizing the concept of ‘place’ or structural equivalence as its fundamental framework.

# TAGS
tags:
- two-mode network
- affiliation network
- alumni
- modern China
- Sino-American relations

publication_types: ["10"]

projects:
- rzine

sources:
- "8"

thematics: ["18"]


update :
- "0"

languages:
- "1"


# Link
url_source: '/docs/20241003_carmand_place/index.html'
url_code: 'https://github.com/rzine-reviews/place-rzine'
url_dataset: 'https://rzine.fr/docs/20241003_carmand_place/data.zip'

links:
- name: HAL
  url: https://hal.science/RZINE/hal-04784832v1

doi: "10.48645/nk8x-8d47"

# ###### NE PAS MODIFIER #####
featured: false
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Auteure** : [Cécile Armand](https://rzine.fr/authors/carmand/)     
**Evaluateurs** : [Laurent Beauguitte](https://rzine.fr/authors/lbeauguitte/), [Paul Gourdon](https://rzine.fr/authors/pgourdon/)     
**Editrices** : [Julie Gravier](https://rzine.fr/authors/jgravier/), [Marion Gentilhomme](https://rzine.fr/authors/mgentilhomme/)          


#### Résumé 

This paper presents an effective approach for handling two-mode networks, utilizing the concept of ‘place’ or structural equivalence as its fundamental framework. It primarily relies on the Places and igraph R packages. To illustrate this method, it employs an edge list representing students and their respective universities in the United States. The data source for this analysis is derived from the directory of an alumni club, specifically the American University Club of Shanghai, which was originally published in 1936. The paper proceeds through four main steps: (1) Identification of places from the edge list, (2) Transformation of the list of places into a network of places, along with its transposed network of universities, (3) Visualization and analysis of the network, including community detection, and (4) Introduction of a more flexible approach grounded in the concepts of regular equivalence or k-places.

#### Citation

<div style="font-size:15px;">
Armand, C. (2024). Uncovering Places in Two-Mode Networks: Using Structural Equivalence to Study Affiliation Networks. Rzine. https://doi.org/10.48645/nk8x-8d47
</div>

<p class="d-inline-flex" style="gap:12px;"><a href="https://doi.org/10.48645/nk8x-8d47"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://zenodo.org/badge/DOI/10.48645/nk8x-8d47.svg" alt="DOI:10.48645/nk8x-8d47"></a><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg" alt="License: CC BY-SA 4.0"></a></p>





