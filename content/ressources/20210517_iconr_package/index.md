---
date: "2021-05-17"
title: Analysis of Prehistoric Iconography - R package iconr
subtitle: Modeling Prehistoric iconography
summary: Modeling Prehistoric iconography with the iconr package 
authors: 
- Thomas Huet
- Jose M. Pozo
- Craig Alexander
publication_types: ["6","10"]
thematics: ["18"]
update: ["0"]
sources: ["0"]
languages: ["1"]
url_source: https://joss.theoj.org/papers/10.21105/joss.03191
url_pdf: https://www.theoj.org/joss-papers/joss.03191/10.21105.joss.03191.pdf
doi: "10.21105/joss.03191" 
links:
- name: Package
  url: https://zoometh.github.io/iconr/
tags: ["iconography", "semiotic", "prehistory", "archeology", "iconr", "graph", "theory", "spatial analysis", "GIS"]
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

The **R package iconr** is grounded in graph theory and spatial analysis to offer concepts and functions for modeling Prehistoric iconographic compositions and preparing for further analysis (clustering, typology tree, Harris diagram, etc.). The package purpose is to contribute to cross-cultural comparison through a greater normalization of quantitative analysis.







