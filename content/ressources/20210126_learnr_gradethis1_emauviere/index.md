---
authors:
- emauviere

title: Concevoir d’efficaces tutoriels R interactifs avec learnr (1/2)
summary: Appréhender de façon graduée le codage dans R, voilà ce que permettent les tutoriels interactifs learnr. ils mixent exposés, exemples commentés et exercices pratiques

date: "2021-01-27"

publication_types: ["11"]


thematics: ["1"]


languages:
- "0"

update:
- "0"

sources:
- "0"


url_source: 'https://www.icem7.fr/r/concevoir-defficaces-tutoriels-r-interactifs-avec-learnr-1-2/'
url_code: 'https://github.com/icem7-open/learnrdemos'

tags:
- learnr
- gradethis
- tutoriels
- pédagogie
- formation R


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


