---
title: Glitter makes SPARQL, un package R pour explorer et collecter des données du web sémantique
subtitle : Séminaire RUSS - Séance du 7 avril 2023
summary: Séminaire RUSS - Séance du 7 avril 2023


authors:
- lvaudor

publication_types: ["12","9"]

sources:
- "7"

thematics: ["6","5"]


update:
- "0"

languages:
- "0"


projects:
- RUSS

date: "2023-04-07T00:00:00Z"


url_source: 'https://www.canal-u.tv/chaines/ined/glitter-makes-sparql-glitter-un-package-r-pour-explorer-et-collecter-des-donnees-du'

links:
- name: Diaporama
  url: http://perso.ens-lyon.fr/lise.vaudor/RECIT/presentation_RUSS.html#/title-slide
- name: Video
  url: https://www.canal-u.tv/chaines/ined/glitter-makes-sparql-glitter-un-package-r-pour-explorer-et-collecter-des-donnees-du-8


tags:
- RUSS
- glitter
- sparql
- web sémantique
- triplets
- linked open data
- LOD-cloud
- dbpédia
- web des données

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Glitter makes SPARQL : glitter, un package R pour explorer et collecter des données du web sémantique**, Lise Vaudor (CNRS, UMR Environnement Ville Société, plateforme ISIG )


La collecte de données du web sémantique, formalisées selon le modèle RDF, nécessite l’élaboration de requêtes dans le langage dédié SPARQL. Ce langage, qui est aux données du web sémantique ce que SQL est aux bases de données relationnelles, a ainsi un objectif très spécifique et demeure assez méconnu des utilisateurs de données. Au contraire, R est un langage de programmation généraliste puisqu’il permet de gérer de nombreux aspects de la chaîne de traitements de données, depuis leur recueil jusqu’à leur valorisation (par des modèles, graphiques, cartes, rapports, applications, etc.).

Le package glitter permet aux utilisateurs de R sans connaissance préalable de SPARQL (analystes de données, chercheurs, étudiants) d’explorer et collecter les données du web sémantique. En effet, il permet à l’utilisateur de générer des requêtes SPARQL, de les envoyer aux points d’accès de son choix, et de recueillir les données correspondantes. Ces étapes sont ainsi intégrées à l’environnement R dans lequel l’utilisateur peut également réaliser les étapes d’analyse et de valorisation des données, dans une chaîne de traitement reproductible.

Dans cette présentation, les principales fonctionnalités du package glitter seront illustrées à partir d’exemples. Le package, quoique toujours en développement, est fonctionnel et documenté et peut être installé par les participants qui souhaitent le tester en suivant les instructions décrites sur [cette page](https://github.com/lvaudor/glitter).










