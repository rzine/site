---
authors:
- rysebaert

title: Travel time calculation with R and data visualization with Observable

subtitle: Application to artifical climbing walls in Paris and its neighbourhood

summary: Application to artifical climbing walls in Paris and its neighbourhood

date: "2023-01-20"

publication_types: ["11"]

thematics: ["11", "19", "2"]

languages:
- "1"

update:
- "0"

sources:
- "0"


url_source: 'https://rysebaert.github.io/climbing_paris/'
url_code: 'https://github.com/rysebaert/climbing_paris' 


tags:
- observable
- bertin
- geoverview
- javascript
- sf
- osmdata
- mapsf
- osrm
- mapiso
- maptiles
- rmapshaper
- spatial
- climbing
- OpenStreetMap

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

The aim of this notebook consists in showing how to build, visualize and reproduce accessibility indicators by combining points of interest (POI) coming from the OpenStreetMap database, socio-economic indicators included in small territorial division (IRIS) coming from institutional data source (INSEE) and routing engines (OSRM).

The graphical outputs displayed in this notebook are further developed in an [Observable collection](https://observablehq.com/collection/@rysebaert/climbing_paris "Go to Observable Notebook"). To introduce the reader to the issues raised by indoor sport-climbing in Paris, have a look to [this notebook](https://observablehq.com/d/d2be39c4dd55a32d?collection=@rysebaert/climbing_paris "Go to Observable Notebook").

This Quarto document combines 2 programming languages : R for data processing, and ObservableJS for data visualizations.
