---
title: Getting the most out of text
subtitle : Using supervised and unsupervised approaches in NLP (R-Ladies Tunis)
summary: Using supervised and unsupervised approaches in NLP (R-Ladies Tunis)

authors:
- Cosima Meyer

publication_types: ["12","9"]


sources:
- "0"

thematics: ["17"]


update:
- "0"

languages:
- "1"


projects:
- divers

date: "2021-01-30T00:00:00Z"

tags:
- corpus
- token
- tokenisation
- DFM
- stemming
- stem
- lemmatization
- knitr
- tidyverse
- quanteda
- stm
- stminsights
- LDAvis
- servr
- topicmodels
- kableExtra
- readtext
- magrittr
- overviewR
- countrycode
- wesanderson
- tidytext
- wordcloud
- topics

url_source: 'https://cosimameyer.rbind.io/slides/nlp-rladies-tunis/talk#1'
url_code: 'https://github.com/cosimameyer/nlp-rladies-tunis'

links:
- name: Recording
  url: https://www.youtube.com/watch?v=NYuTFy_JRLA
- name: Workshop code
  url: https://nlp-tunis.netlify.app/

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Text data provides an oasis of information for both researchers and non-researchers alike to explore. Natural Language Processing (NLP) methods help make sense of this difficult data type. The talk and code give you a smooth introduction to the [quanteda package](https://quanteda.io/). I will also showcase how to quickly visualize your text data and cover both supervised and unsupervised approaches in NLP. As part of the code demo, we will use text [data from the UN](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/0TJX8Y) as a working example to give you first insights into the structure of text data and how to work with it. If you want to see the code that we are using during the workshop in action, it’s also deployed [here](https://nlp-tunis.netlify.app/).



