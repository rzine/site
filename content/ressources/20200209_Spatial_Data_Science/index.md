---
title: Spatial Data Science
subtitle: 
summary: This book introduces and explains the concepts underlying spatial data with R

authors:
- Edzer Pebesma
- Roger Bivand

publication_types: ["5"]


thematics: ["2","19"]


projects:
- divers

sources:
- "0"

update:
- "1"

languages:
- "1"

date: "2020-02-09T00:00:00Z"
doi: ""

tags:
- sf
- lwgeom
- stars
- tidyverse
- gstat
- spdep
- spatstat
- map
- coordinate systems
- ellipsoidal
- geometries
- network
- raster
- vector
- longitude
- latitude
- interpolation
- temporal
- intersection
- join
- projection
- graticule
- geom_stars
- geom_sf
- plot
- spatial regression
- variogram
- kriging
- geostatistics
- autocorrelation
- sp


url_source: 'https://keen-swartz-3146c4.netlify.app/index.html'
url_code: 'https://github.com/edzer/sdsr'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---





