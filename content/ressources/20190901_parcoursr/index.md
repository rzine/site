---
date: "2019-09-01T00:00:00Z"

title: Parcours R
subtitle: Formations R pour les agents des MTES & MCTRC
summary: Formations R pour les agents des MTES & MCTRCT

tags: 
- projet
- formation
- MTES
- MCTRCT
- Rstudio


authors:
- Thierry Zorn
- mlethrosne
- Vivien Roussez
- Pascal Irz
- Nicolas Torterotot
- mtheulière
- Bruno Terseur
- Solène Colin
- Marouane Zellou
- Juliette Engelaere-Lefebvre
- Jean-Daniel Lomenede
- Caroline Coudrin

publication_types: ["8","13"]

thematics: ["0","10","11","12","19", "2"]

projects:
- parcoursr

sources:
- "3"

update:
- "1"

languages:
- "0"

url_source: 'https://mtes-mct.github.io/parcours-r/'
url_code: 'https://github.com/MTES-MCT'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



Ce dispositif inédit de formation vise à faire monter en compétence les agents dans le domaine de la science de la donnée, avec le logiciel R.

Le parcours est composé de plusieurs modules à mettre en oeuvre en fonction des attentes des stagiaires. Les deux premiers sont nécessaires pour prendre en main l’outil. Les autres modules couvrent une ou plusieurs fonctionnalités de R, que les stagiaires pourront découvrir en fonction de leurs attentes. Chacun peut ainsi se former « à la carte » en fonction de ses usages professionnels et des domaines dans lesquels la montée en compétence est nécessaire.

Chaque module comprend, en introduction, l’acquisition ou le rappel des notions statistiques abordées. Suit la mise en œuvre des méthodes avec R, et enfin les clés d’interprétation des sorties statistiques et graphiques.

**Cette formation est le fruit d'un travail inter-services des agents du ministère**. **Les supports sont diffusés en licence ouverte et peuvent être utilisés pour mener des formations à R en citant les contributeurs et la source**.


Retrouvez l'ensemble des modules proposés à cette adresse : [https://mtes-mct.github.io/parcours-r/](https://mtes-mct.github.io/parcours-r/)
