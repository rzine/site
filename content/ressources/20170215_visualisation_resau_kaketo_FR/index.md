---
title: Visualisation de réseaux avec R
subtitle: Réseaux statiques et dynamiques
summary: Version française du tutoriel de référence 'Network visualization with R' (version 2016), traduit par [Laurent Beauguitte](/authors/lbeauguitte/) en 2017. 

authors:
- Katherine Ognyanova

projects:
- divers

sources :
- "0"

thematics: ["18"]





update :
- "0"

languages:
- "0"


publication_types: ["5"]



date: "2017-02-15T00:00:00Z"
doi: ""

tags:
- igraph
- RColorBrewer
- png
- network
- ggraph
- animation
- visNetwork
- htmlwidgets
- threejs
- networkD3
- ndtv
- geosphere
- maps
- Statnet
- sna
- ergm
- stergm
- multiplex
- interactive
- nodes
- links
- layout

url_source: 'https://f-origin.hypotheses.org/wp-content/blogs.dir/2996/files/2017/02/visualiseR.pdf'


featured: true 
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Version française du tutoriel de référence 'Network visualization with R' (version 2016), traduit par [Laurent Beauguitte](/authors/lbeauguitte/) en 2017.



Il s'agit d'un tutoriel complet sur la visualisation des réseaux avec R. Il couvre la saisie et les formats des données, les bases de la visualisation, les paramètres et la présentation des graphiques unimodaux et bipartites ; il traite des liens multiplex, de la visualisation interactive et animée pour les réseaux longitudinaux ; et de la visualisation des réseaux sur des cartes géographiques.


