---
# AUTHOR(S) 
authors:
- jbarnier

# DATE de soumission à Rzine
date: "2017-11-07"

# TITRE
title: Une introduction à R et au tidyverse
subtitle: Traitement de données et d’analyse statistique.

# RESUME COURT - une phrase
summary: Version modernisée de la publication *Introduction à R*, qui propose une introduction à plusieurs de ces extensions (*ggplot2*, *dplyr*, *tidyr*, *readr*, *readxl*, *haven*, *forcats*, *stringr*, *R Markdown*).


# TAGS
tags:
- initiation
- tidyverse
- ggplot2
- dplyr
- tidyr
- readr
- readxl
- haven
- forcats
- stringr
- markdown


# TYPE DE PUBLICATION
publication_types: ["13","5"]


thematics: ["0","10","11","12"]


languages:
- "0"

sources:
- "0"

update:
- "1"


projects:
- divers


url_source: 'https://juba.github.io/tidyverse/'
url_code: 'https://github.com/juba/tidyverse'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Ce document est une introduction à l’utilisation du logiciel libre de traitement de données et d’analyse statistique R. Il se veut le plus accessible possible, y compris pour ceux qui ne sont pas particulièrement familiers avec l’informatique. Il se base à la fois sur les fonctionnalités de R “de base”, et sur une série d’extensions de plus en plus populaires regroupées sous l’appellation tidyverse.

**Ce document n’est pas une introduction aux méthodes statistiques d’analyse de données**.

Ce document est régulièrement corrigé et mis à jour. Il est basé sur R version 3.6.2 (2019-12-12).

Le code source est disponible sur [GitHub](https://github.com/juba/tidyverse).

Pour toute suggestion ou correction, vous pouvez contacter l'auteur par [mail](mailto:julien.barnier@cnrs.fr) ou sur [Twitter](https://twitter.com/lapply).



