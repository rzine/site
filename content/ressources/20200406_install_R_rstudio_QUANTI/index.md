---
title: Installer R et RStudio (presque !) sans peine

summary: Tutoriel à destination de toutes celles et ceux qui souhaitent installer ou réinstaller R, RStudio et les packages de base pour les sciences sociales

authors:
- aperdoncin

projects:
- quanti

sources:
- "0"

thematics: ["0"]


languages:
- "0"



update:
- "0"

publication_types: ["11"]


date: "2020-04-06T00:00:00Z"


tags:
- R
- Rstudio
- package
- mise à jour


url_source: 'https://quanti.hypotheses.org/1813'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Article proposé par le carnet de recherche QUANTI : *Les outils du quanti en sciences humaines et sociales*.

Ce tutoriel est à destination de toutes celles et ceux, étudiants, doctorants, chercheurs…, qui souhaitent installer ou réinstaller R, RStudio et les packages de base pour les sciences sociales.

Il a été élaboré au fil de mes enseignements, afin de déjouer et résoudre les problèmes les plus fréquents auxquels mes étudiants étaient confrontés en installant R, RStudio et les packages. Si vous le suivez jusqu’au bout vous devriez (sauf bug) disposer d’une installation fonctionnelle.

Dans ce tutoriel, vous obtiendrez des réponses aux questions suivantes :

- Comment installer R
- Pourquoi et comment installer RStudio
- Quels sont les packages les plus utiles en sciences sociales
- Comment installer des packages
- Comment mettre à jour des packages
