---
title: Using R for Introductory Econometrics
subtitle: 2nd edition
summary: This book introduces R with a focus on the implementation of standard tools and methods used in econometrics.


authors:
-  Florian Heiss

publication_types: ["5"]


thematics: ["0","10","11","12","13","15","3","4"]


sources :
- "0"

update :
- "0"

languages:
- "1"

projects:
- divers

date: "2020-05-24T00:00:00Z"


tags:


url_source: 'http://www.urfie.net/read/index.html'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This book introduces the popular, powerful and free programming language and software package R with a focus on the implementation of standard tools and methods used in econometrics. Unlike other books on similar topics, it does not attempt to provide a self-contained discussion of econometric models and methods. Instead, it builds on the excellent and popular textbook "Introductory Econometrics" by Jeffrey M. Wooldridge. Some other editions and versions work as well, see below. It is compatible in terms of topics, organization, terminology and notation, and is designed for a seamless transition from theory to practice.

**Only available for reading...**




