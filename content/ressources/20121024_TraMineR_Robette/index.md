---
title: L’analyse de séquences
subtitle: Une introduction avec le logiciel R et le package TraMineR
summary: Une introduction avec le logiciel R et le package TraMineR

authors:
- nrobette

projects:
- quanti

sources:
- "0"

thematics: ["11","16"]


languages:
- "0"


update:
- "0"

publication_types: ["11"]


date: "2012-10-24T00:00:00Z"


tags:
- TraMineR
- analyse de séquence


url_source: 'https://quanti.hypotheses.org/686'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Article proposé par le carnet de recherche QUANTI : *Les outils du quanti en sciences humaines et sociales*.

