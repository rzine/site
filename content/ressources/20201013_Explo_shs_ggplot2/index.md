--- 
authors:
- mtheuliere
- hpecout

date: "2020-10-13"

title: Introduction à ggplot2
subtitle: Explorer des données à l’aide de graphiques statistiques
summary: Atelier d'initiation à la représentation graphique avec ggplot2, réalisé dans le cadre de l'école thématique Explo-shs (Mate-shs)

tags:
- learnr
- tutoriel
- ggplot2
- ggplot
- mapping
- layer
- aes
- aesthetic
- penguins
- COVID
- MATE SHS

publication_types: ["1", "13"]


thematics: ["11"]


languages:
- "0"

sources :
- "0"

update :
- "0"

projects:
- divers

url_source: 'https://hpecout.shinyapps.io/ggplot2/'
url_code: 'https://github.com/MaelTheuliere/ateliers'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Ce tutoriel [learnr](https://rstudio.github.io/learnr/) a été conçu pour l'atelier [*Comment explorer des données à l’aide de graphiques statistiques*](https://explo-shs.sciencesconf.org/data/theuliere_mardi13_9h30.pdf) réalisé dans le cadre de l'[école thématique EXPLO-SHS](https://explo-shs.sciencesconf.org/) organisée par le réseau [Mate-shs](https://mate-shs.cnrs.fr/), qui s'est déroulée à la Rochelle en octobre 2020.



