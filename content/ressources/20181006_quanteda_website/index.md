
---
# AUTHOR(S) 
authors:
- Kenneth Benoit
- Kohei Watanabe
- Haiyan Wang
- Paul Nulty
- Adam Obeng
- Stefan Müller
- Akitaka Matsuo
- Jiong Wei Lua
- Jouni Kuha
- William Lowe

# DATE de soumission à Rzine
date: "2018-10-06"

# TITRE
title: Quanteda

# RESUME COURT - une phrase
subtitle: An R package for the quantitative analysis of textual data
summary: An R package for the quantitative analysis of textual data

# TAGS
tags:
- text
- textual analysis

publication_types: ["6"]


thematics: ["17"]





sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

url_source: 'https://quanteda.io/index.html'


links:
- name: CRAN
  url: https://cran.r-project.org/web/packages/quanteda/index.html

doi: '10.21105/joss.00774'

publishDate: "2018-10-06T00:00:00Z"
# Name of review or book if the publication has been published in
publication: Journal of Open Source Software
publication_short: JOSS
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Official website of **Quanteda**, an **R package for managing and analyzing text**, created by **Kenneth Benoit**. Supported by the European Research Council grant ERC-2011-StG 283794-QUANTESS.


