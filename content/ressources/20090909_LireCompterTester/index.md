---
title: Lire ; Compter ; Tester... avec R
subtitle : Préparation des donnees, analyse univariée et bivariée
summary: Tutorial qui présente rapidement les outils permettants de lire des données et de les analyser (analyse univariée, tests statistiques)


authors:
- Christophe Genolini

projects:
- divers

sources :
- "0"

thematics: ["12"]





update :
- "0"

languages:
- "0"



publication_types: ["5"]



date: "2009-09-09T00:00:00Z"
doi: ""

tags:
- variable
- nature
- classe
- numérique
- qualitative
- quantitative
- univariée
- bivarée
- effectif
- centralité
- mode
- médiane
- moyenne
- quertile
- ecart-type
- variance
- dipersion
- test
- paramétrique
- ANOVA
- pearson
- student



url_source: 'https://cran.r-project.org/doc/contrib/Genolini-LireCompterTesterR.pdf'


featured: true 
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Ce tutorial a pour but de **donner rapidement aux lecteurs les outils permettants de lire des données et de les analyser** (analyse univariée, puis tests statistiques).     
Il ne rentre pas dans les détails de la programmation, dans les types, dans les fonctions ou les boucles. Il traite simplement trois sujets :   
- **lectures des données**     
- **analyse bivariée**     
- **test statistiques** (paramétriques et non paramétriques)


