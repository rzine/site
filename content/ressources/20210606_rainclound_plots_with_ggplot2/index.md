---
title: Visualizing distributions with Raincloud plots with ggplot2
subtitle : 
summary: How to plot different version of violin–boxplot combinations and raincloud plots with the {ggplot2} package


authors:
- Cédric Scherer

publication_types: ["11"]


sources:
- "0"

thematics: ["11"]


update:
- "0"

languages:
- "1"


date: "2021-06-06T00:00:00Z"

url_source: 'https://www.cedricscherer.com/2021/06/06/visualizing-distributions-with-raincloud-plots-with-ggplot2/'


tags:
- raincloud
- plot
- violin
- ggplot2
- box-and-whisker 
- Boxplots


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


How to plot different version of violin–boxplot combinations and raincloud plots with the {[ggplot2](https://ggplot2.tidyverse.org/)} package. Some use functionality from extension packages (that are hosted on CRAN): two of my favorite packages ([1](https://www.youtube.com/watch?v=7UjA_5gNvdw&list=PLAm5TIX-yz7IkKOUcStM_vl8AD0S9v0co&index=35), [2](https://raw.githubusercontent.com/Z3tt/TidyTuesday/master/plots/2021_01/2021_01_geomUsage.png)) namely {[ggforce](https://ggforce.data-imaginist.com/)} and {[ggdist](https://mjskay.github.io/ggdist/)}, plus the packages {[gghalves]()} and {[ggbeeswarm](https://github.com/eclarke/ggbeeswarm)}.



