---
title:  ggplot2  
subtitle: Elegant Graphics for Data Analysis

summary: This is the on-line version of work-in-progress 3rd edition of *ggplot2 - elegant graphics for data analysis* published by Springer.

authors:
- Hadley Wickham
- Danielle Navarro
- Thomas Lin Pedersen

publication_types: ["5"]


thematics: ["11"]


projects:
- divers

sources:
- "0"

update:
- "1"

languages:
- "1"

date: "2020-10-28T10:00:00Z"


tags:
- ggplot2
- maps
- network
- layer
- scales
- grammar


url_source: 'https://ggplot2-book.org/index.html'
url_code: 'https://github.com/hadley/ggplot2-book/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This is the on-line version of work-in-progress 3rd edition of “ggplot2: elegant graphics for data analysis” published by Springer. You can learn what’s changed from the 2nd edition in the Preface.

While this book gives some details on the basics of ggplot2, it’s primary focus is explaining the Grammar of Graphics that ggplot2 uses, and describing the full details. It is not a cookbook, and won’t necessarily help you create any specific graphic that you need. But it will help you understand the details of the underlying theory, giving you the power to tailor any plot specifically to your needs.


