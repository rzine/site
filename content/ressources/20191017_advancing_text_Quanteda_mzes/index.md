---
# Author(s)
authors:
- Cosima meyer
- Cornelius puschmann

# Date
date: "2020-12-17"


# Title
title: Advancing Text Mining with R and quanteda


# Short summary
summary: This blog post is based on [this report](http://cbpuschmann.net/quanteda_mzes/) and on [Cornelius’ post on topic models in R](http://inhaltsanalyse-mit-r.de/themenmodelle.html)

# Tags
tags:
- text mining
- quanteda
- document-feature matrix 
- DFM
- document-term matrix
- DTM
- token
- classification
- Naive Bayes
- NB
- Latent semantic analysis 
- LSA
- Latent Dirichlet Allocation 
- LDA 
- Structural topic models 
- STM
- stminsights
- corpus
- mzes

publication_types: ["11"]

sources:
- "0"

thematics: ["17", "3", "14"]

update:
- "1"

languages:
- "1"

# Link
url_source: 'https://www.mzes.uni-mannheim.de/socialsciencedatalab/article/advancing-text-mining/'

# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

This blog post is based on [this report](http://cbpuschmann.net/quanteda_mzes/) and on [Cornelius’ post on topic models in R](http://inhaltsanalyse-mit-r.de/themenmodelle.html).

Everyone is talking about text analysis. Is it puzzling that this data source is so popular right now? Actually no. Most of our datasets rely on (hand-coded) textual information. Extracting, processing, and analyzing this oasis of information becomes increasingly relevant for a large variety of research fields. This Methods Bites Tutorial by Cosima Meyer summarizes Cornelius Puschmann’s workshop in the [MZES](https://www.mzes.uni-mannheim.de/d7/en) Social Science Data Lab in January 2019 on **advancing text mining with R and the package** `**quanteda**`. The workshop offered guidance through the use of `quanteda` and covered various classification methods, including classification with known categories (dictionaries and supervised machine learning) and with unknown categories (unsupervised machine learning). 


> This post was updated in December 2020 to be consistent with quanteda’s version 2.1.2. For more information on differences between quanteda versions, have a look at this [excellent overview](https://blog.quanteda.org/2020/02/27/whats-new-in-quanteda-version-2.0/).

