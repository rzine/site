---
title: Data Science in Education Using R
subtitle: 
summary: Essential resource for education professionals and researchers looking to increase their data analysis skills as part of their professional and academic development


authors:
- Ryan A. Estrellado
- Emily A. Freer
- Jesse Mostipak
- Joshua M. Rosenberg
- Isabella C. Velásquez


publication_types: ["5"]


sources :
- "0"

thematics: ["0","12","13","16","17","18"]


update :
- "1"

languages:
- "1"

projects:
- divers

date: "2020-10-27T00:00:00Z"
doi: '10.4324/9780367822842'

tags: 
- teaching
- education


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://datascienceineducation.com/'
url_code: 'https://github.com/data-edu/data-science-in-education'

---

*Data Science in Education Using R* is the go-to reference for learning data science in the education field. The book answers questions like: What does a data scientist in education do? How do I get started learning R, the popular open-source statistical programming language? And what does a data analysis project in education look like?

If you’re just getting started with R in an education job, this is the book you’ll want with you. This book gets you started with R by teaching the building blocks of programming that you’ll use many times in your career. The book takes a "learn by doing" approach and offers eight analysis walkthroughs that show you a data analysis from start to finish, complete with code for you to practice with. The book finishes with how to get involved in the data science community and how to integrate data science in your education job.

This book will be an essential resource for education professionals and researchers looking to increase their data analysis skills as part of their professional and academic development.




