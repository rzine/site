---
title: Spatial Modelling for Data Scientists
summary: Manual to learn how to analyse and model different types of spatial data as well as gaining an understanding of the various challenges arising from manipulating such data

authors:
- Francisco Rowe
- Dani Arribas-Bel

publication_types: ["13","5"]


sources:
- "0"

thematics: ["10","13","19"]


update:
- "0"

languages:
- "1"

projects:
- divers

date: "2021-02-10T00:00:00Z"

tags:
- spatial
- points
- flows
- econometrics
- heterogeneity
- modelling
- weighted regression
- spatio-temporal


url_source: 'https://gdsl-ul.github.io/san/'
url_code: 'https://github.com/GDSL-UL/san'
url_pdf: 'https://gdsl-ul.github.io/san/spatial_analysis_notes.pdf'

links:
- name: Materials
  url: https://github.com/GDSL-UL/san/archive/master.zip


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This is the website for “Spatial Modeling for Data Scientists”. This is a course taught by Dr. Francisco Rowe and Dr. Dani Arribas-Bel in the Second Semester of 2020/21 at the University of Liverpool, United Kingdom. You will learn how to analyse and model different types of spatial data as well as gaining an understanding of the various challenges arising from manipulating such data.

The website is licensed under the [Attribution-NonCommercial-NoDerivatives 4.0 International](https://creativecommons.org/licenses/by-nc-nd/4.0/) License. A compilation of this web course is hosted as a GitHub repository that you can access:

- As a [download](https://github.com/GDSL-UL/san/archive/master.zip) of a .zip file that contains all the materials.
- As an [html website](https://gdsl-ul.github.io/san/).
- As a [pdf document](https://gdsl-ul.github.io/san/spatial_analysis_notes.pdf)
- As a [GitHub repository](https://github.com/GDSL-UL/san).


