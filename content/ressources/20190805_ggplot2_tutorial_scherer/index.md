---
title: A ggplot2 Tutorial for Beautiful Plotting in R
subtitle: 
summary: Updated version of a blog entry called *Beautiful plotting in R - A ggplot2 cheatsheet* by Zev Ross

authors:
- Cédric Scherer 

publication_types: ["11"]


sources :
- "0"

thematics: ["11"]


update :
- "1"

languages:
- "1"

projects:
- divers

date: "2019-08-05T00:00:00Z"


tags: 
- ggplot2
- tidyverse
- tidyverse
- dplyr
- tibble
- tidyr
- forcats
- colorspace
- corrr
- cowplot
- ggdark
- ggforce
- ggrepel
- ggridges
- ggsci
- ggtext
- ggthemes
- grid
- gridExtra
- patchwork
- rcartocolor
- scico
- showtext
- shiny
- charter
- echarts4r
- ggiraph
- highcharter
- plotly



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://www.cedricscherer.com/2019/08/05/a-ggplot2-tutorial-for-beautiful-plotting-in-r/'

---


Updated version of a blog entry called *Beautiful plotting in R: A ggplot2 cheatsheet* by Zev Ross




