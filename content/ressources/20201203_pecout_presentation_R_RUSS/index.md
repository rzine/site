---
title: Introduction à R
subtitle: Base du langage, packages, Rstudio et documentation

summary: Séminaire RUSS - Base du langage, packages, Rstudio et documentation


authors:
- hpecout

publication_types: ["12","13"]


thematics: ["0"]


projects:
- RUSS

sources:
- "7"

update:
- "1"

languages:
- "0"

date: "2020-12-01T10:00:00Z"
doi: ""

tags:
- objet
- package
- R base
- Rstudio
- communauté
- documentation
- rzine


url_source: 'https://huguespecout.github.io/Intro_R_Rstudio_FR/#/'
url_code: 'https://github.com/HuguesPecout/Intro_R_Rstudio_FR'

links:
- name: Video
  url: https://www.canal-u.tv/video/ined/introduction_a_r_partie_2_introduction_au_langage_r_et_a_l_ide_rstudio.62647


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Ce diaporama introduit les rudiments du langage R, présente une série de package populaires, liste les atouts de l'IDE Rstudio et réalise un tour d'horizon des ressources documentaires proposées par la communauté R**.  

Il s'agit d'un support utilisé pour la séance [RUSS](https://russ.site.ined.fr/) du 4 décembre 2020 : [*Introduction à R pour la mise en œuvre de chaînes de traitements reproductibles*](https://russ.site.ined.fr/fr/seances-passees/04-12-20) réalisée par Hugues Pecout (CNRS, FR CIST) et Ronan Ysebaert (Université de Paris, UMS RIATE).

Retrouvez l'ensemble des supports utilisés durant la séance à ce [lien](https://huguespecout.github.io/RUSS_seance_introduction/)





