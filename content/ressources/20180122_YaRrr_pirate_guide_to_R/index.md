---
title: YaRrr! The Pirate’s Guide to R
summary: This book is meant to introduce you to the basic analytical tools in R, from basic coding and analyses, to data wrangling, plotting, and statistical inference

authors:
- Nathaniel D. Phillips

publication_types: ["5", "13"]


sources:
- "0"

thematics: ["0","10","11","12"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- psychology

date: "2018-01-22T00:00:00Z"

url_source: 'https://bookdown.org/ndphillips/YaRrr/'
url_code: 'https://github.com/ndphillips/ThePiratesGuideToR'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This book is meant to introduce you to the basic analytical tools in R, from basic coding and analyses, to data wrangling, plotting, and statistical inference.



