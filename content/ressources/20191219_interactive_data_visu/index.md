
---
# AUTHOR(S) 
authors:
- Carson Sievert



# DATE de soumission à Rzine
date: "2019-12-19"

# TITRE
title: Interactive web-based data visualization with R, plotly, and shiny
subtitle: Insight and practical skills for creating interactive and dynamic web graphics

# RESUME COURT - une phrase
summary: Create interactive and dynamic web graphics for data analysis from R


# TAGS
tags:
- plotly
- tidyverse
- shiny
- ggplot2


# TYPE DE PUBLICATION
# manual = 0
# sheet = 1
# course = 2
# exercise = 3
# web = 4
# tool = 5
# article = 6
# annex = 7
# conf = 8
# uncat = 9
publication_types: ["5"]


thematics: ["11"]


sources :
- "6"

update :
- "0"

languages:
- "1"

# PROJET
# Les projets existants :
# Manuel CIST'R = manuel
# Fiche CIST'R = fiche
# Exercice CIST'R = exercice
# Sources diverses = divers
# Resources web (blog et site) = web
# GDR Analyse de réseau en SHS = GDR_ARSHS
# Axe Information territoriale du CIST = INFTER

projects:
- rseries

##### LINKS #####
# DOCUMENTATION
url_source: 'https://plotly-r.com/'
url_code: 'https://github.com/cpsievert/plotly_book/'

doi: "10.1201/9780429447273"

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
publishDate: "2014-09-01T00:00:00Z"
# Name of review or book if the publication has been published in
publication: R et espace. Traitement de l’information géographique
publication_short: R et espace
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

In this book, you’ll gain insight and practical skills for creating interactive and dynamic web graphics for data analysis from R. 

It makes heavy use of plotly for rendering graphics, but you’ll also learn about other R packages that augment a data science workflow, such as the tidyverse and shiny.     
Along the way, you’ll gain insight into best practices for visualization of high-dimensional data, statistical graphics, and graphical perception. 

By mastering these concepts and tools, you’ll impress your colleagues with your ability to generate more informative, engaging, and repeatable interactive graphics using free software that you can share over email, export to pdf/png, and more.

