---
date: "2021-07-13"
title: aRchéologie - Les usages disciplinaires de R
subtitle: Entre recours génériques et développement de packages spécifiques
summary: Entre recours génériques et développement de packages spécifiques, un état des lieux en archéologie (RR2021)
authors:
- splutniak
publication_types: ["12"]
thematics: ["9"]
update: ["0"]
sources: ["11"]
projects: ["RencontresR"]
languages: ["0"]
url_source: https://hal.archives-ouvertes.fr/hal-03287121/file/plutniak2021rconference.pdf
tags: ["archéologie", "packages"]
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Communication réalisée aux Rencontres R, le 13 juillet 2021**.

#### Un état des lieux en archéologie 

L’adoption de R dans les différentes disciplines scientifiques est menée en tension entre, d’une part, le recours aux méthodes génériques de transformation, d’analyse et de visualisation de données et, d’autre part, le développement de packages spécifiques. Cette tension sera illustrée à partir du cas de l’archéologie, en abordant: 1) l’historique des recours à R dans ce domaine au cours des vingt dernières années ; 2) un état des lieux actuel des usages, de la communauté, et des packages disponibles pour des problèmes spécifiques à l’archéologie ; 3) la présentation de l’un de ces packages, archeofrag, permettant d’évaluer la robustesse de distinctions spatiales (e.g., entre couches archéologiques) à partir des relations entre les fragments d’objets matériels brisés et dispersés dans l’espace d’un site archéologique. 





