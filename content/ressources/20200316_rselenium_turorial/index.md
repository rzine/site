---
title: RSelenium Tutorial
subtitle: Introduction to RSelenium
summary: An introduction to RSelenium for webscraping

authors:
- Josh McCrain

publication_types: ["11"]


sources:
- "0"

thematics: ["6"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- Rselenium
- webscraping
- scraping
- harvesting
- HTML
- OpenSecrets

date: "2020-03-16T00:00:00Z"

url_source: 'http://joshuamccrain.com/tutorials/web_scraping_R_selenium.html'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This tutorial is meant as an introduction to RSelenium for webscraping. If you’ve come across a website you can’t figure out how to scrape – typically because the normal downloading of HTML doesn’t produce what you want – RSelenium is more often than not your solution.

This tutorial assumes basic knowledge of R, rvest, and tidyverse functionality (specifically, pipes: %>%). However, the usage of these packages in what follows is fairly straightforward in order to cleanly demonstrate the utility of RSelenium.







