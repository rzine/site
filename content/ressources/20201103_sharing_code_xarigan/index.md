---
# Author(s)
authors:
- Silvia Canelón


# Date
date: "2020-11-03"


# Title
title: Sharing Your Work with xaringan


# Short summary
summary: An Introduction to xaringan for Presentations - The Basics and Beyond


# Tags
tags:
- xarigan



publication_types: ["13", "9"]


sources:
- "0"

thematics: ["1"]


update:
- "0"

languages:
- "1"


# Link
url_source: 'https://spcanelon.github.io/xaringan-basics-and-beyond/'


links:
- name: Slide 1 (basics)
  url: https://spcanelon.github.io/xaringan-basics-and-beyond/slides/day-01-basics.html#1
- name: Vidéo 1 (basics)
  url: https://www.youtube.com/watch?v=M3skTMQbCD0&feature=youtu.be
- name: Slide 2 (beyond)
  url: https://spcanelon.github.io/xaringan-basics-and-beyond/slides/day-02-beyond.html#1
- name: Vidéo 2 (beyond)
  url: https://www.youtube.com/watch?v=cAtpZxW4bTI&feature=youtu.be


# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---


This is the course site for the Sharing Your Work with xaringan workshop created by Dr. Silvia Canelón for the [NHS-R Community](https://nhsrcommunity.com/about/) [2020 Virtual Conference](https://nhsrcommunity.com/about/).

This four-hour hands-on workshop will be a gentle introduction to the [xaringan](https://github.com/yihui/xaringan#xaringan) package as a tool to create impressive presentation slides that can be deployed to the web for easy sharing.

- **Day 1** will cover the nuts and bolts of creating presentation slides using [xaringan](https://github.com/yihui/xaringan#xaringan) and deploying them in HTML format for easy sharing with others.   
- **Day 2** will cover how to take your slides to the next level with the [xaringanExtra](https://pkg.garrickadenbuie.com/xaringanExtra/#/README?id=xaringanextra) package and how to customize slides with CSS.    

This workshop is designed for R users already familiar with R Markdown and GitHub.


