---
authors:
- Steffi LaZerte

title: How to Cite R and R Packages

 
summary: A short primer on how to cite R and R packages to support developers and ensure repeatable science

date: "2021-11-16"

publication_types: ["11"] 


thematics: ["7"]


languages:
- "1" 

update:
- "0" 

sources:
- "5"

projects:
- rOpenSci



url_source: 'https://ropensci.org/blog/2021/11/16/how-to-cite-r-and-r-packages/' 



tags:
- R
- Package
- citation
- Reproducibility
- Repeatability
- good practices
- Citations

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


I teach R to a lot of scientists, those that are new to science (i.e. students) as well as more established scientists, new to R. I find that after all their struggles of dealing with dates, or remembering where to put the comma, they’re so grateful to actual have an analysis, that they often forget or aren’t aware of the next steps.

Many scientists don’t know that they should be citing R packages let alone R, and, if they do know, they often struggle with how. So here’s a short primer on why and how to get started!
