---
title: mapsf
subtitle: Create thematic maps
summary: Create and integrate thematic maps in your R workflow

authors:
- tgiraud

publication_types: ["6"]


sources:
- "0"

thematics: ["2"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- map
- maps
- carte
- cartographie
- cartography
- choropleth
- typology
- flow
- layout
- symbol
- proportional
- discontinuities
- grid
- palettes
- layout
- scale
- north
- arrow
- label
- legend
- thématique
- thematic
- theme
- mapsf
- sf
- mf_map
- mf_credits
- mf_arrow
- mf_shadow
- mf_label
- mf_scale
- mf_annotation
- theme
- mf_inset_on
- mf_init
- mf_layout
- mf_worldmap

date: "2021-03-03T00:00:00Z"

url_source: 'https://riatelab.github.io/mapsf/'
url_code: 'https://github.com/riatelab/mapsf'

links:
- name: Vignette
  url: https://riatelab.github.io/mapsf/articles/mapsf.html
- name: Cheatsheet
  url: https://raw.githubusercontent.com/riatelab/mapsf/master/vignettes/web_only/img/mapsf_cheatsheet.pdf
- name: CRAN
  url: https://cran.r-project.org/web/packages/mapsf/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Create and integrate thematic maps in your R workflow. This package helps to design various cartographic representations such as proportional symbols, choropleth or typology maps. It also offers several functions to display layout elements that improve the graphic presentation of maps (e.g. scale bar, north arrow, title, labels). `mapsf` maps `sf` objects on base graphics.




