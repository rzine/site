---
# Author(s)
authors:
- Ereynolds

# Date
date: "2020-08-01"


# Title
title: Easy Flipbook Recipes with {flipbookr} and {xaringan}

# Short summary
summary: These slides are aimed at helping you get started making flipbooks with {flipbookr} and {xaringan}

# Tags
tags:
- flipbookr
- flipbook
- xaringan
- knitr
- rmarkdown



publication_types: ["12"]

sources:
- "0"

thematics: ["1"]

update:
- "0"

languages:
- "1"


# Link
url_source: 'https://evamaerey.github.io/flipbooks/flipbook_recipes#1'



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

These slides are aimed at helping you get started making flipbooks with `flipbookr` and `xaringan`. It will show you what's needed in your source .Rmd document and what to expect for the flipbooked result.




