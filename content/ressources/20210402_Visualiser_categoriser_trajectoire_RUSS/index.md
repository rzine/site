---
title: Visualiser et catégoriser des trajectoires
subtitle : Séminaire RUSS - Séance du 2 avril 2021
summary: Séminaire RUSS - Séance du 2 avril 2021


authors:
- Luc-Olivier Hervé

publication_types: ["12","9"]


sources:
- "7"

thematics: ["16"]


update:
- "0"

languages:
- "0"


projects:
- RUSS

date: "2021-04-02T00:00:00Z"

url_source: 'https://russ.site.ined.fr/fichier/s_rubrique/31232/seminaire_russ_vicatraj_2avril2021_l.o_herve.fr.pdf'

links:
- name: Video (partie 1)
  url: https://www.canal-u.tv/video/ined/visualiser_et_categoriser_des_trajectoires_partie_1_methodes_de_typologie_de_trajectoires_copie.62641
- name: Video (partie 2)
  url: https://www.canal-u.tv/video/ined/visualiser_et_categoriser_des_trajectoires_partie_2_analyse_de_sequences_presentation_et_illustration.62393
- name: Video (partie 3)
  url: https://www.canal-u.tv/video/ined/visualiser_et_categoriser_des_trajectoires_partie_3_application_vicatraj_demonstration.62387

tags:
- ViCaTraj
- TraMineR
- analyse de séquences
- séquence
- trajectoire
- typologie
- chronogramme
- tapis
- transition
- WeightedCluster
- ggalluvial
- OMA

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Panorama des méthodes existantes, application de l’analyse de séquences par appariement optimal et démonstration d’une nouvelle application dédiée : **ViCaTraj**.

Séance présenté par **Luc-Olivier Hervé** (Data scientist, Cour des comptes & Doctorant-Chercheur, LEMNA)

L’intervention est structurée comme suit :
- Présentation des différentes méthodes pour analyser des données longitudinales/calendaires/biographiques (dresser des typologies) : modèles économétriques ; analyse harmonique quantitative ; recodage disjonctif puis analyses multivariées ; cartes de kohonen ; analyse de séquences par appariement optimal (l’oma)
- Présentation de l’oma : origines, applications, étapes de mise en place
- Illustration d’application de l’oma sur des données relatives à des dispositifs d’aides départementaux (RSA notamment)
- Démonstration de l’application ViCaTraj, application R shiny développée en collaboration avec la MRIE et le Département de l’Isère.
- Pistes d’amélioration de ***ViCaTraj***
- Echanges avec la salle sur la méthode et l’application

