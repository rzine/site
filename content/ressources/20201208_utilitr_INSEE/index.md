---
title: UtilitR
subtitle: Une documentation utile à R

summary: Documentation collaborative à l'usage des agents de l'Insee

authors:
- lgaliana
- omeslin
- Raphaële Adjerad
- Mathias André
- Pierre-Yves Berrard
- Lionel Cacheux
- Milena Suarez Castillo
- Arthur Cazaubiel
- Frédérique Cornuau
- Aurélien D’Isanto
- Sylvain Daubrée
- Arlindo Dos-Santos
- Antoine Dreyer
- Alexis Eidelman
- Marie-Emmanuelle Faure
- Gilles Fidani
- Gaëlle Génin
- Pierre Lamarche
- Claire Legroux
- Jean-Daniel Lomenède
- rlesur
- Pascal Mercier
- Olivier Meslin
- Violaine Poirot
- Géraldine Rochambeau
- Clément Rousset
- Cédric Tassart

publication_types: ["7", "8", "5"]

thematics: ["0","1","10","11","12","17","2","6","7","8"]

projects:
- utilitR

sources:
- "4"

update:
- "1"

languages:
- "0"

date: "2021-05-20T09:00:00Z"
doi: ""

tags:
- INSEE
- collaboratif
- utilitr
- documentation



url_source: 'https://www.utilitr.org/'
url_code: 'https://github.com/InseeFrLab/utilitR'

links:
- name: Version imprimable
  url: https://www.book.utilitr.org/

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Cette documentation s'adresse à tous les agents de l'Insee dans le cadre d'un usage courant de R**. Elle est conçue pour aider les agents à réaliser des traitements statistiques usuels avec R et à produire des sorties (graphiques, cartes, documents). Cette documentation présente succinctement les outils les plus adaptés à ces tâches, et oriente les agents vers les ressources documentaires pertinentes. En revanche, elle n'aborde pas les outils les plus avancés, notamment ceux utilisés dans un cadre de développement logiciel.
Cette documentation a pour ambition de répondre à trois questions générales:

Comment travailler avec R à l'Insee?
Comment réaliser des tâches standards avec R (importation et manipulation de données, exploitation d'enquêtes, graphiques...)?
Quelles sont les bonnes pratiques à respecter pour bien utiliser R?

Deux points importants sont à noter:


- **Cette documentation recommande les outils et les packages les plus adaptés au contexte d'utilisation de R à l'Insee**. Ces recommandations ne sont pas nécessairement adaptées à d'autres contextes, et pourront évoluer lorsque ce contexte évoluera.

- **Cette documentation recommande d'utiliser R avec Rstudio**, qui apparaît comme la solution la plus simple et la plus complète pour un usage courant de R, et qui est par ailleurs le choix effectué par l'Insee.


