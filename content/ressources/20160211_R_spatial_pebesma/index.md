---
title: r-spatial
summary: Blog for those interested in using R to analyse spatial or spatio-temporal data.

authors:
- Edzer Pebesma

publication_types: ["8"]


thematics: ["2"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

date: "2016-02-11T00:00:00Z"


tags:
- sf
- GDAL
- PROJ
- Leaflet
- stars
- spatio
- temporal
- mapedit


url_source: 'https://www.r-spatial.org/'
url_code: 'https://github.com/r-spatial/r-spatial.org'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

r-spatial.org is a website and blog for those interested in using R to analyse spatial or spatio-temporal data.


