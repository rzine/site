---
title: ggplot2tor
subtitle: Handmade tutorials to use ggplot2
summary: Handmade tutorials to help you master ggplot2

authors:
- Christian Burkhart

publication_types: ["8"]


sources:
- "0"

thematics: ["11"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- visualisation
- visualization
- ggplot
- ggplot2
- radar
- streetmap
- barchart
- Gapminder
- aesthetics
- geom
- mapping
- scale


date: "2019-08-01T00:00:00Z"

url_source: 'https://ggplot2tor.com/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This website helps you master the widely used R-package ggplot2 for data visualization. 

Each tutorial provides a step-by-step guide that teaches you how to create visualizations that go beyond the basics of ggplot2.



