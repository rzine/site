---
# the title of your publication
title: Manuel d’analyse spatiale

# an optional subtitle that will be displayed under the title
subtitle: Théorie et mise en oeuvre pratique avec R

# a one-sentence summary of the content on your page. The summary can be shown on the homepage and can also benefit your search engine ranking.
summary: Théorie et mise en oeuvre pratique avec R

# Display the authors of the publication and link to their user profiles (if they exist).
authors:
- Insee Méthodes, Vincent Loonis (dir) et Marie-Pierre de Bellefon (coord) 

# Type de la publication 
# 0 = Manuel; 1 = Fiche technique; 2 = Fiche méthodologique; 3 = Fiche thématique;
# 4 = Cours et formation ; 5 = Exercice et atelier; 6 = Site Web et blog ; 7 =  Communication; 
# 8 = Vidéo; 9 = Annexe et script; 10 = Video
publication_types: ["5"]


sources:
- "4"

thematics: ["19"]


languages:
- "0"


update:
- "0"


# Categorizing your content helps users to discover similar content on your site. Categories can improve search relevancy and display at the top of a page alongside a page’s metadata
  
# Project name associated ?
# Set the name if several publications can be grouped in one projet
projects:
- divers

# Date that the page was published
date: "2018-10-01T00:00:00Z"
doi: ""

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
# Name of review or book if the publication has been published in
# publication:
# publication_short:  


# by setting featured: true, a page can be displayed in the Featured widget. This is useful for sticky, announcement blog posts or selected publications etc. J'ai pas bien compris cet paramètre pour l'instant...
featured: true

# To display a featured image to represent your publication 
# To use, place an image named `featured.jpg/png` in your page's folder.
# set `placement` options to `1`  
# set `focal_point` options to  `Center`
# Set `preview_only` to `true` to just use the image for thumbnails. if not : `false`
# Set `caption` to precise the pitcure credit (with url), if necessary 
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

# Tagging your content helps users to discover similar content on your site. 
# Tags can improve search relevancy and are displayed after the page content and also in the Tag Cloud widget.
tags:
- analyse
- spatiale
- eurostat
- insee
- rgdal
- sf
- GISTools
- sp
- maptools
- tripack
- spdep
- TSP
- miscTools
- fields
- spatstat
- ETAS
- dbmss
- geoR
- nmle
- sas7bdat
- plm
- splm
- maps
- stargazer
- spaMM
- igraph
- cartography


# The url_ links can either point to local or web content. 
# Associated local publication content, may be copied to the publication’s folder and referenced like url_code = "code.zip".


# url_dataset: 'http://www.gis-cist.fr/cours/Calcul_repartition_theorique_ss_hypoth%c3%a8se_dindependance.xlsx'

#url_pdf: http://www.gis-cist.fr/wp-content/uploads/2018/06/cist2018-proceedings.pdf
#url_poster: '#'
#url_project: ""
#url_slides: ""
url_source: 'https://www.insee.fr/fr/statistiques/fichier/version-html/3635442/imet131.pdf'
#url_video: '#'

# url of GIT publication (.html)


  
# if the is an associated slide to this publication which is upload on the website
# Set the name of the folder where the md of the slide is saved.
# slides: example  

---



Le Comité d’experts des Nations Unies sur la gestion de l’information géospatiale mondiale (UN-GGIM) « a reconnu l’importance cruciale d’intégrer les informations géospatiales aux statistiques et aux données socio-économiques et le développement d’une infrastructure statistique géospatiale ». La demande pour les données géolocalisées augmente du fait de l’intérêt croissant accordé au caractère spatial des phénomènes. L’Insee a entrepris de mieux intégrer cette dimension dans son système d’information.

Afin de valoriser ces investissements l'Insee a coordonné la rédaction, par des experts de l’Insee et du monde académique, d'un manuel d'analyse spatiale. Ce manuel a bénéficié du soutien d’Eurostat et du Forum Européen de Géographie et de Statistique (EFGS). Il dresse un panorama des méthodes qui peuvent être employées quand on connaît la localisation des unités statistiques étudiées (logements, établissements, individus, etc.).

L’objectif de l'Insee Méthode n° 131 est de répondre aux questions concrètes des chargés d’étude : que faire avec ces nouvelles sources de données géolocalisées ? Dans quels cas doit-on prendre en compte leur dimension spatiale ? Comment appliquer les méthodes de statistique et d’économétrie spatiale ? La pédagogie est pensée également pour illustrer des enjeux plus spécifiques aux instituts nationaux de statistique : échantillonnage spatial, confidentialité, estimation sur petits domaines.

Après une introduction de la directrice générale d'Eurostat, Mariana Kotzeva, et du président de l'EFGS, Janusz Dygaszewicz, un guide d'utilisation présente l'articulation des 14 chapitres du manuel. La lecture des trois premiers chapitres est recommandée pour faciliter la compréhension de l’ensemble. Ils décrivent les étapes suivies lorsqu’on commence à analyser des données spatiales : décrire la localisation des observations, mesurer les interactions spatiales et choisir le modèle approprié. Le préambule de chaque chapitre précise les autres chapitres dont la lecture préalable est nécessaire à une bonne compréhension. **Le corps du texte présente la théorie, les exemples d’application pratique et le code informatique (langage R) associé**. Les encadrés sont des extensions plus techniques dont la lecture n’est pas indispensable pour comprendre l’essentiel de la méthode.



