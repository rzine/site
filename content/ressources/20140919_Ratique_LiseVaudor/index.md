---
title: R-atique
subtitle: Analyse de données avec R
summary: Blog proposant des billets, cours et applications R portant sur l'analyse statistique.

authors:
- lvaudor

publication_types: ["8"]


thematics: ["0","12","13","10"]


languages:
- "0"

sources:
- "0"

update:
- "1"


projects:
- divers


date: "2014-09-19T00:00:00Z"


tags:
- statistique
- statistic
- blog
- forcats
- stringr
- purrr
- lubridate
- infer
- shiny
- plotly
- scraper
- ggmap
- graphe
- test
- bayésien
- régréssion
- tapply
- date



url_source: 'http://perso.ens-lyon.fr/lise.vaudor/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



Blog de **Lise Vaudor**, **ingénieur de recherche CNRS** à l'ENS de lyon (*UMR 5600 Environnement Ville Société*) et spécialisée en analyses statistiques.     

Ce site web offre un certain nombre de ressources (billets, cours, application shiny), d'astuces et de conseils pour l'analyse statistique avec R.


