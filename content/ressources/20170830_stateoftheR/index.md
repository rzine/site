---
date: "2017-08-30T00:00:00Z"

authors: 
- Julie Aubert
- Julien Chiquet
- Marie-Pierre Étienne

title: State of The R
subtitle: Groupe d'utilisateur⸱rices⸱
summary: Groupe de chercheurs et d'ingénieurs 

tags: 
- séminaire
- formation
- conférence
- groupe
- AgroParisTech
- INRAE
- évènement
- Bootcamp

publication_types: ["4","8","2"]

thematics: ["8"]

projects:
- stateoftheR

sources:
- "0"

update:
- "1"

languages:
- "0"


url_source: 'https://stateofther.github.io/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Groupe de chercheurs et d'ingénieurs se réunissant pour approfondir leur savoir-faire, perfectionner la diffusion de leurs méthodes statistiques et échanger autour des dernières innovations de `R` et `Rstudio`.

#### Fonctionnement des ateliers participatifs

Le groupe se réunit à [AgroParisTech](http://www2.agroparistech.fr/) au cours d’ateliers mensuels de 3 heures autour d’un thème prédéfini. Chaque atelier est animé par quelques membres (généralement deux ou trois) qui se chargent de présenter les enjeux du thème, fournir le bagage méthodologique nécessaire, et proposer des travaux pratiques.

Nous pensons qu’il est important que l’ensemble des participants s’investissent dans l’animation pour préserver l’esprit des ateliers. Les places sont donc limitées à une trentaine par séance.

[voir les ateliers passés](https://stateofther.github.io/#past_workshops)

#### Devenir membre

Pour participer à ces ateliers, un aspirant membre peut assister à une séance pour jauger la teneur des séances et évaluer le niveau des participants. Il s’engage ensuite à animer une séance en proposant un atelier.


[Participer ?](mailto:stateofr@gmail.com)


Consulter le site web de State of the R : [https://stateofther.github.io/](https://stateofther.github.io/)
Pour le suivre sur Twitter :  [@RUG_stateofther](https://twitter.com/RUG_stateofther)


#### Bootcamp State Of The R 

Ce groupe d'utilisateur·rice·s organise chaque année (depuis 2017) un R bootcamp à la station biologique de Roscoff. 
Sites web dédiés :

- [FinistR'17](https://stateofther.github.io/finistR2017/index.html)   
- [FinistR'18](https://stateofther.github.io/finistR2018/index.html)   
- [FinistR'19](https://stateofther.github.io/finistR2019/)   
- [FinistR'20](https://stateofther.github.io/finistR2020/)   
- [FinistR'21](https://stateofther.github.io/finistR2021/)   






 
