---
# AUTHOR(S) 
authors:
- Ereynolds

# DATE de soumission à Rzine
date: "2020-02-01"

# TITRE
title: A ggplot2 grammar guide
subtitle: Teaching slides about grammar of ggplot2 

# RESUME COURT - une phrase
summary: Explaining the grammar of graphics visualization philosophy and using ggplot2 and flipbookr 

# TAGS
tags:
- ggplot2
- graphics
- flipbook
- flipbookr


# TYPE DE PUBLICATION
publication_types: ["13"]


sources :
- "0"

thematics: ["11"]



update :
- "0"

languages:
- "1"


projects:
- divers

##### LINKS #####
# DOCUMENTATION
url_source: 'https://evamaerey.github.io/ggplot2_grammar_guide/about'

# GIT
url_code: 'https://github.com/EvaMaeRey/ggplot2_grammar_guide'

links:
- name: Flipbooks project
  url: https://evamaerey.github.io/flipbooks/

# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Flipbookr slideshow series** to explain the use of **ggplot2**.
