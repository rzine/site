---
title: Calcul en parallèle sur CPU avec R
summary: Calcul en parallèle en exploitant un ordinateur multi-coeurs, la grappe de serveurs de calcul du Département de mathématiques et de statistique ou une instance Amazon EC2

authors:
- Sophie Baillargeon

publication_types: ["5"]


thematics: ["7"]


sources :
- "0"

update :
- "0"

languages:
- "0"

projects:
- divers

date: "2017-11-23T00:00:00Z"


tags:
- doParallel
- DMS
- AWS
- VPC



url_source: 'https://stt4230.rbind.io/autre_materiel/calcul_parallele_r_2017.pdf'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

Ce document est un tutoriel qui explique, à l’aide d’exemples, comment effectuer du calcul en parallèle (sur CPU) avec R. Premièrement, de la terminologie et quelques concepts de base sont décrits à la section 2. La section 3 présente les problèmes utilisés dans les exemples. La programmation parallèle en R est ensuite introduite à la section 4, dans le contexte le plus simple de calculs en parallèle, soit celui de l’exploitation de plusieurs unités de calcul sur un seul ordinateur local. Les sections suivantes montrent comment procéder pour exploiter des ressources plus complexes qu’une seule machine, soit la grappe de serveurs du Département de mathématiques et de statistique (section 5), puis la plate-forme publique de cloud computing Amazon EC2 (section 6).
