---
date: "2011-03-14T00:00:00Z"

title: rOpenSci
subtitle: Transforming science through open data, software & reproducibility
summary: R packages for the sciences via community driven learning, review and maintenance of contributed software in the R ecosystem

tags: 
- reproductible
- community
- ROpenSci
- project
 
authors:
- Karthik Ram
- Scott Chamberlain
- Carl Boettiger

publication_types: ["7", "8"]

thematics: ["5", "8", "7", "9"]

projects:
- ropensci

sources:
- "5"

update:
- "1"

languages:
- "1"

url_source: 'https://ropensci.org/'
url_code: 'https://github.com/ropensci'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


rOpenSci is a non-profit initiative founded in 2011 by Karthik Ram, Scott Chamberlain, and Carl Boettiger. Staff includes developers, researchers, and community builders. Non-staff strategic advisors and editorial board members are volunteers.

### Objectives

- Creating technical infrastructure in the form of carefully vetted, staff and community-contributed R software tools that lower barriers to working with scientific data sources on the web
- Making the right data, tools and best practices more discoverable
- Creating social infrastructure through a welcoming and diverse community
- Promoting advocacy for a culture of data sharing and reusable software
- Building capacity of software users and developers and fostering a sense of pride in their work

