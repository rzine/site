---
authors:
- Thomas Mock

title: RMarkdown - reproducible reporting

summary: A complete presentation of how Rmarkdown works and what it can do

date: "2022-04-26"

publication_types: ["12"] 

thematics: [ "1"]


languages:
- "1" 

update:
- "0" 

sources:
- "0" 

url_source: 'https://jthomasmock.github.io/rmd-nhs/#1' 

links:
- name: Materials
  url: 'https://github.com/jthomasmock/rmd-nhs/tree/master/workshop' 

tags:
- Markdown
- Rmarkdown
- Setup
- YAML
- Chunks
- Graphics


  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

A comprehensive presentation of how Rmarkdown works and what it can do. With an overview of the parameters concerning chunks, texts and graphics without forgetting the YAML

