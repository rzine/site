---
title: Print Personalized Street Maps Using R
subtitle: 
summary: A code walk-through of how to generate streets maps in R using {osmdata} and {sf} packages

authors:
- Taras Kaduk

publication_types: ["11"]


sources :
- "0"

thematics: ["11","2"]


update :
- "1"

languages:
- "1"

projects:
- divers

date: "2021-01-18T00:00:00Z"


tags: 
- osm
- sf
- osmdata
- carte
- map



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://taraskaduk.com/posts/2021-01-18-print-street-maps/'

---

A code walk-through of how to generate streets maps in R using {osmdata} and {sf} packages. Initially published on 2019-12-19 and updated on 2020-01-18.



