---
authors:
- Zuguang Gu

title: ComplexHeatmap Complete Reference


summary: This book gives comprehensive overviews of heatmap visualization in R by using the ComplexHeatmap package

date: "2021-08-04"

publication_types: ["5"] 

thematics:
- "11" 

languages:
- "1"

update:
- "1" 

sources:
- "0" 


url_source: 'https://jokergoo.github.io/ComplexHeatmap-reference/book/' 
url_code: 'https://github.com/jokergoo/ComplexHeatmap' 


tags:
- Heatmap
- Data visualization
- Heatmap Decoration
- tag3
- Heatmap Annotations
- pattern
- clustering


  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Complex heatmaps are efficient to visualize associations between different sources of data sets and reveal potential patterns. Here the ComplexHeatmap package provides a highly flexible way to arrange multiple heatmaps and supports self-defined annotation graphics.

