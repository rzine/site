---
title: Atelier d'analyse spatiale (GWR)
subtitle : Hétérogénéité spatiale et régression géographiquement pondérée
summary: Support d'un atelier réalisé dans le cadre de l'école SIGR2021


authors:
- tfeuillet

publication_types: ["13"]


sources:
- "9"

thematics: ["19"]


update:
- "0"

languages:
- "0"


projects:
- SIGR2021

date: "2021-07-01T00:00:00Z"

url_source: 'https://sigr2021.github.io/gwr/'
url_code: 'https://github.com/sigr2021/gwr'

tags:
- SIGR2021
- GWR
- sf
- tmap
- plotly
- gtsummary
- GGally
- GWmodel
- spdep
- dvf
- MCO
- résidus
- Calibration
- matrice de distances
- voisinage
- pondérations spatiales
- GWR multiscalaire
- multiscale GWR
- Régionalisation
- coefficients GWR
- SKATER
- clusters


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Communication réalisée durant l'école thématique SIGR2021 sur les sciences de l’information géographique reproductibles.

L’objectif de cet atelier est de mettre en pratique, dans R, certains des concepts et méthodes fréquemment employés en analyse spatiale, avec un focus ici sur l’hétérogénéité spatiale et la régression géographiquement pondérée (GWR). La GWR est une méthode de régression locale qui permet précisément d’explorer la non-stationarité spatiale à travers des cartes de relations ([Fotheringham et al., 2003](https://www.wiley.com/en-us/Geographically+Weighted+Regression%3A+The+Analysis+of+Spatially+Varying+Relationships+-p-9780471496168)), et de servir alors de base pour une régionalisation. Nous allons préciser la façon dont ce type de modèle est calibré, estimé et interprété.







