---
title: JavaScript for R
subtitle : Enhance your data science products with JavaScript
summary: Enhance your data science products with JavaScript [**Under development**] 


date: "2021-02-12"

authors:
- John Coene

publication_types: ["5"]


thematics: ["11","5"]


projects:
- rseries

sources:
- "6"

update:
- "1"

languages:
- "1"


tags:
- JavaScript
- shiny
- widget
- webpack
- NPM
- html

url_source: 'https://book.javascript-for-r.com/'
url_code: 'https://github.com/JohnCoene/javascript-for-r'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This is the online version of JavaScript for R, a book currently under development and intended for release as part of the [R series by CRC Press](https://www.routledge.com/Chapman--HallCRC-The-R-Series/book-series/CRCTHERSER).

The R programming language has seen the integration of many languages; C, C++, Python, to name a few, can be seamlessly embedded into R so one can conveniently call code written in other languages from the R console. Little known to many, R works just as well with JavaScript—this book delves into the various ways both languages can work together.

The ultimate aim of this work is to demonstrate to the reader the many great benefits one can reap by inviting JavaScript into their data science workflow. In that respect, the book is not teaching one JavaScript but instead demonstrates how little JavaScript can significantly support and enhance R code. Therefore the focus is on integrating external JavaScript libraries and only limited knowledge of JavaScript is required in order to learn from the book. Moreover, the book focuses on generalisable learnings so the reader can transfer takeaways from the book to solve real-world problems.

Throughout the book, several shiny applications and R packages are put together as examples. All of these, along with the code for the entire book, can be found on the Github repository: github.com/JohnCoene/javascript-for-r.



