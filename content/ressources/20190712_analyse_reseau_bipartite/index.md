---
title: L’analyse de réseaux bipartites
subtitle: Initiation à l'abalyse de Les réseaux bipartites
summary: Support de formation d'initiation à l’analyse de **réseaux bipartites** utilisé pour une école d’été organisée par le GDR ARSHS, à Nice en Juillet 2019.


authors:
- mmaisonobe
publication_types: ["13"]

thematics: ["18"]

languages:
- "0"
update:
- "0"
projects:
- GDR_ARSHS
sources :
- "0"

date: "2019-07-12T00:00:00Z"
publishDate: "2019-07-12T00:00:00Z"
doi: ""

featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  

tags:
- igraph
- analyse de réseau
- graphe
- initiation
- bipartite
- tnet


url_source: 'https://marionmai.frama.io/R-reseaux_bipartis/'
url_code: 'https://framagit.org/MarionMai/R-reseaux_bipartis'
url_dataset: 'https://framagit.org/MarionMai/R-reseaux_bipartis/-/raw/master/freeman_friendship.csv?inline=false'

links:
- name: Diaporama
  url: https://framagit.org/MarionMai/R-reseaux_bipartis/-/blob/master/Les_reseaux_bipartis_MM.pdf
- name: Script R
  url: https://framagit.org/MarionMai/R-reseaux_bipartis/-/blob/master/script_bipartis.R
- name: Liens utiles
  url: https://framagit.org/MarionMai/R-reseaux_bipartis/-/blob/master/Liens.md
- name: Lire Freeman
  url: https://framagit.org/MarionMai/R-reseaux_bipartis/-/blob/master/freeman_friendship_80.pdf


  
---

Ce document est un **support de formation d'initiation à l'analyse de réseaux**.

Il s’agit du support utilisé pour l’atelier *"Réseaux bipartites"* réalisé dans le cadre de l’École d’été : [Initiation à l’analyse de réseaux en SHS](https://arshs.hypotheses.org/1200), organisé par le [**GDR** « *Analyse de Réseaux en Sciences Humaines et Sociales*](https://arshs.hypotheses.org/) » à Nice du 1 au 6 Juillet 2019, au Centre de la Méditerranée Moderne et Contemporaine.

L'introduction comprend un bref historique sur l'analyse des réseaux bipartites suivi d'une définition. Il s'en suit l'analyse d'un réseau reconstitué à partir d'un article de Linton Freeman accessible sur le dépôt git associé à ce support. Plusieurs opérations sont proposées sur ce réseau : calculs d'indicateurs, passage de la forme bipartite aux formes unipartites, visualisation.     

Le dépôt Gitlab contient également :    
- Un diaporama présenté avant la séance de manipulation   
- Un script R contenant l'ensemble du code présenté dans le tutoriel   
- Un inventaire (liste) de données et de tutoriels sur les réseaux bipartites    
- L'article de Linton Freeman    











