---
title: Créer un package R en quelques minutes

subtitle: Billet issue du blog ThinkR

summary: Billet issue du blog ThinkR qui présente la création d’un package R en cinq étapes


authors:
- Vincent Guyader

publication_types: ["11"]


thematics: ["5"]


projects:
- divers

sources:
- "0"

update:
- "0"

languages:
- "0"

date: "2018-05-08T00:00:00Z"
doi: ""

tags:
- devtools
- usethis
- roxygen2
- build
- DESCRIPTION
- check
- fonction
- NAMESPACE
- CRAN

url_source: 'https://thinkr.fr/creer-package-r-quelques-minutes/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---





