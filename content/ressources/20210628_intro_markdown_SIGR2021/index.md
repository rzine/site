---
title: Produire des documents reproductibles avec R Markdown
subtitle : Partager code et documentation
summary: Support d'un atelier réalisé dans le cadre de l'école SIGR2021


authors:
- rysebaert

publication_types: ["12","13"]


sources:
- "9"

thematics: ["1"]


update:
- "0"

languages:
- "0"


projects:
- SIGR2021

date: "2021-06-28T00:00:00Z"

url_source: 'https://sigr2021.github.io/markdown/#1'
url_code: 'https://github.com/sigr2021/markdown'

tags:
- SIGR2021
- markdow
- literate programming
- document computationnel
- rmarkdown
- knitr
- pandoc
- chunk


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Communication réalisée durant l'école thématique SIGR2021 sur les sciences de l’information géographique reproductibles.

Cette présentation vise à :
- Inititier au literate programming avec R Markdown.   
- Transmettre les bonnes pratiques en matière de reproductibilité et documentation de méthodes.   
- Fournir des ressources de référence pour approfondir l'apprentissage.   





