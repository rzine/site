---
authors:
- Ariel Muldoon

title: Advanced R topics

subtitle: Class materials 

summary: Slides and other materials for all 10 working sessions

date: "2021-06-10"

publication_types: ["13", "8"]

thematics: ["1", "11", "7"]

languages: 
- "1" 

update:
- "0"


sources:
- "0" 

url_source: 'https://aosmith16.github.io/spring-r-topics/materials.html' 
url_code: 'https://github.com/aosmith16/spring-r-topics' 


tags:
- git
- distill
- website
- articles
- gt
- tables
- functions
- git conflict
- GitKraken
- leaflet
- dygraphs
- htmlwidgets
- pull request
- package
- interactive plot
- plotly
- reprex
- GitHub issue 
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Website dedicated to slides and materials for 10 working sessions for graduate students held in spring quarter 2021. These are advanced topics in the sense that we cover tools that students are not exposed to when they are introduced to R. It is reminded that it can be nice to work on such topics as a group rather than each student needing to learn on their own.

Programme :

- Week 01: Introduction to Git/GitHub
- Week 02: Build a personal website with distill and postcards
- Week 03: Deploy website using GitHub Pages
- Week 04: Explore gt for making tables
- Week 05: gt flair: colors, images, and themes
- Week 06: Learn to write functions in R
- Week 07: More Git: collaborators, merge conflicts, and pull requests
- Week 08: Interactive graphics and tables
- Week 09: Reproducible examples with reprex
- Week 10: GitHub issues
