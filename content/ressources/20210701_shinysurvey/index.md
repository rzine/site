---
# Author(s)
authors:
- Jonathan Trattner
- Lucy D'Agostino McGowan


# Date
date: "2021-07-01"


# Title
title: shinysurveys
subtitle: Creating and deploying surveys in Shiny

# Short summary
summary: Package for creating and deploying surveys in Shiny

# Tags
tags:
- shiny
- shinyforms
- survey


publication_types: ["6"]

sources:
- "0"

thematics: ["5"]

update:
- "1"

languages:
- "1"


# Link
url_source: 'https://shinysurveys.jdtrat.com/'
url_code: 'https://github.com/jdtrat/shinysurveys/'



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

`shinysurveys` provides easy-to-use, minimalistic code for creating and deploying surveys in `Shiny`. Originally inspired by [Dean Attali’s shinyforms](https://github.com/daattali/shinyforms), our package provides a framework for robust surveys, similar to Google Forms, in R with [Shiny](https://github.com/daattali/shinyforms).


