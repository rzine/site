---
# AUTHOR(S) 
authors:
- srey
- lvaudor
- Fabien Pfaender 


# DATE de soumission à Rzine
date: "2018-09-27"

# TITRE
title: Web Data Collection

subtitle: Summer school workshop (2018) of the LabEx DynamITe

summary: Materials of web scraping workshop carried out as part of the summer school 2018 of the LabEx DynamITe

# TAGS
tags:
- webscraping
- rvest
- RSelenium
- rtweet
- stringr
- purrr
- dplyr
- httr
- photon
- leboncoin
- twitter
- FlightRadar
- docker
- ethic



# TYPE DE PUBLICATION
publication_types: ["13"]


thematics: ["6","2","7"]


languages:
- "1"

sources:
- "0"

update:
- "0"


projects:
- dynamite

links:
- name: Tutorial
  url: https://dynamitestaff.github.io/R-workshops/Web_data_collection/tuto_scraping/tuto_scraping.html
- name: Use rvest
  url: https://dynamitestaff.github.io/R-workshops/Web_data_collection/scrap_leboncoin/projet_leboncoin.html
- name: Use RSelenium
  url: https://dynamitestaff.github.io/R-workshops/Web_data_collection/scrap_flightradar/scrap_flightradar.html
- name: Use API
  url: https://dynamitestaff.github.io/R-workshops/Web_data_collection/API_twitter/API_twitter.html
- name: Ethics of webscraping
  url: https://hackmd.io/g9JrWURjTHyLr9EWQqKoiQ?view



url_code: 'https://github.com/DynamiteStaff/R-workshops'
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Materials for a** *Web Data Collection* **Workshop** organised by the [**Summer School 2018 of the LabEx DynamITe**](http://labex-dynamite.com/fr/evenements-du-labex/ecoles-dete/ecole-dete-2018/)**.

 





