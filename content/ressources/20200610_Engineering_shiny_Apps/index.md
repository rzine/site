---
# AUTHOR(S) 
authors:
- Colin Fay
- Sébastien Rochette
- Vincent Guyader
- Cervan Girard


# DATE de soumission à Rzine
date: "2020-11-27"

# TITRE
title: Engineering Production-Grade Shiny Apps
subtitle: Process of building an application that will later be sent to production

# RESUME COURT - une phrase
summary: This book is about the process of building an application that will later be sent to production.


tags:
- shiny
- apps
- css
- golem
- deploy
- optimize
- javascript
- safety
- ThinkR


publication_types: ["5"]



thematics: ["5"]


sources :
- "6"

update :
- "1"

languages:
- "1"


projects:
- rseries


url_source: 'https://engineering-shiny.org/'
url_code: 'https://github.com/ThinkR-open/engineering-shiny-book'

publishDate: "2020-11-27T00:00:00Z"
publication: Engineering Production-Grade Shiny Apps
publication_short: Engineering Production-Grade Shiny Apps
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**This book will soon be available in print**, published in the **R Series** by Chapman & Hall.

This book will not get you started with Shiny, nor talk how to work with Shiny once it is sent to production. What we’ll see is the process of building an application that will later be sent to production. Why this topic? Lots of blog posts and books talk about getting started with {shiny} (Chang et al. 2020) or about what to do once your application is ready to be sent to production. Very few (if any) talk about this area in-between: how to confidently work with Shiny once you know the basics and before you send it to the production environment.

So this is what this book is going to talk about: building robust Shiny applications. We’ll focus on the process, the workflow, and the necessary tools for building production-grade Shiny Applications.
