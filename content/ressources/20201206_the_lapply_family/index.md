---
title: The lapply() family
summary: Blog post to understand the apply functions

authors:
- Michael Barrowman

publication_types: ["11"]


sources:
- "0"

thematics: ["10"]


update:
- "0"

languages:
- "1"


projects:
- divers

date: "2020-12-06T00:00:00Z"

tags:
- list
- apply
- lapply
- vapply
- sapply
- tapply


url_source: 'https://michaelbarrowman.co.uk/post/the-lapply-family/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

[This question on Reddit](https://www.reddit.com/r/rstats/comments/l0gvh3/good_site_to_understand_the_apply_collection_of/), got me thinking about the lapply() family of functions, and how a beginner might want to learn about them. Here is my take


