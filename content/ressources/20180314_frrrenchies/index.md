---
title: Frrrenchies 
subtitle : Liste collaborative de ressources R francophones ou utiles pour les Français.
summary: Liste collaborative de ressources R francophones ou utiles pour les Français.

authors:
- Paul-Antoine Chevalier

publication_types: ["0"]


sources:
- "0"

thematics: ["8"]


languages:
- "0"

update:
- "1"

projects:
- divers

date: "2018-03-14T00:00:00Z"

tags:
- packages
- ressources
- pédagogiques
- cours
- slide
- aide
- chat
- discussion
- communautaire
- blogs
- meetup
- séminaire
- conférence
- mail

url_source: 'https://frrrenchies.github.io/frrrenchies/'
url_code: 'https://github.com/frrrenchies/frrrenchies'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



L’objectif de Frrrenchies est de répertorier les packages qui permettent de travailler avec des données concernant la France et les ressources pédagogiques ou d’entraide francophone.

**N'hésitez pas à contribuer !**



