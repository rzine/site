---
title: Rstudio Primers
subtitle : Learn with the interactive tutorials
summary: Learn data science basics with the interactive tutorials

authors:
- Rstudio

publication_types: ["13"]


sources:
- "0"

thematics: ["0","1","10","11","5"]


update:
- "1"

languages:
- "1"


projects:
- divers

date: "2020-06-02T00:00:00Z"

tags:
- introduction
- basic
- visualize
- tidy
- iterate
- function
- reproductibility
- web apps
- dataviz
- ggplot
- dplyr
- tidyr
- tibbles
- Rstudio


url_source: 'https://rstudio.cloud/learn/primers'
url_code: 'https://github.com/rstudio-education/primers'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Collections of interactive tutorials made with [learnr](https://rstudio.github.io/learnr/). These primers teach the basics of R and the Tidyverse.



