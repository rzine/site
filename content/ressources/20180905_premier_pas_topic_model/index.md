---
title: Premier pas en Topic Model
summary: Quand les top models font de l’analyse textuelle... Le Topic Model ou quand le data mining de données textuelles devient sexy !

authors:
- glecampion88

publication_types: ["11"]


sources:
- "0"

thematics: ["17"]


update:
- "1"

languages:
- "0"

  
projects:
- divers


date: "2018-09-05T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  
tags:
- topic
- model
- topic model
- LDA
- topicmodels
- proustr
- ldatuning
- wesanderson
- broom
- DTM
- DocumentTermMatrix
- terms


url_source: 'https://ouvrir.passages.cnrs.fr/wp-content/uploads/2019/07/rapp_topicmodel.html'

---

Tutoriel proposé par l'[UMR PASSAGES](https://ouvrir.passages.cnrs.fr/tutoriel-r/) sur une méthode d’analyse textuelle : **le Topic Modeling**.

Ce tutoriel aborde succinctement la théorie du topic model, mais en propose surtout sa réalisation avec R. 

