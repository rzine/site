---
# Author(s)
authors:
- Taylor Arnold
- Lauren Tilton


# Date
date: "2017-03-27"


# Title
title: Basic Text Processing in R 


# Short summary
summary: Learn how to use R to analyze high-level patterns in texts, apply stylometric methods over time and across authors, and use summary methods to describe items in a corpus. Lesson from [The Programming Historian](https://programminghistorian.org/)


# Tags
tags:
- tidyverse
- tokenizers
- tokenization
- tokenize
- Programming Historian
- PH


publication_types: ["10", "13"]


sources:
- "10"

thematics: ["17"]


update:
- "1"

languages:
- "1"
- "2"

projects:
- programming_historian

doi: '10.46430/phen0061'

# Link
url_source: 'https://programminghistorian.org/en/lessons/basic-text-processing-in-r'


links:
- name: Versión en español
  url: https://programminghistorian.org/es/lecciones/procesamiento-basico-de-textos-en-r


# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---


Learn how to use R to analyze high-level patterns in texts, apply stylometric methods over time and across authors, and use summary methods to describe items in a corpus. Lesson from [The Programming Historian](https://programminghistorian.org/).



