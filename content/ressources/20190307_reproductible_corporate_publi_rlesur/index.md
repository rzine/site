---
title: Reproductible Corporate Publications with R
subtitle: Challenges and perspectives
summary: Challenges and perspectives (talk slides)

authors:
- rlesur

publication_types: ["12"]


thematics: ["1"]


sources :
- "0"

update :
- "0"

languages:
- "1"


projects:
- divers

date: "2019-03-07T00:00:00Z"


tags:
- markdown
- languages
- latex
- docx
- pdf
- pptx
- odt
- epub
- knitr
- pandoc
- pagedown
- css


url_source: 'https://rlesur.gitlab.io/statcanrug/slides.pdf'
url_code: 'https://gitlab.com/RLesur/statcanrug/'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

