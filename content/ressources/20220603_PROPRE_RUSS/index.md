---
title: PROcessus de Publications REproductibles avec R
subtitle : Séminaire RUSS - Séance du 3 juin 2022
summary: Séminaire RUSS - Séance du 3 juin 2022


authors:
- Juliette Engelaere-Lefebvre

publication_types: ["12","9"]

sources:
- "7"

thematics: ["7"]


update:
- "0"

languages:
- "0"


projects:
- RUSS

date: "2022-06-03T00:00:00Z"

url_source: 'https://russ.site.ined.fr/fichier/s_rubrique/32841/20220603_com_propre_seminaire_russ.fr.pdf'

links:
- name: Video (Introduction)
  url: https://www.canal-u.tv/chaines/ined/processus-de-publications-reproductibles-avec-r-la-demarche-propre/la-methode-propre
- name: Video (partie 1)
  url: https://www.canal-u.tv/chaines/ined/processus-de-publications-reproductibles-avec-r-la-demarche-propre/la-methode-propre-0
- name: Video (partie 2)
  url: https://www.canal-u.tv/chaines/ined/processus-de-publications-reproductibles-avec-r-la-demarche-propre/la-methode-propre-2
- name: Video (partie 3)
  url: https://www.canal-u.tv/chaines/ined/processus-de-publications-reproductibles-avec-r-la-demarche-propre/la-methode-propre-3
- name: Video (partie 4)
  url: https://www.canal-u.tv/chaines/ined/processus-de-publications-reproductibles-avec-r-la-demarche-propre/la-methode-propre-1

tags:
- RUSS
- package
- reproductibilité
- publication
- science ouverte

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**PROcessus de Publications REproductibles avec R**, Juliette Engelaere-Lefebvre (Responsable du Centre de services de la donnée SCTE/CSD Direction Régionale de l’Environnement, de l’Aménagement et du Logement Pays de Loire)

Depuis 2017, le datalab de la DREAL Pays de la Loire a expérimenté de nouvelles méthodes de travail autour du Design Thinking et des méthodes Agiles pour réaliser ses publications statistiques.
En 2019, dans le prolongement des mutations plus centrées sur les besoins de ses clients, le datalab opère un pivot plus technique : d’une culture de la donnée il se tourne vers une culture logicielle et le formalise par la démarche [PROPRE](https://dreal.statistiques.developpement-durable.gouv.fr/parc_social/2020/www/export_demarchePropre.pdf) pour PROcessus de Publications REproductibles. Inspiré du Reproductible Analytical Pipeline du gouvernement britannique, cette approche utilise l’objet technique qu’est le paquet R comme véhicule de la publication statistique. Les développements sont versionnés, factorisés, documentés, testés et intégrés au sein d’une forge logicielle qui permet de mutualiser, rationaliser et sécuriser les productions. À l’écoute active des besoins de ses clients d’un côté et soucieux de l’opérationnalité en production de l’autre, le statisticien métamorphose son métier vers un continuum DataOps.    
Cette présentation décrit cette (r)évolution, les principaux résultats et les conditions de sa réussite.
