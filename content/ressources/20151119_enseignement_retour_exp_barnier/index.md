---
title: L’enseignement et la formation au logiciel R
subtitle: Un retour d’expérience
summary: Séminaire RUSS - un retour d’expérience sur l'enseignement de R

authors:
- jbarnier

publication_types: ["12"]


sources:
- "7"

thematics: ["9"]


update:
- "0"

languages:
- "0"

projects:
- RUSS

tags:
- RUSS
- enseignement
- formation

date: "2015-11-19T00:00:00Z"

url_source: 'https://halshs.archives-ouvertes.fr/halshs-01241991/file/slides_hal-shs.pdf'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Il s'agit du support de communication utilisé pour la séance [RUSS](https://russ.site.ined.fr/) du 19 Novembre 2015 : [*L’enseignement et la formation au logiciel R : retours d’expériences*](https://www.ined.fr/fr/actualites/rencontres-scientifiques/seminaires-colloques-ined/russ-19-novembre-2015/) réalisée par Julien Barnier (CNRS, Centre Max Weber)








