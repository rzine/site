---
title: Introduction to Spatial Data Programming with R
summary: This book contains the materials of course named *Introduction to Spatial Data Programming with R*, given at Ben-Gurion University of the Negev

authors:
- Michael Dorman

publication_types: ["13"]


sources:
- "0"

thematics: ["10","11","15","2","20"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- spatial
- geometric operation
- spatio-temporal
- interpolation
- raster
- vector


date: "2021-01-16T00:00:00Z"

url_source: 'https://geobgu.xyz/r/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This book contains the materials of the 3-credit undergraduate course named Introduction to Spatial Data Programming with R, given at the Department of Geography and Environmental Development, Ben-Gurion University of the Negev. The course was given in 2013, and then each year in the period 2015-2020. An earlier version of the materials was published by Packt (Dorman 2014).



