---

title: xaringanExtra
subtitle: Extensions for xaringan slides
summary: xaringanExtra is a playground of enhancements and extensions for xaringan slides

authors:
- Garrick Aden-Buie

date: "2021-03-11"

publication_types: ["6","12"]


thematics: ["1"]


languages:
- "1"

update:
- "1"

sources:
- "0"


url_source: 'https://pkg.garrickadenbuie.com/xaringanExtra/#/?id=xaringanextra'
url_code: 'https://github.com/gadenbuie/xaringanExtra/'
url_slides: 'https://www.garrickadenbuie.com/talk/extra-great-slides-nyhackr/'


tags:
- xaringan
- xaringanExtra
- slide
- presentation
- extension


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**xaringanExtra** is a playground of enhancements and extensions for xaringan slides.

- Add an overview of your presentation with tile view
- Make your slides editable
- Share your slides in style with share again
- Broadcast your slides in real time to viewers with broadcast
- Scribble on your slides during your presentation with scribble
- Announce slide changes with a subtle tone
- Animate slide transitions with animate.css
- Add tabbed panels to slides with panelset
- Add a logo to all of your slides with logo
- Add a search box to search through your slides with search
- Use the Tachyons CSS utility toolkit
- Add a live video feed of your webcam
- Add one-click code copying with clipboard
- Fit your slides to fill the browser window
- Add extra CSS styles

Each item can be enabled separately, or load everything at once with a single call.

```
xaringanExtra::use_xaringan_extra(c("tile_view", "animate_css", "tachyons"))
```

##### Installation

You can install the current version of xaringanExtra from GitHub.

```
# install.packages("devtools")
devtools::install_github("gadenbuie/xaringanExtra")
```
