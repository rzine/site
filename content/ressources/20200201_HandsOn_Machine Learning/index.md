---
title: Hands-On Machine Learning with R
subtitle:
summary: This book provides hands-on modules for many of the most common machine learning methods

authors:
- Bradley Boehmke
- Brandon Greenwell

publication_types: ["5"]


sources :
- "0"

thematics: ["3"]


update :
- "0"

languages:
- "1"

projects:
- divers

tags:
- clustering
- autoencoders
- random forest
- deep neural networks
- gradient boosting machines
- stacking
- stacked
- super learners
- regression
- linear
- logistic
- regularized
- deep learning
- machine learning
- kmeans
- hierachical clustering
- model

date: "2020-02-01T00:00:00Z"
doi: ""
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://bradleyboehmke.github.io/HOML/'

---

**Hands-on Machine Learning with R** provides a practical and applied approach to learning and developing intuition into today’s most popular machine learning methods. This book serves as a practitioner’s guide to the machine learning process and is meant to help the reader learn to apply the machine learning stack within R, which includes using various R packages such as **glmnet**, **h2o**, **ranger**, **xgboost**, **keras**, and others to effectively model and gain insight from their data. The book favors a hands-on approach, providing an intuitive understanding of machine learning concepts through concrete examples and just a little bit of theory. 

Throughout this book, the reader will be exposed to the entire machine learning process including feature engineering, resampling, hyperparameter tuning, model evaluation, and interpretation. The reader will be exposed to powerful algorithms such as regularized regression, random forests, gradient boosting machines, deep learning, generalized low rank models, and more! By favoring a hands-on approach and using real word data, the reader will gain an intuitive understanding of the architectures and engines that drive these algorithms and packages, understand when and how to tune the various hyperparameters, and be able to interpret model results. By the end of this book, the reader should have a firm grasp of R’s machine learning stack and be able to implement a systematic approach for producing high quality modeling results.

Features:

- Offers a practical and applied introduction to the most popular machine learning methods.
- Topics covered include feature engineering, resampling, deep learning and more.
- Uses a hands-on approach and real world data.



