
---
authors:
- Tyler Morgan-Wall

date: "2020-08-02T00:00:00Z"

title: rayshader
subtitle: Create Maps and Visualize Data in 2D and 3D
summary: Create Maps and Visualize Data in 2D and 3D

tags:
- map
- 2D
- 3D
- cartography
- elevation

publication_types: ["6"]


thematics: ["2","2"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

url_source: 'https://www.rayshader.com/'
url_code: 'https://github.com/tylermorganwall/rayshader'

links:
- name: CRAN
  url: https://cloud.r-project.org/web/packages/rayshader/index.html
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**rayshader** is an open source package for producing 2D and 3D data visualizations in R. **rayshader** uses elevation data in a base R matrix and a combination of raytracing, spherical texture mapping, overlays, and ambient occlusion to generate beautiful topographic 2D and 3D maps. In addition to maps, **rayshader** also allows the user to translate **ggplot2** objects into beautiful 3D data visualizations.

The models can be rotated and examined interactively or the camera movement can be scripted to create animations. Scenes can also be rendered using a high-quality pathtracer, **rayrender**.The user can also create a cinematic depth of field post-processing effect to direct the user’s focus to important regions in the figure. The 3D models can also be exported to a 3D-printable format with a built-in STL export function.


