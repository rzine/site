---
title: Join (spreadsheet) to spatial data
summary: A step by step interactive [learnr](https://rstudio.github.io/learnr/) tutorial which presents how to create a map from a spreadsheet of summary data by region. Also **available by using** [**afrilearnr**](https://github.com/afrimapr/afrilearnr) **package**
subtitle: learnr tutorial by Afrimapr

authors:
- Laurie Baker
- Andy South

publication_types: ["1", "13"]


sources:
- "1"

thematics: ["0","1"]


update:
- "1"

languages:
- "1"


projects:
- afrimapr

date: "2021-02-04T01:00:00Z"

tags:
- dplyr
- gapminder
- join
- africa
- afrique
- map
- carte
- cartographie
- sf
- afrilearndata
- africapitals
- afrihighway
- africountries
- africontinent
- ggplot2

url_source: 'https://andysouth.shinyapps.io/join-admin/'
url_code: 'https://github.com/afrimapr/afrilearnr'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


A step by step interactive [learnr](https://rstudio.github.io/learnr/) tutorial which presents how to create a map from a spreadsheet of summary data by region.  

You can also use this tutorial by installing [**afrilearnr**](https://github.com/afrimapr/afrilearnr) **package**.



