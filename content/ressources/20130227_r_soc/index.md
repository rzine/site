---
title: R-soc
summary: Liste de discussion francophone dédiée aux utilisateurs de R en sciences sociales

date: "2013-02-27T00:00:00Z"

tags:
- liste
- discussion
- Sciences sociales
- rsoc
- r soc

authors:
- jbarnier

publication_types: ["3"]

thematics: ["8"]

update:
- "1"

languages:
- "0"

sources:
- "0"

url_source: 'https://groupes.renater.fr/sympa/subscribe/r-soc'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

La liste **R-soc**, crée par [Julien Barnier](/authors/jbarnier/), est une liste francophone dédiée aux utilisateur·rices de R en sciences sociales. 

**Pour s'abonner à la liste R-soc** : [https://groupes.renater.fr/sympa/subscribe/r-soc](https://groupes.renater.fr/sympa/subscribe/r-soc)





