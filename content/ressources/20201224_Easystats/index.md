---
title: easystats
subtitle: Collection of R package
summary: Collection of R packages, which aims to provide a unifying and consistent framework for statistics and models

authors:
- Dominique Makowski
- Mattan S. Ben-Shachar
- Daniel Lüdecke 

publication_types: ["6"]


sources:
- "0"

thematics: ["11","12","13","14","5","10"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- report
- correlation
- model
- bayesian
- test
- parameters
- performance
- effectsize
- insight
- see


date: "2020-12-24T00:00:00Z"

url_source: 'https://easystats.github.io/easystats/'
url_code: 'https://github.com/easystats/easystats'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**easystats** is a collection of R packages, which aims to provide a unifying and consistent framework to tame, discipline and harness the scary R statistics and their pesky models. List of easystats packages:

- [report](https://github.com/easystats/report): 📜 🎉 Automated statistical reporting of objects in R
- [correlation](https://github.com/easystats/correlation): 🔗 Your all-in-one package to run correlations
- [modelbased](https://github.com/easystats/modelbased): 📈 Estimate effects, group averages and contrasts between groups based on statistical models
- [bayestestR](https://github.com/easystats/bayestestR): 👻 Great for beginners or experts of Bayesian statistics
- [parameters](https://github.com/easystats/parameters): 📊 Obtain a table containing all information about the parameters of your models
- [performance](https://github.com/easystats/performance): 💪 Models’ quality and performance metrics (R2, ICC, LOO, AIC, BF, …)
- [effectsize](https://github.com/easystats/effectsize): 🐉 Compute, convert, interpret and work with indices of effect size and standardized parameters
- [insight](https://github.com/easystats/insight): 🔮 For developers, a package to help you work with different models and packages
- [see](https://github.com/easystats/see): 🎨 The plotting companion to create beautiful results visualizations









