---
# AUTHOR(S) 
authors:
- Hadley Wickham
- Garrett Grolemund

# DATE de soumission à Rzine
date: "2017-01-07"

# TITRE
title: R for Data Science

subtitle: Import, Tidy, Transform, Visualize, and Model Data
summary: Import, Tidy, Transform, Visualize, and Model Data


# TAGS
tags:
- tidyverse
- tibbles
- ggplot2
- magrittr
- rmarkdown
- RColorBrewer 
- lubridate



# TYPE DE PUBLICATION
publication_types: ["5"]


thematics: ["0","10","11","13","1","7"]


languages:
- "1"

sources:
- "2"

update:
- "1"


projects:
- oreilly


url_source: 'https://r4ds.had.co.nz/'
url_code: 'https://github.com/hadley/r4ds/'

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
publishDate: "2017-01-07T00:00:00Z"
# Name of review or book if the publication has been published in
publication: R for Data Science. Import, Tidy, Transform, Visualize, and Model Data
publication_short:  R for Data Science
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Learn how to use R to turn raw data into insight, knowledge, and understanding. This book introduces you to R, RStudio, and the tidyverse, a collection of R packages designed to work together to make data science fast, fluent, and fun. Suitable for readers with no previous programming experience, R for Data Science is designed to get you doing data science as quickly as possible.

Authors Hadley Wickham and Garrett Grolemund guide you through the steps of importing, wrangling, exploring, and modeling your data and communicating the results. You’ll get a complete, big-picture understanding of the data science cycle, along with basic tools you need to manage the details. Each section of the book is paired with exercises to help you practice what you’ve learned along the way.

You’ll learn how to:    
- **Wrangle**—transform your datasets into a form convenient for analysis    
- **Program**—learn powerful R tools for solving data problems with greater clarity and ease    
- **Explore**—examine your data, generate hypotheses, and quickly test them     
- **Model**—provide a low-dimensional summary that captures true "signals" in your dataset    
- **Communicate**—learn R Markdown for integrating prose, code, and results     


