---
title: Cours de programmation sous R
summary: Cours est dispensé en [M2 Statistique et Informatique (SISE)](https://www.univ-lyon2.fr/master-2-informatique-statistique-et-informatique-sise)

authors:
- Ricco Rakotomalala

publication_types: ["13"]


sources:
- "0"

thematics: ["0","1"]


update:
- "1"

languages:
- "0"

projects:
- divers

tags:
- Tinn-R
- MapReduce
- classe
- tableau
- matrice
- vecteur
- objet


date: "2017-11-06T00:00:00Z"

url_source: 'http://eric.univ-lyon2.fr/~ricco/cours/cours_programmation_R.html'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Ce cours est dispensé en [M2 Statistique et Informatique (SISE)](https://www.univ-lyon2.fr/master-2-informatique-statistique-et-informatique-sise)
