---
authors:
- emauviere

title: Peaufiner ses tutoriels R avec learnr et gradethis (2/2)
summary: Comment tirer parti des dernières fonctionalités de learnr et gradethis pour optimiser les feedbacks, varier les widgets, styler et publier (iframe ou page autonome)

date: "2021-01-26"

publication_types: ["11"]


thematics: ["1"]


languages:
- "0"

update:
- "0"

sources:
- "0"


url_source: 'https://www.icem7.fr/r/peaufiner-ses-tutoriels-r-avec-learnr-et-gradethis-2-2/'
url_code: 'https://github.com/icem7-open/learnrdemos'


tags:
- learnr
- gradethis
- tutoriels
- pédagogie
- formation R

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

