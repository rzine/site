
---
# AUTHOR(S) 
authors:
- Alexis Gabadinho
- Gilbert Ritschard
- Matthias Studer
- Nicolas S. Müller



# DATE de soumission à Rzine
date: "2011-03-18"

# TITRE
title: Mining sequence data in R with the TraMineR package

# RESUME COURT - une phrase
summary: Reference user's guide of the TraMineR R-package for mining and visualizing sequences of categorical data


# TAGS
tags:
- TraMineR
- séquence
- sequence
- transition

# TYPE DE PUBLICATION
# manual = 0
# sheet = 1
# course = 2
# exercise = 3
# web = 4
# tool = 5
# article = 6
# annex = 7
# conf = 8
# uncat = 9
publication_types: ["5"]


thematics: ["11","16"]





sources :
- "0"

languages:
- "1"

update :
- "0"

# PROJET
# Les projets existants :
# Manuel CIST'R = manuel
# Fiche CIST'R = fiche
# Exercice CIST'R = exercice
# Sources diverses = divers
# Resources web (blog et site) = web
# GDR Analyse de réseau en SHS = GDR_ARSHS
# Axe Information territoriale du CIST = INFTER
projects:
- divers

##### LINKS #####
# DOCUMENTATION
url_source: 'http://mephisto.unige.ch/pub/TraMineR/doc/TraMineR-Users-Guide.pdf'



# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
publishDate: "2014-09-01T00:00:00Z"
# Name of review or book if the publication has been published in
publication: R et espace. Traitement de l’information géographique
publication_short: R et espace
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**TraMineR is a R-package for mining and visualizing sequences of categorical data**.     
Its primary aim is the knowledge discovery from event or state sequences describing life courses, although most of its features apply also to non temporal data such as text or DNA sequences for instance.     
The name TraMineR is a contraction of Life Trajectory Miner for R. Indeed, as some may suspect, it was also inspired by the authors’ taste for Gewurztraminer wine.     

This guide is essentially a tutorial that describes the features and usage of the TraMine Rpackage.    
It may also serve, however, as an introduction to sequential data analysis. The presentation is illustrated with data from the social sciences. Illustrative data sets andRscripts (sequence of R-commands) are included in the TraMineR distribution package.     
The functions and options used in the guide as well as their displayed output correspond tothe version indicated on the title page. Though the guide discusses the major functionalities of the package, it is not exhaustive. For a full list and description of available functions, see the Reference Manual of the current version that can be found on the CRAN (http://cran.r-project.org/web/packages/TraMineR/). Check also  the ‘History’ tab on the package webpage (http://mephisto.unige.ch/traminer) for the latest added features.     

For new comers to R, a short introduction to the R-environment is given in Appendix A in which the reader will learn where R can be obtained as well as its basic commands and principles. Chapter 3 shortly explains how to use the package and describes the illustrative data sets provided with it.
