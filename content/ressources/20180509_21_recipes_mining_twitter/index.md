---
title: 21 Recipes for Mining Twitter Data with rtweet
subtitle: 
summary: Manual based on Matthew R. Russell’s [book](https://github.com/ptwobrussell/Recipes-for-Mining-Twitter)

authors:
- Bob Rudis 

publication_types: ["5"]


sources :
- "0"

thematics: ["5","10","11"]


update :
- "0"

languages:
- "1"

projects:
- divers

tags:
- twitter
- API
- tweet
- topic
- retweet
- graph
- tag cloud
- follower
- harvest
- geodata
- geocoding
- visualization
- clique
- relation



date: "2018-05-09T00:00:00Z"

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://rud.is/books/21-recipes/'
url_code: 'https://github.com/hrbrmstr/21-recipes'

---



Manual based on Matthew R. Russell’s [book](https://github.com/ptwobrussell/Recipes-for-Mining-Twitter). That book is out of distribution and much of the content is in Matthew’s “Mining the Social Web” book. It's just an R-centric version of the cookbook.


