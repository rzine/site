---
title: learnr
subtitle: Turn any R Markdown document into an interactive tutorial
summary: The learnr package makes it easy to turn any R Markdown document into an interactive tutorial

authors:
- Barret Schloerke
- J. J. Allaire
- Barbara Borges

projects:
- divers

sources :
- "0"

thematics: ["1"]





update :
- "1"

publication_types: ["6"]


languages:
- "1"


date: "2019-11-12T00:00:00Z"

tags:
- rmarkdown
- tutorial
- interactive


url_source: 'https://rstudio.github.io/learnr/'
url_code: 'https://github.com/rstudio/learnr'

links:
- name : CRAN
  url: https://cran.r-project.org/web/packages/learnr/index.html

featured: true
image:
  caption: "Desirée De Leon (https://desiree.rbind.io/post/2020/learnr-iframes/)"
  focal_point: "Center"
  preview_only: false
---

The **learnr** package makes it easy to turn any R Markdown document into an **interactive tutorial**. Tutorials consist of content along with interactive components for checking and reinforcing understanding. Tutorials can include any or all of the following:

1. Narrative, figures, illustrations, and equations.
2. Code exercises (R code chunks that users can edit and execute directly).
3. Quiz questions.
4. Videos (supported services include YouTube and Vimeo).
5. Interactive Shiny components.

Tutorials automatically preserve work done within them, so if a user works on a few exercises or questions and returns to the tutorial later they can pick up right where they left off.

