---
title: The Epidemiologist R Handbook
subtitle : R for applied epidemiology and public health
summary: A bookdown for applied epidemiology and public health with R

authors:
- Neale Batra
- Alex Spina
- Paula Blomquist
- Finlay Campbell
- Henry Laurenson-Schafer
- Isaac Florence
- Natalie Fischer
- Aminata Ndiaye
- Liza Coyer
- Jonathan Polonsky
- Yurie Izawa
- Chris Bailey
- Daniel Molling
- Isha Berry
- Emma Buajitti
- Mathilde Mousset
- Sara Hollis- Wen Lin

publication_types: ["5"]


sources:
- "0"

thematics: ["10","11","12","13","15","1","19","2","5","7"]


update:
- "1"

languages:
- "1"


date: "2021-05-10T00:00:00Z"

url_source: 'https://epirhandbook.com/index.html'
url_code: 'https://github.com/appliedepi/epiRhandbook_eng'


tags:
- epidemiology
- public health
- Basics
- Data Management
- Analysis 
- Data Visualization
- Reports and dashboards
- epiRhandbook


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This handbook strives to:

- Serve as a quick R code reference manual  
- Provide task-centered examples addressing common epidemiological problems  
- Assist epidemiologists transitioning to R  
- Be accessible in settings with low internet-connectivity via an [offline version](https://epirhandbook.com/download-handbook-and-data.html#download-handbook-and-data)  


