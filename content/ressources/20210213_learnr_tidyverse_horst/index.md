---
title: A quick flight to the edge of the tidyverse
summary: Tutorial created for the February 2021 joint [R-Ladies Tunis](https://twitter.com/RLadiesTunis) and [R-Ladies Saudi Arabia (Dammam)](https://twitter.com/RLadiesDammam) workshop


authors:
- Allison Horst

publication_types: ["1", "13"]


sources:
- "0"

thematics: ["10"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- tidyverse
- R-ladies
- learnr
- dplyr
- tidyr


date: "2021-02-13T00:00:00Z"

url_source: 'https://allisonhorst.shinyapps.io/edge-of-the-tidyverse/'
url_code: 'https://github.com/allisonhorst/r-ladies-tunis-2021'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



This tutorial was created for the February 2021 joint [R-Ladies Tunis](https://twitter.com/RLadiesTunis) and [R-Ladies Saudi Arabia (Dammam)](https://twitter.com/RLadiesDammam) workshop.



