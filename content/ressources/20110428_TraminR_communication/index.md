---
# AUTHOR(S) 
authors:
- Alexis Gabadinho
- Gilbert Ritschard
- Matthias Studer
- Nicolas S. Müller

date: "2011-04-28"

title: L’analyse de séquence dans R avec la librairie TraMineR

summary: Communication réalisée lors des "Rencontres de statistique appliquée" du 28 avril 2011, Paris.

tags:
- TraMineR
- séquence
- sequence
- transition
- trajectoire

publication_types: ["12"]


thematics: ["11","16"]



languages:
- "0"

sources:
- "0"

update:
- "0"

projects:
- divers

url_source: 'http://rug.mnhn.fr/semin-r/PDF/INED-SFdS-MNHN_Gabadinho&Ritschard_280411.pdf'


publishDate: "2014-09-01T00:00:00Z"
publication: R et espace. Traitement de l’information géographique
publication_short: R et espace
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

Communication réalisée lors des *Rencontres de statistique appliquée* du 28 avril 2011, Paris.

