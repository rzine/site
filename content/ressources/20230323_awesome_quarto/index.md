---
title: Awesome Quarto
subtitle: A curated list of Quarto talks, tools, examples & articles
summary: A curated list of Quarto talks, tools, examples & articles

authors:
- Mickaël Canouil

# Type de la publication 
publication_types: ["0"]


sources :
- "0"

thematics: ["1","8"]


update :
- "1"



languages:
- "1"

projects:
- divers

# Date that the page was published
date: "2023-03-23T00:00:00Z"  

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

tags:
- package
- quarto
- list
- extension
- template
- talk
- video
- markdown
- reveal



url_source: 'https://github.com/mcanouil/awesome-quarto'
url_code: 'https://github.com/mcanouil/awesome-quarto'

---

A curated list of [**Quarto®**](https://quarto.org/) talks, tools, examples & articles. 

Contributions welcome! 

