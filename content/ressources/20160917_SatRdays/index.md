---
date: "2016-09-17T00:00:00Z"

title: SatRdays
subtitle: Conférences régionales
summary: Conférences régionales pour soutenir la collaboration, la mise en réseau et l'innovation au sein de la communauté R

tags: 
- conference
- community
- évènement
- event


publication_types: ["2"]

thematics: ["8"]

projects:
- satrdays

sources:
- "0"

update:
- "1"

languages:
- "0"

url_source: 'https://satrdays.org/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Les SatRDays sont des conférences régionales** soutenues par le [R consortium](https://www.r-consortium.org/) et organisées par la communauté pour soutenir la collaboration, la mise en réseau et l'innovation au sein de la communauté de la recherche

Plus d'informations : [https://satrdays.org/](https://satrdays.org/)


