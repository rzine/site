---
authors:
- Colin Fay

title: An Introduction to Docker for R Users

summary: A quick introduction on using Docker for reproducibility in R

date: "2019-01-06"

publication_types: ["11"] 

thematics:
- "7" 

languages:
- "1" 

update:
- "0" 

sources:
- "0"

url_source: 'https://colinfay.me/docker-r-reproducibility/'

tags:
- Docker
- Reproductibility
- Containers
- Image
- Dockerfile

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

This blog post consists in an introduction to Docker and its interest in the framework of R: you can use older versions of a package for a specific task, while still keeping the package on your machine up-to-date. This way, you can “solve” dependencies issues: if ever you are afraid dependencies will break your analysis when packages are updated, build a container that will always have the software versions you desire: be it Linux, R, or any package.

