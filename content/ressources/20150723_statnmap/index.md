---
authors:
- Sébastien Rochette

title: StatnMap
subtitle: R, modélisation et cartographie
summary: Blog principalement axé sur l'analyse spatiale et la cartographie

date: "2015-07-23"

publication_types: ["8"]


thematics: ["0","13","2"]


languages:
- "0"

update:
- "1"

sources:
- "0"


url_source: 'https://statnmap.com/fr/'



tags:
- map
- carte
- bayésiens
- bayesien
- cartographie
- geohacking
- git
- hugo
- modélisation
- package
- publication
- leaflet
- scripts
- shiny
- rmarkdown
- sdmselect
- spatial
- traduction



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Blog principalement axé sur l'analyse spatiale et la cartographie.




