---
title: Supervised machine learning case studies in R
summary: A Free, Interactive Course Using Tidy Tools

authors:
- Julia Silge

publication_types: ["1", "13"]


sources:
- "0"

thematics: ["13","3"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- case studies
- machine learning



date: "2020-10-23T00:00:00Z"

url_source: 'https://supervised-ml-course.netlify.app/'
url_code: 'https://github.com/juliasilge/supervised-ML-case-studies-course'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This course approaches supervised machine learning using:

- the [tidyverse](https://www.tidyverse.org/)
- the [tidymodels](https://www.tidymodels.org/) ecosystem

The interactive course site is built on the amazing framework created by [Ines Montani](https://ines.io/), originally built for her [spaCy course](https://course.spacy.io/en/). The front-end is powered by [Gatsby](https://www.gatsbyjs.com/) and [Reveal.js](https://revealjs.com/) and the back-end code execution uses [Binder](https://mybinder.org/). [Florencia D'Andrea](https://florencia.netlify.app/) helped port the course materials and made the fun logo.

