---
title: Data Wrangling and Management in R 
summary: This tutorial explores how scholars can organize ‘tidy’ data, understand R packages to manipulate data, and conduct basic data analysis. Lesson from [The Programming Historian](https://programminghistorian.org/)

authors:
- Nabeel Siddiqui

publication_types: ["10","13"]

sources:
- "10"

thematics: ["10"]

update:
- "1"

languages:
- "1"
- "2"


tags:
- tidyverse
- dplyr
- wrangling
- magittr
- ggplot2
- readr
- tibbles
- arrange
- summarize
- mutate
- filter
- select
- historydata
- Programming Historian
- PH
  

projects:
- programming_historian



date: "2017-07-31T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  


doi: '10.46430/phen0063'
url_source: 'https://programminghistorian.org/en/lessons/data_wrangling_and_management_in_R'

links:
- name: Versión en español
  url: https://programminghistorian.org/es/lecciones/administracion-de-datos-en-r



---

This tutorial explores how scholars can organize ‘tidy’ data, understand R packages to manipulate data, and conduct basic data analysis. Lesson from [The Programming Historian](https://programminghistorian.org/).



