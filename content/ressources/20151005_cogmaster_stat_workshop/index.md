---
title: Cogmaster-stats
subtitle: Stats workshop in cognitive science
summary: Companion website for a stats workshop in cognitive science ([M2 cogmaster](https://cogmaster.ens.psl.eu/en))


authors:
- clalanne

publication_types: ["13"]


sources :
- "0"

thematics: ["12","13"]


update :
- "0"

languages:
- "1"

projects:
- divers

date: "2015-10-05T00:00:00Z"


tags: 
- string
- stringr
- character
- text
- number
- regular expression
- regex
- metacharacter
- literal character
- anchor
- quantifier
- reversing


  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://cogmaster-stats.github.io/site/'
url_code: 'https://github.com/cogmaster-stats/r-cogstats'

---


Companion website for a stats workshop in cognitive science ([M2 cogmaster](https://cogmaster.ens.psl.eu/en)).








