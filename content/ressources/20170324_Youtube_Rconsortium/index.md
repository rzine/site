---
title: Chaîne YouTube du R Consortium
summary: Vidéos des communications réalisées aux conférences userR!

authors:
- R consortium

publication_types: ["9"]


thematics: ["8"]





sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers
date: "2017-03-24T00:00:00Z"
doi: ""

tags:
- useR!
- conférence
- consortium



url_source: 'https://www.youtube.com/channel/UC_R5smHVXRYGhZYDJsnXTwg/featured'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


Chaîne Youtube du [R consortium](https://www.r-consortium.org/), qui met à disposition de nombreuses vidéos de communications réalisées dans le cadre des conférences internationales useR!.

