---
title: Pimp my RMD
subtitle: A few tips for R Markdown 
summary: This post provides a few tips I use on a daily basis to improve the appearance of output documents

authors:
- yholtz

publication_types: ["11"]


thematics: ["1"]


projects:
- divers

sources:
- "0"

update:
- "0"

languages:
- "1"

date: "2018-12-10T00:00:00Z"
doi: ""

tags:
- markdown
- footer
- header
- figure
- column
- chapter
- space
- DT
- table
- hide
- cache
- template
- website
- hyperlink


url_source: 'https://holtzy.github.io/Pimp-my-rmd/'
url_code: 'https://github.com/holtzy/Pimp-my-rmd'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


[R markdown](https://rmarkdown.rstudio.com/) creates interactive reports from R code. This post provides a few tips I use on a daily basis to improve the appearance of output documents. In any case, an unavoidable resource is the [Rstudio documentation](https://bookdown.org/yihui/rmarkdown/html-document.html).

