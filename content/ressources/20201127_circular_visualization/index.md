---

title: Circular visualization in R
subtitle: Documentation of the circlize package 
summary: This is the documentation of the circlize package

authors:
- Zuguang Gu

date: "2020-11-27"

publication_types: ["5"]


thematics: ["11"]


languages:
- "1"

update:
- "1"

sources:
- "0"


url_source: 'https://jokergoo.github.io/circlize_book/book/'
url_code: 'https://github.com/jokergoo/circlize_book'
doi: '10.1093/bioinformatics/btu393'

tags:
- circlize
- circular
- visualization
- circle
- circos


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Circular layout is very useful to represent complicated information. First, it elegantly represents information with long axes or a large amount of categories; second, it intuitively shows data with multiple tracks focusing on the same object; third, it easily demonstrates relations between elements. It provides an efficient way to arrange information on the circle and it is beautiful.

[Circos](http://circos.ca/) is a pioneer tool widely used for circular layout representations implemented in Perl. It greatly enhances the visualization of scientific results (especially in Genomics field). Thus, plots with circular layout are normally named as “**Circos plot**”. Here the **circlize** package aims to implement **Circos** in R. One important advantage for the implementation in R is that R is an ideal environment which provides seamless connection between data analysis and data visualization. **circlize** is not a front-end wrapper to generate configuration files for **Circos**, while completely coded in R style by using R’s elegant statistical and graphic engine. We aim to keep the flexibility and configurability of **Circos**, but also make the package more straightforward to use and enhance it to support more types of graphics.

In this book, chapters in Part I give detailed overviews of the general **circlize** functionalities. Part II introduces functions specifically designed for visualizing genomic datasets. Part III gives comprehensive guilds on visualizing relationships by Chord diagram.
