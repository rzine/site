---
title: Petit Manuel de S4
subtitle: Programmation Orienté Objet sous R
summary: Guide à la programmation objet sous R (ou S4).


authors:
- Christophe Genolini

projects:
- divers

sources :
- "0"

thematics: ["5","7"]





update :
- "0"

languages:
- "0"



publication_types: ["5"]



date: "2009-09-09T00:00:00Z"
doi: ""

tags:
- S4
- objet
- attribut
- méthodes
- classes


url_source: 'https://cran.r-project.org/doc/contrib/Genolini-PetitManuelDeS4.pdf'


featured: true 
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

*Petit Manuel de S4* est un **guide à la programmation orienté objet sous R** (ou S4).   
Ce tutorial ne demande pas de connaissance en objet. **Il requiert cependant des connaissances générales sur R**.


