---
# the title of your publication
title: Sur la détection de communautés en général et avec R en particulier
subtitle:  Détection de communautés avec le package igraph
summary: Détection de communautés avec le package igraph

authors:
- lbeauguitte

projects:
- GDR_ARSHS

sources:
- "0"

thematics: ["18"]


languages:
- "0"

update:
- "0"

publication_types: ["11"]


date: "2019-04-10T00:00:00Z"
doi: ""

# Tagging your content helps users to discover similar content on your site. 
# Tags can improve search relevancy and are displayed after the page content and also in the Tag Cloud widget.
tags:
- igraph
- communauté
- algorithmes

url_source: 'https://arshs.hypotheses.org/1314'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Détection de communautés avec le package igraph**, présentation des différents algorithmes et visualisation des résultats avec le jeu de données du Zachary karate club.




