---
authors:
- jhguay

date: "2018-07-22"

title: Dimension - Statistiques & sociétés
subtitle: Site pédagogique pour les usagers francophones de R
summary: Site pédagogique proposé par l'Université de Sherbrooke (Canada), à partir du livre *Statistiques en Sciences humaines avec R* de Jean-Herman Guay (2014).


tags:
- shiny
- App
- UI
- modules
- safety
- performance



publication_types: ["8","13","5"]


thematics: ["0","10","11","12","13","15","17"]


languages:
- "0"


sources:
- "0"

update:
- "1"


projects:
- divers


url_source: 'https://dimension.usherbrooke.ca/dimension/v2ssrcadre.html'


# publishDate: "2014-10-28T00:00:00Z"
# publication: Advanced R
# publication_short:  Advanced R
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Dans ce site, on retrouve les codes du livre *Statistiques en sciences humaines avec R* de Jean-Herman Guay (Presses de l'Université Laval, Québec, 2012 et 2014; Éditions de Boeck, Bruxelles, 2014) et une série de pages utiles pour ceux et celles qui veulent aller plus loin.

Les codes récapitulatifs (CR) sont transposables dans R (ou RStudio) par un simple copier/coller. Normalement, le résultat devrait être identique à ce qu'on retrouve dans le livre. Le code est récapitulatif dans la mesure où il reprend chaque fois toutes les étapes : 1) importation de la base de données; 2) transformation des variables et 3) production de tableaux ou graphiques. En ce sens, chaque CR est autonome. Dit autrement, ils sont autant de petites «recettes» que vous pourrez personnaliser en fonction de vos données. 

Il importe d'avoir la version la plus récente de R pour que les packages fonctionnent correctement.

On trouve aussi ici une série d'utilitaires qui peuvent être pertinents pour se retrouver dans la démarche de recherche et surtout pour la découverte de R.



