---
title: Reproducible science workshop
subtitle: A one-day workshop with R and RStudio
summary: One-day workshop with R and RStudio

date: "2021-03-18T00:00:00Z"

authors:
- ogimenez

publication_types: ["13","9"]


sources:
- "0"

thematics: ["10","11","7"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- tidyverse
- rmarkdown
- markdown
- git
- github


url_source: 'https://oliviergimenez.github.io/reproducible-science-workshop/'
url_code: 'https://github.com/oliviergimenez/reproducible-science-workshop'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


##### Timeline

- Motivations ([lecture](https://oliviergimenez.github.io/reproducible-science-workshop/slides/0_introduction.html#1) | [video](https://oliviergimenez.github.io/reproducible-science-workshop/slides/0_introduction.html#1))
- Manipulating data in the tidyverse ([lecture](https://oliviergimenez.github.io/reproducible-science-workshop/slides/3_dplyr.html#1) | [practical](https://oliviergimenez.github.io/reproducible-science-workshop/practicals/1_datamanipulation.html#1) | [solutions](https://oliviergimenez.github.io/reproducible-science-workshop/practicals/1_datamanipulation_solution.html#1) | [video](https://www.youtube.com/watch?t=752&v=xfMbzUb5iKk&feature=youtu.be))
- Visualising data in the tidyverse ([lecture](https://oliviergimenez.github.io/reproducible-science-workshop/slides/4_ggplot.html#1) | [practical](https://oliviergimenez.github.io/reproducible-science-workshop/practicals/2_datavisualisation.html#1) | [solutions](https://oliviergimenez.github.io/reproducible-science-workshop/practicals/2_datavisualisation_solution.html#1) | [video](https://www.youtube.com/watch?t=7381&v=xfMbzUb5iKk&feature=youtu.be))
- Lunch break
- Writing dynamic and reproducible documents with R Markdown ([lecture](https://oliviergimenez.github.io/reproducible-science-workshop/slides/2_markdown.html#1) | [practical](https://oliviergimenez.github.io/reproducible-science-workshop/practicals/3_rmarkdown.html#1) | [solutions](https://oliviergimenez.github.io/reproducible-science-workshop/practicals/3_rmarkdownsolution.Rmd) | [video](https://www.youtube.com/watch?t=10&v=nnwxpD9TMQw&feature=youtu.be))
- Versioning with Git and GitHub in RStudio ([lecture](https://oliviergimenez.github.io/reproducible-science-workshop/slides/1_github.html#1) | [practical](https://oliviergimenez.github.io/reproducible-science-workshop/practicals/4_versioncontrol.html#1) | [video](https://www.youtube.com/watch?v=nnwxpD9TMQw&t=5033s))
- Take-home messages ([lecture](https://oliviergimenez.github.io/reproducible-science-workshop/slides/5_conclusion.html#1) | [video](https://www.youtube.com/watch?v=nnwxpD9TMQw&t=10355s))







