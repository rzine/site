---
title: Chaîne YouTube de Lise Vaudor
subtitle: Tutoriels vidéos d'initiation à R
summary: Tutoriels vidéos francophones d'initiation à R

authors:
- lvaudor

publication_types: ["9"]


thematics: ["0"]


languages:
- "0"

sources:
- "0"

update:
- "1"

projects:
- divers
date: "2020-04-23T00:00:00Z"
doi: ""

tags:
- introduction
- initiation
- objet
- opérateur
- fonction


url_source: 'https://www.youtube.com/channel/UC9QOCBVGHp4gnmcmkG1VbGg'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


Chaîne Youtube de Lise Vaudor, auteure du blog [**R-atique**](http://perso.ens-lyon.fr/lise.vaudor/).

