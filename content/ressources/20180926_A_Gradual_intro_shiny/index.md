---
title: A gRadual intRoduction to Shiny
subtitle: Materials for a Shiny Workshop
summary: Materials for a Shiny Workshop

authors:
- Ted Laderas
- Jessica Minnier

publication_types: ["13","9"]


thematics: ["5"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

date: "2018-09-26T00:00:00Z"


tags:
- shiny
- exercise
- reactives
- connecting
- ui
- serve
- deploy


url_source: 'https://laderast.github.io/gradual_shiny/'
url_code: 'https://github.com/laderast/gradual_shiny'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---
