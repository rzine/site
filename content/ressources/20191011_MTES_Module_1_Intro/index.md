---
title: Découvrir R et RStudio

subtitle : ParcoursR (module 1) - « Premier programme en R »

summary: Module 1 « Premier programme en R ». Formations R aux MTES & MCTRCT.


authors:
- Thierry Zorn
- mlethrosne
- Vivien Roussez
- Pascal Irz

# Type de la publication 
publication_types: ["5","13"]


thematics: ["0"]


languages:
- "0"

sources:
- "3"

update:
- "1"

# Categorizing your content helps users to discover similar content on your site. Categories can improve search relevancy and display at the top of a page alongside a page’s metadata
  
# Project name associated ?
# Set the name if several publications can be grouped in one projet
projects:
- parcoursr

# Date that the page was published
date: "2019-10-11T00:00:00Z"
doi: ""

 
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

tags:
- projet
- objet
- variable
- fonction
- boucle 
- condition
- R
- Rstudio
- import
- filtre
- séléction
- table
- ggplot2
- traitement
- représentation
- formation
- module
- ministère
- MTES
- MCTRCT
- G2R


url_source: 'https://mtes-mct.github.io/parcours_r_socle_introduction/'
url_code: 'https://github.com/MTES-MCT/parcours_r_socle_introduction'




---

Ce support vous permettra de :     
- **Découvrir le fonctionnement de R**   
- **Aborder la dimension modulaire** du logiciel     
- **S’approprier l’interface graphique** du logiciel       
- **Être en capacité d’importer dans R un fichier CSV et de réaliser des calculs statistiques simples** (somme, moyenne, table des fréquences)    


Ce manuel est proposé par les **Ministères de la transition écologique et solidaire** ([MTES](https://www.ecologique-solidaire.gouv.fr/)), et **de la Cohésion des territoires et des Relations avec les collectivités territoriales**([MCTRCT](http://www.cohesion-territoires.gouv.fr/)).

Ce dispositif inédit de formation vise à faire monter en compétence les agents dans le domaine de la science de la donnée, avec le logiciel R. Avec R, le champ des possibles est tel qu’il existe autant d’usages que d’utilisateurs. Le parcours est composé de plusieurs modules à mettre en oeuvre en fonction des attentes des stagiaires. Les deux premiers sont nécessaires pour prendre en main l’outil. Les autres modules couvrent une ou plusieurs fonctionnalités de R, que les stagiaires pourront découvrir en fonction de leurs attentes. Chacun peut ainsi se former « à la carte » en fonction de ses usages professionnels et des domaines dans lesquels la montée en compétence est nécessaire.

Chaque module comprend, en introduction, l’acquisition ou le rappel des notions statistiques abordées. Suit la mise en œuvre des méthodes avec R, et enfin les clés d’interprétation des sorties statistiques et graphiques.

*Ces différents supports on été produits par un groupe de référents sur le logiciel opensource R (nom de code : G2R), qui a été mis en place avec des utilisateurs avancés de tout horizon (administration centrale, services déconcentrés et établissements publics) et de profils très varisés (géomaticiens, statisticiens et spécialistes de diverses thématiques mobilisant la donnée). Grâce à la richesse de leur parcours, chaque référent apporte son expertise dans la construction de ces formations.*



