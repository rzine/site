--- 
authors:
- Scott Chamberlain
- msalmon

date: "2020-12-18"

title: HTTP testing in R
subtitle: Project funded by rOpenSci & the R Consortium
summary: Central reference for developers of R packages accessing web resources, to help them have a faster and more robust development


publication_types: ["5"]


thematics: ["6","7"]


languages:
- "1"

sources :
- "5"

update :
- "1"

projects:
- rOpenSci
- Rconsortium

url_source: 'https://books.ropensci.org/http-testing/index.html'
url_code: 'https://github.com/ropensci-books/http-testing'
url_pdf: 'https://books.ropensci.org/http-testing/main.pdf'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Project funded by **rOpenSci** (Scott Chamberlain’s work) & the **R Consortium** (Maëlle Salmon’s work). 

This book is meant to be a free, central reference for developers of R packages accessing web resources, to help them have a faster and more robust development. Our aim is to develop an useful guidance to go with the great recent tools that vcr, webmockr, httptest and webfakes are.





