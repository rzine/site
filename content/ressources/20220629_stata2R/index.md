---
authors:
- Noah Greifer
- Kyle F Butts
- Grant Mac Dermott
- Nick C. Huntington-Klein

title: Stata2R

subtitle: Translating Stata to R

summary: Website for Stata users who are interested in learning R

date: "2022-06-29"

publication_types: ["8"] 

thematics:
- "0"

languages:
- "1"

sources:
- "0"

update:
- "1"

url_source: 'https://stata2r.github.io/' 
url_code: 'https://github.com/stata2r/stata2r.github.io' 

tags:
- Stata
- Dictionary
- Side-by-side code snippets
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This website is for Stata users who are interested in learning R. But it could also be useful for those going the other way around. It provides side-by-side code snippets for common tasks in both Stata and R, so that users have a dictionary for navigating across the two languages.
