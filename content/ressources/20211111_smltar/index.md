---
# Author(s)
authors:
- Emil Hvitfeldt
- Julia Silge


# Date
date: "2021-11-07"


# Title
title: Supervised Machine Learning for Text Analysis in R

# Short summary
summary: This book focuses on supervised or predictive modeling for text, using text data to make predictions about the world around us

# Tags
tags:
- language
- tokenisation
- stopwords
- stop words
- stemming
- word embeddings
- machine learning
- regression
- classification
- deep learning
- LSTM
- neural network
- tidyverse
- tidytext
- SnowballC
- modeling


publication_types: ["5"]

sources:
- "0"

thematics: ["17", "3", "13"]

update:
- "1"

languages:
- "1"


# Link
url_source: 'https://smltar.com/'
url_code: 'https://github.com/EmilHvitfeldt/smltar'



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

Modeling as a statistical practice can encompass a wide variety of activities. This book focuses on supervised or predictive modeling for text, using text data to make predictions about the world around us. We use the [tidymodels](https://www.tidymodels.org/) framework for modeling, a consistent and flexible collection of R packages developed to encourage good statistical practice.


