---
# AUTHOR(S) 
authors:
- Colin Gillespie
- rlovelace


# DATE de soumission à Rzine
date: "2016-11-30"

# TITRE
title: Efficient R Programming

subtitle: A Practical Guide to Smarter Programming
summary: A Practical Guide to Smarter Programming


# TAGS
tags:
- test


# TYPE DE PUBLICATION
publication_types: ["5"]


thematics: ["7"]


languages:
- "1"

sources:
- "2"

update:
- "1"


projects:
- oreilly

url_source: 'https://csgillespie.github.io/efficientR/'
url_code: 'https://github.com/csgillespie/efficientR'

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
publishDate: "2016-11-30T00:00:00Z"
# Name of review or book if the publication has been published in
publication: Efficient R Programming. A Practical Guide to Smarter Programming
publication_short:  Efficient R Programming
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

There are many excellent R resources for visualization, data science, and package development. Hundreds of scattered vignettes, web pages, and forums explain how to use R in particular domains. But little has been written on how to simply make R work effectively--until now. This hands-on book teaches novices and experienced R users how to write efficient R code.

Drawing on years of experience teaching R courses, authors Colin Gillespie and Robin Lovelace provide practical advice on a range of topics--from optimizing the set-up of RStudio to leveraging C++--that make this book a useful addition to any R user's bookshelf. Academics, business users, and programmers from a wide range of backgrounds stand to benefit from the guidance in Efficient R Programming.

- Get advice for setting up an R programming environment    
- Explore general programming concepts and R coding techniques    
- Understand the ingredients of an efficient R workflow    
- Learn how to efficiently read and write data in R   
- Dive into data carpentry--the vital skill for cleaning raw data   
- Optimize your code with profiling, standard tricks, and other methods   
- Determine your hardware capabilities for handling R computation   
- Maximize the benefits of collaborative R programming    
- Accelerate your transition from R hacker to R programmer    
