---
title: Temporal Network Analysis with R 
summary: Learn how to use R to analyze networks that change over time. Lesson from [The Programming Historian](https://programminghistorian.org/)

authors:
- Alex Brey

publication_types: ["10","13"]

sources:
- "10"

thematics: ["18"]


update:
- "1"

languages:
- "1"
- "2"

tags:
- sna
- tsna
- ndtv
- egde
- node
- visualization
- centrality
- animate
- Programming Historian
- PH
  
projects:
- programming_historian



date: "2018-11-04T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  



doi: '10.46430/phen0080'
url_source: 'https://programminghistorian.org/en/lessons/temporal-network-analysis-with-r'

links:
- name: Versión en español
  url: https://programminghistorian.org/es/lecciones/analisis-temporal-red

---

Learn how to use R to analyze networks that change over time. Lesson from [The Programming Historian](https://programminghistorian.org/).


