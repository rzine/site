---
title: All things R Spatial
subtitle: An introduction to spatial R for ArcGIS users
summary: Work in progress - An introduction to spatial R for ArcGIS users

date: "2021-03-02T00:00:00Z"

authors:
- Nils Ratnaweera
- Nikolaos Bakogiannis

publication_types: ["5"]


sources:
- "0"

thematics: ["2","2"]


update:
- "1"

languages:
- "1"


projects:
- divers

tags:
- arcgis
- arc2r
- sf
- raster
- terra
- toolbox
- spatial statistics
- 3D analyst



url_source: 'https://arc2r.github.io/book/'
url_code: 'https://github.com/arc2r/book'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This is a resource for people wanting to learn spatial-R (Analysis, Visualization) in R and are coming with a background in ArcGIS. This resource should provide some help on this journey. To make use of this ressource, we assume two things: (1) that you have some prior experience with `R` and (2) that you have experience with Geodata and GIS-Concepts.

This book leans heavily on how the Toolboxes and Toolsets are organized in ArcGIS Pro. The idea is that you can look up specific things of which you only know the ArcGIS terminology for. This book is a work in progress, and we highly appreciate contributions to this book: If you have anything to add, please [file an issue or make a pull request](https://github.com/arc2r/book).

We are big fans of using `magrittr`’s pipe function `%>%` and heavily use it throughout this book. In addition for vector data, we use packages from the `tidyverse` (especially from the `dplyr` package: `filter()`, `mutate()` and `summarise()`).


