---
title: Spatially constrained clustering and regionalization
summary: Implementation of the SKATER algorithm

authors:
- Dmitry Shkolnik

publication_types: ["11"]


sources:
- "0"

thematics: ["19"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- spatial
- cancensus
- clustering
- cluster
- constrain
- SKATER
- spdep


date: "2021-02-08T00:00:00Z"

url_source: 'https://www.dshkol.com/post/spatially-constrained-clustering-and-regionalization/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Clustering data is well-covered terrain, and many methods also apply to spatial data. The advantage of spatially constrained methods is that it has a hard requirement that spatial objects in the same cluster are also geographically linked. This provides a lot of upside in cases where there is a real-life application that requires separating geographies into discrete regions (regionalization) such as designing communities, planning areas, amenity zones, logistical units, or even for the purpose of setting up experiments with real world geographic constraints. There are many applications and many situations where the optimal clustering, if solely using traditional cluster evaluation measures, is sub-optimal in practice because of real-world constraints.

In practice, unconstrained clustering approaches on data with spatial characteristics will often still have a regionalization element because real-world data tends to have spatial patterns and autocorrelation but if we want to ensure that all objects are in entirely spatially-contiguous groups we can turn to algorithms specifically designed to the task. There are several used in the geo data science community, but I have a personal preference for the ***skater*** algorithm available in R via the `spdep` package because it is relatively well-implemented and well-documented. This post breaks down how the algorithm works in an illustrated step-by-step example using real-world census data.

