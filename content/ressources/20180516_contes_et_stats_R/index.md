---
# the title of your publication
title: Le Grimoire
subtitle: Contes et stats R

# an optional subtitle that will be displayed under the title
# sub-title : 

# a one-sentence summary of the content on your page. The summary can be shown on the homepage and can also benefit your search engine ranking.
summary: Pratique de l'analyse statistique univariée et bivariée (notions de probabilités/statistiques et introduction aux modèles statistiques)

# Display the authors of the publication and link to their user profiles (if they exist).
authors:
- lvaudor

# Type de la publication 
# 0 = Manuel; 1 = Fiche technique; 2 = Fiche méthodologique; 3 = Fiche thématique;
# 4 = Cours et formation ; 5 = Exercice et atelier; 6 = Site Web et blog ; 7 =  Communication; 
# 8 = Vidéo; 9 = Annexe et script; 10 = Video
publication_types: ["5"]


sources:
- "0"

thematics: ["12","13"]



languages:
- "0"


update:
- "0"



# Categorizing your content helps users to discover similar content on your site. Categories can improve search relevancy and display at the top of a page alongside a page’s metadata
  
# Project name associated ?
# Set the name if several publications can be grouped in one projet
projects:
- divers

# Date that the page was published
date: "2018-05-16T00:00:00Z"
doi: ""

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
# Name of review or book if the publication has been published in
# publication:
# publication_short:  


# by setting featured: true, a page can be displayed in the Featured widget. This is useful for sticky, announcement blog posts or selected publications etc. J'ai pas bien compris cet paramètre pour l'instant...
featured: true

# To display a featured image to represent your publication 
# To use, place an image named `featured.jpg/png` in your page's folder.
# set `placement` options to `1`  
# set `focal_point` options to  `Center`
# Set `preview_only` to `true` to just use the image for thumbnails. if not : `false`
# Set `caption` to precise the pitcure credit (with url), if necessary 
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

# Tagging your content helps users to discover similar content on your site. 
# Tags can improve search relevancy and are displayed after the page content and also in the Tag Cloud widget.
tags:
- statistique
- statistic
- univariée
- bivariée
- corrélation
- test
- chi
- tidyverse
- dplyr
- purrr
- ggplot2
- janitor
- infer
- loi normale
- distribution
- ecart-type
- intervalle
- quantitative
- qualitative
- explicative
- catégorielle
- coefficient
- contingence
- t-test
- test U
- Mann-Whitney
- p-value
- Modèle
- modélisation
- régréssion
- ANOVA
- ANCOVA
- résidus
- R2
- multiple
- linéaire
- facteur
- binomiale
- MASS

# The url_ links can either point to local or web content. 
# Associated local publication content, may be copied to the publication’s folder and referenced like url_code = "code.zip".


# url_dataset: 'http://www.gis-cist.fr/cours/Calcul_repartition_theorique_ss_hypoth%c3%a8se_dindependance.xlsx'

#url_pdf: http://www.gis-cist.fr/wp-content/uploads/2018/06/cist2018-proceedings.pdf
#url_poster: '#'
#url_project: ""
#url_slides: ""
url_source: 'http://perso.ens-lyon.fr/lise.vaudor/grimoireStat/_book/intro.html'
url_code: 'https://github.com/lvaudor/grimoireStat'
#url_video: '#'

# url of GIT publication (.html)


  
# if the is an associated slide to this publication which is upload on the website
# Set the name of the folder where the md of the slide is saved.
# slides: example  

---


R a d’abord été conçu comme logiciel de statistiques. Sa richesse, en terme de modèles stats, est indéniable. Cependant, malgré (ou peut-être à cause de) cette richesse et de l’hétérogénéité des packages dédiés aux analyses statistiques, il peut être assez compliqué de s’y retrouver, d’un point de vue purement pratique, dans les lignes de commande permettant d’arriver à ses fins… En effet, pour produire l’ensemble d’une analyse, on aura la plupart du temps de combiner des commandes de plusieurs types (certaines pour produire un graphique, d’autres pour ajuster un modèle, etc.)

Ce livre peut ainsi servir d’**aide-mémoire** pour retrouver plus facilement les lignes de commande permettant de réaliser les différentes étapes des analyses les plus “classiques”.

Au-delà de cet aspect plus logistique, les statistiques sont un sujet épineux qu’il peut être compliqué de s’approprier si l’on a pas le “parcours académique” adéquat… Or nombreuses sont les personnes qui ont besoin de comprendre et d’appliquer des modèles statistiques, sans pour autant avoir eu une formation de base à cette discipline (ou même sans avoir les notions de probabilités mises en jeu). Or, sans cette formation de base, il est délicat d’appliquer un modèle, de le comprendre et d’avoir le recul nécessaire pour interpréter les résultats de ce modèle…

Ce livre vise ainsi à **introduire les modèles statistiques et leurs applications** en reprenant et en explicitant les **notions de probabilités et statistiques** sous-jacentes de la manière la plus **didactique** et **accessible** possible.


