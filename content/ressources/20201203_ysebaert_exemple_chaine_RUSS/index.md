---
title: Vers une chaine de traitement reproductible
subtitle: Exemple appliqué d'une chaîne de traitement complète et reproductible

summary: Séminaire RUSS - Exemple appliqué d'une chaîne de traitement complète et reproductible


authors:
- rysebaert

publication_types: ["12","13"]


thematics: ["0","7"]


projects:
- RUSS

sources:
- "7"

update:
- "0"

languages:
- "0"

date: "2020-12-01T09:00:00Z"
doi: ""

tags:
- exemple
- reproductible
- projet
- markdown
- chaîne de traitement
- manipulation
- analyse
- représentation
- cartography
- partage


url_source: 'https://rysebaert.gitpages.huma-num.fr/intro_r_use_case/#/'
url_code: 'https://gitlab.huma-num.fr/rysebaert/intro_r_use_case'


links:
- name: Video
  url: https://www.canal-u.tv/video/ined/introduction_a_r_partie_3_exemple_d_application_vers_une_chaine_de_traitement_reproductible.62651



# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Ce diaporama propose un exemple appliqué de chaîne de traitement complète et reproductible. De l'import des données... jusqu'à la diffusion et au partage de son travail**.  

Il s'agit d'un support utilisé pour la séance [RUSS](https://russ.site.ined.fr/) du 4 décembre 2020 : [*Introduction à R pour la mise en œuvre de chaînes de traitements reproductibles*](https://russ.site.ined.fr/fr/seances-passees/04-12-20) réalisée par Hugues Pecout (CNRS, FR CIST) et Ronan Ysebaert (Université de Paris, UMS RIATE).

Retrouvez l'ensemble des supports utilisés durant la séance à ce [lien](https://huguespecout.github.io/RUSS_seance_introduction/)




