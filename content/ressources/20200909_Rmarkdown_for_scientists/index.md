---
title: RMarkdown for Scientists
subtitle: 
summary: This is a book on rmarkdown, aimed for scientists. It was initially developed as a 3 hour workshop, but is now developed into a resource that will grow and change over time as a living book

authors:
- Nicholas Tierney

publication_types: ["5"]


thematics: ["1"]


projects:
- divers

sources:
- "0"

update:
- "1"

languages:
- "1"

date: "2020-09-09T00:00:00Z"
doi: ""

tags:
- markdown
- Rstudio
- workflow
- hmtl
- pdf
- word
- figure
- citation
- bibtex



url_source: 'https://rmd4sci.njtierney.com/'
url_code: 'https://github.com/njtierney/rmd4sci'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This is a book on rmarkdown, aimed for scientists. It was initially developed as a 3 hour workshop, but is now developed into a resource that will grow and change over time as a living book


