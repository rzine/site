---
title: R Charts (& R Coder)
subtitle : Tutorials and examples to learn R and R graphics
summary: In this site you will find tutorials and code examples to start learning R and R graphs made with base R graphics, ggplot2...


authors:
- José Carlos Soage

publication_types: ["8"]


sources:
- "0"

thematics: ["0","10","11"]


update:
- "1"

languages:
- "1"
- "2"


date: "2021-04-25T00:00:00Z"

url_source: 'https://r-charts.com/'
url_code: 'https://github.com/R-CoderDotCom/'

links:
- name: Consultar en español
  url: https://r-charts.com/es/



tags:
- R coder
- graphics
- ggplot2
- Tutorial
- base R
- colors
- gráfico
- colores


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

In this site you will find code examples of R graphs made with base R graphics, ggplot2 and other packages. Feel free to contribute suggesting new visualizations or fixing any bug via GitHub.

This web site has been created without any financial support by José Carlos Soage. It was generated with [blogdown](https://cran.r-project.org/web/packages/blogdown/), an R package which allows creating static web sites using the [HUGO](https://gohugo.io/) generator. The theme of this site is a custom theme inspired by Kross and Academic themes.
The classification of the charts is based on the [Financial Times visual vocabulary](https://raw.githubusercontent.com/ft-interactive/chart-doctor/master/visual-vocabulary/poster.png).



**Español :** Tutoriales y ejemplos para aprender R y gráficos R

En este sitio encontrarás Tutoriales y ejemplos de gráficos de R con su código hechos con el paquete graphics de R base, ggplot2...

Esta página web ha sido creada, sin ningún tipo de financiación hasta la fecha, por José Carlos Soage. Ha sido desarollada con [blogdown](https://cran.r-project.org/web/packages/blogdown/), un paquete de R que permite crear páginas webs estáticas gracias al generador [HUGO](https://gohugo.io/). El tema de esta web es un tema propio inspirado en los temas Kross y Academic.
La clasificación de los gráficos se basa en el [Financial Times visual vocabulary](https://raw.githubusercontent.com/ft-interactive/chart-doctor/master/visual-vocabulary/poster.png). 




