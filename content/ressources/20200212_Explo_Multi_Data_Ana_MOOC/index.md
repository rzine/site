---
title: Exploratory Multivariate Data Analysis
subtitle: 
summary: Complete course (MOOC) on Exploratory Multivariate Data Analysis

authors:
- fhusson

publication_types: ["13","9"]


thematics: ["12"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

date: "2020-02-12T00:00:00Z"


tags:
- PCA
- Principal Component Analysis
- CA
- Correspondence Analysis
- MCA
- Muliple Correspondence Analysis
- Clustering
- MFA
- Multiple Factor Analysis


url_source: 'https://husson.github.io/MOOC.html#AnaDoGB'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


This course is a complete course on Exploratory Multivariate Data Analysis. You will find courses on the following method:

- PCA - Principal Component Analysis
- CA - Correspondence Analysis
- MCA - Muliple Correspondence Analysis
- Clustering
- MFA - Multiple Factor Analysis




