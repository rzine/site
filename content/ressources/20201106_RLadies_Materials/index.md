---

title: R-Ladies Meetups Materials
summary: Shiny app that lists R-Ladies websites, GithUb repositories and material used during R-Ladies meetups

authors:
- Yanina Bellini Saibene

date: "2020-11-06"

publication_types: ["0","1"]


thematics: ["8"]


languages:
- "1"

update:
- "0"

sources:
- "0"


url_source: 'https://yabellini.shinyapps.io/RLadiesLesson/'
url_code: 'https://github.com/rladies/IWD/tree/master/RLadiesLesson'


tags:
- R Ladies
- Rladies
- meetup
- shiny app
- lesson



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This Shiny App was generated for the celebration of International Women's Day 2020. The Twitter campaign can be followed in the [@rladies_iwd](https://twitter.com/rladies_iwd) account. If you missed a tweet, here is all the material we share during the 8M.

