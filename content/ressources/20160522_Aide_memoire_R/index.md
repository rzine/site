---
authors:
- Aymeric Duclert

date: "2016-05-22"


title: Aide mémoire R
subtitle: Site dédié aux statistiques et la représentation graphique avec R
summary: Site dédié aux statistiques et la représentation graphique avec R


tags:
- Aide
- statistique
- graphique


publication_types: ["8"]


thematics: ["0","10","11","12","13","3"]


languages:
- "0"


sources:
- "0"

update:
- "0"


projects:
- divers


url_source: 'http://duclert.org/'

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

#### Introduction

Ce site est dédié à la programmation sous R. le logiciel R est un logiciel orienté vers les statistiques et la représentation graphique des données. R présente les avantages suivants :

- R comporte un véritable langage de programmation de haut niveau orienté vers la manipulation de données vectorielles.    
- R comporte en standard de très nombreuses fonctionnalités de calcul statistique ainsi que de représentation graphique.      
- R est open source.    
- R comporte un nombre très important de librairies additionnelles également open-source (voir notamment le CRAN dans les liens utiles).    
- R est disponible sous les principales plate-formes : linux, Windows et MacOS.    

Ce site n'a pas la prétention de présenter R de manière exhaustive, ni même d'être un tutoriel sur R à l'usage du débutant, mais plutôt de constituer un aide mémoire rapide (y compris pour l'auteur de ce site :-) ! ) permettant de comprendre et d'utiliser rapidement quelques unes des fonctionnalités de R.
Ce site comporte actuellement 88 pages.
Essayez le moteur de recherche, il est précis et permet de faire des recherches booléennes :

- La recherche est insensible à la casse et on peut utiliser des caractères sans accent.    
- Utiliser * en fin de mot (seulement en fin de mot) comme caractère joker : matche une suite quelconque de caractères.    
- On peut utiliser les booléens : and, or, et not et on peut utiliser des parenthèses.
 exemple : (frame and aggr*) or merge     
- opérateur near : device near courant : les 2 mots doivent être présents et distants de pas plus de 10 caractères.    
 attention : seuls des mots sont recherchés, donc "chisq.test" ne renvoie rien. Essayer plutôt "chisq" dans ce cas là.     


