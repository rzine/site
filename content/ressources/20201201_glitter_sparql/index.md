---
# Author(s)
authors:
- lvaudor

# Date
date: "2021-12-01"


# Title
title: glitter 


# Short summary
summary: This package aims at writing and sending SPARQL queries without any knowledge of the SPARQL language syntax

# Tags
tags:
- 

publication_types: ["6"]

sources:
- "0"

thematics: ["6", "10"]

update:
- "1"

languages:
- "1"

# Link
url_source: 'http://perso.ens-lyon.fr/lise.vaudor/Rpackages/glitter/'
url_code: 'https://github.com/lvaudor/glitter/'

# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

`glitter` makes SPARQL…

This package aims at writing and sending SPARQL queries without any knowledge of the SPARQL language syntax. It makes the exploration and use of Linked Open Data (Wikidata in particular) easier for those who do not know SPARQL.

Install this packages through:

```
install.packages("remotes") #if remotes is not already installed
remotes::install_github("lvaudor/glitter")
```


