---
title: Congressional data in R
subtitle: A comprehensive guide
summary: A comprehensive guide to working with Congressional data (US)

authors:
- Josh McCrain

publication_types: ["13", "8"]


sources:
- "0"

thematics: ["0","10","11","12","13"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- dataset
- congressional data
- model
- discontinuity

date: "2020-06-08T00:00:00Z"

url_source: 'http://congressdata.joshuamccrain.com/index.html'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This guide is organized in 6 parts: 

1. [Congressional Datasets](http://congressdata.joshuamccrain.com/congressional_datasets.html) – An overview of available datasets covering different aspects of Congress, from off the shelf data to publicly available government data to other niche sources.
2. [Working with Congressional Data](http://congressdata.joshuamccrain.com/working_with_congressional_data.html) – How to start working with existing data, including merging datasets together by common IDs, wrangling data into proper formats, and creating new measures such as bill introductions. This is a great introduction to working with R’s tidyverse framework in a practical setting.
3. [Messy Congressional Data](http://congressdata.joshuamccrain.com/messy_data.html) – What do you do when you have two datasets that don’t share IDs? This guide goes through first how to clean up these data and prepare them for merging, how to merge, and then how to fix errors. It also shows how to work with publicly available data and replication files and put them into an existing dataset that is ready to use for regressions and visualization.
4. [Descriptive Statistics and Visualizations](http://congressdata.joshuamccrain.com/visualization.html) – So you finally have your dataset ready to go, now what? Here I cover how to produce practical summary statistics, including tables that can be exported into LaTeX, as well as more complex examples of conditional summary statistics. I also show examples of simple and complex visualizations that are ready to be put into academic papers or blog posts.
5. [Working with Models](http://congressdata.joshuamccrain.com/models.html) – You’re ready to run some regressions. This guide shows the basics behind working with linear models including those with high-dimensional fixed effects using the lfe package. I then show examples of working with model output, extracting certain coefficient values, creating coefficient plots, and plotting predicted results.
6. [Regression Discontinuity](http://congressdata.joshuamccrain.com/regression_discontinuity.html) – Getting started with regression discontinuity is a bit intimidating. Here, I show it’s actually quite easy using ggplot2 and the rdrobust package.







