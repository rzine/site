---
title: Introduction to the Tidyverse
subtitle: How to be a tidy data scientist
summary: How to be a tidy data scientist

authors:
- ogimenez

publication_types: ["12","13"]


thematics: ["10","11"]


sources :
- "0"

update :
- "0"

languages:
- "1"

projects:
- divers

date: "2020-11-15T00:00:00Z"

tags:
- tidyverse
- manipulation
- variable
- observation
- value
- explor
- count
- visualize
- pipe

url_source: 'https://oliviergimenez.github.io/intro_tidyverse/#1'
url_code: 'https://github.com/oliviergimenez/intro_tidyverse'


links:
- name : Tips
  url: https://oliviergimenez.github.io/tidyverse-tips/

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

