---
title: Network Visualization with ggplot2
subtitle: 

summary: This paper explores three different approaches to visualize networks by building on the grammar of graphics framework implemented in ggplot2.

authors:
- Samantha Tyner
- fbriatte
- Heike Hofmann

publication_types: ["10"]


thematics: ["11","18"]





projects:
- divers

sources:
- "0"

update:
- "0"

languages:
- "1"

date: "2017-05-10T00:00:00Z"
doi: ""

tags:
- ggplot2
- networkor
- igraph
- ggnet2
- ggnetwork
- geomnet
- GGally
- sna

url_source: 'https://hal.archives-ouvertes.fr/hal-01722543/document'


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This paper explores three different approaches to visualize networks by building on the grammar of graphics framework implemented in the **ggplot2** package. The goal of each approach is to provide the user with the ability to apply the flexibility of**ggplot2** to the visualization of network data, including through the mapping of network attributes to specific plot aesthetics. By incorporating networks in the **ggplot2** framework, these approaches (1) allow users to enhance networks with additional information on edges and nodes, (2) give access to the strengths of **ggplot2**, such as layersand facets, and (3) convert network data objects to the more familiar data frames.

