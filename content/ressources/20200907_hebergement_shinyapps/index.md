---
title: Héberger une application Shiny sur le serveur shinyapps.io
summary: Petit tutoriel qui présente comment héberger une application Shiny sur le serveur shinyapps.io, et comment utiliser ses propres fichiers au travers de l'application.

authors:
- Simon Michel

publication_types: ["11"]


sources:
- "0"

thematics: ["5"]


update:
- "0"

languages:
- "0"


projects:
- divers

date: "2020-09-07T00:00:00Z"

tags:
- shiny
- shinyapps
- Rstudio
- rsconnect
- DropBox
- dropbox

url_source: 'https://www.math.u-bordeaux.fr/~arichou/site/references/tutoShinyApp.html'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Ce tutoriel a pour but de montrer comment héberger une application Shiny sur le serveur shinyapps.io, et comment utiliser ses propres fichiers au travers de l'application.





