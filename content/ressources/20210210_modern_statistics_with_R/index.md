---
title: Modern Statistics with R
summary: From wrangling and exploring data to inference and predictive modelling

authors:
- Måns Thulin

publication_types: ["5"]


sources:
- "0"

thematics: ["0","10","12","13","14","3","7"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- model
- linear
- survival analysis
- predictive model
- machine learning
- forecasting
- Exploratory
- unsupervised 
- learning
- messy
- regression 
- models
- predictive 
- modelling


date: "2021-02-10T00:00:00Z"

url_source: 'http://modernstatisticswithr.com/'
url_dataset: 'http://www.modernstatisticswithr.com/data.zip'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This book was born out of lecture notes and materials that I created for courses at the University of Edinburgh, Uppsala University, Dalarna University, the Swedish University of Agricultural Sciences, and Karolinska Institutet. It can be used as a textbook, for self-study, or as a reference manual for R. No background in programming is assumed.







