---
authors:
- Marc Thévenin

date: "2022-11-15"

title: Analyse des durées
subtitle: Données biographiques
summary: Support de formation INED

tags:
- survie
- discsurv
- lifetable
- contToDisc
- survival
- survfit
- survdif
- survRM2
- rmst2
- coxph
- survsplit
- uncount
- glm
- survreg
- flexsurv
- survreg
- cmprsk
- cuminc
- nnet
 - multinom
- survminer
- jtools
- stargazer


publication_types: ["13"]


sources:
- "0"

thematics: ["12", "13", "15"]

update :
- "1"

languages:
- "0"

url_source: 'https://mthevenin.github.io/analyse_duree/'
url_code: 'https://github.com/mthevenin/analyse_duree'


featured: false
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Support de cours théorique et appliqué avec R sur l'analyse de durée.

On dispose de données dites “longitudinales”, et on cherche à appréhender l’occurence d’un évènement au sein d’une population. Les problématiques se basent sur les questions suivantes:

- Observe-t-on la survenue de l’évènement pour l’ensemble des individus?
- Quelle est la durée jusqu’à la survenue de l’évènement?
- Quels sont les facteurs qui favorisent la survenue de cet évènement? Facteurs fixes ou facteurs pouvant apparaitre/changer au cours de la période d’observation: variables dynamiques (TVC: Time Varying Covariate)
