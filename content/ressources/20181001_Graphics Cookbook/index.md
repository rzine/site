---
title: R Graphics Cookbook, 2nd edition

summary: A practical guide that provides more than 150 recipes to help you generate high-quality graphs quickly, without having to comb through all the details of R’s graphing systems.

authors:
- Winston Chang

publication_types: ["5"]


thematics: ["11"]


projects:
- oreilly

sources:
- "2"

update:
- "1"

languages:
- "1"

date: "2018-10-01T00:00:00Z"
doi: ""

tags:
- plot
- ggplot2
- hist
- barplot
- curve
- boxplot
- graph
- bar
- line
- scatter
- annotation
- axe
- scale
- theme
- legend
- facet
- color
- mosaic
- dendogram
- output
- dimensional



url_source: 'https://r-graphics.org/'
url_code: 'https://github.com/wch/rgcookbook/'

# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Welcome to the R Graphics Cookbook, a practical guide that provides more than 150 recipes to help you generate high-quality graphs quickly, without having to comb through all the details of R’s graphing systems. Each recipe tackles a specific problem with a solution you can apply to your own project, and includes a discussion of how and why the recipe works.
