---
title: Quantitative Politics with R
summary: This book aim to provide an easily accessible introduction to R for the collection, study and presentation of different types of political data

authors:
- Erik Gahner Larsen
- Zoltán Fazekas

publication_types: ["5"]


sources:
- "0"

thematics: ["0","10","11","12","13"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- Data management
- visualisation
- OLS
- regression
- linear
- Multiple linear
- prediction
- Diagnostic test
- Generalized model
- Equation
- Regression table


date: "2019-11-29T00:00:00Z"

url_source: 'http://qpolr.com/'
url_code: 'https://github.com/erikgahner/qpolr'
url_pdf: 'http://qpolr.com/qpolr.pdf'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

In this book, we aim to provide an easily accessible introduction to R for the collection, study and presentation of different types of political data. Specifically, the book will teach you how to get different types of political data into R and manipulate, analyze and visualize the output. In doing this, we will not only teach you how to get existing data into R, but also how to collect your own data.



