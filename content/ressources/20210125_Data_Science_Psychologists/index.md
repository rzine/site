---
title: Data Science for Psychologists
subtitle: 
summary: This book provides an introduction to data science that is tailored to the needs of psychologists, but is also suitable for students of the humanities and other social sciences.

authors:
- Hansjörg Neth


publication_types: ["13"]


sources :
- "0"

thematics: ["0","10","11"]


update :
- "1"

languages:
- "1"

projects:
- divers

date: "2021-01-25T00:00:00Z"

tags: 
- ds4psy
- dplyr
- ggplot2
- tibble
- tidyr
- visualization
- transform
- explore
- EDA
- import
- tidy
- string
- date
- time
- function
- psychology
- color
- regular expression
- regex


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://bookdown.org/hneth/ds4psy/'
url_code: 'https://github.com/hneth/ds4psy'

---

This book provides an introduction to data science that is tailored to the needs of psychologists, but is also suitable for students of the humanities and other biological or social sciences. This audience typically has a basic familiarity with statistics, but rarely an idea how data is prepared and shaped for being analyzed and tested. By working with a variety of data types and many examples, this text teaches tools for transforming, summarizing, and visualizing data. By keeping our eyes open for the perils of misleading representations, the book fosters fundamental skills of data literacy and cultivates reproducible research practices that enable and precede any practical use of statistics.

The materials in this book are based on a course at the [University of Konstanz](https://www.uni-konstanz.de/en/) in 2020. The course is targeted at advanced undergraduate students and provides an introduction to data science in R (R Core Team, 2020) from a tidyverse (Wickham, Averick, et al., 2019) perspective. Book and course are supported by the R package [ds4psy](https://cran.r-project.org/web/packages/ds4psy/index.html) (Neth, 2020), which provides datasets and functions used in the examples and exercises. 





