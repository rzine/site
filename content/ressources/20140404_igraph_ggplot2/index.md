---
# the title of your publication
title: Plotting igraph objects with ggplot2
subtitle : How to use ggplot2 to represent igraph networks
summary: How to use ggplot2 to represent igraph networks


authors:
- Christopher Chizinski

projects:
- divers

sources :
- "0"

thematics: ["11","18"]





update :
- "0"

languages:
- "1"



publication_types: ["11"]



date: "2014-04-04T00:00:00Z"
doi: ""

tags:
- igraph
- ggplot2
- network


url_source: 'https://chrischizinski.github.io/rstats/igraph-ggplotll/'


featured: true 
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

How to customize your graph with ggplot2.


