---
# Author(s)
authors:
- Thomas Mock 


# Date
date: "2021-04-01"


# Title
title: The GT cookbook


# Short summary
summary: This cookbook attempts to walk through many of the advanced applications for `gt`, and provide useful commentary around the use of the various `gt` functions


# Tags
tags:
- gt
- table
- customization
- formatting
- dplyr


publication_types: ["5"]

sources:
- "0"

thematics: ["10", "11", "1"]

update:
- "1"

languages:
- "1"


# Link
url_source: 'https://themockup.blog/static/resources/gt-cookbook.html'


links:
- name: Manual for advanced use cases
  url: https://themockup.blog/static/resources/gt-cookbook-advanced.html



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---


This cookbook attempts to walk through many of the example usecases for `gt`, and provide useful commentary around the use of the various `gt` functions. The full [gt documentation](https://gt.rstudio.com/index.html) has other more succinct examples and full function arguments.

For advanced use cases, make sure to check out the [Advanced Cookbook](https://themockup.blog/static/resources/gt-cookbook-advanced.html).




