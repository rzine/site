---
title:  R pour scientifique
subtitle: Notes du cours 
summary: Site web contenant les notes du cours STT-4230 / STT-6230 R pour scientifique, du département de mathématiques et de statistique de l’Université Laval (Québec)


authors:
- Sophie Baillargeon

publication_types: ["13","8"]


thematics: ["0","1","10","11","12","13","7"]


languages:
- "0"

sources:
- "0"

update:
- "1"


projects:
- divers
date: "2017-11-29T00:00:00Z"
doi: ""


tags:
- geomatique
- cartography
- osm
- tanaka


url_code: 'https://github.com/sophieb/STT-4230'
url_source: 'https://stt4230.rbind.io/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

Site web regroupant les notes du cours STT-4230 / STT-6230 R pour scientifique, offert par le département de mathématiques et de statistique de l’[Université Laval](https://www.ulaval.ca/) (Québec)

