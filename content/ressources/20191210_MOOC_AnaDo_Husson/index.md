---
title: Analyse de données avec R (MOOC)
subtitle: 
summary: Cours pour comprendre et appliquer les méthodes fondamentales de l'analyse des données

authors:
- fhusson

publication_types: ["13","9"]


thematics: ["12"]


sources :
- "0"

update :
- "1"

languages:
- "0"

projects:
- divers

date: "2019-12-10T00:00:00Z"


tags:
- ACP
- AC
- ACM
- CAH
- AFC
- Analyse en Composantes Principales
- Analyse des correspondances
- Analyse des correspondances multiples
- Classification
- Analyse Factorielle Multiple



url_source: 'https://husson.github.io/MOOC_AnaDo/index.html'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


Ce cours vise à comprendre et appliquer les méthodes fondamentales de l'analyse des données, méthodes qui visent à synthétiser et visualiser de vastes tableaux de données. Conçu en vue des applications, ce cours donne une large place aux exemples et à la mise en œuvre logicielle (logiciel FactoMineR de R). La présentation des méthodes recourt le moins possible au formalisme mathématique en privilégiant l'approche géométrique.


#### Objectif

L'objectif est de rendre les participants autonomes dans la mise en œuvre et l'interprétation d'analyses exploratoires multidimensionnelles.

#### A qui s'adresse ce MOOC ?

Ce MOOC a été conçu pour ceux qui, sans être statisticiens, sont confrontés à l'analyse statistique de données. Tous les domaines où l'on recueille des données sont concernés : enquêtes d'opinion, marketing, biologie, écologie, géographie, etc. Ce cours est destiné aux étudiants et professionnels ayant un niveau master et/ou un background dans une discipline scientifique. 

#### Livres conseillés

- Le livre [Analyse de données avec R](http://factominer.free.fr/livre/) formalise ce cours et propose de nombreuses études de cas détaillées pour chaque méthode. 
- Le livre [R pour la statistique et la science des données](https://r-stat-sc-donnees.github.io/) vous permettra d'avoir les bases de R suffisantes pour suivre ce cours. 

#### Pré-requis

Des connaissances de base en statistique sont nécessaires : coefficient de corrélation, test du Chi2, analyse de variance à un facteur.
Enfin, une initiation au langage R est suffisante pour la mise en œuvre concrète des méthodes. 
