---
title: R Markdown - The Definitive Guide

summary: Manuel de référence pour l'utilisation du rmarkdown

authors:
- Yihui Xie
- J. J. Allaire
- Garrett Grolemund

publication_types: ["5"]


thematics: ["1","5"]


projects:
- rseries

sources:
- "6"

update:
- "1"

languages:
- "1"


date: "2018-07-27T00:00:00Z"
doi: "10.1201/9781138359444"

tags:
- document
- markdown
- rmarkdown
- pdf
- html
- latex
- word
- valorisation
- epub




url_code: 'https://github.com/rstudio/rmarkdown-book'
url_source: 'https://bookdown.org/yihui/rmarkdown/'
url_pdf: 'https://bookdown.org/yihui/rmarkdown/rmarkdown.pdf'



# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**R Markdown: The Definitive Guide** is the first official book authored by the core R Markdown developers that provides a comprehensive and accurate reference to the R Markdown ecosystem. With R Markdown, you can easily create reproducible data analysis reports, presentations, dashboards, interactive applications, books, dissertations, websites, and journal articles, while enjoying the simplicity of Markdown and the great power of R and other languages.

In this book, you will learn

- Basics: Syntax of Markdown and R code chunks, how to generate figures and tables, and how to use other computing languages

- Built-in output formats of R Markdown: PDF/HTML/Word/RTF/Markdown documents and ioslides/Slidy/Beamer/PowerPoint presentations

- Extensions and applications: Dashboards, Tufte handouts, xaringan/reveal.js presentations, websites, books, journal articles, and interactive tutorials

- Advanced topics: Parameterized reports, HTML widgets, document templates, custom output formats, and Shiny documents.


**Yihui Xie** is a software engineer at RStudio. He has authored and co-authored several R packages, including knitr, rmarkdown, bookdown, blogdown, shiny, xaringan, and animation. He has published three other books, Dynamic Documents with R and knitr, bookdown: Authoring Books and Technical Documents with R Markdown, and blogdown: Creating Websites with R Markdown.

**J.J. Allaire** is the founder of RStudio and the creator of the RStudio IDE. He is an author of several packages in the R Markdown ecosystem including rmarkdown, flexdashboard, learnr, and radix.

**Garrett Grolemund** is the co-author of R for Data Science and author of Hands-On Programming with R. He wrote the lubridate R package and works for RStudio as an advocate who trains engineers to do data science with R and the Tidyverse.




