---
title: Le Descriptoire
subtitle: Recueil et analyse de texte avec R
summary: Manuel traitant du recueil et de l'analyse de données textuelles avec R

authors:
- lvaudor

publication_types: ["5"]

sources:
- "0"

thematics: ["17", "6", "10"]

languages:
- "0"

update:
- "1"

projects:
- divers

date: "2022-03-06T00:00:00Z"

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

tags:
- API
- web
- scraping
- rvest
- purrr
- OCR
- OCRisation
- tesseract
- pdftools
- transcription
- tuneR
- seewave
- googleLanguageR
- stringr
- Expressions régulières
- pattern
- regex
- token
- tokenisation
- tidytext
- n-gram
- proustr
- racinisation
- stemming
- stemme
- lemmatisation
- lemme
- koRpus
- Co-occurrence
- occurrence
- textometry
- widyr
- mixr
- wordcloud
- nueage de mots
- igraph



url_source: 'http://perso.ens-lyon.fr/lise.vaudor/Descriptoire/_book/index.html'
url_code: 'https://github.com/lvaudor/descriptoire'

---

Manuel traitant du recueil et de l'analyse de données textuelles avec R : collecte (API, scraping, OCRisation, transcription...), manipulation et  représentation de données textuelles.


