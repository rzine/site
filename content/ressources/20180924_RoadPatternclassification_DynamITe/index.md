---
# AUTHOR(S) 
authors:
- ecome
- Charles Bouveyron


# DATE de soumission à Rzine
date: "2018-09-24"

# TITRE
title: Introduction to Supervised Learning
subtitle: An exemple of Road Pattern classification
subtitle: Summer school workshop (2018) of the LabEx DynamITe

summary: 

# TAGS
tags:
- supervised
- classification
- osm
- machine
- learning
- sf
- gridExtra
- osmdata
- street angle
- density
- caret
- keras
- GLM
- k-nn



# TYPE DE PUBLICATION
publication_types: ["13"]


thematics: ["11","19"]


languages :
- "1"

sources :
- "0"

update :
- "0"


projects:
- dynamite

links:
- name: Correction
  url: /docs/20180924_RoadPatternclassification_DynamITe/roadpatternclassification_correction.html
- name: Diaporama
  url: /docs/20180924_RoadPatternclassification_DynamITe/Dynamite-MLtutorial-part1.pdf


url_source : '/docs/20180924_RoadPatternclassification_DynamITe/roadpatternclassification.html'
url_code: 'https://github.com/DynamiteStaff/R-workshops/tree/master/ML'
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Workshop** organised by the [**Summer School 2018 of the LabEx DynamITe**](http://labex-dynamite.com/fr/evenements-du-labex/ecoles-dete/ecole-dete-2018/)**, about supervised learning applicated to Road Pattern.

 





