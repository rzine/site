
---
# AUTHOR(S) 
authors:
- Alexis Gabadinho
- Gilbert Ritschard
- Matthias Studer
- Nicolas S. Müller



# DATE de soumission à Rzine
date: "2012-08-12"

# TITRE
title: TraMineR

# RESUME COURT - une phrase
subtitle: R package for sequence analisis
summary: R package for sequence analisis


# TAGS
tags:
- TraMineR
- séquence
- sequence
- transition
- trajectoire

# TYPE DE PUBLICATION
# manual = 0
# sheet = 1
# course = 2
# exercise = 3
# web = 4
# tool = 5
# article = 6
# annex = 7
# conf = 8
# uncat = 9
publication_types: ["8"]


thematics: ["11","16"]





sources :
- "0"

update :
- "1"

languages:
- "1"

# PROJET
# Les projets existants :
# Manuel CIST'R = manuel
# Fiche CIST'R = fiche
# Exercice CIST'R = exercice
# Sources diverses = divers
# Resources web (blog et site) = web
# GDR Analyse de réseau en SHS = GDR_ARSHS
# Axe Information territoriale du CIST = INFTER
projects:
- divers

##### LINKS #####
# DOCUMENTATION
url_source: 'http://traminer.unige.ch/index.shtml'


links:
- name : CRAN
  url: https://cran.r-project.org/web/packages/TraMineR/index.html


# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
publishDate: "2014-09-01T00:00:00Z"
# Name of review or book if the publication has been published in
publication: R et espace. Traitement de l’information géographique
publication_short: R et espace
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Site web officiel du package TraMineR.

