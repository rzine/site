---
title: Text mining with tidy data principles
subtitle : 
summary: Interactive tutorial with exercices about text mining with tidytext


authors:
- Julia Silge

publication_types: ["13"]


sources:
- "0"

thematics: ["17","1"]


update:
- "1"

languages:
- "1"

projects:
- divers

date: "2021-02-03T00:00:00Z"

tags:
- tidytext
- sentiment
- transcripts
- token
- bigram
- word


url_source: 'https://juliasilge.shinyapps.io/learntidytext/#section-introduction'
url_code: 'https://github.com/juliasilge/learntidytext'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This tutorial approaches text analysis using:

- the tidyverse
- the tidytext package

This tutorial was made with the learnr package in R. See the learnr introduction and some example tutorials here: [https://rstudio.github.io/learnr/](https://rstudio.github.io/learnr/)

Please note that this project is released with a [Contributor Code of Conduct](https://www.contributor-covenant.org/version/2/0/code_of_conduct/). By contributing to this project, you agree to abide by its terms.

The tutorial material in this course is licensed [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/), meaning you are free to use it, change it, and remix it as long as you give appropriate credit and distribute any new materials under the same license. The code within the tutorial is [MIT](https://opensource.org/licenses/MIT)-licensed.

