---
title: Headless Chrome Automation with R
subtitle: About crrri package
summary: Talk slides about the crrri package - useR!2019 


authors:
- rlesur
- Christope Dervieux

publication_types: ["12"]


thematics: ["6","9"]


sources :
- "0"

update :
- "0"

languages:
- "1"


projects:
- divers

date: "2019-07-12T00:00:00Z"

tags:
- chrome
- rstudio
- node.js
- Web Scraping
- Headless Chrome

url_source: 'https://www.user2019.fr/static/pres/lt257794.pdf'


links:
- name: uRosConf2019 version
  url: https://r-project.ro/conference2019/presentations/Lesur1.pdf

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

