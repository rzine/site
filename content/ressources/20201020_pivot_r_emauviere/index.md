---
authors:
- emauviere

title: De long en large ou de large en long ? Transposons facile avec R et le pivot
summary: Présentation détaillée de pivot_longer/pivot_wider à partir de jeux de données Insee. L'intérêt de passer d'une structure large à une structure longue pour l'analyse

date: "2020-10-20"

publication_types: ["11"]


thematics: ["10"]


languages:
- "0"

update:
- "0"

sources:
- "0"


url_source: 'https://www.icem7.fr/r/de-long-en-large-ou-de-large-en-long-transposons-pivot-facile-avec-r/'



tags:
- tidyverse
- tidyr
- pivot_longer
- transpositions


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---




