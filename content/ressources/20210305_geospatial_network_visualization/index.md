---
title: Geospatial Network Visualization
subtitle: sfnetworks meets ggraph
summary: How to visualize spatial networks created with `sfnetworks` using `ggraph`

date: "2021-03-05T00:00:00Z"

authors:
- Lorena Abad

publication_types: ["11"]


sources:
- "0"

thematics: ["11","18"]


update:
- "0"

languages:
- "1"


projects:
- divers

tags:
- sfnetwork
- ggraph
- sf
- network
- distill




url_source: 'https://loreabad6.github.io/posts/2021-03-05-geospatial-network-visualization/'
url_code: 'https://github.com/loreabad6/loreabad6.github.io/tree/master/_posts/2021-03-05-geospatial-network-visualization'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This post is to document my personal exploration on how to visualize spatial networks created with `sfnetworks` using `ggraph`. This all started with a [sfnetworks hackathon](https://github.com/sfnetworks/sfnetworks_viz) and should end with a successful pull request (PR) to `ggraph`.

I should start by saying, that it is probably a good idea to get familiar with `ggraph` (and `tidygraph` and `sfnetworks` while you are at it!) before going through this post.

If you are already a `ggraph` + `tidygraph` user and would like to apply the possibilities to the spatial domain, then this is definitely a resource for you and a good opportunity to learn about `sfnetworks`!



