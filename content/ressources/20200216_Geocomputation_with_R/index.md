---
authors:
- rlovelace
- jnowosad
- Jannes Muenchow

date: "2019-03-25"

title: Geocomputation with R
subtitle: Data analysis, visualization and modeling
summary: Reference book on geographic data analysis, visualization and modeling.


# TAGS
tags:
- geographic
- sf
- spatial
- raster
- osm
- sp
- shape

# TYPE DE PUBLICATION
publication_types: ["5","8"]


thematics: ["2","20","19"]


sources :
- "6"

update :
- "1"

languages:
- "1"

projects:
- rseries

url_source: 'https://geocompr.robinlovelace.net/'
url_code: 'https://github.com/Robinlovelace/geocompr'

links:
- name: Website
  url: https://geocompr.github.io/


publishDate: "2019-03-25T00:00:00Z"
publication: Geocomputation with R
publication_short: Geocomputation
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Geocomputation with R** is for people who want to analyze, visualize and model geographic data with open source software. It is based on R, a statistical programming language that has powerful data processing, visualization, and geospatial capabilities. The book equips you with the knowledge and skills to tackle a wide range of issues manifested in geographic data, including those with scientific, societal, and environmental implications. This book will interest people from many backgrounds, especially Geographic Information Systems (GIS) users interested in applying their domain-specific knowledge in a powerful open source language for data science, and R users interested in extending their skills to handle spatial data.

The book is divided into three parts: (I) Foundations, aimed at getting you up-to-speed with geographic data in R, (II) extensions, which covers advanced techniques, and (III) applications to real-world problems. The chapters cover progressively more advanced topics, with early chapters providing strong foundations on which the later chapters build. Part I describes the nature of spatial datasets in R and methods for manipulating them. It also covers geographic data import/export and transforming coordinate reference systems. Part II represents methods that build on these foundations. It covers advanced map making (including web mapping), "bridges" to GIS, sharing reproducible code, and how to do cross-validation in the presence of spatial autocorrelation. Part III applies the knowledge gained to tackle real-world problems, including representing and modeling transport systems, finding optimal locations for stores or services, and ecological modeling. Exercises at the end of each chapter give you the skills needed to tackle a range of geospatial problems. Solutions for each chapter and supplementary materials providing extended examples are available at https://geocompr.github.io/geocompkg/articles/.

