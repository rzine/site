---
title: Onyxia, une plateforme de traitement de données moderne
subtitle : Séminaire RUSS - Séance du 3 février 2023
summary: Séminaire RUSS - Séance du 3 février 2023


authors:
- Frédéric Comte
- Shiraz Adamaly
- Romain Avouac

publication_types: ["12", "9"]

sources:
- "7"

thematics: ["7"]


update:
- "0"

languages:
- "0"


projects:
- RUSS

date: "2023-02-03T00:00:00Z"


url_source: 'https://www.canal-u.tv/chaines/ined/onyxia-une-plateforme-de-traitement-de-donnees-moderne#themes'

links:
- name: Video (Introduction)
  url: https://www.canal-u.tv/chaines/ined/onyxia-une-plateforme-de-traitement-de-donnees-moderne/introduction-pourquoi-fournir
- name: Video (partie 1)
  url: https://www.canal-u.tv/chaines/ined/onyxia-une-plateforme-de-traitement-de-donnees-moderne/partie-1-une-plateforme-ouverte
- name: Video (partie 2)
  url: https://www.canal-u.tv/chaines/ined/onyxia-une-plateforme-de-traitement-de-donnees-moderne/partie-2-une-plateforme-de
- name: Video (partie 3)
  url: https://www.canal-u.tv/chaines/ined/onyxia-une-plateforme-de-traitement-de-donnees-moderne/partie-3-le-catalogue-de
- name: Video (partie 4)
  url: https://www.canal-u.tv/chaines/ined/onyxia-une-plateforme-de-traitement-de-donnees-moderne/partie-4-catalogue-de
- name: Video (questions/réponses)
  url: https://www.canal-u.tv/chaines/ined/onyxia-une-plateforme-de-traitement-de-donnees-moderne/questions-reponses

tags:
- RUSS
- INSEE
- reproductibilité
- opensource
- science ouverte
- plateforme
- SSPCloud
- Onyxia
- docker

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Onyxia : Une plateforme de traitement de données moderne**, Frédéric Comte (Insee), Shiraz Adamaly (Insee) et Romain Avouac (Insee)

Le projet opensource [Onyxia](https://www.onyxia.sh/), développé par l’Insee, permet de rendre accessible à des utilisateurs statisticiens aux compétences hétérogènes en informatique de nombreux moyens de traitement de données.    
Ce projet est parti du constat que beaucoup d’entre eux travaillent les données directement sur leur poste de travail ce qui a des mauvaises propriétés :  
- une puissance limitée   
- une reproductibilité faible   
- une sécurité des données rendue difficile avec leur dispersion   

En s’appuyant sur cette technologie, l’INSEE propose une plateforme en ligne nommée [SSPCloud](https://datalab.sspcloud.fr/home) permettant à tout agent de l’état et de nombreuses écoles et universités de se former ou de traiter des données ouvertes.

