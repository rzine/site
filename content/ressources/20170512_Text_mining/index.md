---
title: Text Mining with R
subtitle: A tidy approach of text mining with R
summary: A tidy approach of text mining with R

authors:
- Julia Silge
- David Robinson

publication_types: ["5"]


thematics: ["17"]



projects:
- oreilly

sources:
- "2"

update:
- "1"

languages:
- "1"

date: "2017-05-12T00:00:00Z"
doi: "10.1002/9781119282105"

tags:
- tidy
- text
- mining
- word frequency
- sentiment
- three sentiment
- wordcloud
- word
- token
- topic modeling
- document-term matrix
- dtm




url_code: 'https://github.com/dgrtwo/tidy-text-mining'
url_source: 'https://www.tidytextmining.com/'




# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

## Preface

If you work in analytics or data science, like we do, you are familiar with the fact that data is being generated all the time at ever faster rates. (You may even be a little weary of people pontificating about this fact.) Analysts are often trained to handle tabular or rectangular data that is mostly numeric, but much of the data proliferating today is unstructured and text-heavy. Many of us who work in analytical fields are not trained in even simple interpretation of natural language.

We developed the tidytext (Silge and Robinson 2016) R package because we were familiar with many methods for data wrangling and visualization, but couldn’t easily apply these same methods to text. We found that using tidy data principles can make many text mining tasks easier, more effective, and consistent with tools already in wide use. Treating text as data frames of individual words allows us to manipulate, summarize, and visualize the characteristics of text easily and integrate natural language processing into effective workflows we were already using.

This book serves as an introduction of text mining using the tidytext package and other tidy tools in R. The functions provided by the tidytext package are relatively simple; what is important are the possible applications. Thus, this book provides compelling examples of real text mining problems.
