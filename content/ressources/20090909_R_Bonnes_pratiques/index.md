---

title: R, Bonnes Pratiques
subtitle : Quelques astuces et règles pour améliorer la qualité de votre code
summary: Quelques astuces et règles pour améliorer la qualité de votre code


authors:
- Christophe Genolini

projects:
- divers

sources :
- "0"

thematics: ["0","7"]





update :
- "0"

languages:
- "0"



publication_types: ["5"]



date: "2009-09-09T00:00:00Z"
doi: ""

tags:
- éditeur
- architecture
- variable
- commentaire
- astuces
- règles


url_source: 'https://cran.r-project.org/doc/contrib/Genolini-RBonnesPratiques.pdf'


featured: true 
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

C'est une sorte de crédo en informatique, *tous les programmes sont buggués*. Reste que si le bug est une fatalité, il est néanmoins possible de le combattre.     
Les "bonnes pratiques" sont des règles que les programmeurs choisissent de suivre pour améliorer la qualité de leur programmation et diminuer le nombre de bugs présents dans leur code. 

**R, Bonnes Pratiques propose des bonnes pratiques adaptées au langage R**.


