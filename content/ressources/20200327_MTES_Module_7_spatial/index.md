---
title: Les données spatiales avec R

subtitle: ParcoursR (module 5) - « Datavisualisation - produire des graphiques, des cartes et des tableaux »

summary: Module 7 « Analyse spatiale ». Lire, écrire, manipuler et représenter des données spatiales. Formations R aux MTES & MCTRCT.
 
authors:
- mtheuliere

# Type de la publication 
publication_types: ["5","13"]


thematics: ["2"]


sources:
- "3"

update:
- "1"

languages:
- "0"


projects:
- parcoursr

date: "2020-03-27T00:00:00Z"


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
tags:
- MTES
- MCTRCT
- G2R
- sf
- vecteur
- raster
- stars
- mapview
- leaflet
- ggplot2
- géotraitement
- SIG
- rmapshaper
- patchwork
- lwgeom
- tmap
- cartogram
- projection

url_source: 'https://mtes-mct.github.io/parcours_r_module_analyse_spatiale/index.html'
url_code: 'https://github.com/MTES-MCT/parcours_r_module_analyse_spatiale'

---


L’objectif de ce module est de présenter les éléments de manipulation des données spatiales à partir de R. Nous verrons ainsi :

- Ce que sont les données spatiales
- Comment lire des données spatiales ?
- Comment manipuler les données spatiales ?
- Comment visualiser les données spatiales ?




