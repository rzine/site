
---
# AUTHOR(S) 
authors:
- Yihui Xie
- Amber Thomas 
- Alison Hill


# DATE de soumission à Rzine
date: "2017-12-12"

# TITRE
title: Blogdown
subtitle: Creating websites with R Markdown

# RESUME COURT - une phrase
summary: Creating Websites with R Markdown


# TAGS
tags:
- blogown
- markdown
- site
- web
- blog


# TYPE DE PUBLICATION
publication_types: ["5"]


thematics: ["1"]


languages:
- "1"

sources:
- "6"

update:
- "1"


# PROJET
# Les projets existants :
# Manuel CIST'R = manuel
# Fiche CIST'R = fiche
# Exercice CIST'R = exercice
# Sources diverses = divers
# Resources web (blog et site) = web
# GDR Analyse de réseau en SHS = GDR_ARSHS
# Axe Information territoriale du CIST = INFTER

projects:
- rseries

##### LINKS #####
# DOCUMENTATION
url_source: 'https://bookdown.org/yihui/blogdown/'

url_code: 'https://github.com/rstudio/blogdown'

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
publishDate: "2017-12-12T00:00:00Z"
# Name of review or book if the publication has been published in
publication: blogdown - Creating Websites with R Markdown
publication_short: blogdown
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**blogdown: Creating Websites with R Markdown** provides a practical guide for creating websites using the blogdown package in R. In this book, we show you how to use dynamic R Markdown documents to build static websites featuring R code (or other programming languages) with automatically rendered output such as graphics, tables, analysis results, and HTML widgets. The blogdown package is also suitable for technical writing with elements such as citations, footnotes, and LaTeX math. This makes blogdown an ideal platform for any website designed to communicate information about data science, data analysis, data visualization, or R programming.

Note that blogdown is not just for blogging or sites about R; it can also be used to create general-purpose websites. By default, blogdown uses Hugo, a popular open-source static website generator, which provides a fast and flexible way to build your site content to be shared online. Other website generators like Jekyll and Hexo are also supported.

In this book, you will learn how to:

- Build a website using the blogdown package;

- Create blog posts and other website content as dynamic documents that can be easily edited and updated;

- Customize Hugo templates to suit your site’s needs;

- Publish your website online;

- Migrate your existing websites to blogdown and Hugo.


