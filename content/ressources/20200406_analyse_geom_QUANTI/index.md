---
title: Visualiser une analyse géométrique des données
subtitle: 
summary: Réaliser des graphiques et des tableaux statistiques permettant de visualiser et présenter les résultats d’une analyse géométrique


authors:
- aperdoncin

projects:
- quanti

sources:
- "0"

thematics: ["12"]



languages:
- "0"


update:
- "0"

publication_types: ["11"]


date: "2020-04-06T00:00:00Z"


tags:
- FactoMineR
- factoextra
- GDAtools
- tidyverse
- questionr
- RColorBrewer
- ggrepel
- explor
- ACM
- ellipse


url_source: 'https://quanti.hypotheses.org/1871'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Article proposé par le carnet de recherche QUANTI : *Les outils du quanti en sciences humaines et sociales*.

Ce tutoriel a pour objectif de montrer **comment réaliser, avec R et RStudio, des graphiques et des tableaux statistiques sur-mesure**, permettant de **visualiser** et de **présenter** les **résultats d’une analyse géométrique des données**.

Il a aussi pour objectif – secondaire ici mais loin d’être trivial – de **fournir un exemple de script** de programmation statistique **structuré**, **commenté** et **reproductible**.
