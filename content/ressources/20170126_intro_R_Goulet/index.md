---
title: Introduction à la programmation en R
subtitle: Cinquième édition révisée 
summary: Cinquième édition révisée de ce manuel d'introduction de référence (2006-2017)

authors:
- Vincent Goulet

publication_types: ["5"]


thematics: ["0"]


languages:
- "0"

sources:
- "0"

update:
- "1"

projects:
- divers
date: "2017-01-26T00:00:00Z"
doi: ""


tags:
- introduction
- objet
- fonction
- arguments
- opérateurs
- apply
- lapply
- sapply
- replicate
- GNU Emacs
- ESS
- Rstudio
- simulation
- package


url_source: 'https://vigou3.github.io/introduction-programmation-r/'
url_code: 'https://github.com/vigou3/introduction-programmation-r/releases/tag/edition-5a/%3E%20%20%20%20%20%20%3Ca%20href='
url_pdf: 'https://github.com/vigou3/introduction-programmation-r/releases/download/edition-5a/introduction-programmation-r.pdf'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---




De nombreux ouvrages traitent de l’utilisation du système R en sciences naturelles, en sciences sociales, en finance, etc.

*Introduction à la programmation en R* se concentre plutôt sur l’apprentissage du langage de programmation sous-jacent aux fonctionnalités statistiques.

L’ouvrage repose sur une philosophie d’apprentissage du langage R par l’exposition à un maximum de code et par la pratique de la programmation. C’est pourquoi les chapitres sont rédigés de manière synthétique et qu’ils comportent peu d’exemples au fil du texte.

En revanche, le lecteur est appelé à lire et à exécuter le code informatique se trouvant dans les sections d’exemples à la fin de chacun des chapitres. Ce code et les commentaires qui l’accompagnent reviennent sur l’essentiel des concepts du chapitre et les complémentent souvent.

L’ouvrage se base sur des notes de cours et des exercices utilisés à l’École d’actuariat de l’Université Laval.

### Édition

Cinquième édition révisée, ISBN 978-2-9811416-6-8 ([nouveautés](https://github.com/vigou3/introduction-programmation-r/releases/tag/edition-5a))

