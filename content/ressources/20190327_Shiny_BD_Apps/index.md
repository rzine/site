---
authors:
- Matthew Sharkey


date: "2019-03-27"

# TITRE
title: Building Web Applications with Shiny and SQL Server

subtitle: 
summary: 


# TAGS
tags:
- SQL
- connection
- database
- RDBMS
- DBI
- pool
- microbenchmark
- profvis

# TYPE DE PUBLICATION
publication_types: ["5"]


thematics: ["5","7"]


languages:
- "1"




sources:
- "0"

update:
- "0"


projects:
- divers


url_source: 'https://bookdown.org/msharkey3434/ShinyDB_Book/'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This book supplements [my presentation](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiCrums3YvqAhWHD2MBHQtFAFIQFjALegQIAhAB&url=http%3A%2F%2Fwww.sqlsaturday.com%2FSessionDownload.aspx%3Fsuid%3D25134&usg=AOvVaw0VUVDl1RCH8L25wdhU9yRH) at the Omaha R User Group on Thursday, April 4, 2019.

If you develop Shiny Applications and your back end is a relational database, then this book is for you.  The demos and code snippets are written in T-SQL, Microsoft SQL Server dialect of SQL, but the core concepts are applicable across all RDBMS systems.

Data Scientists using R are the primary audience.  Database Developers, BI Developers, Software Engineers, Data Analysts and Managers wanting to learn more about Shiny Database applications will also benefit from the material.
