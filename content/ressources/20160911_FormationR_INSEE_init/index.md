---
title: Formation R Initiation

subtitle: Formation donnée à l'INSEE de 2016 à 2018
summary: Formation donnée à l'INSEE de 2016 à 2018

authors:
- mchevalier

 
publication_types: ["13"]


thematics: ["0","10","12"]


languages:
- "0"

sources :
- "4"

update :
- "0"

projects:
- spyrales


date: "2016-09-11T00:00:00Z"

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

tags:
- objet
- vecteur
- matrice
- list
- dataframe
- graphique
- qualitative
- quantitative
- statistique
- descriptive
- variable
- Pisa


links:
- name: Module 1
  url: https://teaching.slmc.fr/r/module1.html
- name: Module 2
  url: https://teaching.slmc.fr/r/module2.html
- name: Module 3
  url: https://teaching.slmc.fr/r/module3.html

url_source: 'https://teaching.slmc.fr/r/'


---

Tous les supports de la formation *R initiation* conçue par Martin Chevalier et donnée à l’Insee de 2016 à 2018 :

- Supports en format HTML :

	- Module 1 : **Prise en main du logiciel**     
	- Module 2 : **Manipuler les outils fondamentaux du langage**     
	- Module 3 : **Travailler avec des données statistiques**       
  
- Supports en format pdf (avec et sans solution des exercices)

- Données utilisées pour les exercices (.zip)

- Support de présentation utilisé lors de la formation (.pdf)

<br>

#### Conditions d'utilisation

Ces supports seront durablement disponibles à l’adresse http://r.slmc.fr et leur code source sur [github](https://github.com/martinchevalier/r_init). L’ensemble est librement réutilisable sous **© 2016-2020 Martin Chevalier** [CC BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/).





