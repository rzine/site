---
title: CRAN downloads
summary: A shiny app to visualise CRAN downloads with data from the [cranlogs](https://r-hub.github.io/cranlogs/) package

authors:
- Hadley Wickham

publication_types: ["1"]


sources:
- "0"

thematics: ["9"]


update:
- "0"

languages:
- "1"


projects:
- divers

date: "2015-05-07T00:00:00Z"

tags:
- benchmarking
- package
- download
- CRAN
- shiny

url_source: 'https://hadley.shinyapps.io/cran-downloads/'
url_code: 'https://github.com/hadley/cran-downloads'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

A shiny app to visualise CRAN downloads with data from the cranlogs package.


