---
title: Econometrics and Free Software
summary: Blog about econometrics with R

authors:
- brodrigues

# Type de la publication 
publication_types: ["8"]


sources:
- "0"

thematics: ["12", "13", "14", "15", "7", "9"]


update:
- "1"

languages:
- "1"

projects:
- divers

date: "2012-12-11T00:00:00Z"
doi: ""
 
featured: true
image:
  caption: "© Bruno Rodrigues"
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://www.brodrigues.co/'

links:
- name: Youtube channel
  url: https://www.youtube.com/channel/UCTZXht1RTL2Duc3eU8MYGzQ

---



