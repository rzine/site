---
title: R Basics with Tabular Data
summary: This lesson teaches a way to quickly analyze large volumes of tabular data, making research faster and more effective. Lesson from [The Programming Historian](https://programminghistorian.org/)

authors:
- Taryn Dewar

publication_types: ["10","13"]

sources:
- "10"

thematics: ["0", "10"]

update:
- "0"

languages:
- "1"
- "2"


tags:
- Basic
- base 
- Rbase
- load
- save
- summary
- Programming Historian
- PH
  

projects:
- programming_historian


date: "2016-09-05T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  


doi: '10.46430/phen0056'
url_source: 'https://programminghistorian.org/en/lessons/r-basics-with-tabular-data'

links:
- name: Versión en español
  url: https://programminghistorian.org/es/lecciones/datos-tabulares-en-r



---

This lesson teaches a way to quickly analyze large volumes of tabular data, making research faster and more effective. Lesson from [The Programming Historian](https://programminghistorian.org/).



