---
title: Cours R de Master en Statistique & Econométrie
subtitle: Supports et exercices de Master de statistique & économétrie
summary: Supports et exercices utilisés pour des Masters de Statistique & Econométrie, et *Data science for social sciences* à l'université Toulouse Capitole

date: "2022-05-27T00:00:00Z"

authors:
- tlaurent


publication_types: ["13", "9"]


sources:
- "0"

thematics: ["0", "10", "12", "15", "19"]


update:
- "1"

languages:
- "0"


projects:
- divers


tags:
- econométrie
- economics
- TSE
- Master
- course

url_source: 'http://www.thibault.laurent.free.fr/pedago.html'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Supports et exercices de Thibault Laurent (CNRS, UMR TSE-R) utilisés dans le cadre des Masters [*Statistique & Econométrie*](https://www.tse-fr.eu/fr/master-statistique-et-econometrie) et [*Data science for social sciences*](https://www.tse-fr.eu/master-data-science-social-sciences?qt-embed_generic_tabs=2#qt-embed_generic_tabs) à [TSE](https://www.tse-fr.eu/fr) ([Université Toulouse Capitole](https://www.ut-capitole.fr/)) et référencés sur son site personnel.


