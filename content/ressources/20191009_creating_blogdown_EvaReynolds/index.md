---
title: Creating an ‘Academic Themed’ website with blogdown and Hugo
subtitle: A companion guide to Dan Quintana’s thread
summary: A companion guide to [Dan Quintana’s thread](https://twitter.com/dsquintana/status/1139846569623281664)

authors:
- Ereynolds

publication_types: ["11"]


thematics: ["1","7"]


sources :
- "0"

update :
- "0"

languages:
- "1"


projects:
- divers

date: "2019-10-09T00:00:00Z"


tags:
- website
- blogdown
- hugo
- academic
- Netlify
- deploy


url_source: 'https://evamaerey.github.io/what_how_guides/academic_website_w_blogdown'
url_code: 'https://github.com/EvaMaeRey/what_how_guides'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

