---
title: Rseek
subtitle: Moteur de recherche R
summary: Moteur de recherche personalisé qui ne renvoit que du contenu pertinent (filtre requête Google)
date: "2007-09-30T00:00:00Z"

authors:
- Sasha Goodman

publication_types: ["0", "8"]


thematics: ["8", "9"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers


tags:
- search engine
- google
- community

url_source: 'https://rseek.org/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
---

RSeek est un moteur de recherche personnalisé qui facilite la recherche d'informations sur le site officiel, le CRAN, les archives des listes de diffusion, la documentation de R et même certains sites Web... 

**C'est plus efficace qu'une recherche Google !**

RSeek agit en fait comme un filtre sur les recherches Google, pour ne renvoyer que du contenu pertinent.

