---
authors:
- Jeroen Ooms

title: R-universe


summary: The R-universe platform is a new umbrella project under which we experiment with various new ideas for improving publication and discovery of research software in R

date: "2022-07-22"

publication_types: ["8", "6"] 


thematics: ["5", "8","9"] 

languages:
- "1" 


update:
- "1" 

sources:
- "5" 


projects:
- rOpenSci


url_source: 'https://r-universe.dev/search/' 
url_code: 'https://github.com/r-universe-org' 
url_project: 'https://ropensci.org/r-universe/'



tags:
- Package server
- Cran
- RopenSci
- R-universe
- Development
- Infrastructure 
- publication 
- discovery
- Research
- Software
- Packages


  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


The R-universe system provides an array of services to users and developers of R packages, with integrated measuring and monitoring tools to analyze the state and evolution of the ecosystem. The platform is based around a modern build infrastructure, which enables users or research groups to seamlessly setup automated R package repositories for publishing software directly to users.
