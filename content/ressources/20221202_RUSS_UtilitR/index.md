---
title: UtilitR, une documentation collaborative sur l'usage de R pour l'analyse de données
subtitle : Séminaire RUSS - Séance du 2 décembre 2022
summary: Séminaire RUSS - Séance du 2 décembre 2022


authors:
- omeslin

publication_types: ["12","9"]

sources: ["7"]

thematics: ["8"]

update:
- "0"

languages:
- "0"

projects:
- RUSS
- utilitR

date: "2022-12-02T00:00:00Z"



url_source: 'https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjR6dyKpNb-AhVZdKQEHf2kDcgQFnoECAgQAQ&url=https%3A%2F%2Fruss.site.ined.fr%2Ffichier%2Fs_rubrique%2F33126%2F20221202_presentation_utilitr_russ_ined.fr.pdf&usg=AOvVaw1hcfndTRjdfV0nIZftz4zX'


links:
- name: Video (Introduction)
  url: https://www.canal-u.tv/chaines/ined/introduction-genese-du-projet-et-objectifs-d-utilitr
- name: Video (partie 1)
  url: https://www.canal-u.tv/chaines/ined/partie-1-contenu-de-la-documentation-comment-bien-travailler-avec-r-a-l-insee
- name: Video (partie 2)
  url: https://www.canal-u.tv/chaines/ined/partie-2-contenu-de-la-documentation-comment-realiser-de-taches-standard-avec-r
- name: Video (partie 3)
  url: https://www.canal-u.tv/chaines/ined/partie-3-modes-de-diffusion-de-la-documentation
- name: Video (partie 4)
  url: https://www.canal-u.tv/chaines/ined/partie-4-une-demarche-collaborative-et-open-source-organisation-du-projet
- name: Video (partie 5)
  url: https://www.canal-u.tv/chaines/ined/partie-5-une-demarche-collaborative-et-open-source-fonctionnement-du-projet
- name: Video (Conclusion)
  url: https://www.canal-u.tv/chaines/ined/conclusion-et-la-suite
  
  
tags:
- RUSS
- INSEE
- UtiliR
- documentation
- reproductibilité
- opensource
- site

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**[utilitR](https://www.book.utilitr.org/) : une documentation collaborative et ouverte sur l’usage de R pour l’analyse de données**, Olivier Meslin (Insee)

Le projet [utilitR](https://www.book.utilitr.org/) vise à produire une documentation collaborative et open source sur R.

Ce projet est parti du constat qu’il est difficile d’apprendre à utiliser R pour de multiples raisons : multiplicité de packages faisant plus ou moins la même chose, abondance et éclatement de la documentation (souvent en anglais), difficultés supplémentaires pour effectuer des choix éclairés et adaptés à certaines contraintes informatiques…

Les contributeurs du projet utilitR se sont ainsi fixés pour objectif de rassembler dans un seul document tous les éléments utiles pour l’usage de R en cherchant à couvrir la plupart des cas d’usage courants. La documentation a vocation à servir de point d’entrée aux tâches standards d’analyse de données et renvoyer vers des ressources pertinentes pour les usages avancés. Elle a vocation à satisfaire un public large, qu’il s’agisse de débutants ou d’utilisateurs expérimentés ayant besoin d’une aide-mémoire sur une tâche précise. 

Pour correspondre au mieux à des cas d’usages réels dans le domaine de l’analyse de données en sciences sociales, les exemples présents dans la documentation s’appuient tous sur des données diffusées en open-data par l’Insee et sont entièrement reproductibles.




