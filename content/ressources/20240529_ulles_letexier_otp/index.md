---
authors:
- jculles
- mletexier86

# DATE de soumission à Rzine
date: "2024-05-29"

# TITRE
title: Analyse d’accessibilité multimodale
subtitle: Avec R et OpenTripPlanner

# RESUME COURT - une phrase
summary: Cette article présente une méthodologie d’analyse de l’accessibilité multimodale d’un territoire à partir des données ouvertes d’offre de transport et de mobilité et du logiciel OpenTripPlanner mobilisé depuis l’environnement R.


# TAGS
tags:
- OpenTripPlanner
- otp
- OpenStreetMap
- sf

publication_types: ["10"]

projects:
- rzine

sources:
- "8"

thematics: ["19","6","2"]


update :
- "0"

languages:
- "0"


# Link
url_source: '/docs/20240529_ulles_letexier_otp/index.html'
url_code: 'https://github.com/rzine-reviews/OTP_RZINE'
url_dataset: 'https://zenodo.org/records/10793738'

# links:
# - name: HAL
#   url: https://hal.archives-ouvertes.fr/xxxxxxx

doi: "10.48645/5qht-d313"

# ###### NE PAS MODIFIER #####
featured: false
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Auteur·es** : [Jean-Clément Ullès](https://rzine.fr/authors/jculles/), [Marion Le Texier](https://rzine.fr/authors/mletexier86/)    
**Evaluateurs** :  Matthieu Viry, Armand Pons    
**Editeur·rices** : [Timothée Giraud](https://rzine.fr/authors/tgiraud), [Marion Gentilhomme](https://rzine.fr/authors/mgentilhomme/)          


#### Résumé 

Cet article présente une méthodologie d’analyse de l’accessibilité multimodale d’un territoire à partir des données GTFS et du logiciel OpenTripPlanner. Nous détaillerons la manière dont ce nouvel outil ouvre de nouvelles perspectives d’analyse de l’accessibilité en lien avec les nouvelles données de mobilité.  
Le répertoire OTP, contenant le logiciel Java et les données nécessaires aux calculs d'accessibilité, est disponible en ligne.

#### Citation

<div style="font-size:15px;">
Ullès, J.-C., & Le Texier, M. (2024). Analyse d’accessibilité multimodale. Rzine. https://doi.org/10.48645/5qht-d313 
</div>

<p class="d-inline-flex" style="gap:12px;"><a href="https://doi.org/10.48645/5qht-d313"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://zenodo.org/badge/DOI/10.48645/5qht-d313.svg" alt="DOI:10.48645/5qht-d313"></a><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg" alt="License: CC BY-SA 4.0"></a></p>





