---
# the title of your publication
title: analyse-R

# an optional subtitle that will be displayed under the title
subtitle: Introduction à l’analyse d’enquêtes avec R et RStudio  

# a one-sentence summary of the content on your page. The summary can be shown on the homepage and can also benefit your search engine ranking.
summary: Introduction à l’analyse d’enquêtes avec R et RStudio. 

# Display the authors of the publication and link to their user profiles (if they exist).
authors:
- jlarmarange

# Type de la publication 
publication_types: ["8", "13"]


thematics: ["0","1","10","11","12","13","16","6"]


languages:
- "0"

sources:
- "0"

update:
- "1"

# Categorizing your content helps users to discover similar content on your site. Categories can improve search relevancy and display at the top of a page alongside a page’s metadata
  
# Project name associated ?
# Set the name if several publications can be grouped in one projet
projects:
- divers

# Date that the page was published
date: "2019-05-06T00:00:00Z"
doi: "10.5281/zenodo.2669067"

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
# Name of review or book if the publication has been published in
# publication:
# publication_short:  



tags:
- enquête
- manipuler
- analyser
- appronfondir
- prise en main
- manipulation
- import
- export
- statistique
- ACM
- CAH
- intervalles
- pondérée
- régression
- échantillonage
- multicolinéarité
- survie
- séquence
- trajectoire
- graphique
- représentation
- programmation
- réseaux
- spatiale
- tidyverse
- dyplr
- data.table
- stringr
- tidyr
- représentation
- ggplot2
- lattice
- markdown
- scraping

  

url_pdf: 'https://larmarange.github.io/analyse-R/analyse-R.pdf'
url_source: 'https://larmarange.github.io/analyse-R/'
url_code: 'https://github.com/larmarange/analyse-R'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


**L’objectif premier d’analyse-Rest de présenter comment réaliser des analyses statistiques et diverses opérations courantes (comme la manipulation de données ou la production de graphiques) avec R**.      
Il ne s’agit pas d’un cours de statistiques : les différents chapitres présupposent donc que vous avez déjà une connaissance des différentes techniques présentées.     
Si vous souhaitez des précisions théoriques / méthodologiques à propos d’un certain type d’analyses, nous vous conseillons d’utiliser votre moteur de recherche préféré. En effet, on trouve sur internet de très nombreux supports de cours (sans compter les nombreux ouvrages spécialisés disponibles en libraririe).

<br>

#### Contributeurs 

Par ordre alphabétique :     

Julien Barnier, Julien Biaudet, François Briatte, Milan Bouchet-Valat, Ewen Gallic, Frédérique Giraud, Joël Gombin, Mayeul Kauffmann, Christophe Lalanne, **Joseph Larmarange**, Nicolas Robette.


<br>

#### Licence **CC by-nc-sa**

Le contenu de ce site est diffusé sous licence Creative Commons Attribution - Pas d’utilisation commerciale - Partage dans les mêmes conditions (https://creativecommons.org/licenses/by-nc-sa/3.0/fr/).

Cela signifie donc que vous êtes libre de recopier / modifier / redistribuer les contenus d’analyse-R, à condition que vous citiez la source et que vos modifications soient elle-mêmes distribuées sous la même licence (autorisant ainsi d’autres à pouvoir réutiliser à leur tour vos ajouts).
Contribuer

**analyse-R est développé avec RStudio et le code source est librement disponible sur GitHub** : [https://github.com/larmarange/analyse-R](https://github.com/larmarange/analyse-R)

**Ce projet se veut collaboratif**. N’hésitez donc pas à proposer des corrections ou ajouts, voire même à rédiger des chapitres additionnels.


