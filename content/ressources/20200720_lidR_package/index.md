---
authors:
- Jean-Romain Roussel
- Tristan R.H. Goodbody
- Piotr Tompalski

date: "2020-07-20"

title: The lidR package
subtitle: Hands on descriptions and tutorials for using lidR
summary: Hands on descriptions and tutorials for using lidR, a package for manipulating and visualizating airborne laser scanning data with an emphasis on forestry application.


tags:
- liDAR
- las
- raster
- sp
- sf
- rgdal
- classification
- terrain
- model
- DTM
- raster
- tree
- grid
- point
- ABA


publication_types: ["6","5"]


thematics: ["2","19"]


languages:
- "1"




sources:
- "0"

update:
- "1"


projects:
- divers


url_source: 'https://r-lidar.github.io/lidRbook/index.html'
url_code: 'https://github.com/r-lidar/lidRbook' 



links:
- name: package (CRAN)
  url: https://cran.r-project.org/web/packages/lidR/
- name: package (code source)
  url: https://github.com/Jean-Romain/lidR/


# publishDate: "2014-10-28T00:00:00Z"
# publication: Advanced R
# publication_short:  Advanced R
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

lidR is an R package for manipulating and visualizating airborne laser scanning (ALS) data with an emphasis on forestry applications. The package is entirely open source and is integrated within the geospatial R ecosytem (i.e. raster, sp, sf, rgdal etc.). This guide has been written to help both the ALS novice, as well as seasoned point cloud processing veterans. Key functionality of lidR includes functions to:

- Read and write .las and .laz files and render customized point-cloud display
- Process point clouds including point classification, digital terrain models, normalization and digital surface models
- Perform individual tree segmentation
- Compute standard and user-defined metrics at different levels of regularisation
- Manage processing for sets of point-cloud files - referred to as a LAScatalog 
- Guidelines for implementing area-based approaches to forest modelling using ALS data
- Facilitate user-defined processing streams for research and development
- Discover the plugin system

The current release version of lidR can be found on [CRAN](https://cran.r-project.org/web/packages/lidR/) and the source code is hosted on [GitHub](https://github.com/Jean-Romain/lidR).

Development of the lidR package between 2015 and 2018 was made possible thanks to the financial support of the AWARE project [NSERC CRDPJ 462973-14](https://aware.forestry.ubc.ca/); grantee [Prof. Nicholas C. Coops](https://forestry.ubc.ca/faculty-profile/nicholas-coops/).

Development of the lidR package between 2018 and 2020 was made possible thanks to the financial support of the [Ministère des Forêts, de la Faune et des Parcs of Québec](https://mffp.gouv.qc.ca/).

The book is shared under [CC-BY-NC-SA 2.0](https://creativecommons.org/licenses/by-nc-sa/2.0/)


---

*This book was created to provide hands on descriptions and tutorials for using lidR and is not the formal package documentation. The comprehensive package documentation is shipped with the package.*
