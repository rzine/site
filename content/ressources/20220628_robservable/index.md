---
authors:
- jbarnier
- Kenton Russel

title: robservable

summary: This package allows the use of Observable notebooks (or parts of them) as htmlwidgets in R

date: "2022-06-28"

publication_types: ["6"]

thematics: ["11", "5"]

languages:
- "1" 

update:
- "1" 

sources:
- "0"

url_source: 'https://juba.github.io/robservable/' 
url_code: 'https://github.com/juba/robservable/' 

tags:
- Observable
- htmlwidget
- Shiny app
- Interactive plot
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Main features of the `robservable` package are :

- Display an entire published or shared notebook as an htmlwidget
- Display specific cells of a published or shared notebook as an htmlwidget
- Use R data to update cell values
- Add observers on cell values to communicate with a Shiny app
- Use inside Shiny app or Rmarkdown document (as any htmlwidget)
