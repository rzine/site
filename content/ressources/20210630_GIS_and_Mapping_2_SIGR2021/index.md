---
title: GIS and mapping (Part 2)
subtitle : A workshop on GIS and mapping with R
summary: Second part of a workshop on GIS and mapping with R at thematic school SIGR2021 


authors:
- jnowosad
- rlovelace


publication_types: ["12","13"]


sources:
- "9"

thematics: ["2","2"]


update:
- "0"

languages:
- "1"

projects:
- SIGR2021

date: "2021-06-30T02:00:00Z"

url_source: 'https://nowosad.github.io/SIGR2021/workshop1/workshop1_jn.html#1'
url_code: 'https://github.com/Nowosad/SIGR2021/tree/master/workshop1'

tags:
- SIGR2021
- mapping
- sf
- terra
- tmap
- tmaptools
- spData
- spDataLarge
- shape
- layer
- grids
- graticules
- layout


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Second part of a workshop on GIS and mapping with R at thematic school SIGR2021.



