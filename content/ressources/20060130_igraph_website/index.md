
---
# AUTHOR(S) 
authors:
- Gábor Csárdi
- Tamás Nepusz 

# DATE de soumission à Rzine
date: "2006-10-06"

# TITRE
title: Igraph

# RESUME COURT - une phrase
subtitle: The network analysis package
summary: The reference network analysis package
# TAGS
tags:
- network 

publication_types: ["6"]


thematics: ["18"]





sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

url_source: 'https://igraph.org/r/'


links:
- name : CRAN
  url: https://cran.r-project.org/web/packages/igraph/index.html

publishDate: "2005-11-25T00:00:00Z"
# Name of review or book if the publication has been published in
publication: Journal of Open Source Software
publication_short: JOSS
  

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Official website of **igraph**, which is **a collection of network analysis tools** with the emphasis on efficiency, portability and ease of use. igraph is open source and free. igraph can be programmed in R, Python, Mathematica, and C/C++


