---
title: The R Manuals
subtitle: by the R Development Core Team
summary: R Manuals edited by the R Development Core Team
date: "2001-01-01T00:00:00Z"

authors:
- R Core Team

publication_types: ["5"]


thematics: ["0","10","11","5"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers


tags:
- core team
- An Introduction to R
- R Data Import/Export
- R Installation and Administration
- Writing R Extensions
- The R language definition
- R Internals
- The R Reference Index

url_source: 'https://cran.r-project.org/manuals.html'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

The following manuals for R were created on Debian Linux and may differ from the manuals for Mac or Windows on platform-specific pages, but most parts will be identical for all platforms. The correct version of the manuals for each platform are part of the respective R installations. The manuals change with R, hence we provide versions for the most recent released R version (R-release), a very current version for the patched release version (R-patched) and finally a version for the forthcoming R version that is still in development (R-devel).

- *An Introduction to R*
- *R Data Import/Export*
- *R Installation and Administration*
- *Writing R Extensions*
- *The R language definition*
- *R Internals*
- *The R Reference Index*




**All theses R manuals are available [here](https://colinfay.me/r-manuals/) as bookdown format (by [Colin Fay](https://colinfay.me/))**  


