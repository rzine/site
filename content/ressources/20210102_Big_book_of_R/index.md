---
title: Big Book of R
subtitle: 150+ free R programming books
summary: The biggest collection of free R programming books

authors:
- Oscar Baruffa

publication_types: ["0"]


thematics: ["8"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

date: "2021-01-02T00:00:00Z"


tags:
- bookdown
- book
- community
- blogdown
- datascience
- visualization
- geospatial
- life science
- distributed computing
- machine learning
- network analysis
- report
- markdown
- package
- shiny
- sport analytics
- statistics
- time series
- forecasting
- teaching
- text analysis 
- version control
- Workflow


url_source: 'https://www.bigbookofr.com/'
url_code: 'https://github.com/oscarbaruffa/BigBookofR'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---



