---
title: Beyond Multiple Linear Regression
subtitle: Applied Generalized Linear Models and Multilevel Models in R
summary: Applied Generalized Linear Models and Multilevel Models in R

authors:
- Paul Roback
- Julie Legler

publication_types: ["5"]


sources :
- "0"

thematics: ["13"]


update :
- "1"

languages:
- "1"

projects:
- divers

date: "2021-01-26T00:00:00Z"
doi: '10.1201/9780429066665'

tags: 
- inference
- model
- regression
- linear
- multiple
- likelihood
- overdispersion
- generalized
- logistic
- correlation
- multilevel
- coveriance
- poisson


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://bookdown.org/roback/bookdown-BeyondMLR/'
url_code: 'https://github.com/proback/BeyondMLR'

---

**Beyond Multiple Linear Regression: Applied Generalized Linear Models and Multilevel Models in R** is designed for undergraduate students who have successfully completed a multiple linear regression course, helping them develop an expanded modeling toolkit that includes non-normal responses and correlated structure. Even though there is no mathematical prerequisite, the authors still introduce fairly sophisticated topics such as likelihood theory, zero-inflated Poisson, and parametric bootstrapping in an intuitive and applied manner. The case studies and exercises feature real data and real research questions; thus, most of the data in the textbook comes from collaborative research conducted by the authors and their students, or from student projects. Every chapter features a variety of conceptual exercises, guided exercises, and open-ended exercises using real data. After working through this material, students will develop an expanded toolkit and a greater appreciation for the wider world of data and statistical modeling.






