---
authors:
- brodrigues

date: "2023-06-20"

title: Building reproducible analytical pipelines with R

subtitle: How using a few ideas from software engineering can help data scientists, analysts and researchers write reliable code

summary: How using a few ideas from software engineering can help data scientists, analysts and researchers write reliable code

tags:
- reproducible
- pipeline
- git
- literate programming
- docker
- CI
- CD
- continuous integration
- continuous deployment
- targets
- package
- renv
- fusen

publication_types: ["5"]


sources:
- "0"

thematics: ["7"]

update :
- "1"

languages:
- "1"

url_source: 'https://raps-with-r.dev/'
url_code: 'https://github.com/b-rodrigues/rap4all'

links:
 - name: Download PDF
   url: https://raps-with-r.dev/Building-reproducible-analytical-pipelines-with-R.pdf
 - name: Download ePub
   url: https://raps-with-r.dev/Building-reproducible-analytical-pipelines-with-R.epub

featured: false
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This book will not teach you about the R programming language, machine learning, statistics or visualisation. The goal is to teach you a set of tools, practices and project management techniques that should make your projects easier to reproduce, replicate and retrace. These tools and techniques can be used right from the start of your project at a minimal cost, such that once you’re done with the analysis, you’re also done with making the project reproducible. Your projects are going to be reproducible simply because they were engineered, from the start, to be reproducible.

Building on your knowledge of R, you will learn about several packages to build reproducible analytical pipelines: `renv`, `targets`, `fusen` but also about trunk-based development with Git and Github, and Docker.

You can read the online version for free here: [https://raps-with-r.dev/](https://raps-with-r.dev/).


