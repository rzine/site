---

title: ExploratR
subtitle: Exploration uni- bi- et multivariée
summary: Outil interactif d'exploration statistique uni- bi- tri- et multi-variée.


authors:
- hcommenges

projects:
- divers


publication_types: ["1"]


thematics: ["12","2"]


languages:
- "0"

sources:
- "0"

update:
- "0"

date: "2016-09-26"


tags:
- univarié
- bivarié
- trivarié
- multivarié
- cartographie
- exploration
- régression
- ANOVA
- ANCOVA
- AFC
- Analyse factorielle
- CAH
- classification


url_source: 'https://analytics.huma-num.fr/geographie-cites/ExploratR/'
url_code: 'https://github.com/hcommenges/ExploratR'

doi: "10.5281/zenodo.155333"


featured: true
image:
  caption: 'Copyright'
  focal_point: "Center"
  preview_only: false
---


Cette application **permet d'explorer son propre tableau de données en quelques clics**. Elle permet de réaliser des **analyses univariées**, **bivariées**, **multivariées** et même de **cartographier les variables**.

Les résultats calculés (résidus d'une régression linéaire ou coordonnées factorielles issues d'une analyse en composantes principales) peuvent être téléchargés. Ces nouvelles variables peuvent être ré-utilisées dans l'analyse et l'utilisateur peut aussi récupérer son tableau enrichi.




