---
title: Modern Data Science with R (2nd edition)
summary: This is the online version of the 2nd edition of [Modern Data Science with R](https://www.routledge.com/Modern-Data-Science-with-R/Baumer-Kaplan-Horton/p/book/9780367191498)

authors:
- Benjamin S. Baumer
- Daniel T. Kaplan
- Nicholas J. Horton


publication_types: ["5"]


sources:
- "0"

thematics: ["0","10","11","12","13","18","2","3","4","7"]


update:
- "1"

languages:
- "1"

projects:
- divers

date: "2021-01-10T00:00:00Z"

url_source: 'https://mdsr-book.github.io/mdsr2e/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Modern Data Science with R** is a comprehensive data science textbook for undergraduates that incorporates statistical and computational thinking to solve real-world data problems. Rather than focus exclusively on case studies or programming syntax, this book illustrates how statistical programming in the state-of-the-art R/RStudio computing environment can be leveraged to extract meaningful information from a variety of data in the service of addressing compelling questions.

The second edition is updated to reflect the growing influence of the tidyverse set of packages. All code in the book has been revised and styled to be more readable and easier to understand. New functionality from packages like sf, purrr, tidymodels, and tidytext is now integrated into the text. All chapters have been revised, and several have been split, re-organized, or re-imagined to meet the shifting landscape of best practice. 



