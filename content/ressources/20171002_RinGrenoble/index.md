---
date: "2017-10-02T00:00:00Z"

title: R in Grenoble
subtitle: User group
summary: R user group in Grenoble

tags: 
- séminaire
- formation
- conférence
- évènement
- groupe
- user
- rgrenoble

authors:
- Florian Privé

publication_types: ["4","8","2"]

thematics: ["8"]

projects:
- ringrenoble

sources:
- "0"

update:
- "0"

languages: ["0", "1"]

url_source: 'https://r-in-grenoble.github.io/index.html/'
url_code: 'https://github.com/r-in-grenoble/r-in-grenoble.github.io'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


*R in Grenoble* est un Groupe d’utilisateur.trice.s Grenoblois (non-french speakers are all welcome).

Site web : [https://r-in-grenoble.github.io/index.html](https://r-in-grenoble.github.io/index.html)     



