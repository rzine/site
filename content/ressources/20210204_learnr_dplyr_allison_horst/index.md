---
title: Wrangling penguins - some basic data wrangling in R with dplyr
summary: A learnr tutorial to introduce some common functions in dplyr, part of the tidyverse.

authors:
- Allison Horst

publication_types: ["1","13"]


sources:
- "0"

thematics: ["10"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- dplyr
- filter
- select
- relocate
- rename
- mitat
- group_by
- across
- count
- case_when


date: "2021-02-04T00:00:00Z"

url_source: 'https://allisonhorst.shinyapps.io/dplyr-learnr/'
url_code: 'https://github.com/allisonhorst/dplyr-learnr'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

In this tutorial, we’ll learn some basic functions to help you work with data using functions in the `dplyr` package, part of the `tidyverse` in R.




