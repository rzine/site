---
title: Analyse de réseau avec igraph
subtitle: Introduction à l’analyse de réseau avec R 
summary: Introduction à l’analyse de réseau avec R 

authors:
- lbeauguitte 
- Megane Fernandez 
- hpecout 

publication_types: ["5", "6"]

thematics: ["18"]

sources:
- "12"

update:
- "0"

languages:
- "0"

date: "2023-05-23"


tags:
- igraph
- reseau
- network
- elementR

url_source: 'https://elementr.gitpages.huma-num.fr/session_reseau/intro_reseau_igraph/'
url_code: 'https://gitlab.huma-num.fr/elementr/session_reseau/intro_reseau_igraph'


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Manuel d'introduction à l'analyse de réseau avec le package `igraph`.     
Ce support a été construit dans le cadre d'une séance de formation proposée par le groupe d'utilisateur·rices [ElementR](https://elementr.netlify.app/).

Page web de la séance : [https://elementr.netlify.app/posts/seance7_igraph](https://elementr.netlify.app/posts/seance7_igraph)




