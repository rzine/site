---
title: Writing reproducible research papers with R Markdown
subtitle: A Two-Day Workshop
summary: Two-Day Workshop will introduce participants to the fundamentals and applications of R Markdown

date: "2020-11-15T00:00:00Z"

authors:
- Resul Umit

publication_types: ["13"]


sources:
- "0"

thematics: ["1"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- rmarkdown
- git


url_source: 'https://resulumit.com/teaching/rmd_workshop.html#1'

links:
- name: Syllabus
  url: https://resulumit.com/syllabi/20autumn_rmd.pdf
- name: Materials
  url: https://github.com/resulumit/rmd_workshop


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This workshop will introduce participants to the fundamentals and applications of R Markdown — a relatively new document type to analyse data and communicate results, in a reproducible and efficient manner. 

Although one can produce various outputs with R Markdown, such as presentations and websites, here we will focus on what it offers for the process of writing research papers.  By combining code, text, and resultsin a single document, *Rmarkdown* allows automation of otherwise manual — therefore tedious, costly to repeat, and error-prone — steps in writing research papers: creating tables and figures, updating themas the data and/or analyses change, keeping track of all these changes, switching between different output formats (e.g., Word or PDF), and more. 

Therefore, those interested in writing research-based papers, from essays to journal articles, are likely tobenefit from this course.





