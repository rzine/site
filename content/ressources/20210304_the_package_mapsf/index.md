---
title: Le package mapsf
subtitle: Cartographie thématique
summary: Les principales fonctionnalités du package `mapsf`


authors:
- tgiraud

publication_types: ["11"]


sources:
- "0"

thematics: ["2"]


update:
- "1"

languages:
- "0"
- "1"

projects:
- divers

tags:
- map
- maps
- cartography
- choropleth
- typology
- layout
- flow
- symbol
- proportional
- discontinuities
- grid
- palette
- layout
- scale
- north
- arrow
- label
- legend
- thematic
- mapsf
- sf
- mf_map
- mf_credits
- mf_arrow
- mf_shadow
- mf_label
- mf_scale
- mf_annotation
- theme
- mf_inset_on
- mf_init
- mf_layout
- mf_worldmap
- carte
- cartographie
- cartography
- typologie
- habillage
- flux
- symbole
- proportionel
- discontinuité
- palettes
- échelle
- fléche
- thématique
- thème

date: "2021-03-04T00:00:05Z"

url_source: 'https://rgeomatic.hypotheses.org/2077'


links:
- name: English version
  url: https://rgeomatic.hypotheses.org/2212

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



Créez et intégrez des cartes thématiques dans votre flux de travail R. Ce package permet de concevoir diverses représentations cartographiques telles que des symboles proportionnels, des cartes choroplèthes ou typologiques. Il offre également plusieurs fonctions permettant d'afficher des éléments de mise en page qui améliorent la présentation graphique des cartes (barre d'échelle, flèche du nord, titre, étiquettes...). `mapsf` cartographie les objets `sf` sur des graphiques de base.


**English :** Thematic Mapping - Main features of the `mapsf` package

`mapsf` helps to design various cartographic representations such as proportional symbols, choropleth or typology maps. It also offers several functions to display layout elements that improve the graphic presentation of maps (e.g. scale bar, north arrow, title, labels). `mapsf` maps `sf` objects on base graphics.



