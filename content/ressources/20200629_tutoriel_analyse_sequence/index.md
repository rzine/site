---
title: Tutoriel d’analyse de séquences
subtitle: 
summary: Tutoriel conçu pour accompagner le manuel ***L'analyse statistique des trajectoires. Typologies de séquences et autres approches***.

authors:
- nrobette

projects:
- divers

sources:
- "0"

thematics: ["16"]


languages:
- "0"

update:
- "1"

publication_types: ["13"]


date: "2023-04-02T00:00:00Z"

tags:
- Tutoriel
- analyse de séquence
- TraMineR
- TraMineRextras
- cluster
- WeightedCluster
- FactoMineR
- ade4
- RColorBrewer
- questionr
- GDAtools
- dplyr
- purrr
- ggplot2
- seqhandbook
- trajact

url_source: 'https://nicolas-robette.github.io/seqhandbook/articles/Tutoriel.html'
url_code: 'https://github.com/nicolas-robette/seqhandbook/blob/master/vignettes/articles/Tutoriel.Rmd'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Tutoriel sur l'analyse de séquence, qui accompagne le manuel : **"L'analyse statistique des trajectoires. Typologies de séquences et autres approches"**.





