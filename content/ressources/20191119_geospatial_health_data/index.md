---
title: Geospatial Health Data - Modeling and Visualization with R-INLA and Shiny
subtitle: 
summary: This book describes spatial and spatio-temporal statistical methods and visualization techniques to analyze georeferenced health data in R

authors:
- Paula Moraga


publication_types: ["5"]


sources :
- "0"

thematics: ["19","2","5"]


update :
- "0"

languages:
- "1"

projects:
- divers

tags:
- sante
- health
- INLA
- Bayesian inference
- spatial
- spatio-temporal
- geostatistical
- dashboard
- markdown
- shiny
- disease


date: "2019-11-19T00:00:00Z"
doi: "10.1201/9780429341823 "

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://www.paulamoraga.com/book-geospatial/index.html'

links:
- name: Materials
  url: https://www.paulamoraga.com/tutorial-open-spatial-data/


---


Geospatial health data are essential to inform public health and policy. These data can be used to quantify disease burden, understand geographic and temporal patterns, identify risk factors, and measure inequalities. Geospatial Health Data: Modeling and Visualization with R-INLA and Shiny describes spatial and spatio-temporal statistical methods and visualization techniques to analyze georeferenced health data in R. The book covers the following topics:

- Manipulating and transforming point, areal, and raster data,
- Bayesian hierarchical models for disease mapping using areal and geostatistical data,
- Fitting and interpreting spatial and spatio-temporal models with the integrated nested Laplace approximation (INLA) and the stochastic partial differential equation (SPDE) approaches,
- Creating interactive and static visualizations such as disease maps and time plots,
- Reproducible R Markdown reports, interactive dashboards, and Shiny web applications that facilitate the communication of insights to collaborators and policymakers.

The book features fully reproducible examples of several disease and environmental applications using real-world data such as malaria in The Gambia, cancer in Scotland and USA, and air pollution in Spain. Examples in the book focus on health applications, but the approaches covered are also applicable to other fields that use georeferenced data including epidemiology, ecology, demography or criminology. The book provides clear descriptions of the R code for data importing, manipulation, modelling, and visualization, as well as the interpretation of the results. This ensures contents are fully reproducible and accessible for students, researchers and practitioners.




