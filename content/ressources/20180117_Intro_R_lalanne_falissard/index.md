---
title: Tutoriel d'introduction au langage R
subtitle: 
summary: Présentation succincte des bases du langage R pour l'analyse statistique interactive de données

authors:
- clalanne
- bfalissard

publication_types: ["13"]


thematics: ["0","10","11","12"]


sources :
- "0"

update :
- "0"

languages:
- "0"

projects:
- divers

date: "2018-01-17T00:00:00Z"

url_source: 'https://r.developpez.com/tutoriels/introduction-r/'
url_pdf: 'http://r.developpez.com/tutoriels/introduction-r/introduction-a-R.pdf'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Ce tutoriel constitue une présentation succincte des bases du langage R pour l'analyse statistique interactive de données. En particulier, on s'intéressera à la représentation et la manipulation des données numériques et qualitatives, à l'importation de source de données externes et à la sauvegarde d'une session de travail. Un glossaire des principales commandes est également fourni en annexe, ainsi qu'une liste des commandes utiles pour la modélisation.
