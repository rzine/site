---
title: Créer son site web avec RStudio, blogdown, github et netlify
subtitle: 
summary: Comment créer son site web en moins d'une heure avec RStudio et le package blogdown 

authors:
- ogimenez

publication_types: ["9"]


thematics: ["1","5"]


sources :
- "0"

update :
- "0"

languages:
- "0"

projects:
- divers

date: "2020-09-29T00:00:00Z"

tags:
- blogdown
- hugo
- academic
- github
- netlify
- blog
- site web


url_source: 'https://www.youtube.com/watch?v=4OUEss2XF7E&t=1s'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


Comment créer son site web en moins d'une heure avec RStudio et le package [blogdown](https://bookdown.org/yihui/blogdown/​.) 
