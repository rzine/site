---
# Author(s)
authors:
- Rafael A. Irizarry



# Date
date: "2021-07-03"


# Title
title: Introduction to Data Science
subtitle: Data Analysis and Prediction Algorithms with R

# Short summary
summary: Data Analysis and Prediction Algorithms with R

# Tags
tags:
- tidyverse
- ggplot2
- visualization
- statistics
- wrangling
- rstudio
- machine learning
- text mining
- caret
- git
- project
- rstudio



publication_types: ["5", "13"]

sources:
- "0"

thematics: ["0", "10", "11", "12", "13", "17", "3", "7"]

update:
- "1"

languages:
- "1"


# Link
url_source: 'https://rafalab.github.io/dsbook/'
url_code: 'https://github.com/rafalab/dsbook'
url_pdf: 'https://leanpub.com/datasciencebook'



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

This book introduces concepts and skills that can help you tackle real-world data analysis challenges. It covers concepts from probability, statistical inference, linear regression, and machine learning. It also helps you develop skills such as R programming, data wrangling with **dplyr**, data visualization with **ggplot2**, algorithm building with **caret**, file organization with UNIX/Linux shell, version control with Git and GitHub, and reproducible document preparation with **knitr** and R markdown. The book is divided into six parts: **R**, **Data Visualization**, **Data Wrangling**, **Statistics with R**, **Machine Learning**, and **Productivity Tools**. Each part has several chapters meant to be presented as one lecture and includes dozens of exercises distributed across chapters.

