---
title: Twitter for R programmers
summary: The aim of this book is to give people a basic introduction to Twitter, that allows to take the real-time pulse of the community R

authors:
- Oscar Baruffa
- Veerle van Son

publication_types: ["5"]


sources:
- "0"

thematics: ["8","9"]


update:
- "1"

languages:
- "1"

projects:
- divers


date: "2021-01-02T00:00:00Z"

url_source: 'https://www.t4rstats.com/'
url_code: 'https://github.com/oscarbaruffa/Book-Twitter-for-R'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


The R community is very active on Twitter. You can learn a lot about the language, about new approaches to problems, make friends and even land a job or next contract. It’s a real-time pulse of the R community...




