---
title: Reprex, ou comment demander de l’aide efficacement
summary: Billet issu du [blog ThinkR](https://thinkr.fr/blog/), pour construire un exemple reproductible et solutionner son problème avec l'aide de la communauté. 

authors:
- Elena Salette

publication_types: ["11"]


sources:
- "0"

thematics: ["8","7"]


update:
- "0"

languages:
- "0"


projects:
- divers

date: "2020-07-30T00:00:00Z"

tags:
- reprex
- aide
- exemple reproductible
- formattage de code


url_source: 'https://thinkr.fr/reprex-ou-comment-demander-de-laide-efficacement/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

En tant que fervent utilisateur de R, vous avez sûrement déjà dû vous appuyer sur cette belle communauté mondiale grâce à laquelle on a quasiment toujours une réponse à nos questions R. Mais dans le cas où, après 1h de recherche intensive, vous n’avez toujours pas réussi à solutionner votre problème, vous commencez à envisager de demander de l’aide, que ce soit sur les forums, Stackoverflow, GitHub ou encore vos workspaces Slack préférés. Et si vous n’avez pas bien préparé votre question, il y a de grandes chances qu’on vous demande si vous n’avez pas un “reprex”. Un quoi ? Un exemple reproductible. Explications…



