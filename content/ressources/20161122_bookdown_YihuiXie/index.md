
---
# AUTHOR(S) 
authors:
- Yihui Xie


# DATE de soumission à Rzine
date: "2016-11-21"

# TITRE
title: Bookdown
subtitle: Authoring Books and Technical Documents with R Markdown

# RESUME COURT - une phrase
summary: Authoring Books and Technical Documents with R Markdown


# TAGS
tags:
- bookdown
- markdown
- manual


# TYPE DE PUBLICATION
# manual = 0
# sheet = 1
# course = 2
# exercise = 3
# web = 4
# tool = 5
# article = 6
# annex = 7
# conf = 8
# uncat = 9
publication_types: ["5"]


thematics: ["1"]


sources :
- "6"

update :
- "1"

languages:
- "1"

# PROJET
# Les projets existants :
# Manuel CIST'R = manuel
# Fiche CIST'R = fiche
# Exercice CIST'R = exercice
# Sources diverses = divers
# Resources web (blog et site) = web
# GDR Analyse de réseau en SHS = GDR_ARSHS
# Axe Information territoriale du CIST = INFTER

projects:
- rseries

##### LINKS #####
# DOCUMENTATION
url_source: 'https://bookdown.org/yihui/bookdown/'

url_code: 'https://github.com/rstudio/bookdown'

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
publishDate: "2016-12-09T00:00:00Z"
# Name of review or book if the publication has been published in
publication: bookdown - Authoring Books and Technical Documents with R Markdown
publication_short: bookdown
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This short book introduces an R package, **bookdown**, to change your workflow of writing books. It should be technically easy to write a book, visually pleasant to view the book, fun to interact with the book, convenient to navigate through the book, straightforward for readers to contribute or leave feedback to the book author(s), and more importantly, authors should not always be distracted by typesetting details.

The **bookdown** package is built on top of R Markdown (http://rmarkdown.rstudio.com), and inherits the simplicity of the Markdown syntax (you can learn the basics in five minutes; see Section 2.1), as well as the possibility of multiple types of output formats (PDF/HTML/Word/…). It has also added features like multi-page HTML output, numbering and cross-referencing figures/tables/sections/equations, inserting parts/appendices, and imported the GitBook style (https://www.gitbook.com) to create elegant and appealing HTML book pages. This book itself is an example of how you can produce a book from a series of R Markdown documents, and both the printed version and the online version can look professional. You can find more examples at [https://bookdown.org.](https://bookdown.org/)

Despite the package name containing the word “book”, bookdown is not only for books. The “book” can be anything that consists of multiple R Markdown documents meant to be read in a linear sequence, such as course handouts, study notes, a software manual, a thesis, or even a diary. In fact, many bookdown features apply to single R Markdown documents as well (see Section 3.4).


