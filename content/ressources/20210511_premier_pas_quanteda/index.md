---
title: Premiers pas avec {Quanteda} pour l’analyse linguistique
summary: Cet exercice a pour but de vous familiariser avec le module Quanteda pour l’analyse linguistique

authors:
- André Ourednik

publication_types: ["11","13"]

sources:
- "0"

thematics: ["17"]

update:
- "1"

languages:
- "0"

projects:
- divers

date: "2021-05-11T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  
tags:
- quanteda
- readtext
- udpipe
- quanteda.textplots
- quanteda.textstats
- ggplot2
- corpus
- token
- tokenisation
- Lemmatisation
- DFM
- document feature matrix
- n-grammes
- wordcloud
- nuage de mots
- cooccurrence



url_source: 'https://ourednik.info/maps/2020/12/06/premiers-pas-avec-le-module-r-quanteda-pour-lanalyse-linguistique/'

---


Cet exercice a pour but de vous familiariser avec le package [`Quanteda`](https://quanteda.io/) pour l’analyse linguistique. Il présuppose que vous avez fait les [premiers pas avec R et Rstudio](https://ourednik.info/maps/2019/03/14/premiers-pas-avec-r-et-rstudio/). 
