---
authors:
- Thomas Mock

title: Get Started with Quarto

subtitle: Workshop for those who have no experience with Quarto

summary: This workshop is designed for those who have no or little prior experience with Quarto

date: "2022-08-09"

publication_types: ["13", "8", "12"]


thematics: ["1", "7", "9"]

languages:
- "1"

update:
- "1" 

sources:
- "0" 

url_source: 'https://jthomasmock.github.io/quarto-2hr-webinar/'
url_code: 'https://github.com/jthomasmock/quarto-2hr-webinar'

links:
- name: Part I
  url: https://jthomasmock.github.io/quarto-2hr-webinar/materials/01-intro-quarto.html#/TitleSlide
- name: Part II
  url: https://jthomasmock.github.io/quarto-2hr-webinar/materials/02-authoring.html#/authoring-quarto

tags:
- quarto
- notebook
- literate programming
- qmd
- workshop
- document
- Rstudio
- julia
- python
- javascript

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This workshop is designed for those who have no or little prior experience with Quarto. Quarto is the next generation of RMarkdown for publishing, including dynamic and static documents and multi-lingual programming language support. With Quarto you can create documents, books, presentations, blogs or other online resources.
