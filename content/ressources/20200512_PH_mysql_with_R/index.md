---
title: Introduction to MySQL with R
summary: This lesson will help you store large amounts of historical data in a structured manner, search and filter that data, and visualize some of the data as a graph. Lesson from [The Programming Historian](https://programminghistorian.org/)

authors:
- Jeff Blackadar

publication_types: ["10","13"]

sources:
- "10"

thematics: ["7"]


update:
- "1"

languages:
- "1"


tags:
- MySQL
- Community Server
- database
- RMariaDB
- Programming Historian
- PH
  
projects:
- programming_historian



date: "2018-05-03T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  



doi: '10.46430/phen0076'
url_source: 'https://programminghistorian.org/en/lessons/getting-started-with-mysql-using-r'



---

This lesson will help you store large amounts of historical data in a structured manner, search and filter that data, and visualize some of the data as a graph. Lesson from [The Programming Historian](https://programminghistorian.org/).



