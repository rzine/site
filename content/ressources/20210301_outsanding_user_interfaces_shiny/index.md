
---
title: Outstanding User Interfaces with Shiny
summary: Book under active development - Manipulate Shiny tags, Harness the power of CSS and JavaScript, import and convert existing web frameworks... 

authors:
- David Granjon

publication_types: ["5"]


sources:
- "0"

thematics: ["5"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- shiny
- CSS
- template
- charpente
- shinyMobile
- widget
- reactR
- SASS
- htmltools


date: "2021-03-01T00:00:00Z"

url_source: 'https://unleash-shiny.rinterface.com/index.html'
url_code: 'https://github.com/DivadNojnarg/outstanding-shiny-ui'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This book will help you to:

- Manipulate Shiny tags from R to create custom layouts.
- Harness the power of CSS and JavaScript to quickly design apps standing out from the pack.
- Discover the steps to import and convert existing web frameworks like Bootstrap 4, framework7 and more Learn how Shiny internally deals with inputs. 
- Learn more about less documented Shiny mechanisms (websockets, sessions, ...)



