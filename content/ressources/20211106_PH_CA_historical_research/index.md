---
title: Correspondence Analysis for Historical Research with R 
summary: This tutorial explains how to carry out and interpret a correspondence analysis, which can be used to identify relationships within categorical data. Lesson from [The Programming Historian](https://programminghistorian.org/)

authors:
- Ryan Deschamps

publication_types: ["10","13"]

sources:
- "10"

thematics: ["12"]


update:
- "1"

languages:
- "1"


tags:
- Correspondence Analysis
- CA
- FactoMineR
- factoextra
- Programming Historian
- PH
  

projects:
- programming_historian



date: "2017-09-13T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  


doi: '10.46430/phen0062'
url_source: 'https://programminghistorian.org/en/lessons/correspondence-analysis-in-R'



---

This tutorial explains how to carry out and interpret a correspondence analysis, which can be used to identify relationships within categorical data. Lesson from [The Programming Historian](https://programminghistorian.org/).



