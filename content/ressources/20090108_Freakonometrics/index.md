---
# Author(s)
authors:
- acharpentier


# Date
date: "2009-01-08"


# Title
title: Freakonometrics


# Short summary
summary: An Open Lab-Notebook Experiment (personal blog)


# Tags
tags:
- economy
- actuarial
- statistics
- econometrics
- actuaries


publication_types: ["8"]


sources:
- "0"

thematics: ["1","10","11","12","13","14","15","18","19","3"]


update:
- "1"

languages:
- "1"
- "0"


# Link
url_source: 'https://freakonometrics.hypotheses.org'



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

##### Academic blog of Arthur Charpentier

*Some sort of unpretentious (academic) blog, by a surreptitious economist and born-again mathematician. A blog activist, and an actuary, too. Always curious. Because academics are probably more than the sum of our publication lists, grants and conference talks...*





