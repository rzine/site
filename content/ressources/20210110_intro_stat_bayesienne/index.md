---
authors:
- Ladislas Nalborczyk

title: Introduction à la modélisation statistique bayésienne
subtitle : Un cours avec R, Stan, et brms
summary: L’objectif de cette formation est de vous faire découvrir la modélisation statistique bayésienne.

date: "2021-01-10"

publication_types: ["13"]

thematics: ["12", "13", "14", "3", "4"]
 
languages:
- "0"

update:
- "1"

sources:
- "0"


url_source: 'https://lnalborczyk.github.io/IMSB2022/'



tags:
- Monte Carlo
- Chaîne
- Markov
- Inférence
- Statistique
- Modélisation
- Régression
- Multiniveaux
- Bivarié
- Modèle linéaire
- brms
- Binomial


  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

L’objectif de cette formation est de vous faire découvrir la modélisation statistique bayésienne. Les concepts et outils qui seront introduits tout au long de la formation seront illustrés par des cas concrets d’analyse de données. Ce cours est construit autour du langage `R` et de l’utilisation du package `brms`, une interface au langage probabiliste `Stan`. Par conséquent, il est indispensable d’avoir quelques connaissances élémentaires du langage `R`.

La formation est proposée sous une double étiquette Collège doctoral / MaiMoSiNE (Maison de la Modélisation et de la Simulation) avec une priorité d’accès aux étudiant.e.s du collège doctoral de Grenoble.
