---
authors:
- Robin Genuer
- Boris Hejblum

title: Formation R avancée

subtitle: Outils de développement et de performance 

summary: Outils de développement et de performance  

date: "2022-06-08"

publication_types: ["13", "5"]

thematics: ["5", "7"]

languages: 
- "0" 

update:
- "1"

sources:
- "0"

url_source: 'https://r-dev-perf.borishejblum.science/'

tags:
- Package
- git
- Rcpp
- Parallélisation
- Optimisation
- Contrôle de version
- Temps de calcul

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Le but principal de cette formation est de donner des outils pour faciliter le développement de code avec R : Construire un package, tracer les changements et partager son code, mesurer le temps de calcul, profiler le code, utiliser `Rcpp` pour optimiser du code en `C++`, paralléliser son code.
