---
authors:
- Michael Matiu

title: How to manage code and data for open science (and yourself)

summary: Good practices for building a R project

date: "2022-05-26"

publication_types: ["12"] 

thematics: ["0", "7"] 

languages:
- "1" 

sources:
- "0"

update:
- "0" 

url_source: 'https://mitmat.github.io/slides/2022-05-26-egu/code-data-open-science.html#1'
url_code: 'https://github.com/mitmat/slides' 

tags:
- Good practice
- R project
- Open Science
- Workflow
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Good practices reminder when launching a R project: set up a working directory, manage data, code advices, share efficiently the project.  
