---
title: afrimapr intro to spatial data
summary: Interactive [learnr](https://rstudio.github.io/learnr/) tutorials to introduce spatial data types, the packages sf, tmap and raster, with data from [afrilearndata](https://github.com/afrimapr/afrilearndata). Also **available by using** [**afrilearnr**](https://github.com/afrimapr/afrilearnr) **package** 
subtitle: learnr tutorial by Afrimapr

authors:
- Laurie Baker
- Andy South

publication_types: ["1", "13"]


sources:
- "1"

thematics: ["0"]


update:
- "1"

languages:
- "1"


projects:
- afrimapr

date: "2021-02-04T03:00:00Z"

tags:
- africa
- afrique
- map
- tmap
- carte
- cartographie
- raster
- sf
- afrilearndata
- africapitals
- afrihighway
- africountries
- rgdal
- africontinent
- mapview

url_source: 'https://andysouth.shinyapps.io/intro-to-spatial-r/'
url_code: 'https://github.com/afrimapr/afrilearnr'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Interactive [learnr](https://rstudio.github.io/learnr/) tutorials produced by [afrimapr](https://afrimapr.github.io/afrimapr.website/) to introduce spatial data types, the packages sf, tmap and raster, with data from [afrilearndata](https://github.com/afrimapr/afrilearndata). 

You can also use this tutorial by installing [**afrilearnr**](https://github.com/afrimapr/afrilearnr) **package**.


