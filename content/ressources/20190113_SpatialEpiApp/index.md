---
title: SpatialEpiApp
subtitle: An Shiny web application
summary: Shiny web application for the analysis of spatial and spatio-temporal disease data

authors:
- Paula Moraga 

publication_types: ["1"]


sources :
- "0"

thematics: ["19"]


update :
- "1"

languages:
- "1"

projects:
- divers

tags:
- shiny
- spatial analysis
- spatio-temporal
- disease

date: "2019-01-13T00:00:00Z"
doi: "10.1016/j.sste.2017.08.001"


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://paulamoraga.shinyapps.io/spatialepiapp'
url_code: 'https://github.com/Paula-Moraga/SpatialEpiApp'

links:
- name: slide
  url: http://wp.lancs.ac.uk/research-software-forum/files/2017/12/PaulaMoragaResearchSoftwareForum.pdf


---


The R package SpatialEpiApp runs a Shiny web application that allows to visualize spatial and spatio-temporal disease data, estimate disease risk, and detect clusters. The application incorporates modules for

- Disease risk estimation using Bayesian hierarchical models with [INLA](https://www.r-inla.org/),
- Detection of clusters using the scan statistics implemented in [SaTScan](https://www.satscan.org/),
- Interactive visualizations such as maps supporting padding and zooming and tables that allow for filtering,
- Generation of reports containing the analyses performed.

SpatialEpiApp allows user interaction and creates interactive visualizations by using the R packages Leaflet for rendering maps, dygraphs for plotting time series, and DataTables for displaying data objects. It also enables the generation of reports containing the analyses performed by using RMarkdown.

SpatialEpiApp may be useful for many researchers working in health surveillance lacking the adequate statistical and programming skills to effectively use the statistical software required to conduct the statistical analyses. With SpatialEpiApp, we simply need to upload the map and data and then click the buttons that create the input files required, execute the software and process the output to generate tables of values and plots with the results.

This [blog post](https://www.paulamoraga.com/blog/2018/01/04/2018-01-04_spatialepiapp/), and the package [vignette](https://cran.r-project.org/web/packages/SpatialEpiApp/vignettes/manual.pdf) can be checked for more details about its use, methods and examples. Data for running examples are in [https://Paula-Moraga.github.io/software](https://Paula-Moraga.github.io/software)



