---
title: Le logiciel R - Une (rapide) présentation
subtitle: 
summary: Journée de la donnée 2019 - Commissariat général au développement durable


authors:
- mlethrosne
- mtheuliere
- Marouane Zellou

publication_types: ["12"]


thematics: ["0"]


sources :
- "0"

update :
- "0"

languages:
- "0"


projects:
- divers

date: "2019-11-18T00:00:00Z"

tags:
- chrome
- rstudio
- node.js
- Web Scraping

url_source: 'https://maeltheuliere.github.io/presentation_g2r_2019/#1'
url_code: 'https://github.com/MaelTheuliere/presentation_g2r_2019'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


