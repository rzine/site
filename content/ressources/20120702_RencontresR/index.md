---
date: "2012-07-02T00:00:00Z"

title: Rencontres R
subtitle: Conférences R nationales et interdisciplinaires
summary: Conférences R nationales et interdisciplinaires

authors:
- Marie Chavent 
- Stéphane Dray
- Rémy Drouilhet
- Robin Genuer
- Francois Husson
- Julie Josse
- Benoit Liquet

tags: 
- séminaire
- conférence
- évènement
- rencontre

publication_types: ["2",  "9"]

thematics: ["8"]

projects:
- rencontresr

sources:
- "11"

update:
- "1"

languages:
- "0"


url_source: 'https://rr2023.sciencesconf.org/'
url_code: 'https://github.com/Rencontres-R'

links:
- name: Youtube
  url: https://www.youtube.com/@RencontresR/



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



Les Rencontres R existent depuis 2012 et fournissent à l’échelle nationale un lieu d’échange autour de la galaxie R dans différentes disciplines (visualisation, analyse de données, machine learning, bioinformatique, statistiques, calcul haute performance, applications web, etc.) et dans différentes communautés (académique, entreprise, formation).

Les Rencontres R passées :

- [Rencontres R 2023, Paris](https://rr2023.sciencesconf.org/)
- [Rencontres R 2021, Paris](https://paris2021.rencontresr.fr/)
- [Rencontres R 2020, Paris](https://paris2020.rencontresr.fr/) **[REPORTEES à 2021]**
- [Rencontres R 2018, Rennes](https://r2018-rennes.sciencesconf.org/)
- [Rencontres R 2017, Anglet](https://angletr2017.sciencesconf.org/)
- [Rencontres R 2016, Toulouse](https://r2016-toulouse.sciencesconf.org/)
- [Rencontres R 2015, Grenoble](https://r2015-grenoble.sciencesconf.org/)
- [Rencontres R 2014, Montpellier](https://r2014-mtp.sciencesconf.org/)
- [Rencontres R 2013, Lyon](https://r2013-lyon.sciencesconf.org/index.html)
- [Rencontres R 2012, Bordeaux](http://r2012.bordeaux.inria.fr/index.html)

Chaîne Youtube : [https://www.youtube.com/@RencontresR/](https://www.youtube.com/@RencontresR/)

