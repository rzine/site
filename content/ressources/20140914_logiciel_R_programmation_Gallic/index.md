---
title: Logiciel R et programmation
subtitle: Supports de cours de MASTER 1 "Statistique & Économétrie", Université de Rennes 1
summary: Supports de cours de MASTER 1 "Statistique & Économétrie", Université de Rennes 1

authors:
- egallic


publication_types: ["5"]


thematics: ["0","10","11","12","13"]


languages:
- "0"

sources:
- "0"

update:
- "0"

projects:
- divers

date: "2014-09-14T00:00:00Z"


tags:
- introduction
- fonction
- test
- while
- boucle
- lubridate
- data.table
- graphique
- ggplot2
- régression linéaire
- expressions régulières
- regex
- grep
- stringr
- texreg
- gdata
- readxl
- dplyr
- devtools
- modify
- tidyr
- reshape2
- beepr
- plyr
- doMC
- magrittr
- scale
- grid
- gridExtra
- rworldmap
- rgdal
- maptools
- MASS
- rgl

url_source: 'http://egallic.fr/Enseignement/R/m1_stat_eco_logiciel_R.pdf'





featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


Ces notes de cours ont été réalisées dans le cadre d’un enseignement d’introduction à R adressé à des étudiants du Master de Statistique et Économétrie de la Faculté des Sciences Économiques de l’Université de Rennes 1.

#### Objectifs

Cet ouvrage a pour but l’initiation au logiciel statistique et au langage informatique R, afin d’être capable de s’en servir de manière efficace et autonome. Le lecteur peut exécuter tous les exemples fournis (et est vivement encouragé à le faire). Des exercices viennent clore certains chapitres. Les corrections sont disponibles en ligne à l’adresse suivante : [http://egallic.fr/enseignement/](http://egallic.fr/enseignement/)

