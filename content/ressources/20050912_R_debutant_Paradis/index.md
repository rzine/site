---
title:  R pour les débutants
subtitle: Manuel d'introduction à R
summary: Publication historique de référence. L'un des premiers manuels francophones d'introduction à R.

authors:
- Emmanuel Paradis

publication_types: ["5"]


thematics: ["0","10", "11", "12"]


languages:
- "0"


sources :
- "0"

update :
- "1"


projects:
- divers
date: "2005-09-12T00:00:00Z"
doi: ""


tags:
- débutant
- introduction
- concept
- objet
- graphique
- statistique


url_source: 'https://r.developpez.com/tutoriels/r/debutants/'
url_pdf: 'http://r.developpez.com/tutoriels/r/debutants/debutants.pdf'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

Le but du présent document est de fournir un point de départ pour les novices intéressés par R. J'ai fait le choix d'insister sur la compréhension du fonctionnement de R, bien sûr dansle but d'une utilisation de niveau débutant plutôt qu'expert. Les possibilités offertes par R étant très vastes, il est utile pour le débutant d'assimiler certaines notions et concepts afin d'évoluer plus aisément par la suite. J'ai essayé de simplifier au maximum les explications pour les rendre accessibles à tous, tout en donnant les détails utiles, parfois sous forme de tableaux...
