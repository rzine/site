---
title: Leaflet for R
subtitle: Package to integrate and control Leaflet maps
summary: This R package makes it easy to integrate and control Leaflet maps in R

authors:
- Rstudio

publication_types: ["6"]


thematics: ["2"]



sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

date: "2016-03-25T00:00:00Z"


tags:
- map
- carte
- interactive
- cartography
- osm
- Third-Party
- WMS
- shape
- GeoJSON
- raster
- TopoJSON
- shiny
- colors
- Legend
- Choropleth
- projection
- Leaflet
- javascript



url_source: 'https://rstudio.github.io/leaflet/'
url_code: 'https://github.com/rstudio/leaflet'

links:
- name: cheatsheet
  url: https://raw.githubusercontent.com/rstudio/cheatsheets/master/leaflet.pdf
 

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


[Leaflet](https://leafletjs.com/) is one of the most popular open-source JavaScript libraries for interactive maps. It’s used by websites ranging from The New York Times and The Washington Post to GitHub and Flickr, as well as GIS specialists like [OpenStreetMap](https://www.openstreetmap.org/#map=6/46.449/2.210), [Mapbox](https://www.mapbox.com/), and [CartoDB](https://carto.com/).


