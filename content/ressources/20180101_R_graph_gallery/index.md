---
title: The R Graph Gallery
summary: Une collection de plusieurs centaines de graphiques, organisés par section et présentés avec leur code reproductible.

authors:
- yholtz

# Type de la publication 
publication_types: ["0", "8"]


sources :
- "0"

thematics: ["11"]


update :
- "1"

languages:
- "1"

projects:
- divers

tags:
- histogram
- boxplot
- ridgeline
- scatter plot
- heatmap
- correlogram
- bubble
- barplot
- radar
- wordcloud
- treemap
- ggplot2
- ggplot
- lineplot
- maps
- network
- flows
- animation
- plot

date: "2018-01-01T00:00:00Z"
doi: ""
 
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://www.r-graph-gallery.com/'

---

Cette galerie présente environ 400 graphiques, toujours fournis avec un code reproductible et des explications. Les graphiques sont organisés par famille et par type.


