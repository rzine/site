---
date: "2017-03-29T00:00:00Z"

title: Forum CIRAD
summary: Forum francophone d'échange autour du logiciel de calcul statistique R

authors:
- Centre de coopération internationale en recherche agronomique pour le développement

tags: 
- forum 
- Bayésien 
- Bioinformatique 
- Calcul distribué 
- Graphisme et visualisation 
- Optimisation 
- Programmation 
- Spatial 
- Statistique 

publication_types: ["3"]

thematics: ["8"]

update:
- "1"

languages:
- "0"

sources:
- "0"

url_source: 'http://forums.cirad.fr/logiciel-R/index.php'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Ce site se présente comme **Un forum francophone d'échange autour du logiciel de calcul statistique R** dans le domaine de la recherche agronomique tropicale

Il constitue l'un des sites de **Groupe des utilisateurs du logiciel R** (RUGs) français.

Il permet de poser des questions (et d'avoir des réponses) au travers d'un forum très actif et de trouver des annonces, des fiches, des packages et fonctions.

Pour y accéder : [http://forums.cirad.fr/logiciel-R/index.php](http://forums.cirad.fr/logiciel-R/index.php)

