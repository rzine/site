---
title: Text analysis in R
subtitle:  
summary: Article publied in *Communication Methods and Measures*, 2017

authors:
- Kasper Welbers
- Wouter Van Atteveldt
- Kenneth Benoit

publication_types: ["10"]


thematics: ["17"]


sources :
- "0"

update :
- "0"

languages:
- "1"

projects:
- divers

date: "2017-11-02T00:00:00Z"
doi: 10.1080/19312458.2017.1387238

tags:
- Communication


url_source: 'http://eprints.lse.ac.uk/86659/1/Benoit_Text%20analysis%20in%20R_2018.pdf'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


#### Abstract

Computational text analysis has become an exciting research field with many applications in communication research. It can be a difficult method to apply, however, because it requires knowledge of various techniques, and the software required to perform most of these techniques is not readily available in common statistical software packages. In this teacher’s corner, we address these barriers by providing an overview of general steps and operations in a computational text analysis project, and demonstrate how each step can be performed using the R statistical software. As a popular open-source platform, R has an extensive user community that develops and maintains a wide range of text analysis packages. We show that these packages make it easy to perform advanced text analytics.


