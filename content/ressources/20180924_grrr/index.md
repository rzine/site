---
date: "2018-09-24T00:00:00Z"

title: Grrr
summary: Groupe Slack francophone dédié aux échanges et à l’entraide autour de R. 

tags: 
- forum
- slack
- communauté

authors:
- jbarnier

publication_types: ["3"]

thematics: ["8"]

update:
- "1"

languages:
- "0"

sources:
- "0"

url_source: 'https://r-grrr.slack.com'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



[**Grrr**](https://app.slack.com/client/T9ML8RLMP) (“pour quand votre R fait Grrr”) est un groupe [**Slack**](https://r-grrr.slack.com) (plateforme de discussion instantanée) francophone **dédié aux échanges et à l’entraide autour de R**. Il est ouvert à tou.te.s et se veut accessible aux débutants. Vous pouvez même utiliser un pseudonyme si vous préférez.

Pour rejoindre la plateforme discussion, cliquez sur [ce lien](https://join.slack.com/t/r-grrr/shared_invite/zt-46utbgb9-uvo_bg5cbuxOV~H10YUX8w)



