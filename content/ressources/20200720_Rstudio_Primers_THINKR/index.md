---
title: Apprendre le langage R
subtitle : Adaptation en français des tutoriels *Rstudio Primers*
summary: Adaptation en français des tutoriels interactifs « Primers » de Rstudio

authors:
- ThinkR

publication_types: ["13"]


sources:
- "0"

thematics: ["0","10","11","5"]


update:
- "1"

languages:
- "0"


projects:
- divers

date: "2020-07-20T00:00:00Z"

tags:
- introduction
- dataviz
- ggplot
- dplyr
- tidyr
- tibbles


url_source: 'https://thinkr.fr/tutoriels-gratuits-pour-apprendre-r/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Chez [ThinkR](https://thinkr.fr/), nous croyons en l’apprentissage par la pratique, avec ces outils vous n’avez rien à installer sur vos ordinateurs. Vous pouvez directement suivre ces tutoriels gratuits et répondre aux exercices en ligne depuis notre site Internet.
Ces exercices sont les adaptations en français des [« primers » de nos confrères de chez Rstudio](https://rstudio.cloud/learn/primers), vous n’avez qu’à vous laisser guider. Si vous rencontrez des soucis techniques ou pratiques avec ces outils [n’hésitez pas à le nous signaler](https://thinkr.fr/contact/).




