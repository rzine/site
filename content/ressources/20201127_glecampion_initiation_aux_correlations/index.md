---
# Author(s)
authors:
- glecampion88


# Date
date: "2021-06-25"


# Title
title: Analyse des corrélations avec easystats


# Short summary
summary: Cette fiche vise à présenter une méthode efficace et simple pour réaliser, analyser et représenter des corrélations avec R, de la mise en forme des données à la réalisation de graphiques. Elle ne contient que de brefs rappels sur la notion de corrélation en statistique. 


# Tags
tags:
- easystats
- Pearson
- Spearman
- Kendall
- correlation
- see
- Test d'hypothèses
- Statistiques inférentielles
- corrélogramme
- corrplot
- cor.test


publication_types: ["10"]


projects:
- rzine

sources:
- "8"

thematics: ["12"]




update :
- "0"

languages:
- "0"


# Link
url_source: '/docs/20200526_glecampion_initiation_aux_correlations/index.html'
url_code: 'https://github.com/rzine-reviews/Rzine_correlation'
url_dataset: '/docs/20200526_glecampion_initiation_aux_correlations/data.zip'

doi: "10.48645/qhav-cb52"

links:
- name: HAL
  url: https://hal.archives-ouvertes.fr/halshs-03376599v1


# Image credit
image:
  caption: '© logo du package correlation'
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---



**Auteur** : [Grégoire Le Campion](https://rzine.fr/authors/glecampion88/)  
**Evaluateur.rice.s** : Comité éditorial Rzine  
**Editeur.rice.s** : [Marion Gentilhomme](https://rzine.fr/authors/mgentilhomme/), [Hugues Pecout](https://rzine.fr/authors/hpecout)     


#### Résumé 

Cette fiche vise à présenter une méthode efficace et simple pour réaliser, analyser et représenter des corrélations avec R, de la mise en forme des données à la réalisation de graphiques. Elle ne contient que de brefs rappels sur la notion de corrélation en statistique.

#### Citation

<div style="font-size:15px;">
Le Campion G (2021). <i>"Analyse des corrélations avec easystats"</i>, doi: 10.48645/QHAV-CB52, URL: https://rzine.fr/publication_rzine/20201127_glecampion_initiation_aux_correlations/.
</div>

<p class="d-inline-flex" style="gap:12px;"><a href="https://doi.org/10.48645/qhav-cb52"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://zenodo.org/badge/DOI/10.48645/qhav-cb52.svg" alt="DOI:10.48645/qhav-cb52"></a><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg" alt="License: CC BY-SA 4.0"></a></p>

