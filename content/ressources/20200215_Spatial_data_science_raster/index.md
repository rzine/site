---
title: Spatial Data Science with R
subtitle: Introduction to R, raster & terra package
summary: Spatial data analysis and modeling with *raster* and *terra* package


authors:
- Robert J. Hijmans 

publication_types: ["8"]


thematics: ["0","2","19","20"]


projects:
- divers

sources:
- "0"

update:
- "1"

languages:
- "1"

date: "2020-02-15T00:00:00Z"
doi: ""

tags:
- introduction
- raster
- terra
- Remote sensing
- Species Distribution Modeling
- Geographic Information Analysis
- GIS
- Processing MODIS data


url_code: 'https://github.com/rspatial/rspatial-raster-web'
url_source: 'https://rspatial.org/index.html'

links:
- name: terra version
  url: https://rspatial.org/terra/index.html
- name: raster version
  url: https://rspatial.org/raster/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

The materials presented here teach spatial data analysis and modeling with R. R is a widely used programming language and software environment for data science. R also provides unparalleled opportunities for analyzing spatial data for spatial modeling.

If you have never used R, or if you need a refresher, you should start with our [Introduction to R](https://rspatial.org/intr/index.html) ([pdf](https://rspatial.org/pdf/Rintro.pdf))

There are two version of this website, the [“raster” version](https://rspatial.org/raster/index.html) and the [“terra” version](https://rspatial.org/terra/index.html). The “raster” version is well established and more elaborate. If in doubt, go there.

The version using the “terra” package is new, and under development. It is particularly useful for those who are interested in switching from the raster to the terra package, for faster processing and for remote sensing
