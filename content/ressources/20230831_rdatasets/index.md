---
title: Rdatasets
subtitle: Collection of 2264 datasets available in R
summary: Collection of 2264 datasets available in R

authors:
- Vincent Arel-Bundock

publication_types: ["0"]

thematics: ["8", "9"]

sources:
- "0"

update:
- "1"

languages:
- "1"

date: "2023-08-31"


tags:
- dataset
- datasets
- add-on packages


url_source: 'https://vincentarelbundock.github.io/Rdatasets/'
url_code : 'https://github.com/vincentarelbundock/Rdatasets/'


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


`Rdatasets` is a collection of 2264 datasets which were originally distributed alongside the statistical software environment `R` and some of its add-on packages. The goal is to make these data more broadly accessible for teaching and statistical software development.



