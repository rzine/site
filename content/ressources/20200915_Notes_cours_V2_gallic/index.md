---
title: Notes de cours de R
subtitle: Seconde version
summary: Ce document (seconde version) a pour but l’initiation à R afin d’être capable de s’en servir de manière efficace et autonome.

authors:
- egallic

publication_types: ["13"]


thematics: ["0","10","11","13","5"]


sources:
- "0"

update:
- "0"

languages:
- "0"

projects:
- divers

date: "2020-09-14T00:00:00Z"


tags:
- introduction
- fonction
- test
- while
- boucle
- lubridate
- purrr
- plyr
- tidyverse
- data.table
- graphique
- ggplot2
- régression linèaire
- expressions régulières
- regex
- grep
- stringr
- texreg
- gdata
- readxl
- dplyr
- devtools
- modify
- tidyr
- reshape2
- beepr
- doMC
- magrittr
- scale
- grid
- gridExtra
- rworldmap
- rgdal
- maptools
- MASS
- rgl
- carte
-

url_source: 'http://egallic.fr/Enseignement/R/Book/avant-propos.html'
url_code: 'https://github.com/3wen/R_course'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

Ces notes de cours ont été réalisées dans le cadre d’un enseignement à R adressé à des étudiants en Économie. La [première version](http://egallic.fr/Enseignement/R/m1_stat_eco_logiciel_R.pdf) remonte à 2014. Le contenu était proposé à des étudiant•e•s de Master à la Faculté de Sciences Economiques de l’Université de Rennes 1. En six ans, le langage R a beaucoupé évolué, notamment avec l’émergence d’une collection de packages à destination de la data science : *tidyverse*. Les notes de cours de la première version se devaient être dépoussiérées pour présenter ce que j’appelle les nouveautés du langage. Cette refonte partielle de la première version s’effectue dans le cadre d’un enseignement à destination des étudiant•e•s de 3e année de Licence et Economie et Gestion à la [Faculté d’Economie et de Gestion](https://feg.univ-amu.fr/) d’Aix-Marseille Université.
