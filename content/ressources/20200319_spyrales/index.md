---
date: "2020-03-19T00:00:00Z"

title: Spyrales
subtitle: Communauté d'agents de l'Etat
summary: Une communauté d'agents de l'Etat pour s'entraider en R et Python

authors: 
- INSEE 

tags: 
- INSEE
- Python
- communauté
- réseau
- groupe

publication_types: ["7", "4", "2"]

thematics: ["8"]

projects:
- spyrales

sources:
- "4"

update:
- "1"

languages:
- "0"

url_source: 'https://spyrales.netlify.app/'
url_code: 'https://github.com/spyrales/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Cette **communauté d'agents publics**, titulaires et non titulaires, travaillant dans le domaine de la **statistique ou de la datascience**, en central ou en déconcentré **propose un soutient à là formation, au perfectionnement et à la découverte de nouvelles pratiques en science des données**, et en particulier sur deux langages très utilisés dans ce domaine : **R et Python**.

Consulter le blog de Spyrales : [https://spyrales.netlify.app/](https://spyrales.netlify.app/)


