---
title: Chaîne YouTube de Vincent Goulet
subtitle: Tutoriels vidéos d'introduction à R
summary: Tutoriels vidéos francophones d'introduction à R

authors:
- Vincent Goulet

publication_types: ["9"]


thematics: ["0","1"]


languages:
- "0"

sources:
- "0"

update:
- "1"

projects:
- divers
date: "2015-03-17T00:00:00Z"
doi: ""


tags:
- introduction
- projet
- git
- apply
- indiçage
- matrice
- raccourci
- order
- GNU emacs
- outer
- package
- youtube
- vidéo


url_source: 'https://www.youtube.com/user/VincentGouletIntroR/videos'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


Chaîne Youtube de Vincent Goulet, auteur du manuel de référence : [*Introduction à la programmation en R*](https://vigou3.github.io/introduction-programmation-r/).


