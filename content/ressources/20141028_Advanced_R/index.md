---

authors:
- Hadley Wickham

date: "2014-10-28"

title : Advanced R
subtitle: 
summary: Book designed primarily for users who want to improve their programming skills and understanding of the language


tags :
- function
- S3
- R6
- S4
- HTML
- Latex
- Debugging
- performance
- C++
- Quote


publication_types: ["5"]


thematics: ["5"]


languages:
- "1"

sources:
- "6"

update:
- "1"


projects:
- rseries


url_source: 'https://adv-r.hadley.nz/'
url_code: 'https://github.com/hadley/adv-r'

publishDate: "2014-10-28T00:00:00Z"
publication: Advanced R
publication_short:  Advanced R
  

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Advanced R presents useful tools and techniques for attacking many types of R programming problems, helping you avoid mistakes and dead ends. With more than ten years of experience programming in R, the author illustrates the elegance, beauty, and flexibility at the heart of R.

The book develops the necessary skills to produce quality code that can be used in a variety of circumstances. You will learn:

- The fundamentals of R, including standard data types and functions    
- Functional programming as a useful framework for solving wide classes of problems    
- The positives and negatives of metaprogramming    
- How to write fast, memory-efficient code     

This book not only helps current R users become R programmers but also shows existing programmers what’s special about R. Intermediate R programmers can dive deeper into R and learn new strategies for solving diverse problems while programmers from other languages can learn the details of R and understand why R works the way it does.
