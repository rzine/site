---
title: Les données spatiales avec R
subtitle: Support d'enseignement du master Mégadonnées et Analyse sociale (CNAM)

summary: Support d'enseignement du master Mégadonnées et Analyse sociale (CNAM)


authors:
- mtheuliere

publication_types: ["13","5"]


thematics: ["2"]


projects:
- divers

sources:
- "0"

update:
- "1"

languages:
- "0"

date: "2019-07-17T00:00:00Z"
doi: ""

tags:
- vecteur
- raster
- sf
- sp
- stars
- tidyverse
- systèmes de coordonnées
- EPSG
- DT
- RPostgres
- mapview
- ggplot2
- rmapshaper
- patchwork
- lwgeom
- variousdata
- ggspatial
- tmap
- cartogram
- leaflet
- cartographie
- carte


url_source: 'https://maeltheuliere.github.io/rspatial/'
url_code: 'https://github.com/MaelTheuliere/rspatial'



# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Support d'enseignement **utilisé dans le cadre du master Mégadonnées et Analyse sociale (MéDAS) proposé par le CNAM** de Loire-Atlantique.

**L’objectif de ce cours** est de présenter les éléments de manipulation des données spatiales à partir de R. Plus précisément :  

- **Ce que sont les données spatiales**   
- **Comment lire** des données spatiales ?   
- **Comment manipuler** les données spatiales ?   
- **Comment visualiser** les données spatiales ?    

*Ce document est mise à disposition selon les termes de la [Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/).*

