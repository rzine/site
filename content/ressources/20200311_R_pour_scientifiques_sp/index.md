
---
# AUTHOR(S) 
authors:
- frebaudo


# DATE de soumission à Rzine
date: "2020-03-11"

# TITRE
title: Aprender R 
subtitle: iniciación y perfeccionamiento

# RESUME COURT - une phrase
summary: iniciación y perfeccionamiento


# TAGS
tags:
- iniciación
- Rstudio
- objeto
- funciones
- import
- export
- prueba
- bucle
- while
- for
- repeat
- apply
- lapply
- tapply
- mapply
- sapply
- proyecto
- graphique
- plot
- hist
- barplot
- boxplot
- color
- ggplot2
- plotly
- reproductible


# TYPE DE PUBLICATION
publication_types: ["5"]



thematics: ["0","10","11"]





languages:
- "2"

sources :
- "0"

update :
- "1"


# PROJET
# Les projets existants :
# Manuel CIST'R = manuel
# Fiche CIST'R = fiche
# Exercice CIST'R = exercice
# Sources diverses = divers
# Resources web (blog et site) = web
# GDR Analyse de réseau en SHS = GDR_ARSHS
# Axe Information territoriale du CIST = INFTER
projects:
- divers

##### LINKS #####
# DOCUMENTATION
url_source: 'https://myrbooksp.netlify.app'
url_code: 'https://github.com/frareb/myRBook_SP'


  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


El propósito de este libro es proporcionar a los estudiantes y aquellos que deseen aprender sobre R una base sólida para luego implementar sus propios proyectos científicos y la valoración de sus resultados. Hay muchos libros dedicados a R, pero ninguno cubre los elementos básicos de este lenguaje con el fin de hacer que los resultados científicos sean publicables y reproducibles.

En general, este libro está dirigido a toda la comunidad científica y en particular a aquellos interesados en las ciencias de la vida, y los ejemplos en este libro se basarán en estudios de biología.

Este libro nació de la solicitud de los estudiantes de las universidades que colaboran con el IRD en América del Sur. Por lo tanto, su primera versión está escrita en español (hay pocos documentos de calidad en R en español). Comencé su traducción al francés en 2018 y hoy ambas versiones coevolucionan con contenido que puede variar (por ejemplo, para estudios de casos).

### Licencia
  
Licencia Reconocimiento-NoComercial-SinObraDerivada 3.0 España ([CC BY-NC-ND 3.0 ES](https://creativecommons.org/licenses/by-nc-nd/3.0/es/)). 



