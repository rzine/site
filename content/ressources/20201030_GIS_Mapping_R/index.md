---
title: GIS and mapping in R
subtitle: Introduction to the sf package
summary: Introduction to the sf package

authors:
- ogimenez

publication_types: ["12"]


thematics: ["2"]


sources :
- "0"

update :
- "0"

languages:
- "1"

projects:
- divers

date: "2020-10-30T00:00:00Z"

tags:
- sf
- ggplot2
- distance
- Geometric calculations


url_source: 'https://oliviergimenez.github.io/intro_spatialR/#1'
url_code: 'https://github.com/oliviergimenez/intro_spatialR'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

