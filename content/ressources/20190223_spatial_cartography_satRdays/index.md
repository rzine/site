--- 
authors:
- kantuki
- ecome
- tgiraud


date: "2018-09-27"

title: Spatial Data and Cartography with R
subtitle: Summer school workshop
summary: Workshop held as part of **summer school 2018 of the LabEx DynamiTe**


tags:
- rgdal
- sp
- sf
- rgeos
- cartography
- tmap
- leaflet
- ggplot2
- tidyverse
- mapview
- mapdeck
- ggspatial
- rmdformats
- saturday
- Labex
- dynamite



publication_types: ["13"]


thematics: ["10","2"]


languages:
- "1"

sources:
- "0"


update:
- "0"

projects:
- SatRdays
- dynamite


links:
- name: Exercises
  url: https://antuki.github.io/satRday2019_workshop/exercises/exercises

url_source : 'https://dynamitestaff.github.io/R-workshops/Geovisualisation/lecture/lecture.html'
url_code: 'https://github.com/DynamiteStaff/R-workshops'
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Ce workshop s'est déroulé le 27 septembre 2018 dans le cadre de l'[Ecole d'été 2018 du LabEx DynamITe](http://labex-dynamite.com/fr/evenements-du-labex/ecoles-dete/ecole-dete-2018/)**. 

Une [version allégée](https://antuki.github.io/satRday2019_workshop/lecture/lecture.html) a également été présentée dans le cadre d'une rencontre [**SatRday**](https://paris2019.satrdays.org/), en février 2019. 






