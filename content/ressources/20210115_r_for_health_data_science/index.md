---
authors:
- Ewen Harrison
- Riinu Pius

title: R for Health Data Science

summary: Statistics and data visualization applied to Health Data Science

date: "2021-01-15"

publication_types: ["5"]

thematics: ["10", "11", "12", "13"]

languages:
- "1" 

update:
- "1" 

sources:
- "0"

url_source: 'https://argoshare.is.ed.ac.uk/healthyr_book/' 
url_code: 'https://github.com/SurgicalInformatics/healthyr_book#datasets-used-in-the-book' 
url_dataset: 'https://github.com/SurgicalInformatics/healthyr_book/tree/master/data'

tags:
- Health
- Linear regression
- Logistic regression
- Plots
- Missing data
- Kaplan Meier
- KM
- survival
- Mortality index

doi: "10.1201/9780367855420"

publishDate: "2021"
publication: R for Health Data Science, Harrison E. and Pius R., Taylor & Francis
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**R for Health Data Science** provides inputs dedicated to healthcare professional. 

**Features**

- Provides an introduction to the fundamentals of R for healthcare professionals.
- Highlights the most popular statistical approaches to health data science.
- Written to be as accessible as possible with minimal mathematics
- Emphasises the importance of truly understanding the underlying data through the use of plots.
- Includes numerous examples that can be adapted for your own data.
- Helps you create publishable documents and collaborate across teams
