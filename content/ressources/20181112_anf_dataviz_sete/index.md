
---
# AUTHOR(S) 
authors:
- nlambert76
- rysebaert

# DATE de soumission à Rzine
date: "2018-11-16"

# TITRE
title: Géo-visualisation - Manipulation de données spatiales et cartographie.

# RESUME COURT - une phrase
summary: Support utilisé dans le cadre de l'[Action Nationale de Formation **DataViz**](https://anf-mate2018.sciencesconf.org/), qui s'est déroulée à Séte en Novembre 2018. 



# TAGS
tags:
- cartographie
- cartography
- sf
- ANF
- Géo-visualisation
- cartogram
- countrycode
- rmapshaper
- osmdata
- eurostat
- naturalearth
- SIG
- carte


publication_types: ["13"]


thematics: ["2"]


languages:
- "0"

update:
- "0"


projects:
- mateshs

sources :
- "0"


url_source: 'https://riatelab.github.io/anfdataviz/'
url_code: 'https://github.com/riatelab/anfdataviz'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Ce document a été produit dans le cadre de l'**Action Nationale de Formation** [**DataViz**](https://anf-mate2018.sciencesconf.org/) (ANF CNRS), organisée par le réseau [MATE-SHS](http://mate-shs.cnrs.fr/)  et qui s'est déroulée à Séte en Novembre 2018. 

Il s'agit du support utilisé **pour un atelier pratique (3h30) sur la manipulation et la visualisation de données géographiques** avec R. Y sont abordées les traitements SIG de base, la cartographie thématique (cartes en figurés proportionnels, cartes choroplèthes, cartes de typologie, etc) et des cartes reposant sur des techniques plus avancées comme les cartes sur grille ou les cartes de discontinuités.

Les principaux packages utilisés sont  **sf** (manipulation de données spatiales) et **cartography** (cartographie thématique). 

*Chaque exemple propose une chaîne de traitements depuis le chargement des données, leur mise en forme, jusqu’à la mobilisation de méthodes adaptées permettant de répondre à des questions spatiales. Les exemples proposés sont réalisés à différentes échelles, du local (région Occitanie) au Monde, en passant par l’Europe*.
