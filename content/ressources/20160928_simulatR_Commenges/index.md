---
# TITRE
title: SimulatR
subtitle : Simuler les situations d'inférence

# A one-sentence summary of the content on your page. (170 characters)
# The summary can be shown on the homepage and can also benefit your search engine ranking. )
summary: Outil interactif permettant de simuler les situations d'inférence.

# Display the authors of the publication and link to their user profiles (if they exist).
authors:
- hcommenges


# Project name
projects:
- divers

# Type de la publication 
publication_types: ["1"]



thematics: ["12","13"]



languages:
- "0"


sources:
- "0"

update:
- "0"


# publishing date 
date: "2016-09-28"

# Tagging your content helps users to discover similar content on your site. 
# Tags can improve search relevancy and are displayed after the page content.
tags:
- Student
- chi2
- échantillonage
- shiny

# Source de la publication
url_source: 'https://analytics.huma-num.fr/geographie-cites/SimulatR/'

# Lien Github/Gitlab ?
url_code: 'https://github.com/hcommenges/SimulatR'

# DOI
doi: "10.5281/zenodo.157162"



# #################### DON'T TOUCH ##########################
featured: true
image:
  caption: 'Copyright'
  focal_point: "Center"
  preview_only: false
---


Dans l'enseignement des statistiques en SHS, il n'est pas possible de faire la démonstration analytique des propriétés des estimateurs et des statistiques de test. **L'idée de SimulatR est donc de simuler une situation d'échantillonage et de la répéter pour visualiser une distribution d'échantillonage de la moyenne ou une distribution d'une statistique de test** (ici le t de Student et le chi2). Trois situations peuvent être simulées :

- estimation d'un intervalle de confiance : distribution d'échantillonage de la moyenne
- comparaison de moyennes : distribution de la statistique t de Student
- tableau de contingence : distribution de la statistique chi2





