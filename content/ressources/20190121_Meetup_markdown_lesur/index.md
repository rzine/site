---
authors:
- rlesur

date: "2019-01-21"

title: Créez vos documents avec rmarkdown
subtitle: Communication *Meetup R Nantes* du 21 Janvier 2019
summary: Présentation de Romain Lesur lors d'une rencontre *Meetup R Nantes* le 21 janvier 2019.

tags:
- rmarkdown
- meetup
- markdown
- knitr
- pandoc
- bookdown
- blogdown
- tinytex
- officer
- flextable
- kableExtra
- gt
- chunk


publication_types: ["12"]


thematics: ["1"]


languages:
- "0"


sources:
- "0"

update:
- "0"

projects:
- meetup


url_source: 'https://rlesur.github.io/meetup-r-nantes-rmd/#1'
url_code: 'https://github.com/RLesur/meetup-r-nantes-rmd'

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Communication de Romain Lesur** lors d'une rencontre *Meetup R Nantes* le 21 janvier 2019.



