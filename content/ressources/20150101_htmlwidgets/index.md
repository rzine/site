---
title: HTML Widgets for R
subtitle: Documentation, showcase and gallery
summary: The htmlwidgets package provides a framework for easily creating R bindings to JavaScript libraries (leaflet, dygraphs, networkD3, DT...)


authors:
- Ramnath Vaidyanathan
- Kenton Russell
- RStudio

publication_types: ["6"]


sources:
- "0"

thematics: ["11","2","5"]


update:
- "1"

languages:
- "1"

projects:
- divers

date: "2015-01-05T00:00:00Z"

tags: 
- html
- widget
- leaflet
- dygraphs
- networkD3
- sparkline
- DT
- rthreejs

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'http://www.htmlwidgets.org/'
url_code: 'https://github.com/ramnathv/htmlwidgets'

---


The htmlwidgets package provides a framework for easily creating R bindings to JavaScript libraries. Widgets created using the framework can be:

- Used at the R console for data analysis just like conventional R plots (via RStudio Viewer).
- Seamlessly embedded within R Markdown documents and Shiny web applications.
- Saved as standalone web pages for ad-hoc sharing via email, Dropbox, etc.

There are already several R packages based on htmlwidgets, including:

- **leaflet** -- Interactive maps with OpenStreetMap
- **dygraphs** --- Interactive time series visualization
- **networkD3** --- Network visualization with D3
- **sparkline** --- Small inline charts
- **DT** --- Tabular data via DataTables
- **rthreejs** -- Interactive 3D graphics


The package was created in collaboration by Ramnath Vaidyanathan, Joe Cheng, JJ Allaire, Yihui Xie, and Kenton Russell. We've all spent countless hours building bindings between R and the web, and were motivated to create a framework that made this as easy as possible for all R developers.








