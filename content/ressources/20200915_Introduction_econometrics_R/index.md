---
title:  Introduction to Econometrics with R 
summary:  This manual is described as an interactive script in the style of a reproducible research report

authors:
- Christoph Hanck
- Martin Arnold
- Alexander Gerber
- Martin Schmelzer

publication_types: ["5"]


thematics: ["12","13","15"]


projects:
- divers

sources:
- "0"

update:
- "1"

languages:
- "1"

date: "2020-09-15T10:00:00Z"
doi: ""


url_source: 'https://www.econometrics-with-r.org/index.html'
url_code: 'https://github.com/mca91/EconometricsWithR'
url_pdf: 'https://www.econometrics-with-r.org/ITER.pdf'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


