---
# Author(s)
authors:
- Vebash Naidoo


# Date
date: "2021-03-24"


# Title
title: SQL in rmarkdown


# Short summary
summary: Mix SQL and R code chunks in a .Rmd file to leverage our knowledge in both languages

# Tags
tags:
- sql
- rmarkdown
- RSQLite
- DBI
- tidyverse
- tidyquery
- dplyr



publication_types: ["11"]

sources:
- "0"

thematics: ["1", "7"]


update:
- "0"

languages:
- "1"


# Link
url_source: 'https://sciencificity-blog.netlify.app/posts/2021-03-27-sql-in-rmarkdown/'
url_code: 'https://github.com/sciencificity/sql-in-rmd'



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

In this post we’re going to write SQL syntax and intertwine that with R code to enable you to use both, if you are familiar with both.

We will also look at the `tidyquery` package which allows you to learn `dplyr` if you’re more familiar with SQL, but keen to grasp more `dplyr`.




