---
# Author(s)
authors:
- Jennifer Bryan


# Date
date: "2019-09-19"


# Title
title: Data wrangling, exploration, and analysis with R (STAT 545)


# Short summary
summary: This site is about everything that comes up during data analysis except for statistical modelling and inference

# Tags
tags:
- dplyr
- tidyr
- function
- ggplot2
- colors
- package
- API
- scraping
- shiny
- encoding



publication_types: ["5","8"]


sources:
- "0"

thematics: ["10", "11", "7", "6", "5"]


update:
- "0"

languages:
- "1"


# Link
url_source: 'https://stat545.com/'
url_code: 'https://github.com/rstudio-education/stat545'



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

Learn how to:    

- explore, groom, visualize, and analyze data,   
- make all of that reproducible, reusable, and shareable,    
- using R.    

This site is about everything that comes up during data analysis except for statistical modelling and inference. This might strike you as strange, given R’s statistical roots. First, let me assure you we believe that modelling and inference are important. But the world already offers a lot of great resources for doing statistics with R.

The design of STAT 545 was motivated by the need to provide more balance in applied statistical training. Data analysts spend a considerable amount of time on project organization, data cleaning and preparation, and communication. These activities can have a profound effect on the quality and credibility of an analysis. Yet these skills are rarely taught, despite how important and necessary they are. STAT 545 aims to address this gap.


