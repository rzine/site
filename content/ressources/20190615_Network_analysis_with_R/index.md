---
title: Network visualization with R
subtitle: Static and dynamic network visualization
summary: Reference tutorial on static and dynamic network visualization. Continuously updated and developed


authors:
- Katherine Ognyanova

projects:
- divers

sources :
- "0"

thematics: ["18","11"]





update :
- "1"

languages:
- "1"


publication_types: ["13","5"]



date: "2019-06-15T00:00:00Z"
doi: ""

tags:
- igraph
- RColorBrewer
- png
- network
- ggraph
- animation
- visNetwork
- htmlwidgets
- threejs
- networkD3
- ndtv
- geosphere
- maps
- Statnet
- sna
- ergm
- stergm
- multiplex
- interactive
- nodes
- links
- layout



url_source: 'https://kateto.net/network-visualization'
url_pdf: 'http://www.kateto.net/wp-content/uploads/2019/06/Sunbelt%202019%20R%20Network%20Visualization%20Workshop.pdf'
url_code: 'https://github.com/kateto/R-Network-Visualization-Workshop'


featured: true 
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This is a comprehensive tutorial on network visualization with R. It covers data input and formats, visualization basics, parameters and layouts for one-mode and bipartite graphs; dealing with multiplex links, interactive and animated visualization for longitudinal networks; and visualizing networks on geographic maps.
