---
title: Le projet Rzine
subtitle: Pour la diffusion et le partage de ressources sur la pratique de R en sciences humaines et sociales 

summary: Support de présentation du projet Rzine utilisé aux Journées Annuelles du réseau MATE-SHS 2022

authors:
- hpecout
- tgiraud

publication_types: ["12"]

thematics: ["9"]

sources:
- "8"

update:
- "1"

languages:
- "0"


date: "2022-05-18T11:00:00Z"
doi: ""

tags:
- rzine
- mate
- shs


url_code: 'https://gitlab.huma-num.fr/rzine/communications/projet_rzine'
url_source: 'https://rzine.gitpages.huma-num.fr/communications/projet_rzine'


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Support de présentation du projet Rzine utilisé aux [Journées Annuelles du réseau MATE-SHS](https://ja-mate2022.sciencesconf.org/) (17 mai 2022), ainsi que lors d'une séance des [Midis de Géotéca](http://geoteca.u-paris.fr/actualites/midis-de-geoteca/) (12 avril 2022).







