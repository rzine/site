---
title: Être un•e utilisateur•rice de R résilient•e
subtitle : 
summary: Séminaire RUSS - Séance du 5 février 2021


authors:
- msalmon

publication_types: ["12","9"]


sources:
- "7"

thematics: ["0","8"]


update:
- "0"

languages:
- "0"


projects:
- RUSS

date: "2021-02-05T00:00:00Z"

url_source: 'https://rresilient.netlify.app/#/'

links:
- name: Video (partie 1)
  url: https://www.canal-u.tv/video/ined/etre_un_e_utilisateur_rice_de_r_resilient_e_partie_1_limiter_ses_peines.62089
- name: Video (partie 2)
  url: https://www.canal-u.tv/video/ined/etre_un_e_utilisateur_rice_de_r_resilient_e_partie_2_se_tenir_au_courant.62093
- name: Video (partie 3)
  url: https://www.canal-u.tv/video/ined/etre_un_e_utilisateur_rice_de_r_resilient_e_partie_3_resoudre_ses_problemes.62095
- name: Video (partie 4)
  url: https://www.canal-u.tv/video/ined/etre_un_e_utilisateur_rice_de_r_resilient_e_partie_4_ou_demander_de_l_aide.62105

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Apprendre R est une excellente idée pour réaliser différentes tâches au travail. Cependant, même une fois passé·e au-delà de "Bonjour Monde", vous continuerez régulièrement à avoir des problèmes dans R... Triste constat ? Non, car vous apprendrez aussi à devenir un·e utilisateur·rice de R résilient·e !

Dans cette présentation, Maëlle Salmon partage des astuces à cet effet : comment suivre les nouveautés de R sans se sentir submergé·e ; et comment et où demander de l’aide efficacement. 




