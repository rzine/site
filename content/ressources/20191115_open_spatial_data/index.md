---
title: R packages to download open spatial data
subtitle: Materials based on the book Geospatial Health Data  
summary: Materials based on the book Geospatial Health Data  

authors:
- Paula Moraga

publication_types: ["6","11"]

thematics: ["2", "19", "20"]

sources:
- "0"

update:
- "0"

languages:
- "1"



date: "2019-11-15"


tags:
- material
- health
- spatial
- spatio-temporal
- geostatistical
- package


url_source: 'https://www.paulamoraga.com/tutorial-open-spatial-data/'

links:
- name: Associated book
  url: https://www.paulamoraga.com/book-geospatial/index.html


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

[Spatial data](https://f1000research.com/articles/11-770) are used in a wide range of disciplines including environment, health, agriculture, economy and society. Several R packages have been recently developed as clients for various databases that can be used for easy access of spatial data including administrative boundaries, climatic, and OpenStreetMap data. Here, we give short reproducible examples on how to download and visualize spatial data that can be useful in different settings. More extended examples and details about the capabilities of each of the packages can be seen at the packages’ websites, and the [rspatialdata](https://rspatialdata.github.io/) website which provides a collection of tutorials on R packages to download and visualize spatial data using R.






