---
title: R Markdown - An Incomplete History
summary: Communication from [Toronto Data Workshop](https://rohanalexander.com/toronto_data_workshop.html)

authors:
- Garrick Aden-Buie

publication_types: ["12","9"]


sources:
- "0"

thematics: ["1"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- markdown
- literate programming
- sweave
- knitr
- pandoc
- epoxy


date: "2021-02-11T00:00:00Z"

url_source: 'https://www.garrickadenbuie.com/talk/toronto-data-workshop/'
url_code: 'https://github.com/gadenbuie/slides/tree/gh-pages/toronto-data-workshop'



links:
- name: slide
  url: https://slides.garrickadenbuie.com/toronto-data-workshop/#1
- name: Recording
  url:  https://www.youtube.com/watch?v=Hl798H6J-bg


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Communication from [Toronto Data Workshop](https://rohanalexander.com/toronto_data_workshop.html)




