---
title: semin-R

summary: Groupe d'utilisateur·rices et liste de diffusion animés par le Muséum national d’Histoire naturelle, l'INED et l'Université Paris Descartes

date: "2009-05-14T00:00:00Z"

tags:
- liste
- réseau
- RUG
- MNHN
- groupe

authors:
- Michel Baylac
- Amandine Blin
- Elisabeth Morand 
- Loïc Ponger
- Christophe Pouzat
- Jérôme Sueur 

publication_types: ["4", "3"]

thematics: ["8"]

update:
- "0"

languages:
- "0"

sources:
- "0"

url_source: 'http://rug.mnhn.fr/semin-r/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Semin-R est groupe d'utilisateurs de R ouvert à tou.te.s
Il est géré par le Muséum national d'Histoire naturelle, l'INED et l'Université Paris Descartes.

Ce groupe a organisé de nombreuses journées et réunions jusqu'en juin 2017. Vous pouvez toujours consulter les supports de présentation utilisés sur leur site : [http://rug.mnhn.fr/semin-r/](http://rug.mnhn.fr/semin-r/)

**Semin-R est également à l'origine d'une liste de diffusion toujours active**. Retrouvez les modalités d'inscription sur leur site web.





