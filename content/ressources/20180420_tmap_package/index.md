---
authors:
- Martijn Tennekes

title: tmap
subtitle: R package for thematic maps 
summary: This package helps to draw thematic maps. The syntax is based on [A Layered Grammar of Graphics](http://vita.had.co.nz/papers/layered-grammar.pdf) and resembles the syntax of ggplot2
date: "2018-04-20"

publication_types: ["6"]


thematics: ["2"]


languages:
- "1"

update:
- "1"

sources:
- "0"

doi: '10.18637/jss.v084.i06'
url_source: 'https://r-tmap.github.io/tmap/'
url_code: 'https://github.com/mtennekes/tmap'


links:
- name: vignette
  url: https://cran.r-project.org/web/packages/tmap/vignettes/tmap-getstarted.html
- name: CRAN
  url: https://cran.r-project.org/web/packages/tmap/index.html
  

tags:
- tmap
- map
- maps
- cartography
- choropleth
- typology
- symbol
- bubble
- facets
- layout
- scale
- north
- arrow
- label
- legend
- thematic


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This package helps to draw thematic maps. The syntax is based on [A Layered Grammar of Graphics](http://vita.had.co.nz/papers/layered-grammar.pdf) and resembles the syntax of ggplot2.


