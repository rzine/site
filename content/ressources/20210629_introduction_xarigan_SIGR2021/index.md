---
title: Présentez vos travaux en Markdown avec Xaringan
subtitle : Introduction à Xaringan et Xaringanthemer
summary: Support d'un atelier réalisé dans le cadre de l'école SIGR2021


authors:
- nlambert76

publication_types: ["12","13"]


sources:
- "9"

thematics: ["1"]


update:
- "0"

languages:
- "0"


projects:
- SIGR2021

date: "2021-06-29T00:00:00Z"

url_source: 'https://sigr2021.github.io/xaringan/?panelset1=r-code2#1'
url_code: 'https://github.com/sigr2021/xaringan'

tags:
- SIGR2021
- markdow
- rmarkdown
- Xaringan
- Xaringanthemer
- xaringanExtra
- présentation


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Communication réalisée durant l'école thématique SIGR2021 sur les sciences de l’information géographique reproductibles.

Cette présentation vise à introduire à l'utilisation du package *Xarigan* pour la création de diaporama avec R. Les extensions *Xaringanthemer* et *xaringanExtra* sont également abordées.





