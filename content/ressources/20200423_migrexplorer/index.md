---
title: MigrExploreR
subtitle: Exploring International Migrant Stock
summary: This application is part of a set of tools called "MigrExplorer" that allow to visualize international migrations in various aspects

date: "2020-04-23T00:00:00Z"

authors:
- nlambert76
- Françoise Bahoken

publication_types: ["1"]


sources:
- "0"

thematics: ["18","2"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- immigration
- emmigration
- flux
- flow
- map
- carte


url_source: 'https://analytics.huma-num.fr/Nicolas.Lambert/migrexplorer/'
url_code: 'https://gitlab.huma-num.fr/nlambert/migrexplorer'

links:
- name: Billet associé
  url: https://neocarto.hypotheses.org/9872

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**MigrExploreR** est une application développée en R et portée sur le web via le package `Shiny`. Elle permet de représenter par des symboles proportionnels dont il est possible de faire varier la taille, le nombre de migrants (ou de migrantes) dans les différents pays du monde à plusieurs dates. 




