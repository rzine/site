---
title: Using R in hydrology
subtitle: Review of recent developments and future directions
summary: Review of recent developments and future directions

authors:
- Louise J. Slater
- Guillaume Thirel
- Shaun Harrigan
- Olivier Delaigue
- Alexander Hurley
- Abdou Khouakhi
- Ilaria Prosdocimi
- Claudia Vitolo
- Katie Smith

projects:
- divers

sources :
- "0"

thematics: ["8"]





update :
- "0"

publication_types: ["10"]


languages:
- "1"


date: "2019-07-10T00:00:00Z"

tags:
- hydrology
- tidyhydat
- waterData
- dataRetrieval
- rnrfa
- hydroscoper
- easyNCDF
- daymetr
- stars
- prism
- getMet
- topmodel
- dynatopmodel
- SWATmodel
- hydrostats
- hydromad
- hydroTSM
- fuse
- airGR
- raster
- TUWmodel
- vgam
- caret
- gamiss
- mgcv
- lfstat
- trend
- hydroGOF
- hddtools
- ecmwfr
- rnoaa
- RHMS
- Ecohydmod
- brook90
- berryFunctions
- hydrolinks


url_source: 'https://hess.copernicus.org/articles/23/2939/2019/hess-23-2939-2019.pdf'

doi: "10.5194/hess-23-2939-2019"

featured: true
image:
  caption: "Desirée De Leon (https://desiree.rbind.io/post/2020/learnr-iframes/)"
  focal_point: "Center"
  preview_only: false
---

### Abstract

The  open-source  programming  language  R  has gained  a  central  place  in  the  hydrological  sciences  over the last decade, driven by the availability of diverse hydro-meteorological data archives and the development of open-source computational tools. The growth of R’s usage in hydrology  is  reflected  in  the  number  of  newly  published  hydrological packages, the strengthening of online user communities, and the popularity of training courses and events. In  this  paper,  we  explore  the  benefits  and  advantages  of R’s usage in hydrology, such as the democratization of data science  and  numerical  literacy,  the  enhancement  of  reproducible  research and  open  science, the  access to  statistical tools, the ease of connecting R to and from other languages, and the support provided by a growing community. This paper  provides  an  overview  of  a  typical  hydrological  workflow based on reproducible principles and packages for retrieval of hydro-meteorological data, spatial analysis, hydro-logical modelling, statistics, and the design of static and dynamic visualizations and documents. We discuss some of the challenges  that  arise  when  using  R  in  hydrology  and  useful tools to overcome them, including the use of hydrological libraries, documentation, and vignettes (long-form guides that illustrate how to use packages); the role of integrated development environments (IDEs); and the challenges of big data and parallel computing in hydrology. Lastly, this paper provides  a  roadmap  for  R’s  future  within  hydrology,  with R packages as a driver of progress in the hydrological sciences, application programming interfaces (APIs) providing new  avenues  for  data  acquisition  and  provision,  enhanced teaching of hydrology in R, and the continued growth of the community via short courses and events.

