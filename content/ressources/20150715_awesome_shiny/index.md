---
title: Awesome R Shiny
subtitle: A curated list of resources for R Shiny.
summary: A curated list of resources for R Shiny. This awesome list was inspired by [awesome-shiny](https://github.com/dpastoor/awesome-shiny). Also see [Awesome Shiny Apps for Statistics](Awesome-shiny-apps-for-statistics)

authors:
- Rob Gilmore

# Type de la publication 
publication_types: ["0"]


sources :
- "0"

thematics: ["5","8"]


update :
- "1"





languages:
- "1"

projects:
- divers

# Date that the page was published
date: "2015-07-15T00:00:00Z"
doi: ""  

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

# Tagging your content helps users to discover similar content on your site. 
# Tags can improve search relevancy and are displayed after the page content and also in the Tag Cloud widget.
tags:
- package
- tool
- shiny
- awesome


url_source: 'https://grabear.github.io/awesome-rshiny/'
url_code: 'https://github.com/grabear/awesome-rshiny'

---

This awesome list was inspired by [awesome-shiny](https://github.com/dpastoor/awesome-shiny). Also see [Awesome Shiny Apps for Statistics](Awesome-shiny-apps-for-statistics)

