--- 
authors:
- bfalissard
- clalanne

date: "2020-02-24"

title: Introduction à la statistique avec R
subtitle : MOOC de l'université Paris-Saclay
summary: MOOC proposé par l'université Paris-Saclay

tags:
- MOOC
- introduction
- initiation

publication_types: ["13","9"]


thematics: ["0","10","11","12","13","1"]


languages:
- "0"

sources:
- "0"

update:
- "1"

projects:
- divers

url_source: 'https://www.fun-mooc.fr/fr/cours/introduction-a-la-statistique-avec-r/'

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


### À propos du cours

Ce cours permet d’apprendre la statistique à l’aide du **logiciel libre R**.

Le recours aux mathématiques est minimal. L’objectif est de savoir analyser des données, de comprendre ce que l’on fait, et de pouvoir communiquer ses résultats.

Ce cours s’adresse aux étudiants et praticiens de toutes disciplines qui recherchent une formation pratique. Il sera utile à toute personne ayant le besoin d’analyser un jeu de données réel dans le cadre d’un enseignement, de son activité professionnelle ou de recherche, ou par simple curiosité d’analyser un jeu de données par soi-même (données du web, données publiques…).

Le cours s’appuie sur le logiciel libre R qui est un des logiciels de statistique les plus puissants disponibles actuellement.

Les méthodes abordées sont : les techniques descriptives, les tests, l’analyse de variance, les modèles de régression linéaire et logistique, les données censurées (de survie).


### Formats

Le cours comprend deux volets :   

- des chapitres de cours abordant des notions de statistiques et des connaissances essentielles sur le logiciel R
- des "labs": vidéos de pratique du logiciel R sous l’interface RStudio.

*Chaque semaine, 4 à 6 vidéos de cours d’une dizaine de minutes et un “lab” seront mis en ligne.*

### Évaluation

L’évaluation comporte trois niveaux :

- des quiz “mémoire” (5 à 10 questions à la fin de chaque vidéo),
- des quiz “exercice” (des calculs simples à réaliser avec R),
- un devoir à rendre sous forme de script R.

*Une attestation de suivi avec succès sera attribuée par FUN et l'Université Paris-Saclay à l'issue du MOOC aux apprenants qui auront obtenu une note supérieure à 60 %.*


### Pré-requis

Il n’y a pas de pré-requis pour suivre ce cours. Des connaissances de base en algèbre ou en programmation sont utiles mais ne sont pas nécessaires. En revanche, la volonté d’apprendre à analyser un jeu de données par soi-même est essentielle.

### Public visé

Ce cours s'adresse à un public francophone cherchant à se former aux bases de la statistique avec un logiciel libre (disponible gratuitement). Il vise aussi bien un public en formation initiale qu'un public en formation continue. Compte tenu des nombreux domaines d'application des statistiques et de la taille importante de la communauté R, ce cours concerne une large audience. Sciences fondamentales comme appliquées, sciences humaines, gestion de projet, les statistiques concernent potentiellement l'ensemble des professions.





