---
title: Introduction au tidyverse, avec game of throne
subtitle: 
summary: Exercices pratiques d'introduction à R et au tidyverse en explorant les données de la série *Game of throne* produites par Jeffrey Lancaster

authors:
- ecome 

# Type de la publication 
publication_types: ["13"]


sources :
- "0"

thematics: ["10","11","2"]


update :
- "0"

languages:
- "0"
- "1"

projects:
- divers

# Date that the page was published
date: "2020-11-15T00:00:00Z"
doi: ""

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
# Name of review or book if the publication has been published in
# publication:
# publication_short:  

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://comeetie.github.io/got/got_tp.html'
url_code: 'https://github.com/comeetie/got'

links:
- name: english version
  url: https://comeetie.github.io/got/got_tp_en.html

---


Dans ce TP nous allons pratiquer un peu de R, en explorant les données de la série Game of thrones avec les librairies dplyr et sf et nous les visualiserons à l’aide de la bibliothèque ggplot2. Les données tabulaires utilisées ont été collectées par Jeffrey Lancaster et sont issues de ce [projet](https://github.com/jeffreylancaster/game-of-thrones). Les données spatiales ont été créer par ESRI et sont disponnibles [ici](https://www.arcgis.com/home/item.html?id=43d03779288048bfb5d3c46e4bc4ccb0). Pour débuter vous pouvez cloner [le repository](https://github.com/comeetie/got) afin de récupérer l’ensemble des données de ce tp.


- Exercise 1 : importer les données et visualiser leurs structures
- Exercise 2 : commandes R de bases
- Exercise 3 : dplyr
- Exercise 4 : ggplot
- Exercise 5 : données spatiale et géo-traitement avec sf
- Exercise 6 : cartographie avec les packages sf et ggplot




