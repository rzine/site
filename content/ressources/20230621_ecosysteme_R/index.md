---
title: "L'écosystème spatiale de R"
subtitle: Keynote speech aux Rencontres R 2023
summary: Keynote speech aux Rencontres R 2023


authors:
- tgiraud

publication_types: ["12", "9"]


sources:
- "11"

thematics: ["2","20","19"]


update:
- "0"

languages:
- "0"


projects:
- Rencontres R

date: "2023-06-21T00:00:00Z"

url_source: 'https://rcarto.github.io/RencontresR_2023/#/title-slide'
url_code: 'https://github.com/rCarto/RencontresR_2023'


links:
- name: Video
  url: https://www.youtube.com/watch?v=rzARlulrVgQ&list=PLC0_Y4EpEglW-9XRKOzW1QUB2RpBWeHUO&index=6


tags:
- spatial
- sf
- terra
- tmap
- renv
- mapiso
- stars
- raster
- rgeos
- ggplot2
- rgdal
- sp
- maptools
- mapsf
- maptiles
- spatstat
- gstat
- rgeoda
- GWmodel
- spgwr
- osmdata
- OpenStreetMap
- osm
- tidygeocoder
- osrm
- stplanr



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Si R permet depuis longtemps de traiter les données spatiales, plusieurs packages assez récents (`sf`, `terra`, `stars`...) ont renouvelé le socle permettant la mise en œuvre de ces traitements.

La plupart des développements actuels s'appuient sur ce socle et forment un écosystème robuste qui offre aux utilisateurs la plupart des fonctionnalités autrefois réservées aux Systèmes d'Information Géographique.

Cet exposé présente un panorama de l'écosystème spatial de R, illustré par une série d'exemples mobilisant des données de natures différentes (données vectorielles et données matricielles ou raster) issues de la base de données géographiques libre OpenStreetMap.

L'acquisition, la manipulation et la cartographie de données géographiques, les opérations classiques de géotraitement ainsi que des traitements plus poussés d'analyse spatiale sont abordés.








