---
# Author(s)
authors:
- rysebaert
- cgrasland


# Date
date: "2021-10-25"


# Title
title: Analyse territoriale multiscalaire


# Short summary
summary: Analyse sur la concentration de l’emploi dans les communes de la métropole du Grand Paris, qui montre l’usage des fonctions du package MTA (Multiscalar Territorial Analysis) pour révéler l’existence d’inégalités territoriales dans un contexte multiscalaire. 


# Tags
tags:
- MTA
- multiscalaire
- territoire
- INSEE
- déviation
- équirépartition
- voisinage
- autocorrélation
- spatiale
- HyperAtlas
- Lorenz
- Métropole
- Grand Paris

publication_types: ["10"]


projects:
- rzine

sources:
- "8"

thematics: ["19"]




update :
- "0"

languages:
- "0"


# Link
url_source: '/docs/20211101_ysebaert_grasland_MTA/index.html'
url_code: 'https://github.com/rzine-reviews/mta_rzine'
url_dataset: '/docs/20211101_ysebaert_grasland_MTA/data.zip'

links:
- name: HAL
  url: https://hal.archives-ouvertes.fr/hal-03433206v1

doi: "10.48645/HJVQ-YP94"

# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---



**Auteur** : [Ronan Ysebaert](https://rzine.fr/authors/rysebaert/), [Claude Grasland](https://rzine.fr/authors/cgrasland/)    
**Evaluateur.rice.s** : Comité éditorial Rzine    
**Editeur.rice.s** : [Marion Gentilhomme](https://rzine.fr/authors/mgentilhomme/), [Hugues Pecout](https://rzine.fr/authors/hpecout)     


#### Résumé 

Cette fiche présente une analyse territoriale multiscalaire menée sur la concentration de l’emploi dans les communes de la métropole du Grand Paris (MGP). Cette analyse reproductible utilise des données de l’INSEE librement accessibles et montre l’usage et l’intérêt des fonctions du package `MTA` (Multiscalar Territorial Analysis) pour révéler l’existence d’inégalités territoriales dans un contexte multiscalaire.

#### Citation

<div style="font-size:15px;">
Ysebaert R, Grasland C (2021). <i>"Analyse territoriale, multiscalaire"</i>, doi: 10.48645/HJVQ-YP94, URL: https://rzine.fr/publication_rzine/20211101_ysebaert_grasland_MTA/.
</div>

<p class="d-inline-flex" style="gap:12px;"><a href="https://doi.org/10.48645/HJVQ-YP94"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://zenodo.org/badge/DOI/10.48645/HJVQ-YP94.svg" alt="DOI:10.48645/HJVQ-YP94"></a><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg" alt="License: CC BY-SA 4.0"></a></p>

