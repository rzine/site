---
title: L’analyse de données avec factominer
subtitle: Quelle évolution en 10 ans ?
summary: Communication réalisée dans le cadre d'un semin-R du MNHN

authors:
- fhusson

# Type de la publication 
publication_types: ["12"]


sources:
- "0"

thematics: ["12"]


update:
- "0"

languages:
- "0"

projects:
- seminr

tags:
- FactoMiner
- Factoshiny
- FactoInvestigate
- MissMDA
- PCA
- CA
- MCA
- MFA
- RcmdrPlugin
- ACP
- AC
- ACM


# Date that the page was published
date: "2016-06-10T00:00:00Z"
doi: ""

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
# Name of review or book if the publication has been published in
# publication:
# publication_short:  

featured: true

image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'http://rug.mnhn.fr/semin-r/PDF/semin-R_factomineR_FHusson_100616.pdf'




---


Communication réalisée dans le cadre d'un semin-R du MNHN



