---
# AUTHOR(S) 
authors:
- Hadley Wickham
- Jennifer Bryan


# DATE de soumission à Rzine
date: "2015-03-20"

# TITRE
title: R Packages

subtitle: Organize, test, document, and share your code
summary: Organize, test, document, and share your code


# TAGS
tags:
- devtools
- tidyverse
- fs
- foofactors
- testthat
- roxygen2
- knitr
- foo
- usethis
- Rstudio
- vignette
- metadata
- CRAN
- Git 
- workflow
- r packages
- R packages
- package




# TYPE DE PUBLICATION
publication_types: ["5"]


thematics: ["5","7"]


languages :
- "1"

sources :
- "2"

update :
- "1"


projects:
- oreilly


url_source: 'https://r-pkgs.org/'
url_code: 'https://github.com/hadley/r-pkgs'

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
publishDate: "2015-03-20T00:00:00Z"
# Name of review or book if the publication has been published in
publication: R Packages. Organize, test, document, and share your code
publication_short:  R Packages
  
  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Turn your R code into packages that others can easily download and use. This practical book shows you how to bundle reusable R functions, sample data, and documentation together by applying author Hadley Wickham’s package development philosophy. In the process, you’ll work with devtools, roxygen, and testthat, a set of R packages that automate common development tasks. Devtools encapsulates best practices that Hadley has learned from years of working with this programming language.

Ideal for developers, data scientists, and programmers with various backgrounds, this book starts you with the basics and shows you how to improve your package writing over time. You’ll learn to focus on what you want your package to do, rather than think about package structure.

- Learn about the most useful components of an R package, including vignettes and unit tests     
- Automate anything you can, taking advantage of the years of development experience embodied in devtools       
- Get tips on good style, such as organizing functions into files     
- Streamline your development process with devtools     
- Learn the best way to submit your package to the Comprehensive R Archive Network (CRAN)     
- Learn from a well-respected member of the R community who created 30 R packages, including ggplot2, dplyr, and tidyr     
