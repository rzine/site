---
title: R - Past and Future History
subtitle: A Draft of a Paper for Interface '98
summary: A Draft of a Paper for Interface '98

date: "1998-03-01T00:00:00Z"

authors:
- Ross Ihaka 

publication_types: ["10"]


sources:
- "0"

thematics: ["9"]


update:
- "0"

languages:
- "1"


projects:
- divers


tags:
- core team

url_source: 'https://www.stat.auckland.ac.nz/~ihaka/downloads/Interface98.pdf'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


R began as an experiment in trying to use the meth-ods of Lisp implementors to build a small testbed which could be used to trial some ideas on how a statistical environment might be built. Early on, the decision was made to use an S-like syntax.  Once that decision was made, the move toward being more and more like S has been irresistible. R has now outgrown its origins and its development is now a collaborative effort undertaken using the Internet to exchange ideas and distribute the results. The focus is now on how the initial experiment can be turned into a viable piece of free software.This paper reviews the past and present status of R and takes a brief look at where future development might lead.



