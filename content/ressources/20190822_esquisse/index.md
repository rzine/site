---
title: esquisse
subtitle: ggplot2 builder addin
summary: This addin allows you to interactively explore your data by visualizing it with the ggplot2 package

authors:
- Fanny Meyer
- Victor Perrier


projects:
- divers

sources :
- "0"

thematics: ["11"]





update :
- "1"

publication_types: ["6","1"]


languages:
- "1"


date: "2019-08-22T00:00:00Z"

tags:
- esquisse
- dreamRs
- ggplot2
- builder
- addin



url_source: 'https://dreamrs.github.io/esquisse/index.html'
url_code: 'https://github.com/dreamRs/esquisse'

links:
- name : CRAN
  url: https://cran.r-project.org/web/packages/esquisse/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**esquisse** is a *shiny* gadget to create *ggplot2* charts interactively with drag-and-drop to map your variables. You can quickly visualize your data accordingly to their type, export to 'PNG' or 'PowerPoint', and retrieve the code to reproduce the chart.
