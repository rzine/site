--- 
authors:
- rlamarcheperrin

date: "2017-10-23"

title: triversity - Diversity measures on tripartite graphs
subtitle: Meeting of the ANR AlgoDiv Project
summary: An R package for the computation of diversity measures on multipartite graphs

tags:
- graph
- tripartite
- multipartite
- diversity



publication_types: ["12","6"]


thematics: ["18"]


languages:
- "1"

sources :
- "0"

update :
- "1"

projects:
- divers

url_source: 'https://www-complexnetworks.lip6.fr/~lamarche/pdf/lamarche-perrin_2017_ALGODIV_slides_V2.pdf'


links:
- name : Package's repository
  url: https://github.com/Lamarche-Perrin/triversity
- name : CRAN
  url: https://cran.r-project.org/web/packages/triversity/index.html


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This package first implements a parametrized family of such diversity measures which apply on probability distributions. Sometimes called "True Diversity", this family contains famous measures such as the richness, the Shannon entropy, the Herfindahl-Hirschman index, and the Berger-Parker index. Second, the package allows to apply these measures on probability distributions resulting from random walks between the levels of tripartite graphs. By defining an initial distribution at a given level of the graph and a path to follow between the three levels, the probability of the walker's position within the final level is then computed, thus providing a particular instance of diversity to measure


