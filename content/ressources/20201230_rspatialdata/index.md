---
title: rspatialdata
summary: Collection of data sources and tutorials on visualising spatial data using R
subtitle: Collection of data sources and tutorials

authors:
- Paula Moraga
- Dilinie Seimon
- Varsha Ujjinni Vijay Kumar

publication_types: ["8","13"]


sources:
- "0"

thematics: ["19","2","20"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- map
- health
- rgeoboundaries
- osmdata
- wopr
- elevatr
- raster
- ggplot2
- malariaAtlas



date: "2020-12-30T00:00:00Z"


url_source: 'https://rspatialdata.github.io/index.html'
url_code: 'https://github.com/rspatialdata/rspatialdata.github.io'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Open and reliable data and analytical tools as well as collaborative research are crucial for solving global challenges and achieving sustainable development. In this website we provide a collection of data sources and tutorials **on downloading and visualising spatial data using R**. The website is currently in construction and includes tutorials on how to download and visualise administrative boundaries, Open Street Map data, population and malaria data. 





