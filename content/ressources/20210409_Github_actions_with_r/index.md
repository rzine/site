---
authors:
- Chris Brown
- Murray Cadzow
- Paula A Martinez
- Rhydwyn McGuire
- David Neuzerling
- David Wilkinson
- Saras Windecker

title: Github actions with R

summary: An introduction to use github actions with R

date: "2021-04-09"

publication_types: ["5"] 

thematics: ["1", "7"]


languages:
- "1"

update:
- "1" 

sources:
- "0" 


url_source: 'https://orchid00.github.io/actions_sandbox/' 



tags:
- Github
- git_pull
- git_push
- Workflow
- gh-pages
- pkgdown
- bookdown
- blogdown

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


GitHub actions allow us to trigger automated steps after we launch GitHub interactions such as when we push, pull, submit a pull request, or write an issue.

For example, there are actions that will automatically trigger:

   - continuous integration (CI)
   - messages in response to issues or pull requests
   - rendering/compiling e.g. of rmarkdown, bookdown, blogdowns etc

GitHub actions follow the steps designated in a yaml file, which we place in the .github/workflows folder of the repo. We can add these yaml files to our repo either by clicking on a series of steps on GitHub.com, or using wrapper functions provided by the usethis package, depending on which actions you wish to include. We describe both ways here.

