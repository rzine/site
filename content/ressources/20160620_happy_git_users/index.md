---
# AUTHOR(S) 
authors:
- Jennifer Bryan
- the STAT 545 TAs
- Jim Hester

# DATE de soumission à Rzine
date: "2016-06-20"

# TITRE
title: Happy Git and GitHub for the useR

subtitle: Let’s Git started
summary: A reference tutorial to use Git with R

# TAGS
tags:
- git
- github
- rstudio
- commit
- push
- merge

# TYPE DE PUBLICATION
publication_types: ["5"]


thematics: ["7"]


languages:
- "1"




sources:
- "0"

update:
- "1"

projects:
- divers


url_source: 'https://happygitwithr.com/'
url_code: 'https://github.com/jennybc/happy-git-with-r'

# publishDate: "2014-10-28T00:00:00Z"
# publication: Advanced R
# publication_short:  Advanced R
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Happy Git provides opinionated instructions on how to:

- Install Git and get it working smoothly with GitHub, in the shell and in the RStudio IDE.
- Develop a few key workflows that cover your most common tasks.
- Integrate Git and GitHub into your daily work with R and R Markdown.

The target reader is someone who uses R for data analysis or who works on R packages, although some of the content may be useful to those working in adjacent areas.

The first two parts, Installation and Connect Git, GitHub, RStudio, provide a “batteries included” quick start to verify your setup.

In Early GitHub Wins, we rack up some early success with the basic workflows that are necessary to get your work onto GitHub. We also show the special synergy between R/R Markdown/RStudio and GitHub, which provides a powerful demonstration of why all this setup is worthwhile.

The use of Git/GitHub in data science has a slightly different vibe from that of pure software develoment, due to differences in the user’s context and objective. Happy Git aims to complement existing, general Git resources by highlighting the most rewarding usage patterns for data science. This perspective on the Git landscape is presented in Basic Git Concepts and Daily Workflows.


#### License

Happy Git and GitHub for the useR by Jennifer Bryan is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/).



