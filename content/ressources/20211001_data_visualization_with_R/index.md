---
authors:
- Rob Kabacoff

title: Data Visualization with R

summary: This book helps you create the most popular visualizations

date: "2021-10-01"

publication_types: ["5"]

thematics: ["11"]

languages:
- "1"

update:
- "0"

sources:
- "0"


url_source: 'https://rkabacoff.github.io/datavis/'


tags:
- datavisualisation
- visualisation
- graphic
- graphs
- ggplot2
- plotly
- leaflet
- rbokeh
- rCharts
- hightcharter
- scatterplot3d
- heatmap
- superheat


  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

R is an amazing platform for data analysis, capable of creating almost any type of graph. This book helps you create the most popular visualizations - from quick and dirty plots to publication-ready graphs. The text relies heavily on the ggplot2 package for graphics, but other approaches are covered as well.


This work is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://creativecommons.org/licenses/by-nc-nd/4.0/).

The goal is make this book as helpful and user-friendly as possible. Any feedback is both welcome and appreciated. [rkabacoff@wesleyan.edu](mailto:rkabacoff@wesleyan.edu)
