---
title: Introduction à l’environnement R pour l’analyse de données quantitatives en psychologie
subtitle: Enseignement de Master de Psychologie
summary: Support de cours complet destiné à accompagner l'enseignement, la pratique et l'usage de méthodes quantitative en Psychologie.

authors:
- Jean Luc Kop

publication_types: ["5"]

sources:
- "0"

thematics: ["0", "10", "12", "13"]

languages:
- "0"

update:
- "0"

projects:
- divers

date: "2018-01-02T00:00:00Z"

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

tags:
- Analyse de variance
- Corrélation
- Régression simple
- Régression multiple
- Analyse en composantes principales
- Anova
- ACP
- Univariée
- Bivariées
- Chi2
- Spearman
- Kendall
- Pearson
- T de student




url_source: 'https://hal.univ-lorraine.fr/hal-02276521/document'


---

Support de cours complet destiné à accompagner l'enseignement, la pratique et l'usage de méthodes quantitative en Psychologie.


