---
authors:
- Rstudio

title: Quarto

subtitle: 0pen-source scientific and technical publishing system 

summary: 0pen-source scientific and technical publishing system built on Pandoc

date: "2022-09-01"

publication_types: ["1","8"]


thematics: ["1", "7", "9"]


languages:
- "1"

update:
- "1"

sources:
- "0" 

url_source: 'https://quarto.org/' 
url_code: 'https://github.com/quarto-dev/quarto-cli'


tags:
- quarto
- notebook
- qmd
- rstudio
- julia
- python
- javascript
- markdown
- literate programming

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Quarto is an open-source scientific and technical publishing system built on [Pandoc](https://pandoc.org). Quarto documents are authored using [markdown](https://en.wikipedia.org/wiki/Markdown), an easy to write plain text format.

In addition to the core capabilities of Pandoc, Quarto includes:

1.  Embedding code and output from Python, R, Julia, and JavaScript via integration with [Jupyter](https://jupyter.org/), [Knitr](https://yihui.org/knitr/), and [Observable](https://github.com/observablehq/).
2.  A variety of extensions to Pandoc markdown useful for technical writing including cross-references, sub-figures, layout panels, hoverable citations and footnotes, callouts, and more.
3.  A project system for rendering groups of documents at once, sharing options across documents, and producing aggregate output like [websites](https://quarto.org/docs/websites/) and [books](https://quarto.org/docs/books/).
4. Authoring using a wide variety of editors and notebooks including [JupyterLab](https://quarto.org/docs/tools/jupyter-lab.html), [RStudio](https://quarto.org/docs/tools/rstudio.html), and [VS Code](https://quarto.org/docs/tools/vscode.html).
5. A [visual markdown editor](https://quarto.org/docs/visual-editor/) that provides a productive writing interface for composing long-form documents.

Learn more about Quarto at <https://quarto.org>.
