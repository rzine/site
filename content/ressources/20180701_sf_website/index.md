
---
 
authors:
- Edzer Pebesma

date: "2018-07-01"

title: sf
subtitle: A package that provides simple features access for R
summary: The reference package for spatial data management

tags:
- spatial
- OGC
- GIS
- SIG
- GEOS
- GDAL
- PROJ
- DBI
- lwgeom
- map

publication_types: ["6"]


thematics: ["2"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

doi: '10.32614/RJ-2018-009'

url_source: 'https://r-spatial.github.io/sf/'

links:
- name : CRAN
  url: https://cran.r-project.org/web/packages/sf/index.html

publishDate: "2018-07-01T00:00:00Z"
publication: Journal of Open Source Software
publication_short: JOSS
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

A package that provides [simple features access](https://en.wikipedia.org/wiki/Simple_Features) for R. Package sf:

- represents simple features as records in a data.frame or tibble with a geometry list-column    
- represents natively in R all 17 simple feature types for all dimensions (XY, XYZ, XYM, XYZM)    
- interfaces to [GEOS](https://trac.osgeo.org/geos) to support geometrical operations including the [DE9-IM](https://en.wikipedia.org/wiki/DE-9IM)     
- interfaces to [GDAL](https://gdal.org/), supporting all driver options, Date and POSIXct and list-columns    
- interfaces to [PRØJ](https://proj.org/) for coordinate reference system conversions and transformations    
- uses [well-known-binary](https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry#Well-known_binary) serialisations written in C++/Rcpp for fast I/O with GDAL and GEOS    
- reads from and writes to spatial databases such as [PostGIS](http://postgis.net/) using [DBI](https://cran.r-project.org/web/packages/DBI/index.html)    
- is extended by pkg [lwgeom](https://github.com/r-spatial/lwgeom/) for further liblwgeom/PostGIS functions, including some spherical geometry functions     



