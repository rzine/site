---
date: "2020-07-28T00:00:00Z"


title: R Lille
subtitle: Groupe d'utilisateur⸱rices⸱
summary: Groupe d'utilisateur⸱rice⸱s R de Lille et sa périphérie

authors:
- Mickaël Canouil

tags: 
- séminaire
- formation
- conférence
- évènement
- groupe
- lille
- rlille

publication_types: ["4","8","2", "9"]

thematics: ["8"]

projects:
- rlille

sources:
- "0"

update:
- "1"

languages:
- "0"


url_source: 'https://rlille.fr/'
url_code: 'https://github.com/RLille/'

links:
- name: Meetup
  url: https://www.meetup.com/fr-FR/R-Lille/
- name: Youtube
  url: http://youtube.rlille.fr/


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


R Lille est le groupe des utilisateurs et utilisatrices R de Lille (et sa région) et a été créé début février 2020.

Vous utilisez R pour le travail et/ou le loisir ? Vous êtes curieux⸱se, débutant⸱e ou expert⸱e en R ? Vous êtes localisé⸱e sur Lille et sa périphérie, ou simplement de passage ?
Et plus important encore, vous avez envie d’échanger sur R et les nombreuses possibilités qu’offre ce logiciel et ses plus de 18 milles extensions ?

Si la réponse est "oui" à l’une de ces questions, rejoignez le groupe [MeetUp](https://www.meetup.com/fr-FR/R-Lille/)) R Lille sans plus tarder !

**Groupe MeetUp** : [https://www.meetup.com/fr-FR/R-Lille/](https://www.meetup.com/fr-FR/R-Lille/)     
**Site Internet** : [https://rlille.fr/](https://rlille.fr/)    
**Chaîne YouTube** : [http://youtube.rlille.fr/](http://youtube.rlille.fr/)    
**Twitter** : [https://twitter.com/rlille_rug](https://twitter.com/rlille_rug)    



