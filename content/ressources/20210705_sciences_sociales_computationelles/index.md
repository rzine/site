---
title: Stratégies Numériques en Sciences Sociales
subtitle : Analyse des données numériques en sciences sociales
summary: Cours et tutoriels proposés par l'axe sciences sociales computationnelles du CREST-CNRS


authors:
- eollion
- jboelaert


publication_types: ["13","9"]



sources:
- "0"

thematics: ["0","10","6"]


update:
- "0"

languages:
- "0"


date: "2021-07-05T00:00:00Z"

url_source: 'https://www.css.cnrs.fr/strategies-numeriques/'


tags:
- introduction
- web
- XPath
- expressions régulières
- regex
- automatisation
- téléchargement
- parsing
- RSelenium


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Ce cours part d’un constat : nous sommes de plus en plus entourés de données numériques. Dans le débat public, dans notre quotidien, dans la décision publique, et parfois jusque dans notre vie privée, des traces numériques enregistrent nos activités.

Cette abondance de données est déjà largement analysée par les sciences sociales. Elle est aussi utilisée, afin de poser des questions nouvelles, ou de reposer d’anciennes questions à partir de ces nouvelles sources.

L’enjeu de ce cours, c’est d’offrir à des étudiantes, des jeunes chercheuses qui n’ont pas forcément de compétences en informatique des éléments pour qu’elles puissent d’une part saisir certains des enjeux que pose la multiplication des données numériques, et d’autre part pour qu’elles puissent en tirer partie de façon empirique, en maitrîsant différents outils qui permettent de travailler à partir de ces données numériques.

Le cours a pour objectif principal de donner des bases de programmation. Mais ce faisant, il propose des éléments de réflexion théorique et épistémologiques sur ce que peuvent ces données numériques pour les sciences sociales, ce qu’elles peuvent leur faire. Il nous semble en effet qu’on ne peut dissocier l’analyse des données des conditions de leur production. 

Le cours peut être suivi en intégralité, ou de manière sélective. Chaque chapitre s’appuie toutefois sur des notions qui ont été vues précédemment. Aucune connaissance préalable en informatique n’est nécessaire.







