---
title: explor
subtitle: Interactive Interfaces for Results Exploration
summary: An R package to allow interactive exploration of multivariate analysis results

authors:
- jbarnier

projects:
- divers

sources :
- "0"

thematics: ["11","12"]





update :
- "1"

publication_types: ["6"]



languages:
- "1"


date: "2020-05-02T00:00:00Z"

tags:
- explor
- ACP
- ACM
- FactoMineR
- ade4
- GDAtools
- MASS
- shiny
- DT
- scatterD3
- RColorBrewer


url_source: 'https://juba.github.io/explor/'
url_code: 'https://github.com/juba/explor/'

links:
- name : CRAN
  url: https://cran.r-project.org/web/packages/explor/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**explor** is an R package to allow interactive exploration of multivariate analysis results.
