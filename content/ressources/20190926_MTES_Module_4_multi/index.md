---
title: Analyse des données multi-dimensionnelles
subtitle : ParcoursR (module 4) - « Exploration avancée de données »
summary: Module 4 « Exploration avancée de données ». Formations R aux MTES & MCTRCT.

authors:
- Vivien Roussez
- Pascal Irz

publication_types: ["5","13"]


thematics: ["12"]


sources:
- "3"

update:
- "1"

projects:
- parcoursr

languages:
- "0"

date: "2019-09-26T00:00:00Z"
doi: ""

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

tags:
- projet
- ACP
- AFC
- ACM
- CAH
- factoMiner
- factoextra
- clustering
- k-moyennes
- formation
- module
- ministère
- MTES
- MCTRCT
- G2R


url_source: 'https://mtes-mct.github.io/parcours_r_module_analyse_multi_dimensionnelles/'
url_code: 'https://github.com/MTES-MCT/parcours_r_module_analyse_multi_dimensionnelles'


---

Ce support présente la **méthodologie pour évaluer, en fonction des caractéristiques des données, la pertinence des méthodes usuelles d'analyse multidimensionnelle (ACP, AFC, ACM, CAH)**.
Il propose une **mise en en oeuvre avec le package factoMiner et des sorties graphiques avec le package factoextra**.


Il est proposé par les **Ministères de la transition écologique et solidaire** ([MTES](https://www.ecologique-solidaire.gouv.fr/)), et **de la Cohésion des territoires et des Relations avec les collectivités territoriales**([MCTRCT](http://www.cohesion-territoires.gouv.fr/)).

Ce dispositif inédit de formation vise à faire monter en compétence les agents dans le domaine de la science de la donnée, avec le logiciel R. Avec R, le champ des possibles est tel qu’il existe autant d’usages que d’utilisateurs. Le parcours est composé de plusieurs modules à mettre en oeuvre en fonction des attentes des stagiaires. Les deux premiers sont nécessaires pour prendre en main l’outil. Les autres modules couvrent une ou plusieurs fonctionnalités de R, que les stagiaires pourront découvrir en fonction de leurs attentes. Chacun peut ainsi se former « à la carte » en fonction de ses usages professionnels et des domaines dans lesquels la montée en compétence est nécessaire.

Chaque module comprend, en introduction, l’acquisition ou le rappel des notions statistiques abordées. Suit la mise en œuvre des méthodes avec R, et enfin les clés d’interprétation des sorties statistiques et graphiques.

*Ces différents supports on été produits par un groupe de référents sur le logiciel opensource R (nom de code : G2R), qui a été mis en place avec des utilisateurs avancés de tout horizon (administration centrale, services déconcentrés et établissements publics) et de profils très varisés (géomaticiens, statisticiens et spécialistes de diverses thématiques mobilisant la donnée). Grâce à la richesse de leur parcours, chaque référent apporte son expertise dans la construction de ces formations.*



