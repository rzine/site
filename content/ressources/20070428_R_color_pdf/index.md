---
title: Colors in R
subtitle: Use colors by names
summary: List of colour implemented by name in R

authors:
- Tian Zheng

publication_types: ["0"]


thematics: ["11"]


sources :
- "0"

update :
- "0"

languages:
- "1"

projects:
- divers

date: "2007-04-28T00:00:00Z"

url_source: 'http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


