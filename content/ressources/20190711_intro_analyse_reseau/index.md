---
# the title of your publication
title: Introduction à l'analyse de réseau avec R
subtitle: Mesures et représentations

# a one-sentence summary of the content on your page. The summary can be shown on the homepage and can also benefit your search engine ranking.
summary: Support de formation d'initiation à R et à l'analyse de réseau (igraph) utilisé pour une école d’été organisée par le GDR ARSHS, à Nice en Juillet 2019.


# An abstract - the summary of your publication
abstract: 

# Display the authors of the publication and link to their user profiles (if they exist).
authors:
- hpecout
- lbeauguitte

# Type de la publication 
publication_types: ["13"]


thematics: ["0","18"]



languages:
- "0"


update:
- "0"


# Project name associated ?
# Set the name if several publications can be grouped in one projet
projects:
- GDR_ARSHS

sources :
- "0"


# Date that the page was published
date: "2019-07-11T00:00:00Z"
doi: ""

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
publishDate: "2019-07-11T00:00:00Z"
# Name of review or book if the publication has been published in
# publication: 
# publication_short:  


# by setting featured: true, a page can be displayed in the Featured widget. This is useful for sticky, announcement blog posts or selected publications etc. J'ai pas bien compris cet paramètre pour l'instant...
featured: true

# To display a featured image to represent your publication 
# To use, place an image named `featured.jpg/png` in your page's folder.
# set `placement` options to `1`  
# set `focal_point` options to  `Center`
# Set `preview_only` to `true` to just use the image for thumbnails. if not : `false`
# Set `caption` to precise the pitcure credit (with url), if necessary 
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  

# Tagging your content helps users to discover similar content on your site. 
# Tags can improve search relevancy and are displayed after the page content and also in the Tag Cloud widget.
tags:
- igraph
- analyse de réseau
- graphe
- initiation
- centralité
- densité
- composante connexe
- diamètre
- transitivité
- ego network
- communauté


# The url_ links can either point to local or web content. 
# Associated local publication content, may be copied to the publication’s folder and referenced like 


url_code: 'https://github.com/HuguesPecout/Analyse_r-seaux_GDR_ARSHS'
url_dataset: 'https://github.com/HuguesPecout/Analyse_r-seaux_GDR_ARSHS/raw/main/data.zip'
url_source: 'https://huguespecout.github.io/Analyse_r-seaux_GDR_ARSHS/'


  
# if the is an associated slide to this publication which is upload on the website
# Set the name of the folder where the md of the slide is saved.
# slides: example  

---

Ce document est un **support de formation d'initiation à R/Rstudio et à l'analyse de réseaux** avec **igraph**.

Il s’agit du support utilisé pour l’atelier *"mesures et représentations"* réalisé dans le cadre de l’École d’été : [Initiation à l’analyse de réseaux en SHS](https://arshs.hypotheses.org/1200), organisé par le [**GDR** « *Analyse de Réseaux en Sciences Humaines et Sociales*](https://arshs.hypotheses.org/) » à Nice du 1 au 6 Juillet 2019, au Centre de la Méditerranée Moderne et Contemporaine.

La première partie de ce document introduit le langage/logiciel R ainsi que l'IDE Rstudio. Une seconde partie présente les fonctionnalités, principes et manipulations de base à connaitre pour utiliser R. Le reste du document est consacré à la pratique de l'analyse de réseau avec le package **igraph**. Les principales fonctions de mesure (globale et locale) et de représentation de graphes y sont présentées.







