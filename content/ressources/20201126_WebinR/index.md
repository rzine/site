---
authors:
- jlarmarange

title: webin-R
summary: Wébinaire hebdomadaire sur l’analyse d’enquêtes avec R et RStudio, qui s'appuie sur le projet [analyse-R](https://larmarange.github.io/analyse-R/)

date: "2020-11-26"

publication_types: ["9", "13"]


thematics: ["0","10","11","12","13","16"]


languages:
- "0"

update:
- "1"

sources:
- "0"


url_source: 'https://larmarange.github.io/webin-R/'

links:
- name: Chaîne Youtube
  url: https://www.youtube.com/c/webinR
- name: analyse-R
  url: https://larmarange.github.io/analyse-R/


tags:
- enquête
- manipuler
- analyser
- appronfondir
- prise en main
- manipulation
- import
- export
- statistique
- ACM
- CAH
- intervalles
- pondérée
- régression
- échantillonage
- multicolinéarité
- survie
- séquence
- trajectoire
- graphique
- représentation
- programmation
- réseaux
- spatiale
- tidyverse
- dyplr
- data.table
- stringr
- tidyr
- représentation
- ggplot2
- lattice
- markdown
- scraping



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Webin-R est un wébinaire hebdomadaire de deux heures, tous les jeudis de 17h à 19h (CET heure de Paris). Webin-R s’appuie sur le projet [analyse-R]( https://larmarange.github.io/analyse-R/) dont il reprend les contenus.

### Comment participer ?

webin-R est diffusé en live et enregistré. Les vidéos des sessions précédentes sont accessibles via le menu [Séances](https://larmarange.github.io/webin-R/seances.html).
Les liens de connexion à la plateforme de diffusion sont diffusés par email. Pour s’inscrire à la liste de diffusion webin-R et recevoir les liens de connexion hebdomadaires, il vous suffit d’envoyer un email avec pour objet "subscribe webin-R" à sympa@listes.ird.fr.

Retrouvez le programme des prochaines séances à ce [lien](https://larmarange.github.io/webin-R/#programme-prochaines-s%C3%A9ances).



