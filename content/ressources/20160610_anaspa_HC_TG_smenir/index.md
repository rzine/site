---
title: Du point à la surface
subtitle: Interpolation et interaction spatiale
summary: Communication réalisée dans le cadre d'un semin-R du MNHN

authors:
- hcommenges
- tgiraud

# Type de la publication 
publication_types: ["12"]


sources :
- "0"

thematics: ["19"]


update :
- "0"




languages:
- "0"


projects:
- seminr

tags:
- interpolation
- interaction
- régression
- KDE
- GWR
- IDW
- MASS
- spatstat
- gstat
- stats
- SpatialPosition
- semin-r


# Date that the page was published
date: "2016-06-10T00:00:00Z"
doi: ""

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
# Name of review or book if the publication has been published in
# publication:
# publication_short:  

featured: true

image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'http://rug.mnhn.fr/semin-r/PDF/semin-R_interpolation-interaction-spatiale_HCommenges-TGiraud_100616.pdf'




---


Communication réalisée dans le cadre d'un semin-R du MNHN



