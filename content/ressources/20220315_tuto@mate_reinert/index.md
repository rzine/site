---
authors:
- jbarnier

date: "2022-03-15"

title: Tutot@Mate - La méthode Reinert de classification textuelle

subtitle: Mise en œuvre avec le package `Rainette`

summary: Mise en œuvre avec le package `Rainette`

tags:
- rainette
- tuto@mate
- analyse textuelle
- classification
- non supervisée
- Reinert
- CDH
- Segmentation
- min_segment_size

publication_types: ["12","9","6"]

sources:
- "0"

thematics: 
- "17"

update:
- "0"

languages:
- "0"

# Link
url_source: 'https://mate-shs.cnrs.fr/wp-content/uploads/2022/03/Tuto42_slides_Barnier_rainette.pdf'


links:
- name: Vidéo
  url: https://www.youtube.com/watch?v=T9r8T5WZYHY
- name: Corpus et code 1
  url: https://mate-shs.cnrs.fr/wp-content/uploads/2022/03/Tuto42_Ex1_grand_debat.zip
- name: Corpus et code 1
  url: https://mate-shs.cnrs.fr/wp-content/uploads/2022/03/Tuto42_Ex1_robinson.zip
- name: Package rainette
  url: https://juba.github.io/rainette/index.html


# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---


**Les Tuto@Mate ont reçu Julien Barnier le mardi 15 mars 2022, pour présenter le package `rainette`.**

**`Rainette` est une extension pour R permettant de mettre en œuvre la méthode de classification textuelle de Reinert**, déjà présente dans d’autres logiciels comme *Alceste* ou *Iramuteq*. Cette méthode exploratoire permet de déterminer, à partir d’un corpus textuel (réponses à des questions ouvertes, tweets, discours, entretiens…), des groupes de documents ou de segments de documents en fonction des termes qui les composent.
Outre l’implémentation de l’algorithme, `rainette` propose également quelques outils facilitant l’exploration des résultats.

Dans ce tuto est présenté le principe de la méthode Reinert, ainsi qu’un ou deux exemples d’application sous R.

Julien Barnier est ingénieur d’études CNRS au Centre Max Weber. Il est l’auteur du document “[Introduction à R et au tidyverse](https://rzine.fr/publication/20200202_jbarnier_tidyverse/)” et développeur de plusieurs extensions pour R ([`explor`](https://rzine.fr/publication/20200502_explor_barnier/), `questionr`, `rmdformats`, `robservable`…).

Retrouvez [plus d'info sur cette présentation](https://mate-shs.cnrs.fr/actions/tutomate/tuto42-rainette-julien-barnier/) et sur l'ensemble des Tuto@Mate sur le site du réseau MATE-SHS : https://mate-shs.cnrs.fr/actions/tutomate/







