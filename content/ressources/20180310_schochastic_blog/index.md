---
title: Schochastics
subtitle: Articles, tutorials and R packages on network analysis and sports data visualisation
summary: Articles, tutorials and R packages on network analysis and sports data visualisation

authors:
- David Schoch


# Type de la publication 
publication_types: ["8"]


thematics: ["18"]





sources :
- "0"

update :
- "1"


languages:
- "1"


projects:
- divers

date: "2018-03-10T00:00:00Z"
doi: ""


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
tags:
- network
- graphlayouts
- snahelper
- netrankr
- networkdata
- signet
- visualisation
- package
- centrality
- multilevel
- ggraph
- Rstudio
- mapping
- ggplot2
- neighborhood
- nba
- soccer



url_source: 'http://blog.schochastics.net/'
url_code: 'https://github.com/schochastics'

links:
- name: Tutorials & Packages
  url: http://mr.schochastics.net/#tutorials

  

---


**Personal website** and **blog** about **network analysis**, **sports data analysis** and **visualisation**.

