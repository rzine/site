---
title: Accessibilité et données OpenStreetMap
subtitle : Mesurer l'accessibilité aux maternités avec des données Open Source
summary: Atelier pratique portant sur la mesure d'accessibilité à partir de données OSpenStreetMap

authors:
- tgiraud
- rysebaert
- hpecout


projects:
- INFTER

sources:
- "0"

publication_types: ["13"]


thematics: ["19"]


update :
- "0"

languages:
- "0"

categories: ["santé"]

date: "2019-06-12T00:00:00Z"


publishDate: "2019-12-01T00:00:00Z"
publication: Axe Information territoriale locales, FR CIST
publication_short:  Axe INFTER, FR CIST

tags:
- osm
- osrm
- SpatialPosition
- cartography
- sf
- banr
- osmdata
- maternité
- accessibilité
- potentiel


url_code: 'https://gitlab.huma-num.fr/rCarto/santelocal'
url_dataset: 'https://gitlab.huma-num.fr/rCarto/sante-infer/-/archive/master/sante-infer-master.zip'
url_source: 'https://rcarto.gitpages.huma-num.fr/santelocal/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Cet atelier a été réalisé dans le cadre de l'[axe information territoriale locale](https://cist.prod.lamp.cnrs.fr/informations-territoriales-locales-et-analyse-comparative-des-dynamiques-metropolitaines-le-projet-grandes-metropoles/) de la [FR CIST](https://cist.cnrs.fr/), en Juin 2019




