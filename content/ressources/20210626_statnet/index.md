---
title: Statnet
subtitle: Suite of R packages for the management of network data
summary: Suite of R packages for the Statistical Modeling of Network Data

authors:
- Pavel N. Krivitsky
- Mark S. Handcock
- David R. Hunter
- Carter T. Butts
- Chad Klumb
- Steven M. Goodreau
- Martina Morris

publication_types: ["6"]

thematics: ["18"]

sources:
- "0"

update:
- "1"

languages:
- "1"


date: "2021-06-26"


tags:
- network
- sna
- latentnet
- network
- ergm
- statnetWeb
- ergm.ego
- ergm.multi
- networkDynamic 	
- tergm
- ndtv
- relevent 
- tsna
- ergm
- ergm.count
- ergm.rank
- ergm.userterms

url_code: 'https://github.com/statnet/statnet'
url_source: 'https://statnet.org/'


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


The `statnet` project publishes a suite of open source R-based software packages for network analysis, along with a comprehensive set of training materials. The software is developed on GitHub, and published to the Comprehensive R Archive Network (CRAN). The training materials can be found on this site.





