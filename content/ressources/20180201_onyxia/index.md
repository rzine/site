---
date: "2018-02-01T00:00:00Z"

title: Onyxia - SSP Cloud
subtitle: Plateforme open-source de traitement de données
summary: Plateforme open-source de traitement de données et un espace communautaire pour la statistique publique

authors:
- INSEE

tags: 
- INSEE
- Python
- R
- plateforme
- open-source
- web
- collaborative
- image
- communauté
- projet

publication_types: ["7", "1"]

thematics: ["7", "8", "9"]

update:
- "1"

languages:
- "0"

sources:
- "4"

url_source: 'https://datalab.sspcloud.fr/home'
url_code: 'https://github.com/InseeFrLab/onyxia'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Le projet **Onyxia développé par l'INSEE** part du constat de difficultés communes rencontrées par les *data scientists* du secteur public :

- des agents souvent isolés, du fait de la relative rareté des compétences data dans l'administration ;
- des infrastructures inadaptées, aussi bien en matière de ressources que de technologies, qui constituent un frein à l'innovation ;
- une difficulté à passer de l'expérimentation à la mise en production, du fait de multiples séparations (séparation physique, langage de développement, modes de travail) entre les directions métier et la production informatique.

Face à ce constat, le [Datalab SSP Cloud](https://datalab.sspcloud.fr/home) a été construit pour proposer une plateforme de mutualisation à plusieurs niveaux : 

- partage d'une infrastructure moderne, centrée autour du déploiement de services via des conteneurs, et dimensionnée pour les usages de data science ;
- partage de méthodes, via une mutualisation des services de data science proposés, auxquels chacun peut contribuer ;
- partage de connaissances, via des formations associées au Datalab ainsi que la constitution de commaunautés d'entraide centrées sur son utilisation.


**Onyxia, Datalab SSP Cloud : quelles différences ?**

[**Onyxia**](https://github.com/InseeFrLab/onyxia) est un projet open-source qui propose une plateforme de services de data science, accessible via une application Web. Le [**Datalab SSP Cloud**](https://datalab.sspcloud.fr/home) est une instance du projet Onyxia, hébergée à l'Insee.


</br>

<center>
<a href="https://datalab.sspcloud.fr/home" target="_blank" class="btn btn-info" role="button" aria-disabled="true"><i class="bi bi-cloud-arrow-down"></i> <b>Datalab SSP cloud</b> </a>
</center>


</br>

**Ressources :**

- [**Projet Onyxia**](https://github.com/InseeFrLab/onyxia)  
- [**Site web SSP cloud**](https://www.sspcloud.fr/)  
- [**Documenation SSP cloud**](https://docs.sspcloud.fr/)  

</br>



