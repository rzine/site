---
title: The Mockup Blog
subtitle:
summary: Personal blog about data wrangling, analyzing, modeling, and visualization

authors:
- Thomas Mock

publication_types: ["8"]


sources :
- "0"

thematics: ["11","1"]


update :
- "1"

languages:
- "1"

projects:
- divers

date: "2020-01-07T00:00:00Z"


tags: 
- visualization
- espnscrapeR
- ggplot2
- gt
- JSON
- magick
- nfl
- NFL
- nflfastR 
- nflscrapR
- plotly
- purrr
- reactable
- rmarkdown
- SQL
- tables
- tidymodels
- TidyTuesday
- tidyverse
- web scraping


  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://themockup.blog/'
url_code: 'https://github.com/jthomasmock/radix_themockup'

---


Personal blog about data wrangling, analyzing, modeling, and visualization.









