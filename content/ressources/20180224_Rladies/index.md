---
title: R-Ladies
subtitle: A world-wide organization to promote gender diversity in the R community
summary: A world-wide organization to promote gender diversity in the R community

date: "2018-02-24T00:00:00Z"

authors:
- R-Ladies global team 

publication_types: ["8", "9", "12"]


sources:
- "0"

thematics: ["8"]


update:
- "1"

languages:
- "1"


projects:
- divers


tags:
- rladies
- R Ladies
- Group
- user
- community
- event
- learning

url_source: 'https://rladies.org/'


links:
- name: GitHub
  url: https://github.com/rladies
- name: Youtube chanel
  url: https://www.youtube.com/c/RLadiesGlobal
- name: Meetup
  url: https://www.meetup.com/pro/rladies/
- name: Twitter
  url: https://twitter.com/RLadiesGlobal


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



As a diversity initiative, R-Ladies’ mission is to achieve proportionate representation by encouraging, inspiring, and empowering the minorities currently underrepresented in the R community. R-Ladies’ primary focus, therefore, is on supporting the R enthusiasts who identify as an underrepresented minority to achieve their programming potential, by building a collaborative global network of R leaders, mentors, learners, and developers to facilitate individual and collective progress worldwide.


