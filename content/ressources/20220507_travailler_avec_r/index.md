---
title: Travailler avec R

authors:
- Eric Marcon

summary: Introduction à R, R Markdown, Shiny, la création de packages et l'utilisation de GitHub avec RStudio

date: "2022-07-05"

publication_types: ["5"]

thematics: ["0", "1", "5", "7"]

languages: 
- "0" 

update:
- "1" 

sources:
- "0"

url_source: 'https://ericmarcon.github.io/travailleR/' 
url_code: 'https://github.com/EricMarcon/travailleR' 

tags:
- R Markdown
- Shiny
- Package
- GitHub
- Git
- site 
- document
- présentation
- Intégration continue
- enseignement
- flux de travail
- parallélisation
- S3
- S4
- RC
- S6


doi: "10.5281/zenodo.5778902"
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Ce support de cours propose une organisation du travail autour de R et RStudio pour, au-delà des statistiques, rédiger des documents efficacement avec R Markdown, aux formats variés (mémos, articles scientifiques, mémoires d’étudiants, livres, diaporamas), créer son site web et des applications R en ligne (Shiny), produire des packages et utiliser R pour l’enseignement.

