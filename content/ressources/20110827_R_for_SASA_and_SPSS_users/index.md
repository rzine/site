---
# AUTHOR(S) 
authors:
- Robert A. Muenchen

# DATE de soumission à Rzine
date: "2011-08-01"

# TITRE
title: R for SAS and SPSS Users

subtitle: Second Edition
summary: This book helps you translate what you know about SAS or SPSS into a working knowledge of R as quickly and easily as possible.

# TAGS
tags:
- springer
- SAS
- SPSS



# TYPE DE PUBLICATION
publication_types: ["5"]


thematics: ["7"]


languages:
- "1"

sources:
- "0"

update:
- "0"


projects:
- divers

doi: "10.5555/2049757"

url_source: '/docs/2011_R_for_SASA_and_SPSS_users/2011_Book_RForSASAndSPSSUsers.pdf'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

## Preface

*While SAS and SPSS have many things in common, R is very different. My goal in writing this book is to help you translate what you know about SAS or SPSS into a working knowledge of R as quickly and easily as possible. I point out how they differ using terminology with which you are familiar, and show you which add-on packages will provide results most like those from SAS or SPSS. I provide many example programs done in SAS, SPSS, and R so that you can see how they compare topic by topic. When finished, you should know how to :*

- *Install R, choose a user interface, and choose and install add-on packages.*
- *Read data from various sources such as text or Excel files, SAS or SPSS data sets, or relational databases.*
- *Manage your data through transformations, recodes, and combining data sets from both the add-cases and add-variables approaches and restructuring data from wide to long formats and vice versa.*
- *Create publication-quality graphs including bar, histogram, pie, line, scatter, regression, box, error bar, and interaction plots.*
- *Perform the basic types of analyses to measure strength of association and group differences, and be able to know where to turn to learn how to do more complex methods.*


