---
title: Analyse d’images raster (et télédétection)
subtitle : Introduction à la manipulation et l'analyse de raster
summary: Support d'un cours réalisé dans le cadre de l'école SIGR2021


authors:
- mmadelin

publication_types: ["12","13"]


sources:
- "9"

thematics: ["20","2"]


update:
- "0"

languages:
- "0"


projects:
- SIGR2021

date: "2021-07-01T03:00:00Z"

url_source: 'https://mmadelin.github.io/sigr2021/SIGR2021_raster_MM.html#1'
url_code: 'https://github.com/mmadelin/sigr2021'

tags:
- SIGR2021
- raster
- image
- télédétection
- modisTSP
- sen2r
- stars
- terra
- projection
- crop
- mask
- découpage
- masque
- agrégation
- fusion
- reclassification
- calcul matriciel
- cellStats
- sp
- rasterVis
- leaflet
- tmap
- rayshader
- mapview



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Communication réalisée durant l'école thématique SIGR2021 sur les sciences de l’information géographique reproductibles.

Cette présentation introduit à la manipulation et l'analyse d'image raster.








