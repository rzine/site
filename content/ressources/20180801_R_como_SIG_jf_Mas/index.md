---
title: Análisis espacial con R
subtitle: Usa R como un Sistema de Información Geográfica
summary: Usa R como un Sistema de Información Geográfica

authors:
- Jean-Francois Mas

date: "2018-08-01"


tags:
- SIG
- Información Geográfica
- Análisis espacial

publication_types: ["5"]


thematics: ["2"]


sources :
- "0"

update :
- "0"

languages:
- "2"


projects:
- divers


url_source: 'https://eujournal.org/files/journals/1/books/JeanFrancoisMas.pdf'

featured: true
image:
  caption: "© José Cuerda"
  focal_point: "Center"
  preview_only: false
---

