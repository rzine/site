---
title: Coding club
subtitle: A Positive Peer-Learning Community
summary: A Positive Peer-Learning Community

authors:
- Students and researchers form University of Edinburgh

publication_types: ["8"]


thematics: ["0","1","10","11","13","15","19","2","20","3","5","6","7","8"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

date: "2016-10-15T00:00:00Z"


tags:
- basics
- manipulation
- visualisation
- modelling
- spatial
- reproductible
- Google earth engine



url_source: 'https://ourcodingclub.github.io/'
url_code: 'https://github.com/ourcodingclub'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


#### About us

We are an enthusiastic group of ecology and environmental science students and researchers from the University of Edinburgh. We want to replace statistics anxiety and code fear with inspiration and motivation to learn. We believe that coding can be really fun and also understand the importance of quantitative skills for professional development.

Over the last four years we have been writing [tutorials](https://ourcodingclub.github.io/tutorials.html) as well as organising in-person workshops. All of our materials are free for people to use and adapt - you can find more information on organising your own workshops or using Coding Club tutorials in your teaching [here](https://ourcodingclub.github.io/involve.html).

Coding Club is for everyone, regardless of their career stage or current level of knowledge. Coding Club is a place that brings people together, regardless of their gender or background. We all have the right to learn, and we believe learning is more fun and efficient when we help each other along the way.


