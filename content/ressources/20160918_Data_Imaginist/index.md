---
title: Data Imaginist
subtitle: Visualization and beyond…
summary: Blog on data visualization. The author is involved in several reference R packages (ggplot2, gganimate, ggraph, tidygraph...)



authors:
- Thomas Lin Pedersen


# Type de la publication 
publication_types: ["8"]


thematics: ["11","18"]





sources :
- "0"

update :
- "1"


languages:
- "1"



projects:
- divers

date: "2016-09-18T00:00:00Z"
doi: ""


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
tags:
- ggplot2
- network
- ggraph
- edges
- node
- layout
- blogdown
- ggplot2
- tidygraph
- tweenr
- scico
- transformr
- gganimate
- ggfroce
- farver
- transformr
- ambient



url_source: 'https://www.data-imaginist.com/'
url_code: 'https://github.com/thomasp85'


  

---


Personal blog on **data visualization**. The author is involved in **several reference R packages (ggplot2, gganimate, ggraph, tidygraph...)**.

