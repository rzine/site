---
authors:
- Daniel Dauber

date: "2023-04-26"

title: R for Non-Programmers - A Guide for Social Scientists

summary: This book provides a helpful resource for anyone looking to use R for their research projects, regardless of their level of programming or statistical analysis experience

tags:
- rstudio
- project
- Linearity 
- additivity
- independence
- Normality
- homoscedasticity
- outlier
- correlation
- regression
- text
- N-gram
- stop word


publication_types: ["5"]


sources:
- "0"

thematics: ["0", "10", "12", "17"]

update :
- "1"

languages:
- "1"

url_source: 'https://bookdown.org/daniel_dauber_io/r4np_book/'
url_code: 'https://github.com/ddauber/r4np_book'


featured: false
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This book provides a helpful resource for anyone looking to use R for their research projects, regardless of their level of programming or statistical analysis experience. Each chapter presents essential concepts in data analysis in a concise, thorough, and user-friendly manner. R4NP will guide you through your first steps on a possibly endless journey full of ‘awe and wonder’.

This book is designed to be accessible to beginners while also providing valuable insights for experienced analysts looking to transition to R from other computational software. You don’t need any prior knowledge or programming skills to get started. R4NP provides a comprehensive entry into R programming, regardless of your background.

