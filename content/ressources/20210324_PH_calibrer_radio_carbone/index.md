---
title: Calibrer des âges radiocarbone avec R
summary: Cette leçon vous apprend à calibrer des âges radiocarbone avec R. Elle explique pas à pas comment réaliser la calibration d’un ensemble de dates et comment explorer et présenter les résultats. Leçon proposée par [The Programming Historian](https://programminghistorian.org/)

authors:
- Nicolas Frerebeau
- Brice Lebrun

publication_types: ["10","13"]

sources:
- "10"

thematics: ["13"]

update:
- "0"

languages:
- "0"

tags:
- carbone 14
- radiocarbone
- datation
- rcarbon
- Programming Historian
- PH
  

projects:
- programming_historian


date: "2021-03-25T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  


doi: '10.46430/phfr0016'
url_source: 'https://programminghistorian.org/fr/lecons/calibration-radiocarbone-avec-r'



---

Cette leçon vous apprend à calibrer des âges radiocarbone avec R. Après un rappel sur le principe du radiocarbone et les enjeux de la calibration, cette leçon explique pas à pas comment réaliser la calibration d’un ensemble de dates et comment explorer et présenter les résultats. Leçon proposée par [The Programming Historian](https://programminghistorian.org/).



