---
title: What They Forgot to Teach You About R
subtitle: 
summary: Adopt or revise good practices (organization of projects, work environment)

authors:
- Jennifer Bryan
- Jim Hester

# Type de la publication 
publication_types: ["5"]


sources:
- "0"

thematics: ["7"]


update:
- "1"

languages:
- "1"


projects:
- divers

date: "2019-01-15T00:00:00Z"


url_source: 'https://rstats.wtf/'
url_code: 'https://github.com/rstats-wtf/what-they-forgot'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


You’ll learn holistic workflows that address the most common sources of friction in data analysis. We’ll work on project-oriented workflows, version control for data science (Git/GitHub!), and how to plan for collaboration, communication, and iteration (incl. RMarkdown). In terms of your R skills, expect to come away with new knowledge of your R installation, how to maintain it, robust strategies for working with the file system, and ways to use the purrr package for repetitive tasks.

You should take this workshop if you’ve been using R for a while and you feel like writing R code is not what’s holding you back the most. You’ve realized that you have more pressing “meta” problems that no one seems to talk about: how to divide your work into projects and scripts, how to expose your work to others, and how to get more connected to the R development scene. The tidyverse is not an explicit focus of the course (other than the purrr segment) and you can certainly work through the content without it. But you should expect a great deal of tidyverse exposure.



