---
authors:
- Agustin Calatroni

title: Interactive dashboards without Shiny

summary: Presentation of alternatives to Shiny to produce interactive dashboards

date: "2022-06-22"

publication_types: ["12"]

thematics:
- "5" 

languages:
- "1" 

update:
- "0" 

sources:
- "0"

url_source: 'https://agstn.github.io/UseR2022_dashboards/'
url_code: 'https://github.com/agstn/UseR2022_dashboards'

tags:
- Dashboards
- flexdashboard
- plotly
- trelliscopejs
- DT
- leaflet
- crosstalk
- UseR!
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

RStudio’s `flexdashboard` package is a powerful tool to create interactive dashboards in R using R Markdown. A variety of layouts can be quickly generated including multiple pages, storyboards, and commentaries, as well as embedded tabs and drop-down menus. Additionally, with minimal programming effort, the dashboards can be customized via pre-packaged themes or custom CSS. Dashboards can be further extended for user interactivity with tables and visualizations by judicious use of HTML widgets to create a standalone HTML file with no special client or server requirement. This talk present a workflow utilizing flexdashboard and leveraging the abilities of other individual packages, such as `trelliscopejs`, `plotly`, `DT`, `reactable`, `leaflet`, `crosstalk`, to create highly interactive clinical trial reports for data monitoring and/or statistical analyses results. By avoiding the use of shiny, these reports can be conveniently emailed, deployed on an internal company webpage, or added to GitHub pages for widespread accessibility. Demonstrations of dashboards will include those listed below as well as additional examples that have been developed for current clinical trials monitoring.
