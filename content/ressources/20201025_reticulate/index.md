---
title: reticulate
subtitle: R Interface to Python
summary: This package provides a comprehensive set of tools for interoperability between Python and R

authors:
- Kevin Ushey
- J. J. Allaire
- Yuan Tang 
- Rstudio

publication_types: ["6"]


sources:
- "0"

thematics: ["7","9"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- python
- panda


date: "2020-10-25T00:00:00Z"

url_source: 'https://rstudio.github.io/reticulate/index.html'
url_code: 'https://github.com/rstudio/reticulate'

links:
- name: CRAN
  url: https://cran.r-project.org/web/packages/reticulate/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

The **reticulate** package provides a comprehensive set of tools for interoperability between Python and R. The package includes facilities for:

- Calling Python from R in a variety of ways including R Markdown, sourcing Python scripts, importing Python modules, and using Python interactively within an R session.
- Translation between R and Python objects (for example, between R and Pandas data frames, or between R matrices and NumPy arrays).
- Flexible binding to different versions of Python including virtual environments and Conda environments.

Reticulate embeds a Python session within your R session, enabling seamless, high-performance interoperability. If you are an R developer that uses Python for some of your work or a member of data science team that uses both languages, reticulate can dramatically streamline your workflow!



