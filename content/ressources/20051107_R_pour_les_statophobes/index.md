---
title:  R pour les statophobes 
summary: Utilisation du logiciel statistique R pour réaliser les analyses statistiques  de  base,  à  l’attention  des  étudiants  allergiques aux statistiques en général et aux logiciels en particulier

authors:
- Denis Poinsot

# Type de la publication 
publication_types: ["5"]


sources :
- "0"

thematics: ["12"]


update :
- "0"

languages:
- "0"

projects:
- divers

date: "2005-11-07T00:00:00Z"
doi: ""
 
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://perso.univ-rennes1.fr/denis.poinsot/Statistiques_%20pour_statophobes/R%20pour%20les%20statophobes.pdf'

---

Utilisation du logiciel statistique R pour réaliser les analyses statistiques  de  base,  à  l’attention  des  étudiants  allergiques aux statistiques en général et aux logiciels en particulier.

