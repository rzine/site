---
title: Document computationnel avec R
subtitle: Pour le partage et la reproductibilité de ses analyses 
summary: Pour le partage et la reproductibilité de ses analyses 

authors:
- hpecout 

publication_types: ["12", "13"]

thematics: ["1", "7"]

sources:
- "12"

update:
- "0"

languages:
- "0"

date: "2023-10-03"


tags:
- rmarkdown
- markdown
- quarto
- programmation lettrée
- literate programming

url_source: 'https://elementr.gitpages.huma-num.fr/session_notebook/programmation_lettree/#/title-slide'
url_code: 'https://gitlab.huma-num.fr/elementr/session_notebook/programmation_lettree'


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Après plus de 50 en de recherche & développement, les notebooks (documents computationnels) apparaissent aujourd’hui comme un format de publication efficace dans le domaine de recherche scientifique. Cette pratique, bien développée dans la communauté R, permet autre autre de partager et valoriser ses traitements et ses analyses réaliser en langage de programmation. Les différentes fonctionnalités mises à disposition par les systèmes de notebook en font également aujourd’hui un format légitime pour la publication de diaporama, de manuel, de livre et même de site web.

Ce diaporama a été utilisé dans le cadre d'une séance de formation proposée par le groupe d'utilisateur·rices [ElementR](https://elementr.netlify.app/), ayant pour objectif d'introduire au paradigme de programmation lettrée, au concept des notebooks, ainsi qu'aux fonctionnalités et aux différents cas d’usage des documents computationnels. Des exercices pratiques sont proposées avec `rmarkdown` et `quarto`.


Page web de la séance : [https://elementr.netlify.app/posts/seance8_quarto](https://elementr.netlify.app/posts/seance8_quarto)

