---
title: sfnetworks
subtitle: Tidy Geospatial Networks in R
summary: Package for analysis of geospatial networks. It connects the functionalities of the `tidygraph` package for network analysis and the `sf` package

date: "2021-03-28T00:00:00Z"

authors:
- Lucas van der Meer
- Lorena Abad 
- Andrea Gilardi
- rlovelace

publication_types: ["6"]


sources:
- "0"

thematics: ["18","2"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- tidygraph
- sf
- network
- spatial

url_source: 'https://luukvdmeer.github.io/sfnetworks/'
url_code: 'https://github.com/luukvdmeer/sfnetworks/'


links:
- name: CRAN
  url: https://cran.r-project.org/web/packages/sfnetworks/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

`sfnetworks` is an R package for analysis of geospatial networks. It connects the functionalities of the `tidygraph` package for network analysis and the `sf` package for spatial data science.








