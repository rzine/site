---
title: Covid-19 - dataviz et vulgarisation
subtitle : Séminaire RUSS - Séance du 1er avril 2022
summary: Séminaire RUSS - Séance du 1er avril 2022


authors:
- fdebarre

publication_types: ["12"]


sources:
- "7"

thematics: ["11"]


update:
- "0"

languages:
- "0"


projects:
- RUSS

date: "2022-04-08T00:00:00Z"

url_source: 'https://russ.site.ined.fr/fichier/s_rubrique/32427/2022.04_russ_covid19_dataviz_et_vulgarisation.fr.pdf'

links:


tags:
- RUSS
- reproductible
- Covid-19
- opendata
- dataviz
- représentation
- shiny app
- communication
- vulgarisation
- mediatisation
- git

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Covid-19 : dataviz et vulgarisation**, par Florence Débarre (CNRS)

Au cours de cet exposé, différents travaux de visualisation de données publiques réalisés au cours de la pandémie de Covid-19 à des fins de vulgarisation et élaborés à partir de données publiques seront présentés. Les sélections de sujets et de jeux de données seront abordés ainsi que les outils utilisés (R et ses dérivés), et les choix de représentations. Enfin, les écueils de communication grand public seront évoqués.   

Les vidéos de la présentation seront prochainement mises en ligne.
