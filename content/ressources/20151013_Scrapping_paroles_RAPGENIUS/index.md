---
title: Récupérer des paroles de rap du site Rap Genius
summary: Billet de blog présentant une méthode d'extraction automatique de contenu issu du site [Rap Genius](https://genius.com/rap-genius-france)

authors:
- croquebert 

# Type de la publication 
publication_types: ["11"]


sources :
- "0"

thematics: ["6","17"]


update :
- "0"

languages:
- "0"

projects:
- divers


tags:
- scraping
- token
- xml2
- rvest
- rjson
- bitops
- RCurl


date: "2015-10-13T00:00:00Z"
doi: ""
 
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://nycthemere.hypotheses.org/533'

---

Ce tutoriel est écrit dans le but d’être utilisable par quelqu’un qui ne connait pas du tout R...

