---
title: Analyse de données avec R
subtitle: Contextualisation et présentation de R et Rstudio

summary: Séminaire RUSS - Contextualisation et présentation de R et Rstudio


authors:
- hpecout

publication_types: ["12","13"]


thematics: ["0"]


projects:
- RUSS

sources:
- "7"

update:
- "1"

languages:
- "0"
- "1"
- "2"

date: "2020-12-01T11:00:00Z"
doi: ""

tags:
- introducción
- introduction
- R
- Rstudio
- SAS
- SPSS
- sata
- python


url_code: 'https://github.com/HuguesPecout/R_presentation_FR'
url_source: 'https://huguespecout.github.io/R_presentation_FR/#/'

links:
- name: Video
  url: https://www.canal-u.tv/video/ined/introduction_a_r_partie_1_contextualisation_et_presentation_de_r_et_de_rstudio.62395

links:
- name: English version
  url: https://huguespecout.github.io/R_presentation_EN/#/
- name: English code
  url: https://github.com/HuguesPecout/R_presentation_EN
- name: Versión en español
  url: https://huguespecout.github.io/R_presentation_SP/#/
- name: Código en español
  url: https://github.com/HuguesPecout/R_presentation_SP


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Ce diaporama introduit le langage R et l'IDE Rstudio dans le paysage de l'analyse de données**.  

Il s'agit d'un support utilisé pour la séance [RUSS](https://russ.site.ined.fr/) du 4 décembre 2020 : [*Introduction à R pour la mise en œuvre de chaînes de traitements reproductibles*](https://russ.site.ined.fr/fr/seances-passees/04-12-20) réalisée par Hugues Pecout (CNRS, FR CIST) et Ronan Ysebaert (Université de Paris, UMS RIATE).

Retrouvez l'ensemble des supports utilisés durant la séance à ce [lien](https://huguespecout.github.io/RUSS_seance_introduction/)


**English :** Data analysis with R - Contextualisation and presentation

This slides introduce to the practice of R and Rstudio.  
 
It contextualizes R in the data analysis landscape, comparing it to the proprietary software that dominates the market (not for long!), as well as to the python language. The second part focuses on the presentation of R (history, strengths, weaknesses) then on IDE Rstudio. All you have to do is install R and Rstudio to get started ! 


**Español :** Análisis de datos con R - Contextualización y presentación

Estas diapositivas introducen a la práctica de R y Rstudio.  
 
Contextualiza R en el panorama del análisis de datos, comparándolo con el software propietario que domina el mercado (¡no por mucho tiempo!), así como con el lenguaje Python. La segunda parte se centra en la presentación de R (historia, puntos fuertes, puntos débiles) y luego en IDE Rstudio. Todo lo que tiene que hacer es instalar R y Rstudio para empezar.   






