---
title: Simplifying geospatial features
subtitle: With sf and rmapshaper
summary: Simplifying geospatial features in R with sf and rmapshaper

date: "2021-03-15T00:00:00Z"

authors:
- Markus Konrad

publication_types: ["11"]


sources:
- "0"

thematics: ["2"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- ggplot2
- magrittr
- sf
- st_simplify
- st_buffer
- st_union

url_source: 'https://datascience.blog.wzb.eu/2021/03/15/simplifying-geospatial-features-in-r-with-sf-and-rmapshaper/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---







