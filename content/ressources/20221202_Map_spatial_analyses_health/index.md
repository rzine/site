---
title: Mapping and spatial analyses in R for One Health studies
subtitle: GeoHealth Training Series

summary: Training designed for people interested in using R for mapping and spatial analysis for applications in the various fields related to “one health” approaches

authors:
- Vincent Herbreteau
- Florian Girond
- Lucas Longour
- Sokeang Hoeun
- Léa Douchet

publication_types: ["5","13"]


sources:
- "0"

thematics: ["2", "20", "19"]


update:
- "0"

languages:
- "1"

  
projects:
- divers


date: "2022-12-02T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  
tags:
- SIG
- geomatic
- GIS
- map
- sf
- terra
- OpenStreetMap
- osm
- osmdata
- osmextract
- maptiles
- banr
- tidygeocoder
- raster
- rgdal
- crs
- projection
- geometry
- distance
- geocoder
- geocoding
- digitalisation
- SpatRsater
- SpatVector
- map algebra
- local
- zonal
- focal
- global
- mask
- crop
- statistic
- spatial analysis
- matrix
- acquisition
- leaflet
- mapview
- osrm


url_code: 'https://forge.ird.fr/espace-dev/personnels/longour/geohealth/documentation/rspatial-for-onehealth'
url_source: 'http://rspatial4onehealth.geohealthresearch.org/'




---



This training is designed for people interested in using R for mapping and spatial analysis for applications in the various fields related to “one health” approaches. R is an open source software widely used by the scientific community for data analysis and which is becoming an interesting choice for cartography and spatial analysis, as a Geographic Information System.

This training was first prepared to be given from 28th November to 2nd December 2022, in Phnom Penh Cambodia, by the GeoHealth team under the French National Research Institute for Sustainable Development (IRD) and the Institut Pasteur du Cambodge (IPC), in the frame of:

- the [OHSEA Project](https://ohsea.ird.fr/) (One Health in Practices in South-East Asia), funded by the FSPI (Solidarity Fund for Innovative Projects) under the French Ministry for Europe and Foreign Affairs,
- the Dissemination activities of the [EASIMES](https://sesstim.univ-amu.fr/fr/projet/easimes) (Environment Analysis and Surveillance to Improve Malaria Elimination Strategy) Project funded by the Global Fund RAI2E.

Conception: It was first designed from the courses “[Géomatique avec R](https://rcarto.github.io/geomatique_avec_r/)” and “[Cartographie avec R](https://rcarto.github.io/cartographie_avec_r/)” by Timothée Giraud and Hugues Pecout. It was then translated and its examples were adapted for Cambodia and health applications.

**Development team:**   
*Coordination:* Vincent Herbreteau, Florian Girond and Lucas Longour   
*Implementation in R with Quarto:* Lucas Longour and Sokeang Hoeun    
*Core of the tutorial:* Timothée Giraud and Hugues Pecout   
*Development of examples on Cambodia:* Vincent Herbreteau, Lucas Longour and Sokeang Hoeun    
*OSM extract chapter:* Lucas Longour    
*Geostatistics chapter:* Léa Douchet   
