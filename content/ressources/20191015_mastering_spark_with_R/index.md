---
title: Mastering spark with R
subtitle: The Complete Guide to Large-Scale Analysis and Modeling
summary: In this book you will learn how to use [Apache Spark](https://spark.apache.org/) with R

authors:
- Javier Luraschi
- Kevin Kuo
- Edgar Ruiz

publication_types: ["5"]


sources :
- "0"

thematics: ["6","3","7"]


update :
- "1"

languages:
- "1"

projects:
- divers

tags:
- spark
- apache
- hadoop
- sprklyr
- modeling
- pipelines
- cluster
- connection
- data
- tuning
- H2O
- XGboost
- Spatial
- Deep Learning
- streaming




date: "2019-10-15T00:00:00Z"
doi: ""
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://therinspark.com/'
url_code: 'https://github.com/r-spark/the-r-in-spark'

---

In this book you will learn how to use [Apache Spark](https://spark.apache.org/) with R. The book intends to take someone unfamiliar with Spark or R and help you become proficient by teaching you a set of tools, skills and practices applicable to large-scale data science.

You can purchase this book from Amazon, O’Reilly Media, your local bookstore, or use it online from this free to use website.

This work is licensed under the [Creative Commons Attribution-NonCommercial-NoDerivs 3.0 United States License](https://creativecommons.org/licenses/by-nc-nd/3.0/us/).

