---
title: Modern R with the tidyverse
subtitle: 
summary: Learn to use R, the tidyverse collection of packages and functional programming concepts to write efficient and readable code

authors:
- brodrigues

publication_types: ["5"]


thematics: ["0","10","11","12","13","5"]


sources:
- "0"

update:
- "1"

languages:
- "1"

projects:
- divers

date: "2019-08-16T00:00:00Z"

url_source: 'https://b-rodrigues.github.io/modern_R/'
url_code: 'https://github.com/b-rodrigues/modern_R'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



#### Who is this book for?

This book can be useful to different audiences. If you have never used R in your life, and want to start, start with Chapter 1 of this book. Chapter 1 to 3 are the very basics, and should be easy to follow up to Chapter 9. Starting with Chapter 9, it gets more technical, and will be harder to follow. But I suggest you keep on going, and do not hesitate to contact me for help if you struggle! Chapter 9 is also where you can start if you are already familiar with R and the {tidyverse}, but not functional programming. If you are familiar with R but not the {tidyverse} (or have no clue what the {tidyverse} is), then you can start with Chapter 4. If you are familiar with R, the {tidyverse} and functional programming, you might still be interested in this book, especially Chapter 9 and 10, which deal with package development and further advanced topics respectively.
