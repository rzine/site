---
title: R Workflow
subtitle: R Workflow for Reproducible Data Analysis and Reporting
summary: R Workflow for Reproducible Data Analysis and Reporting 

authors:
- Frank E Harrell Jr

publication_types: ["5"]

thematics: ["7"]

sources:
- "0"

update:
- "0"

languages:
- "1"

date: "2024-01-07"


tags:
- workflow
- quarto
- report
- reproducible

url_source: 'https://hbiostat.org/rflow/'


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This work is intended to foster best practices in reproducible data documentation and manipulation, statistical analysis, graphics, and reporting. It will enable the reader to efficiently produce attractive, readable, and reproducible research reports while keeping code concise and clear. Readers are also guided in choosing statistically efficient descriptive analyses that are consonant with the type of data being analyzed. The Statistical Thinking article [R Workflow](https://www.fharrell.com/post/rflow/) provides an overview of this book and includes some more motivation from the standpoint of doing good scientific research.




