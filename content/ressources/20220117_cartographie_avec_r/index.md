---
title: Cartographie avec R
summary: Créer des cartes thématiques conformes aux règles de la sémiologie graphique et de la cartographie avec R.

authors:
- tgiraud
- hpecout

publication_types: ["5","13"]


sources:
- "0"

thematics: ["2"]


update:
- "1"

languages:
- "0"


  
projects:
- divers


date: "2022-01-17T00:00:00Z"

featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  
tags:
- cartographie
- map
- cartography
- thematic
- carte
- thématique
- mapsf
- mf_map
- sf
- choroplèthe
- typologie
- symbole 
- proportionnel
- stock
- ratio
- thème
- flèche
- échelle
- source
- légende
- zoom
- carton
- 3D
- tanaka
- relief
- linemap
- rayvista
- rayshader
- raster
- cartogram
- cartogramme
- dorling
- anamoprhose
- Dougenik



url_code: 'https://github.com/rCarto/cartographie_avec_r'
url_source: 'https://rcarto.github.io/cartographie_avec_r/'

---

**Créer des cartes thématiques conformes aux règles de la sémiologie graphique et de la cartographie avec R.**

L'objectif de ce document est de fournir un support à l'apprentissage de la cartographie avec R.

#### Comment utiliser le manuel
Le projet RStudio contenant l’ensemble des données utilisées dans le manuel est disponible [ici](https://github.com/rCarto/geodata/archive/refs/heads/main.zip). Une fois le dossier décompressé il est possible de tester l’ensemble des manipulations proposées dans le projet RStudio.

#### Contribution et feedback
Vous pouvez nous envoyer vos remarques et suggestions en [postant une *issue*](https://github.com/rCarto/cartographie_avec_r/issues) sur le [dépôt GitHub](https://github.com/rCarto/cartographie_avec_r) de ce document.

#### Contexte
Ce manuel a été initialement conçu pour accompagner le cours “Cartographie avec R” du Master 2 Géomatique, géodécisionnel, géomarketing et multimédia (G2M) de l’Université Paris 8 Vincennes - Saint-Denis. Un deuxième manuel centré sur la cartographie est disponible ici : [Géomatique avec R](https://rcarto.github.io/geomatique_avec_r/).


[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC_BY--NC--SA_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
La version en ligne de ce document est sous licence [Creative Commons Attribution-NonCommercial-ShareAlike 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/).

