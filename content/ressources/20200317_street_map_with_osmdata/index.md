---
title: Pretty street maps in R with the osmdata package
subtitle: And ggplot2
summary: This tutorial is going to demonstrate how to make gorgeous maps of cities using streets and other geographic features

authors:
- Josh McCrain

publication_types: ["11"]


sources:
- "0"

thematics: ["2"]


update:
- "0"

languages:
- "1"

projects:
- divers

date: "2020-03-17T00:00:00Z"

tags:
- osmdata
- rvest
- ggmap
- showtext
- geom_sf


url_source: 'http://joshuamccrain.com/tutorials/maps/streets_tutorial.html'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


This tutorial is going to demonstrate how to make gorgeous maps of cities using streets and other geographic features. This is enabled by the osmdata package.

I borrow heavily from the excellent tutorial at [ggplot2tutor.com](https://ggplot2tor.com). I extend this tutorial in order to a) demonstrate deeper functionality of this package; and b) to provide additional instruction and application in ggplot, the tidyverse, and rvest.

I’ve given one of these maps as a gift, printed in a large poster format and framed. You’ll be able to make and customize your own maps and learn some new R functionality along the way.







