---
title: Pour un usage réflexif de R dans sa pratique des sciences sociales
subtitle : Séminaire RUSS - Séance du 3 décembre 2021
summary: Séminaire RUSS - Séance du 3 décembre 2021


authors:
- bgarnier
- Elisabeth Morand
- Judith Gourmelin

publication_types: ["12","9"]

sources:
- "7"

thematics: ["9"]


update:
- "0"

languages:
- "0"


projects:
- RUSS

date: "2021-12-03T00:00:00Z"

url_source: 'https://russ.site.ined.fr/fichier/s_rubrique/32182/2021.12.05.pour.un.usage.reflexif.de.r.dans.sa.pratique.des.sciences.sociales.fr.pdf'

links:
- name: Video (Introduction)
  url: https://www.canal-u.tv/chaines/ined/pour-un-usage-reflexif-de-r-dans-sa-pratique-des-sciences-sociales/introduction
- name: Video (partie 1)
  url: https://www.canal-u.tv/chaines/ined/pour-un-usage-reflexif-de-r-dans-sa-pratique-des-sciences-sociales/partie-1
- name: Video (partie 2)
  url: https://www.canal-u.tv/chaines/ined/pour-un-usage-reflexif-de-r-dans-sa-pratique-des-sciences-sociales/partie-2
- name: Video (partie 3)
  url: https://www.canal-u.tv/chaines/ined/pour-un-usage-reflexif-de-r-dans-sa-pratique-des-sciences-sociales/partie-3


tags:
- RUSS
- apprentissage
- enseignement
- usage de R

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Pour un usage réflexif de R dans sa pratique des sciences sociales**, par Bénédicte Garnier, Elisabeth Morand (INED) et Judith Gourmelin (sociologue indépendante)

Après une présentation générale de R et son environnement, nous montrerons sa polyvalence au service des sciences sociales à partir de cas concrets et mettrons en avant l’avantage acquis après investissement dans cet outil. 
Nous tenterons par la suite d’identifier les freins au bon usage R et proposerons des esquisses de solution incitant à un apprentissage progressif et structuré de R, permettant de relier les vertus de cet apprentissage technique à l’analyse scientifique.
