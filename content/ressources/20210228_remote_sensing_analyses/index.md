---
authors:
- Aniruddha Ghosh
- Robert Hijmans

title: Remote Sensing with terra

summary: Short introduction to satellite data analysis with R

date: "2021-02-28"

publication_types: ["13", "5", "8"]

thematics:  ["19", "2", "20"]


languages:
- "1" 

update:
- "1" 

sources:
- "0"

url_source: 'https://rspatial.org/rs/index.html' 
url_code: 'https://github.com/rspatial/rspatial-terra-web' 

tags:
- remote sensing
- raster
- terra
- MODIS
- télédétection
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This section of the *Spatial Data Science* website provides a short introduction to satellite data analysis with R. Before reading this you should first learn [the basics of the terra package](https://rspatial.org/index.html).
