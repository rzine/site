---
title: Géomatique avec R
summary: Ce manuel est aussi bien destiné aux utilisateurs de R souhaitant réaliser des traitements de l'information géographique, qu’aux utilisateurs de SIG souhaitant se former à R

authors:
- tgiraud
- hpecout

publication_types: ["5","13"]


sources:
- "0"

thematics: ["2", "20"]


update:
- "1"

languages:
- "0"

  
projects:
- divers


date: "2022-01-17T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  
tags:
- SIG
- géomatique
- GIS
- geomatic
- map
- sf
- terra
- OpenStreetMap
- osm
- osmdata
- osmextract
- maptiles
- banr
- tidygeocoder
- raster
- rgdal
- crs
- projection
- géotraitement
- géométrie
- distance
- géocoder
- géocodage
- digitalisation
- SpatRsater
- SpatVector
- algèbre spatiale
- local
- zonal
- focal
- global
- mask
- crop
- reclassification
- aggrégation
- fusion
- intersection
- NDVI
- conversion
- itinéraire
- matrice
- acquisition
- leaflet
- mapview
- osrm


url_code: 'https://github.com/rCarto/geomatique_avec_r'
url_source: 'https://rcarto.github.io/geomatique_avec_r/'

---


Ce manuel est destiné tant aux utilisateurs de R souhaitant mettre en place des traitements de l’information géographique qu’aux utilisateurs souhaitant utiliser R pour réaliser les taches qu’ils réalisent habituellement avec un SIG.
Les principales étapes du traitement de l’information géographiques y sont abordées. L’accent est porté sur le traitement des données vectorielles mais une partie est tout de même dédiée aux données raster.

#### Comment utiliser le manuel
Le projet RStudio contenant l’ensemble des données utilisées dans le manuel est disponible [ici](https://github.com/rCarto/geodata/archive/refs/heads/main.zip). Une fois le dossier décompressé il est possible de tester l’ensemble des manipulations proposées dans le projet RStudio.

#### Contribution et feedback
Vous pouvez nous envoyer vos remarques et suggestions en [postant une *issue*](https://github.com/rCarto/geomatique_avec_r/issues) sur le [dépôt GitHub](https://github.com/rCarto/geomatique_avec_r) de ce document.

#### Contexte
Ce manuel a été initialement conçu pour accompagner le cours “Cartographie avec R” du Master 2 Géomatique, géodécisionnel, géomarketing et multimédia (G2M) de l’Université Paris 8 Vincennes - Saint-Denis. Un deuxième manuel centré sur la cartographie est disponible ici : [Cartographie avec R](https://rcarto.github.io/cartographie_avec_r/).

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC_BY--NC--SA_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
La version en ligne de ce document est sous licence [Creative Commons Attribution-NonCommercial-ShareAlike 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/).


