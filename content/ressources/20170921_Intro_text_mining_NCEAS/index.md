---
title: Introduction to text mining
subtitle: Learn how to mine PDFs to get information out of them
summary: Learn how to mine PDFs to get information out of them


authors:
- Julien Brun

publication_types: ["13"]


thematics: ["17"]





projects:
- divers

sources:
- "0"

update:
- "0"

languages:
- "1"

date: "2017-09-21T00:00:00Z"
doi: ""

tags:
- readtext
- quanteda
- tm
- tidytext
- pdf
- NLP
- word
- cloud
- document feature matrix
- DFM
- corpus
- keyword



url_source: 'https://nceas.github.io/oss-lessons/data-liberation/intro-text-mining.html'
url_code: 'https://github.com/NCEAS/oss-lessons/tree/gh-pages/data-liberation'



# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Course material used in a training course** ([**OSS 2017**](https://nceas.github.io/oss-2017/)) offered by the [**National Center for Ecological Analysis and Synthesis**](https://www.nceas.ucsb.edu/)

