---
title: Forecasting
subtitle: Principles and Practice

summary: A comprehensive introduction to forecasting methods. Learn to improve your forecast accuracy using dozens of real data examples.

authors:
- Rob J. Hyndman
- George Athanasopoulos

publication_types: ["5"]


thematics: ["13","15"]





projects:
- divers

sources:
- "0"

update:
- "0"

languages:
- "1"

date: "2018-05-01T00:00:00Z"
doi: ""

tags:
- forecast
- forecasting
- time series
- seasonal
- prediction
- accuracy
- residual
- regression
- model
- predictor
- linear
- nonlinear
- correlation
- decomposition
- smoothing
- exponential
- ARIMA
- dynamic
- stochastic
- deterministic
- hierachical
- fpp2



url_source: 'https://otexts.com/fpp2/'



# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

## Preface

**This textbook is intended to provide a comprehensive introduction to forecasting methods and to present enough information about each method for readers to be able to use them sensibly**. We don’t attempt to give a thorough discussion of the theoretical details behind each method, although the references at the end of each chapter will fill in many of those details.

The book is written for three audiences:        
- people finding themselves doing forecasting in business when they may not have had any formal training in the area.   
- undergraduate students studying business.   
- MBA students doing a forecasting elective. 
