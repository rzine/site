---
title: Exploratory Data Analysis with R
subtitle: 
summary: This book covers the essential exploratory techniques for summarizing data

authors:
- Roger D. Peng

publication_types: ["5"]


thematics: ["10","11","12"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

date: "2016-04-20T00:00:00Z"


tags:
- dplyr
- graph
- plot
- Hierarchical
- Clustering
- K-Means
- PCA
- ggplot2

url_source: 'https://bookdown.org/rdpeng/exdata/'
url_code: 'https://github.com/rdpeng/exdata'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This book covers the essential exploratory techniques for summarizing data with R. These techniques are typically applied before formal modeling commences and can help inform the development of more complex statistical models. Exploratory techniques are also important for eliminating or sharpening potential hypotheses about the world that can be addressed by the data you have. We will cover in detail the plotting systems in R as well as some of the basic principles of constructing informative data graphics. We will also cover some of the common multivariate statistical techniques used to visualize high-dimensional data.
