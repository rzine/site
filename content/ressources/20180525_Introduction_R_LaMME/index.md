---
title: Introduction à R
summary: Supports de cours et de TP proposés par le Laboratoire de Mathématique et Modélisation d'Evry (LaMME - UMR 8071)

authors:
- Christophe Ambroise
- Claire Dandine

# Type de la publication 
publication_types: ["13"]


sources :
- "0"

thematics: ["0","10","11","12"]


update :
- "0"

languages:
- "0"

projects:
- divers

date: "2018-05-25T00:00:00Z"
doi: ""
 
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'http://www.math-evry.cnrs.fr/members/cambroise/teaching/isv51'

---




