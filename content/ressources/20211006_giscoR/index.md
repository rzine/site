---
authors:
- Diego Hernangómez

title: giscoR

summary: An API package that helps to retrieve data from Eurostat - GISCO

date: "2021-10-06"

publication_types: ["6"] 

thematics: ["2", "6", "7"]

languages:
- "1" 

update:
- "1" 

sources:
- "0"


url_source: 'https://ropengov.github.io/giscoR/'
url_code: 'https://github.com/rOpenGov/giscoR/'

tags:
- Eurostat
- Gisco
- NUTS
- Geometries
- API
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

[giscoR](https://ropengov.github.io/giscoR/) is an API package that helps to retrieve data from [Eurostat - GISCO](https://ec.europa.eu/eurostat/web/gisco) ([the Geographic Information System of the COmmission](https://ec.europa.eu/eurostat/web/gisco)). It also provides some lightweight data sets ready to use without downloading.

GISCO ([FAQ](https://ec.europa.eu/eurostat/web/gisco/faq)) is a geospatial open data repository including several data sets as countries, coastal lines, labels or [NUTS levels](https://ec.europa.eu/eurostat/web/regions-and-cities/overview). The data sets are usually provided at several resolution levels (60M/20M/10M/03M/01M) and in 3 different projections (4326/3035/3857).

