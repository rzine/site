---
# AUTHOR(S) 
authors:
- kantuki


# DATE de soumission à Rzine
date: "2018-07-06"

# TITRE
title: Manipulation de données communales avec le package COGugaison

subtitle: Gérer les évolutions des limites communales
summary: Présentation du package **COGugaison** aux 7em rencontres R. Ce package permet de manipuler des données communales produites à différents millésimes et les agréger à des niveaux supra-communaux.

# TAGS
tags:
- INSEE
- COGugaison
- CARTElette
- Découpage
- communes


# TYPE DE PUBLICATION
publication_types: ["12"]


thematics: ["10"]


languages:
- "0"


sources:
- "11"

update:
- "0"


projects:
- RencontresR


url_source: 'https://antuki.github.io/slides/180706_RencontresR2018_COGugaison/180706_RencontresR2018_COGugaison.html#1'

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**COGugaison est un package permettant de manipuler des données communales produites à différents millésimes et les agréger à des niveaux supra-communaux**.

Le découpage des territoires français, en particulier les communes, n’est pas un phénomène immuable. Chaque année certaines communes changent de codes, ou bien de nom, fusionnent ou encore se divisent. Certains périmètres supra-communaux changent également, comme celui des cantons qui a été récemment redéfini. C’est à l’Insee que revient le suivi de ces changements afin d’établir chaque année le code officiel géographique (COG).

Ce package R a alors pour objectif global de manipuler des données communales produites à différents millésimes et de les agréger à différents niveaux supra-communaux.

