---
title: Using the flextable R package
subtitle: Tables for reporting and publications
summary: User documentation for the flextable package, which provides a framework for easily create tables for reporting and publications

date: "2021-03-10T00:00:00Z"

authors:
- David Gohel

publication_types: ["6","12"]


sources:
- "0"

thematics: ["1","11"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- flextable
- data.frame
- design
- format
- cell
- layout


url_source: 'https://ardata-fr.github.io/flextable-book/index.html'

links:
- name: slides
  url: https://ardata-fr.github.io/flextable-whyr/#1
- name: CRAN
  url: https://cloud.r-project.org/web/packages/flextable/index.html



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


The flextable package provides a framework for easily create tables for reporting and publications. Functions are provided to let users create tables, modify and format their content and define their content.

Tables can be embedded within:

- R Markdown documents with support for HTML, Word, PDF and PowerPoint documents.
- Microsoft Word or PowerPoint documents with package [officer](https://cran.r-project.org/web/packages/officer/index.html).

Tables can also be rendered as R plots or graphic files (png, pdf and jpeg).








