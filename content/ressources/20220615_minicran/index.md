---
# Author(s)
authors:
- Andrie de Vries

# Date
date: "2022-02-14"


# Title
title: miniCRAN


# Short summary
summary: Create a mini version of CRAN containing only selected packages

# Tags
tags:
- 

publication_types: ["6"]

sources:
- "0"

thematics: ["7"]

update:
- "1"

languages:
- "1"

# Link
url_source: 'http://andrie.github.io/miniCRAN/'
url_code: 'https://github.com/andrie/miniCRAN'

links:
- name: CRAN
  url: https://cran.r-project.org/web/packages/miniCRAN/index.html



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

The `miniCRAN` package makes it possible to create an internally consistent repository consisting of selected packages from CRAN-like repositories. The user specifies a set of desired packages, and miniCRAN recursively reads the dependency tree for these packages, then downloads only this subset.
