---
title:  FactoMineR
subtitle: Package dédié à l'analyse exploratoire multidimensionnelle de données
summary: FactoMineR est un package de référence dédié à l'analyse exploratoire multidimensionnelle de données (à la Française)

authors:
- fhusson
- Julie Josse
- Sébastien Lê

publication_types: ["6"]


thematics: ["12","11"]


languages:
- "0"


sources :
- "0"

update :
- "1"


projects:
- divers
date: "2008-03-18T00:00:00Z"
doi: "10.18637/jss.v025.i01"


tags:
- FactoMiner
- Factoshiny
- FactoInvestigate
- MissMDA
- ACP
- AC
- ACM



url_source: 'http://factominer.free.fr/'

links:
- name: CRAN
  url: https://cran.r-project.org/web/packages/FactoMineR/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

#### Pourquoi utiliser FactoMineR ?

- Il permet de mettre en oeuvre des méthodes analyses de données telles que l'analyse en composantes principales (ACP), l'analyse des correspondances (AC), l'analyse des correspondances multiples (ACM) ainsi que des analyses plus avancées.
- Il permet l'ajout d'information supplémentaire telle que des individus et/ou des variables supplémentaires.
- Il fournit un point de vue géométrique et de nombreuses sorties graphiques.
- Il fournit de nombreuses aides à l'interprétation (description automatique des axes, nombreux indicateurs, ...).
- Il peut prendre en compte diverses structures sur les données (structure sur les variables, hiérarchie sur les variables, structure sur les individus).
- Beaucoup de matériels pédagogique (**MOOC**, **livres**, etc.) est disponible pour expliquer aussi bien les méthodes que la façon de les mettre en oeuvre avec FactoMineR.
- Il **gère les données manquantes** avec **missMDA** ([voir ici](http://factominer.free.fr/course/manquantes.html)).
- Il a une **interface Shiny qui permet de construire des graphes de façon interactive** avec **Factoshiny** ([voir ici](http://factominer.free.fr/graphs/factoshiny-fr.html))
- Il propose une **interprétation automatique** des résultats obtenus avec FactoMineR grâce à **FactoInvestigate** ([voir ici](http://factominer.free.fr/Reporting/index_fr.html)).
