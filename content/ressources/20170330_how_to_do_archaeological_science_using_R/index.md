---
title: How To Do Archaeological Science Using R
subtitle : 
summary: This bookdown is a early draft of an edited volume of contributions to the ‘How To Do Archaeological Science Using R’ forum of the 2017 Society of American Archaeology annual meeting in Vancouver, BC


authors:
- Ben Marwick

publication_types: ["5"]


sources:
- "0"

thematics: ["19","11","10"]


update:
- "0"

languages:
- "1"


date: "2017-03-30T00:00:00Z"

url_source: 'https://benmarwick.github.io/How-To-Do-Archaeological-Science-Using-R/'
url_code: 'https://github.com/benmarwick/How-To-Do-Archaeological-Science-Using-R'

tags:
- spatial analysis
- linear model
- NetLogo
- Agent-based
- Tempo Plot
- Radiocarbon Dates
- logictic regression
- Predicting

- GIS
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



This bookdown is a early draft of an edited volume of contributions to the ‘How To Do Archaeological Science Using R’ forum of the 2017 Society of American Archaeology annual meeting in Vancouver, BC. The forum was organised by Ben Marwick, who is the editor of this collection. The chapters here are early drafts and are still in preparation. If you see a typo, you can make a correction by editing the source files at [https://github.com/benmarwick/How-to-do-archaeological-science-using-R](https://github.com/benmarwick/How-to-do-archaeological-science-using-R).



