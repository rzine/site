---
title: R Géomatique
summary: Ce carnet partage et diffuse des travaux de géomatique réalisés avec R

authors:
- tgiraud

publication_types: ["8"]


thematics: ["2"]


sources:
- "0"

update:
- "1"

languages:
- "0"

categories: ["geomatique"]

projects:
- divers


date: "2015-01-01T00:00:00Z"


publishDate: "2019-09-30T00:00:00Z"


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
tags:
- geomatique
- cartography
- osm
- tanaka
- map
- carte
- sf
- potential
- spatial

url_source: 'https://rgeomatic.hypotheses.org/'

---

This blog aims to share and disseminate several geomatics projects done thanks to the R software. This initiative is part of the broader framework of the development of free software, open data and reproducible research. Various of my works make use of resources (data, scripts, documents …) that may be interesting to broadcast to the community of engineers and researchers in geomatics. The posts published here are more or less finalized.



