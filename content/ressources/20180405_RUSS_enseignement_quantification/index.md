---
title: R, RStudio et l’enseignement de la quantification
subtitle: Enseigner R en SHS

summary: Séminaire RUSS - Enseigner R en SHS

authors:
- aperdoncin

publication_types: ["12"]


thematics: ["9"]





projects:
- RUSS

sources:
- "7"

update:
- "0"

languages:
- "0"

date: "2018-04-05T00:00:00Z"
doi: ""

tags:
- enseignement
- R
- Rstudio
- russ
- quanti
- cours

url_source: 'docs/20180405_russ_enseignement_quantification/russ_perdoncin.pdf'
 
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Support utilisé dans le cadre d'une **séance du séminaire RUSS** intitulée ***Enseigner R en SHS***, qui s'est déroulée le jeudi 5 avril 2018. Pour plus d'information, consultez le [site de RUSS](https://www.ined.fr/fr/actualites/rencontres-scientifiques/seminaires-colloques-ined/russ-5-4-2018/).



