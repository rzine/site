---
title: Exploring the E-road network
summary: Have a look at a graph representation of the E-road network that involves major roads in Europe and parts of central Asia.

date: "2021-03-15T00:00:00Z"

authors:
- Christian Hohenfeld 

publication_types: ["11"]


sources:
- "0"

thematics: ["18","11"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- igraph
- ggraph
- tidygraph
- Euro-Roads
- centrality
- Betweenness
- local transitivity
- visualisations
- longest geodesic
- community

url_source: 'https://hohenfeld.is/posts/exploring-the-e-road-network/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

We will look at the network of [Euro-Roads](https://en.wikipedia.org/wiki/International_E-road_network), which is an international system of numbering important roads throughout Europe. The marking of roads as Euro-Roads usually co-exists with national numberings and the visibility of the E-numbers varies by country.

The dataset was downloaded from [KONECT](http://konect.cc/networks/subelj_euroroad/). There is no licence information included so I won’t share the original data.







