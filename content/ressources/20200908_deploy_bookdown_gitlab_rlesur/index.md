---
title: Get the full power of GitLab CI for your bookdown project
subtitle:
summary: Host and deploy a bookdown on GitLab

authors:
- rlesur

publication_types: ["11"]


thematics: ["7"]


sources :
- "0"

update :
- "0"

languages:
- "1"


projects:
- divers

date: "2020-09-08T00:00:00Z"


tags:
- gitlab
- docker
- bookdown


url_source: 'https://rlesur.gitlab.io/bookdown-gitlab-pages/index.html'
url_code: 'https://gitlab.com/RLesur/bookdown-gitlab-pages'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

#### Overview: a powerful workflow

Publishing a bookdown (Xie 2020) website with [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) is as easy as:

- hosting a repository on [GitLab](https://about.gitlab.com/)
- adding a configuration file to the project

You will get a hosting service for your bookdown book (like GitHub Pages) and great features which are not available with GitHub Actions and GitHub Pages:

- pull requests previews
- a review application dedicated to non git users that allows them to comment on these previews



