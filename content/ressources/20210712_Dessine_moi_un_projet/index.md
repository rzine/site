---
title: S'il-vous-plaît... dessine-moi un projet
subtitle: Keynote speech aux Rencontres R 2021
summary: Keynote speech aux Rencontres R 2021


authors:
- msalmon

publication_types: ["12", "9"]


sources:
- "11"

thematics: ["7"]


update:
- "0"

languages:
- "0"


projects:
- Rencontres R

date: "2021-07-12T00:00:00Z"

url_source: 'https://rr2021.netlify.app/#/'

links:
- name: Video
  url: https://www.youtube.com/watch?v=nc5w1ROsP3A


tags:
- projet
- reproductible
- bonnes pratiques
- git
- renv
- usethis
- rrtools
- holepunch
- R-universe
- targets
- R Targetopia
- orderly

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**S'il-vous-plaît... dessine-moi un projet**, par Maëlle Salmon (rOpenSci)

Bien structurer ses projets R, c’est utile, même si l’on n’a pas des collaborateurs ou collaboratrices aussi exigent·e·s que le Petit Prince. Mais on fait comment ? On met le projet dans un boa ou une boîte à trous ? Dans cette présentation, je vous parlerai d’outils qu’il est bon d’apprivoiser : here, renv, targets, devtools… ça vous dit quelque chose ? Faire de tout un paquet R, pour, contre, ou réponse de Normand·e ? Après m’avoir écoutée, vous aurez en poche des astuces à rapporter sur votre planète pour faire fleurir vos roses et mourir vos baobabs, avec la confiance pour développer votre propre manière de jardiner.
