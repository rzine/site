---
title: R Programming for Data Science
subtitle: 
summary: This book is about the fundamentals of R programming

authors:
- Roger D. Peng

publication_types: ["5"]


thematics: ["0"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

date: "2016-04-20T00:00:00Z"




tags:
- introduction
- fonction
- test
- while
- for
- purrr
- plyr
- tidyverse
- ggplot2
- regex
- grep
- stringr
- texreg
- readr
- dplyr
- apply
- tidyr
- debugging
- simulation
- parallel



url_source: 'https://bookdown.org/rdpeng/rprogdatascience/'
url_code: 'https://github.com/rdpeng/rprogdatascience'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

This book is about the fundamentals of R programming. You will get started with the basics of the language, learn how to manipulate datasets, how to write functions, and how to debug and optimize code. With the fundamentals provided in this book, you will have a solid foundation on which to build your data science toolbox.

