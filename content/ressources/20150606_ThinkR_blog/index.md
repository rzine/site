---
authors:
- ThinkR

title: ThinkR - Le blog
summary: Blog généraliste proposé par la société ThinkR


date: "2015-06-06"

publication_types: ["8"]


thematics: ["0", "1", "10", "11", "12", "3", "5", "6", "7", "8", "9"]


languages:
- "0"

update:
- "1"

sources:
- "0"


url_source: 'https://thinkr.fr/blog/'


tags:
- sas
- dplyr
- cartography
- shiny
- bookdown
- tidyr
- golem
- machine learning
- ggplot2
- tm
- tidytext
- purrr
- tidyverse
- rtsudio

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

ThinkR alimente également un [blog qui présente ses différents ses travaux](https://rtask.thinkr.fr/fr/blog).

