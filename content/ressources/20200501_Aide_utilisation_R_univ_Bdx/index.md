---
title: Aide à l'utilisation de R
summary: Guide à l'utilisation de R (prise en main, graphiques, statistiques, application) réalisé par des enseignants de l'université de Bordeaux 

authors:
- Antoine Massé
- Julien Bousquet

# Type de la publication 
publication_types: ["13","8"]


sources :
- "0"

thematics: ["0","10","11","12","17","2","20"]


update :
- "1"

languages:
- "0"

projects:
- divers

date: "2020-05-01T00:00:00Z"
doi: ""
 
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://sites.google.com/site/rgraphiques/home'

---

Guide à l'utilisation de R (prise en main, graphiques, statistiques, application) réalisé par des enseignants de l'université de Bordeaux.
