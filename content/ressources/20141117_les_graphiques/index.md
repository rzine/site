---
title: Les graphiques
summary: Petit manuel en deux parties sur les fonctions graphiques et les représentations classiques pour la statistique descriptive uni et bidimensionnelle


authors:
- tlaurent

publication_types: ["5"]


sources:
- "0"

thematics: ["11","12"]


update:
- "0"

languages:
- "0"


projects:
- divers

date: "2014-11-17T00:00:00Z"

tags:
- fenêtre
- par
- symbol
- pch
- lty
- couleur
- mfrow
- layout
- marge
- mar
- omi
- pdf
- png
- sauvegarde
- série temporelle
- distribution
- histogramme
- boîte à moustaches
- diagramme circulaire
- quantitative
- qualitative

links:
- name: Partie 1
  url: http://www.thibault.laurent.free.fr/cours/graphiques-partie1.html
- name: Partie 2
  url: http://www.thibault.laurent.free.fr/cours/graphiques-partie2.html
- name: Pdf 1
  url: http://www.thibault.laurent.free.fr/cours/graphiques-partie1.pdf
- name: pdf 2
  url: http://www.thibault.laurent.free.fr/cours/graphiques-partie2.pdf

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Fidèle à son principe, le logiciel R a recours à l’utilisation de fonctions pour la réalisation de graphiques.Le concept est dans un premier temps d’ouvrir une fenêtre graphique dans laquelle on va représenter unou plusieurs graphiques. On pourra paramétrer d’une part la fenêtre graphique (marges, couleurs de fond,etc.) et les graphiques eux-mêmes (taille et couleur des traits, type de symbole, couleurs, etc.). Certainslogiciels utilisent le principe de couches superposables qu’on peut ajouter ou enlever comme on veut. Ce n’estmalheureusement pas le cas de R : lorsqu’on ajoute un trait, un point ou une légende dans un graphique,il est indélébile; en d’autres termes on ne peut pas revenir dessus sauf en reprenant le graphique depuis ledébut. Toutefois, malgré ces quelques inconvénients, on verra que grâce à un nombre important de fonctionsprédéfinies, on est à peu près capable de représenter tout ce que l’on peut imaginer de faire avec R.


