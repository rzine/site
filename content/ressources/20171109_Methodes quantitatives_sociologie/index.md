---
title: Méthodes quantitatives pour la sociologie - Niveau 2

subtitle: Les méthodes de régression

summary:  Supports de cours sur les méthodes de régréssion, proposés dans le cadre du Master en sciences sociales de l’EHESS

authors:
- mchevalier


publication_types: ["13"]


thematics: ["12","13"]


languages:
- "0"

sources:
- "0"

update:
- "0"

projects:
- divers

# Date that the page was published
date: "2017-11-09T00:00:00Z"
doi: ""

 

image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

tags:
- régression
- modèle
- logistique
- probit
- linéaire
- R²
- fisher
- relation
- Hétéroscédasticité
- ROC
- Pisa
- exercice


url_source: 'https://teaching.slmc.fr/mqs2_1516/index.html'

links:
- name: Exercices
  url: https://teaching.slmc.fr/mqs2/


---

**Supports de cours et exercices appliqués sur les méthodes de régréssion**, proposés dans le cadre du Master en sciences sociales de l’EHESS, spécialité « Sociologie générale » ou « Sociologie et statistique ».


