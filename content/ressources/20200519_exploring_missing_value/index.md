---
title: Exploring missing values in naniar
summary: Learnr tutorial to expore and visualize `NA` data


authors:
- Allison Horst

publication_types: ["1", "13","12","9"]


sources:
- "0"

thematics: ["10"]


update:
- "0"

languages:
- "1"

projects:
- divers

tags:
- NA
- missing value
- naniar
- learnr

date: "2020-05-19T00:00:00Z"

url_source: 'https://allisonhorst.shinyapps.io/missingexplorer/'
url_code: 'https://github.com/allisonhorst/explore-na'

links:
- name: Video
  url: https://ucsb.app.box.com/s/pr2z7ygeycrfwefmsdknrq3lurzka5ux
- name: Slide
  url: https://docs.google.com/presentation/d/1EzMU6be01aicPGeBcbXrXy1iX8CdWhBz1o2pJDPsXBM/edit#slide=id.p


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Missing values, indicated by (or coerced to) `NA` in R, are common in environmental data due to equipment malfunction, survey non-response, human error, resource limitations, and any number of other unforeseen hiccups that can occur during data collection. Despite their ubiquity, NAs are rarely considered in exploratory data analysis, and are commonly “dealt with” (read: disappeared) by listwise deletion. Listwise deletion (in which any row with an `NA` is removed) may be the best method for handling missings, but also omits valuable existing observations, reduces statistical power, and depending on the mechanism of missingness can increase bias in parameter estimates. Exploring and thinking critically about missing data is an important and often overlooked part of exploratory data analysis that can help us to understand what data are missing and why, so that we choose an appropriate method for handling them.






