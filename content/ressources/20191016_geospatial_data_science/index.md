---
title: Geospatial Data science with R
summary:  Website about spatial data processing in R, for agriculture, soil science, environmental health, environmental engineering, and data science. 

authors:
- Zia Ahmed

publication_types: ["8"]


sources :
- "0"

thematics: ["19","20","2"]


update :
- "1"

languages:
- "1"

projects:
- divers

date: "2019-10-16T00:00:00Z"

tags: 
- point
- raster
- vector
- polygon
- weighted
- random forest
- OLS
- poisson
- interpolation
- kriging
- remote sensing
- image classification
- spectral indice
- deep learning
- supervised classification
- RODBC
- sqldf
- RPostgreSQL
- snow
- doParalle
- devtools
- rJava
- RColorBrewer
- latticeExtra
- tmap
- ggmap:
- rasterVis
- corrplot
- agricoale
- MASS
- nlme
- lme4
- lmerTest
- caret
- caretEnsemble
- H20
- keras
- sp
- sf
- rgdal
- raster
- maptools
- maps
- rgeos
- rgrass7
- plotGoogleMaps
- landsat
- RStoolbox
- wrspathrow
- ncdf4
- RNetCDF
- NetCDF
- PCICt
- gstat
- spdep
- automap
- GSIF
- GWmodel
- dismo



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://zia207.github.io/geospatial-r-github.io/index.html'
url_code: 'https://github.com/zia207/geospatial-r-github.io'

---

Website about spatial data processing in R, for agriculture, soil science, environmental health, environmental engineering, and data science. 








