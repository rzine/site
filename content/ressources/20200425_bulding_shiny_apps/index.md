
---
title: Building Shiny apps 
subtitle: An interactive tutorial
summary: This tutorial is a hands-on activity complement to a set of [presentation slides](https://docs.google.com/presentation/d/1dXhqqsD7dPOOdcC5Y7RW--dEU7UfU52qlb0YD3kKeLw/edit#slide=id.p) for learning how to build Shiny apps.

authors:
- Dean Attali


publication_types: ["11"]


sources:
- "0"

thematics: ["5"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- shiny
- UI
- uiOuput
- shinyapps.io
- Server
- markdown
- JavaScript


date: "2020-04-25T00:00:00Z"

url_source: 'https://deanattali.com/blog/building-shiny-apps-tutorial/'

links:
- name: Slides
  url: https://docs.google.com/presentation/d/1dXhqqsD7dPOOdcC5Y7RW--dEU7UfU52qlb0YD3kKeLw/edit#slide=id.p

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This tutorial should take approximately an hour to complete. If you want even more practice, another great tutorial is the official Shiny tutorial. RStudio also provides a handy cheatsheet to remember all the little details after you already learned the basics.




