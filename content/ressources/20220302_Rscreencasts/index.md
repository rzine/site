---
authors:
- David Robinson
- Alex Cookson
- Eric Fletcher
- Oscar Baruffa

title: R Screencasts
subtitle: Awesome data analysis screencasts from a top Data Scientist 
summary: List of real-world examples of data analyses and visualization, provided with codes, datasets and the workshops recordings

date: "2022-03-02"

publication_types: ["0","8"]

thematics: ["10", "11", "12", "13", "17", "18", "2", "3", "8"]


languages:
- "1"

update:
- "1" 

sources:
- "0"


url_source: 'https://www.rscreencasts.com/' 
url_code: 'https://github.com/dgrtwo/data-screencasts' 



tags:
- Screecasts
- Tidytuesday
- Data Visualization
- Data Manipulation
- Data Cleaning
- Correlation
- T-test
- Map
- Textmining
- Regression
- Anova
- Heatmap
- Choroplet
- PCA
- Network
- Survey data
- Survival Analysis


  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


If you’re looking for real-world examples of live data analyses, you’ve come to the right place.

David Robinson, a highly experienced Data Scientist, has recorded many screencasts where he analyses data that he’s never seen before. These are fantastic examples of how to think about approaching an analysis. You couldn’t ask for a better mentor!

The recordings were done as part of a weekly R programming challenge called TidyTuesday. All code is shared and all datasets are publicly available.
