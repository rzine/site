---
title: Rencontres R 2023
subtitle: Conférence R francophone 
summary: Conférence R francophone 

authors:
- Société Française de Statistique
- INRAE 
- Avignon Université


publication_types: ["2", "9", "12"]

thematics: ["8", "9"]

sources:
- "11"

update:
- "0"

languages:
- "0"

date: "2023-06-23"


tags:
- rencontres R
- évenement
- conférence
- colloque
- INRAE
- SFdS
- Avignon

url_source: 'https://github.com/Rencontres-R/Rencontres_R_2023/tree/main'
url_code: 'https://github.com/Rencontres-R/Rencontres_R_2023/tree/main'

links:
- name: Site officiel
  url: https://rr2023.sciencesconf.org/
- name: Enregistrement vidéo
  url: https://www.youtube.com/playlist?list=PLC0_Y4EpEglW-9XRKOzW1QUB2RpBWeHUO


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Les Rencontres R 2023 se sont tenues du 21 au 23 juin à Avignon, France.

Les Rencontres R, portées par la Société Française de Statistique ([SFdS](https://www.sfds.asso.fr/)), ont pour objectif d'offrir à la communauté francophone un lieu d'échange et de partage d'idées sur l'usage du langage R toutes disciplines confondues.

L'édition 2023 a été co-organisée par INRAE et Avignon Université. Elle s'adresse aussi bien aux débutants qu'aux utilisateurs confirmés et expérimentés issus de tous les secteurs d'activités.

Plus d'information : [https://rr2023.sciencesconf.org](https://rr2023.sciencesconf.org)

Twitter : [@rencontres_R](https://twitter.com/rencontres_R)

LinkedIn groupe : [Rencontres R groupe](https://www.linkedin.com/groups/14126026/)

Chaine Youtube : [@RencontresR](https://www.youtube.com/@RencontresR) | [Playlist 2023](https://www.youtube.com/playlist?list=PLC0_Y4EpEglW-9XRKOzW1QUB2RpBWeHUO)




