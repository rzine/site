---
title: Reproductibilité des analyses avec R
subtitle : Séminaire RUSS - Séance du 11 juin 2021
summary: Séminaire RUSS - Séance du 11 juin 2021


authors:
- tgiraud

publication_types: ["12","9"]


sources:
- "7"

thematics: ["7"]


update:
- "0"

languages:
- "0"


projects:
- RUSS

date: "2021-06-09T00:00:00Z"

url_source: 'https://rcarto.github.io/spectre-reproductibilite-r/#1'
url_code: 'https://github.com/rCarto/spectre-reproductibilite-r'

links:
- name: Video (partie 1)
  url: https://www.canal-u.tv/video/ined/reproductibilite_des_analyses_avec_r_partie_1_recherche_reproductible.62281
- name: Video (partie 2)
  url: https://www.canal-u.tv/video/ined/reproductibilite_des_analyses_avec_r_partie_2_gerer_ses_packages.62285
- name: Video (partie 3)
  url: https://www.canal-u.tv/video/ined/reproductibilite_des_analyses_avec_r_partie_3_utiliser_des_containers.62289


tags:
- RUSS
- reproductibilité
- reproductible
- replication
- projet
- rmarkdown
- sessionInfo()
- renv
- target
- package
- container
- docker
- miniCRAN
- svn
- git

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Panorama des solutions assurant la reproductibilité des analyses avec R**, par Timothée Giraud (CNRS UMS-RIATE)

Le logiciel R est un outil de choix pour assurer la reproductibilité des traitements de données, sa nature même de langage de script est favorable à l’enregistrement explicites des procédures mobilisées dans les analyses. 
Utilisé en association avec d’autres briques logicielles telles que des packages dédiés (renv), les projets RStudio, le langage markdown, les conteneurs Docker ou les logiciels de gestion de version (git), R offre une variété de solutions pouvant assurer la reproductibilité des analyses. En fonction des options choisies cette reproductibilité peut être minimale ou très complète.



