---
date: "2018-09-25T00:00:00Z"

title: R Toulouse
subtitle: Groupe d'utilisateur.rices
summary: Groupe d'utilisateur.rice.s R de Toulouse

authors: 
- Marion Aguirrebengoa   
- Laurent Cauquil  
- Guillaume Devailly  

tags: 
- séminaire
- formation
- conférence
- évènement
- groupe

publication_types: ["4", "2", "9"]

thematics: ["8"]

projects:
- rtoulouse

sources:
- "0"

update:
- "1"

languages:
- "0"

url_source: 'https://r-toulouse.netlify.app/'
url_code: 'https://github.com/R-Toulouse'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

R Toulouse est un Groupe d’utilisatrices et d’utilisateurs de R à Toulouse. Il est ouvert à tou·te·s, quelques soit le niveau, le domaine, le statut.

Site web : [https://r-toulouse.netlify.app/](https://r-toulouse.netlify.app/)     
Youtube : [https://www.youtube.com/@r-toulouserug6316](https://www.youtube.com/@r-toulouserug6316)   
Twitter : [@RUG_Toulouse](https://twitter.com/RUG_Toulouse)
