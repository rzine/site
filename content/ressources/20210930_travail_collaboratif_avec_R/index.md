---
# Author(s)
authors:
- lgaliana
- rlesur
- omeslin
- Mathias André
- Annie Moineau


# Date
date: "2021-09-30"


# Title
title: Travail collaboratif avec R


# Short summary
summary: Supports associés à la formation de l'INSEE intitulée *Travail collaboratif avec R*.


# Tags
tags:
- INSEE
- Git
- gitlab
- markdown
- package


publication_types: ["5", "13"]


sources:
- "4"

thematics: ["7", "1", "5"]

update:
- "1"

languages:
- "0"


# Link
url_source: 'https://linogaliana.gitlab.io/collaboratif/'
url_code: 'https://gitlab.com/linogaliana/collaboratif'




# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

Supports associés à une formation interne de l'INSEE.


Partie 1: **Introduction au travail collaboratif**

- Rstudio   
- Git   
- R Markdown   

Partie 2: **Développement collaboratif**

- Fonctions   
- Packages    
   
Cette formation est associée à un guide des bonnes disponible dans la documentation [utilitR](https://www.book.utilitr.org/) que nous recommandons de consulter.

