---
title: A crash course in mapping with African data
summary: Interactive [learnr](https://rstudio.github.io/learnr/) tutorial which presents a gallery of plots using tmap, ggplot2 & mapview. Also **available by using** [**afrilearnr**](https://github.com/afrimapr/afrilearnr) **package**
subtitle: learnr tutorial by Afrimapr

authors:
- Laurie Baker
- Andy South

publication_types: ["1", "13"]


sources:
- "1"

thematics: ["0","11"]


update:
- "1"

languages:
- "1"


projects:
- afrimapr

date: "2021-02-04T02:00:00Z"

tags:
- africa
- afrique
- map
- tmap
- carte
- cartographie
- raster
- sf
- afrilearndata
- africapitals
- afrihighway
- africountries
- rgdal
- africontinent
- mapview
- ggplot2

url_source: 'https://andysouth.shinyapps.io/afrilearnr-crash-course/'
url_code: 'https://github.com/afrimapr/afrilearnr'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Interactive [learnr](https://rstudio.github.io/learnr/) tutorial which presents a gallery of plots using tmap, ggplot2 & mapview, with data from [afrilearndata](https://github.com/afrimapr/afrilearndata)

You can also use this tutorial by installing [**afrilearnr**](https://github.com/afrimapr/afrilearnr) **package**.


