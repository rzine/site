---
title: Introduction à R pour les chatons
summary: Rédaction en cours - Ce livre s'adresse à toute personne qui n'a jamais fait de programmation et qui désire apprendre les bases du langage R

authors:
- Pierre Jacquel

publication_types: ["5"]


sources:
- "0"

thematics: ["0","1"]


update:
- "1"

languages:
- "0"

projects:
- divers

tags:
- base
- objet
- vecteur
- liste
- opérateur
- date
- matrice
- facteur
- Rstudio
- package
- tibbles
- Rmarkdown
- environnement
- fonction
- purrr

date: "2021-02-25T00:00:00Z"

url_source: 'https://demandred.gitbooks.io/introduction-a-r-pour-les-chatons/content/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


*Ce livre est encore en version alpha. Son contenu risque encore d'évoluer dans le temps et de nombreuses corrections et modifications restent encore à apporter. Il contient en particulier de nombreuses erreurs d'ortographe qui ne seront corrigées que quand il aura atteint sa forme finale.*

R est un langage de plus en plus utilisé à travers le monde, en particulier par les universitaires et les data scientistes. Ce langage, spécialisé dans l'analyse des données, a connu une croissance rapide ses dernières années. Cela n'est pas sans rapport avec la montée en puissance de ce qu'il est convenu d'appeller le big data. L'explosion des données accessibles et de la puissance de calcul disponible ont radicalement changé la donne dans de nombreux domaines. De l'économie au marketing, en passant par la santé ou les politiques publiques, il est devenu nécessaire de traiter correctement cette masse de données nouvelles pour prendre de meilleures décisions. Le langage R est l'un des un outil conçu pour explorer ce nouveau monde de données.

Malgrès son succés ces dernières années, il reste néanmoins difficile de trouver des ressources fiables et accessibles pour débuter avec ce langage. La plupart des ouvrages et cours actuels sont soit destinés à un public de programmeurs, soit orientiés vers un aspect pratique du langage, sans en expliquer les bases. Il n'existe pas à notre connaissance de cours qui soit à la fois destiné aux débutants, et qui se concentre sur les mécanismes principaux du langage R.

Nous aimerions ici essayer de proposer une solution à ce problème, en proposant un cours nouveau. Ce cours se veut à la fois accessible aux personnes n'ayant jamais appris à programmer de leur vie, tout en couvrant de façon rigoureuse les bases du langage. Nous pensons que une fois que vous maitriserez correctement les fondementaux, vous serez alors capable de progresser rapidement par vous même pour explorer les nombreuses possibilités qu'offre R : visualisation de données, maching learning, économétrie, etc.



