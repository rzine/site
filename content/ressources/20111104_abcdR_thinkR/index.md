---
title: Abcd'R
summary: Site collaboratif de partage de scripts, de codes et d’astuces

authors:
- ThinkR

publication_types: ["0", "8"]


sources:
- "0"

thematics: ["0","1","10","11","12","13","2","20","8"]


update:
- "1"

languages:
- "0"

projects:
- divers


date: "2011-11-04T00:00:00Z"


url_source: 'https://abcdr.thinkr.fr/'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


L’Abcd’R est un site collaboratif de partage de scripts, de codes et d’astuces sur le logiciel R (GNU-R).
Combien d’entre nous ont été bloqués par des soucis de base: « Comment importer des données excel ? », « Comment fait-on déjà pour utiliser apply ? »…

Ce site se veut simple et didactique, **chaque script est commenté**, classé et répond à une **problématique précise**. Les scripts R proposés peuvent être lancés directement, c’est-à-dire que les packages  utiles sont précisés et que des petits jeux de données sont proposés lorsque nécessaire.
N’hésitez pas vous aussi à [proposer vos scripts R](https://abcdr.thinkr.fr/soumettre-un-article/) et à noter ceux des autres.




