---
title: dygraph for R
subtitle: The dygraphs package is an R interface to the dygraphs JavaScript charting library
summary: The dygraphs package is an R interface to the dygraphs JavaScript charting library

authors:
- Rstudio

publication_types: ["6"]

thematics: ["11"]

sources:
- "0"

update:
- "1"

languages:
- "1"

projects: ["RencontresR"]

date: "2018-07-11"


tags:
- javascript
- time series
- interactive

url_code: 'https://github.com/rstudio/dygraphs'
url_source: 'https://rstudio.github.io/dygraphs/'


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


The dygraphs package is an R interface to the [dygraphs](https://dygraphs.com/) JavaScript charting library. It provides rich facilities for charting time-series data in R, including:

- Automatically plots xts time series objects (or any object convertible to xts).
- Highly configurable axis and series display (including optional second Y-axis).
- Rich interactive features including zoom/pan and series/point highlighting.
- Display upper/lower bars (e.g. prediction intervals) around series.
- Various graph overlays including shaded regions, event lines, and point annotations.
- Use at the R console just like conventional R plots (via RStudio Viewer).
- Seamless embedding within R Markdown documents and Shiny web applications.




































