---
title: Why R? Webinars
summary: Webinars organised by a team of R enthusiasts from both business and academia who aim to support local R communities around the world


authors:
- Why R? Foundation

publication_types: ["9","8"]


sources:
- "0"

thematics: ["8"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- webinar


date: "2020-04-02T00:00:00Z"

url_source: 'http://whyr.pl/foundation/webinars/'

links:
- name: Youtube channel
  url: https://www.youtube.com/WhyRFoundationVideos


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Webinars organised by a team of R enthusiasts from both business and academia who aim to support local R communities around the world
Major activities focus on international events of R statistical software users, like conferences, hackathons or webinars.


