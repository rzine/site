---
authors:
- Alicia A. Johnson
- Miles Q. Ott
- Mine Dogucu

title: Bayes Rules !

subtitle: An introduction to applied bayesion modeling

summary: This book empowers readers to weave Bayesian approaches into an everyday modern practice of statistics and data science

date: "2021-12-01"

publication_types: ["5"]

thematics: ["12", "13", "14", "16", "3", "4"]

languages:
- "1"


update:
- "1"

sources:
- "0"


url_source: 'https://www.bayesrulesbook.com/' 


links:
- name: project
  url: 'https://bayes-rules.github.io/' 



tags:
- Bayesian Modeling
- Bayes
- Models
- Inference
- Simulation
- Markov chains
- Prediction
- Regression
- Classification
- Naive Bayes

  

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


Bayesian statistics?! Once an obscure term outside specialized industry and research circles, Bayesian methods are enjoying a renaissance. The title of this book speaks to what all the fuss is about: Bayes Rules! Bayesian methods provide a powerful alternative to the frequentist methods that are ingrained in the standard statistics curriculum. Though frequentist and Bayesian methods share a common goal – learning from data – the Bayesian approach to this goal is gaining popularity for many reasons: (1) Bayesian methods allow us to interpret new data in light of prior information, formally weaving both into a set of updated information; (2) relative to the confidence intervals and p-values utilized in frequentist analyses, Bayesian results are easier to interpret; (3) Bayesian methods can shine in settings where frequentist “likelihood” methods break down; and (4) the computational tools required for applying Bayesian techniques are increasingly accessible. Unfortunately, the popularity of Bayesian statistics has outpaced the curricular resources needed to support it. To this end, the primary goal of Bayes Rules! is to make modern Bayesian thinking, modeling, and computing accessible to a broader audience.

