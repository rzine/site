---
title: rOpenSci Packages 
subtitle: Development, Maintenance, and Peer Review
summary: This book is a guide for authors, maintainers, reviewers and editors of rOpenSci... and more !

authors:
- rOpenSci
- Brooke Anderson
- Scott Chamberlain
- Laura DeCicco
- Julia Gustavsen
- Anna Krystalli
- Mauro Lepore
- Lincoln Mullen
- Karthik Ram
- Noam Ross
- msalmon
- Melina Vidoni


publication_types: ["5"]


sources:
- "5"

thematics: ["5"]


update:
- "1"

languages:
- "1"

projects:
- rOpenSci

doi: '10.5281/zenodo.4554776'
date: "2020-04-15T00:00:00Z"

url_source: 'https://devguide.ropensci.org/index.html'
url_code: 'https://github.com/ropensci/dev_guide'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

The [first section](https://devguide.ropensci.org/building.html#building) of the book contains our guidelines for creating and testing R packages.

The [second section](https://devguide.ropensci.org/softwarereviewintro.html#softwarereviewintro) is dedicated to rOpenSci’s software peer review process: what it is, our policies, and specific guides for authors, editors and reviewers throughout the process.

The [third and last section](https://devguide.ropensci.org/collaboration.html#collaboration) features our best practice for nurturing your package once it has been onboarded: how to collaborate with other developers, how to document releases, how to promote your package and how to leverage GitHub as a development platform. The third section also features a [chapter for anyone wishing to start contributing to rOpenSci packages](https://devguide.ropensci.org/contributingguide.html#contributingguide).

This book is a living document. You can view updates to our best practices and policies via the [release notes](https://devguide.ropensci.org/booknews.html#booknews).



