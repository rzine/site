---
title: Econometrics in R
subtitle:  
summary: This paper was originally written as part of a teaching assistantship and has subsequently become a personal reference

authors:
- Grant V. Farnsworth

publication_types: ["5"]


thematics: ["0","11","12","15"]


sources :
- "0"

update :
- "0"

languages:
- "1"

projects:
- divers

date: "2008-10-26T00:00:00Z"


tags:
- regression
- math
- plotting
- statistics
- time series



url_source: 'https://cran.r-project.org/doc/contrib/Farnsworth-EconometricsInR.pdf'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---



This paper was originally written as part of a teaching assistantship and has subsequently become a personal reference

