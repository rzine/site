--- 
authors:
- Kevin Ushey

date: "2021-01-09"

title: renv
subtitle: Package for reproducible environments
summary: The renv package helps you create reproducible environments for your R projects

tags:
- rstudio
- reproductible
- environment
- package

publication_types: ["6"]


thematics: ["7"]


languages:
- "1"

sources :
- "0"

update :
- "1"

projects:
- divers

url_source: 'https://rstudio.github.io/renv/index.html'
url_code: 'https://github.com/rstudio/renv/'

links:
- name : CRAN
  url: https://cran.r-project.org/web/packages/renv/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


The *renv* package helps you create **r**eproducible **env**ironments for your R projects. Use *renv* to make your R projects more:

- **Isolated**: Installing a new or updated package for one project won’t break your other projects, and vice versa. That’s because renv gives each project its own private package library.
- **Portable**: Easily transport your projects from one computer to another, even across different platforms. renv makes it easy to install the packages your project depends on.
- **Reproducible**: renv records the exact package versions you depend on, and ensures those exact versions are the ones that get installed wherever you go.


