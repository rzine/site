
---
authors:
- hcommenges
- lbeauguitte
- Élodie Buard
- rcura
- Florent Le Néchet
- mletexier86
- hmathian
- srey

date: "2014-09-01"

title: R et espace
subtitle: Traitement de l’information géographique
summary: Un manuel d’initiation à la programmation avec R appliqué à l’analyse de l’information géographique. Analyse spatiale, méthode et traitement des données.

tags:
- cartographie
- analyse spatiale
- sp
- carte
- information géographique
- statistique


publication_types: ["5"]


thematics: ["10","11","12","18","2","19"]



languages:
- "0"

sources:
- "12"

update:
- "0"

projects:
- elementr

tags:
- elementr


url_source: 'https://framabook.org/docs/Respace/RetEspace_final_20140901.pdf'


publishDate: "2014-09-01T00:00:00Z"
publication: R et espace. Traitement de l’information géographique
publication_short: R et espace
  
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


*R et espace. Traitement de l’information géographique* est un irremplaçable support technique pour les utilisateurs de R appliqué à l’analyse de l’information géographique !

**Ce manuel a pour ambition d’offrir un support technique à des étudiants, enseignants-chercheurs, ingénieurs de toutes disciplines qui partagent notre intérêt pour l’utilisation des méthodes d’analyse de l’information géographique et souhaiteraient pouvoir développer leurs travaux à l’aide du logiciel libre R. Il est accompagné de jeux de données et peut être utilisé comme support pédagogique.**

Plus spécifiquement, ce manuel propose une prise en main dédiée à des personnes s’initiant simultanément au traitement de données géographiques et à la programmation avec R. L’éclairage est celui de l’analyse spatiale, à savoir des méthodes mises en œuvre pour l’étude des organisations dans l’espace.



