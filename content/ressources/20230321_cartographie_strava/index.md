---
title: Cartographie et analyse spatiale de traces Strava/garmin
subtitle : Exploration de traces GPS à partir de fichiers .fit
summary: Exploration de traces GPS à partir de fichiers .fit


authors:
- tfeuillet

publication_types: ["11"]


sources:
- "0"

thematics: ["2", "19"]


update:
- "0"

languages:
- "0"



date: "2023-03-21T00:00:00Z"

url_source: 'https://tfeuillet.gitpages.huma-num.fr/mapstrava/'
url_code: 'https://gitlab.huma-num.fr/tfeuillet/mapstrava'

tags:
- strava
- garmin
- GPS
- fit
- FITfileR
- sfheaders
- raster
- terra
- sf
- tmap


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Pour celles et ceux qui disposent d'un compte strava/garmin actif, sachez qu'il vous est tout à fait possible de récupérer rapidement toutes vos données personnelles de tracking, et de les cartographier à souhait. Vous pourrez alors passer des heures à admirer vos traces en spaghettis, vous auto-congratuler pour tous les secteurs déjà explorés, calculer des densités, et localiser facilement toutes les zones vierges qu'il vous reste à parcourir...


Ce *notebook* vous permettra d'y parvenir, et de suggérer quelques pistes d'analyse spatiale complémentaire de ces traces.






