---
authors:
- Luigi Ranghetti
- Lorenzo Busetto
- Marina Ranghetti

title: sen2r - Find, Download and Process Sentinel-2 Data

summary: A R library which helps to download and preprocess Sentinel-2 optical images

date: "2022-04-12"

publication_types: ["6"]

thematics: ["7", "20"]

languages:
- "1" 

update:
- "1"

sources:
- "0"



url_source: 'http://sen2r.ranghetti.info/' 
url_code: 'https://github.com/ranghetti/sen2r/' 

tags:
- Satellite
- imagery
- Spectral indices
- Preprocessing
- Docker
- sen2r
- Copernicus 
- Open Access Hub
- Google Cloud Sentinel-2 bucket
- Sentinel

  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

`sen2r` is an R library which helps to download and preprocess Sentinel-2 optical images. The purpose of the functions contained in the library is to provide the instruments required to easily perform  all the steps necessary to build a complete Sentinel-2 processing chain, without the need of any manual intervention nor the needing to manually integrate any external tool.

In particular, `sen2r` allows to :

- Retrieve the list of available products on a selected area.
- Download them.
- Obtain the required products (Top of Atmosphere radiances, Bottom of Atmosphere reflectances, Surface Classification Maps, True Colour Images)
- Mask cloudy pixels.
- Computing spectral indices and RGB images.
