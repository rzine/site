---
title: PsyTeachR
subtitle: Reproductible research
summary: This website contains our open materials (books, tutorial, video...) for teaching reproducible research.

authors:
- Heather Cleland Woods
- Helena Paterson
- Niamh Stack
- Dale Barr
- Phil McAleer
- Lisa DeBruine

publication_types: ["13","8"]


sources:
- "0"

thematics: ["10","12","13","11","4"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- psychology
- programming
- visualisation
- wrangling
- report
- reproductible
- model


date: "2021-01-06T00:00:00Z"

url_source: 'https://psyteachr.github.io/index.html'
url_code: 'https://github.com/PsyTeachR'



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

The psyTeachR team at the University of Glasgow [School of Psychology](https://www.gla.ac.uk/schools/psychology/) and [Institute of Neuroscience and Psychology](https://www.gla.ac.uk/researchinstitutes/neurosciencepsychology/) has successfully made the transition to teaching reproducible research using R across all undergraduate and postgraduate levels. Our curriculum now emphasizes essential ‘data science’ graduate skills that have been overlooked in traditional approaches to teaching, including programming skills, data visualisation, data wrangling and reproducible reports. Students learn about probability and inference through data simulation as well as by working with real datasets.

This website contains our open materials for teaching reproducible research.


