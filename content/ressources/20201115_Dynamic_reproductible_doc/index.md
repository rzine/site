---
title: Writing dynamic and reproducible documents
subtitle: An introduction to R Markdown
summary: An introduction to R Markdown

authors:
- ogimenez

publication_types: ["12"]


thematics: ["1"]


sources :
- "0"

update :
- "0"

languages:
- "1"

projects:
- divers

date: "2020-11-15T00:00:00Z"

tags:
- markdown
- rmarkdown
- latex
- html
- knitr
- chunck
- yaml
- kint
- bibliography
- citation


url_source: 'https://oliviergimenez.github.io/intro_rmarkdown/#1'
url_code: 'https://github.com/oliviergimenez/intro_rmarkdown'

links:
- name: pratical
  url: https://github.com/oliviergimenez/intro_rmarkdown_practical


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

