---
title: Introduction à Git
subtitle : Configurer GitHub pour partager une présentation et son code source
summary: Support d'un atelier réalisé dans le cadre de l'école SIGR2021


authors:
- tgiraud

publication_types: ["12","13"]


sources:
- "9"

thematics: ["7"]


update:
- "0"

languages:
- "0"


projects:
- SIGR2021

date: "2021-06-29T03:00:00Z"

url_source: 'https://sigr2021.github.io/git/#1'
url_code: 'https://github.com/sigr2021/git'

tags:
- SIGR2021
- Git
- rmarkdown
- GitHub
- GitLab
- repository
- token
- SSH
- clone
- commit
- push
- pull


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Communication réalisée durant l'école thématique SIGR2021 sur les sciences de l’information géographique reproductibles.

Cette présentation introduit à Git et à la configuration d'un compte GitHub pour partager une présentation et son code source.





