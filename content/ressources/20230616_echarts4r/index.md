---
title: echarts4r
subtitle: Create Interactive Graphs with 'Echarts JavaScript' Version 5
summary:  Create Interactive Graphs with 'Echarts JavaScript' Version 5

authors:
- John Coene
- David Munoz Tord


projects:
- divers

sources :
- "0"

thematics: ["11", "5"]


update :
- "1"

publication_types: ["6"]


languages:
- "1"


date: "2023-06-16T00:00:00Z"

tags:
- echarts.js
- apache echarts
- javascript
- Interactive
- visualization
- map
- graph
- chart
- timeline
- 3d



url_source: 'https://echarts4r.john-coene.com/index.html'
url_code: 'https://github.com/JohnCoene/echarts4r'

links:
- name: CRAN
  url: https://cloud.r-project.org/web/packages/echarts4r/index.html
- name : Shiny demo
  url: https://shiny.john-coene.com/echarts4rShiny/



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Package for interactive visualisations via Apache ECharts.
