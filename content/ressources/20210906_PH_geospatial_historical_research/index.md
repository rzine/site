---
title: Using Geospatial Data to Inform Historical Research in R
summary: In this lesson, you will use R-language to analyze and map geospatial data. Lesson from [The Programming Historian](https://programminghistorian.org/)

authors:
- Eric Weinberg

publication_types: ["10","13"]

sources:
- "10"

thematics: ["2", "19"]


update:
- "1"

languages:
- "1"


tags:
- sf
- tmap
- plotly
- census data
- geocoding
- Programming Historian
- PH
  
projects:
- programming_historian



date: "2018-08-20T00:00:00Z"


featured: true
image:
  placement : 1
  caption: ""
  focal_point: "Center"
  preview_only: false
  



doi: '10.46430/phen0075'
url_source: 'https://programminghistorian.org/en/lessons/geospatial-data-analysis'



---

In this lesson, you will use R-language to analyze and map geospatial data. Lesson from [The Programming Historian](https://programminghistorian.org/).



