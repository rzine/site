
---
# AUTHOR(S) 
authors:
- Rstudio


# DATE de soumission à Rzine
date: "2020-01-20"

# TITRE
title: Shiny from Rstudio 
subtitle: Tutorials, gallery and articles
summary: Official shiny website (Tutorials, gallery and articles)


# TAGS
tags:
- application
- shiny
- gallery
- tutorial



# TYPE DE PUBLICATION
publication_types: ["6","13"]



thematics: ["5"]





languages:
- "1"

sources :
- "0"

update :
- "1"



projects:
- divers


url_source: 'https://shiny.rstudio.com/'
url_code: 'https://github.com/rstudio/shiny'

links:
- name: Tutorial
  url: https://shiny.rstudio.com/tutorial/
- name: CRAN
  url: https://cran.r-project.org/web/packages/shiny/index.html



  
# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**Shiny is an R package that makes it easy to build interactive web apps straight from R**. You can host standalone apps on a webpage or embed them in R Markdown documents or build dashboards. You can also extend your Shiny apps with CSS themes, htmlwidgets, and JavaScript actions. 



