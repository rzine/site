---
title: bookdown.org
subtitle : An important list of free online bookdowns
summary: The bookdown package facilitates writing books with R Markdown. On this website, you will find a list of free online bookdowns.


authors:
- Rstudio

publication_types: ["8"]


sources:
- "0"

thematics: ["8"]


update:
- "1"

languages:
- "1"


date: "2016-12-15T00:00:00Z"

url_source: 'https://bookdown.org/'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

The website bookdown.org is a service provided by [RStudio, PBC](https://www.rstudio.com/) to host books. It is free for you to publish the static output files of your book, and you hold the full copyright of your own books. Please note that bookdown.org is based on [RStudio Connect](https://www.rstudio.com/products/connect/), so in theory you could publish any types of content here (single R Markdown reports, slides, dashboards, Shiny apps, and so on). However, we only support books here, and reserve the right to stop serving and delete other types of content you publish to bookdown.org. Please consider using RStudio Connect, [ShinyApps.io](https://www.shinyapps.io/), other hosting services, or your own server for publishing those types of content instead.








