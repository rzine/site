---
title: rstudio4edu
subtitle: A Handbook for Teaching and Learning with R and RStudio
summary: A Handbook for Teaching and Learning with R and RStudio
date: "2019-09-11T00:00:00Z"

authors:
- Desirée De Leon
- Alison Hill

publication_types: ["5"]


thematics: ["1","5","9"]


sources :
- "0"

update :
- "0"

languages:
- "1"

projects:
- divers


tags:
- Rstudio
- teaching
- learning
- package
- markdown
- distill
- website
- blogdown
- git


url_source: 'https://rstudio4edu.github.io/rstudio4edu-book/'
url_code: 'https://github.com/rstudio4edu/rstudio4edu-book'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


#### Preface

Welcome! If you do educational work and use tools in the data science ecosystem, you may have seen a great tutorial made with R Markdown, or attended a streamlined workshop where learning the content was front-and-center and all the headaches around installation and setup were absent. Or maybe you have stumbled upon a polished workshop, book, or course website and thought: **“I want to make that too!”** So that is what we have set out to do: make a handbook that will fill in those gaps for educators to create educational materials that engage learners, inspire other educators, and most importantly save you time and make it more fun for you to teach.

This handbook was inspired by the [Handbook for Teaching and Learning with Jupyter](https://jupyter4edu.github.io/jupyter-edu-book/)- a book written by and for educators who teach data science. It is a thoughtful and inspiring resource for all educators, with a focus on the Python ecosystem. We aimed to create a similar resource for educators working with the R and RStudio ecosystem.

If you find our materials useful, give us a shout-out on Twitter using #rstudio4edu. We hope you do! If you have ideas for ways to improve them, please consider contributing to the project.


