---
title: Représentation graphique avec R
subtitle: "`ggplot2` et la grammaire graphique"
summary: "`ggplot2` et la grammaire graphique"

authors:
- rcura 
- Léa Christophe 

publication_types: ["12", "13"]

thematics: ["11"]

sources:
- "12"

update:
- "0"

languages:
- "0"

date: "2023-01-25"


tags:
- ggplot
- ggplot2
- grammaire graphique
- grammar of graphics

url_source: 'https://elementr.gitpages.huma-num.fr/session_notebook/programmation_lettree/#/title-slide'
url_code: 'https://gitlab.huma-num.fr/elementr/session_notebook/programmation_lettree'


# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



Ce diaporama a été utilisé dans le cadre d'une séance de formation proposée par le groupe d'utilisateur·rices [ElementR](https://elementr.netlify.app/), ayant pour objectif de présenter le package `ggplot2` et son fonctionnement.

Un exercice appliqué est associé à cette présentation. Vous pouvez récupérer le matériel nécessaire sur la page web associée : [https://elementr.netlify.app/posts/seance4_ggplot](https://elementr.netlify.app/posts/seance4_ggplot)



