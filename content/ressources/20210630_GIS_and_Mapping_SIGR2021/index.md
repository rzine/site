---
title: GIS and mapping (Part 1)
subtitle : A workshop on GIS and mapping with R
summary: First part of a workshop on GIS and mapping with R at thematic school SIGR2021 


authors:
- rlovelace
- jnowosad

publication_types: ["12","13"]


sources:
- "9"

thematics: ["2"]


update:
- "0"

languages:
- "1"


projects:
- SIGR2021

date: "2021-06-30T01:00:00Z"

url_source: 'https://nowosad.github.io/SIGR2021/workshop1/workshop1_rl.html#1'
url_code: 'https://github.com/Nowosad/SIGR2021/tree/master/workshop1'

tags:
- SIGR2021
- spData
- osm
- osmdata
- osmextract
- nzelect
- mapview
- sf
- rmapshaper


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


First part of a workshop on GIS and mapping with R at thematic school SIGR2021.



