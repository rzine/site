---
title: Constituer un corpus Europresse utilisable dans R, Iramuteq et TXM
subtitle: 
summary:

authors:
- croquebert 

projects:
- quanti

sources:
- "0"

thematics: ["10","17"]


languages:
- "0"


update:
- "0"

publication_types: ["11","13"]


date: "2018-12-04T00:00:00Z"


tags:
- europresse
- iramuteq
- TXM
- xml2
- XML
- stringr
- stringdist
- stringi
- lubridate
- quanti
- rpubs


url_source: 'https://quanti.hypotheses.org/1416'

links:
- name: Version RPubs
  url: https://rpubs.com/CorentinRoquebert/europresse



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Billet proposé par le carnet de recherche QUANTI : *Les outils du quanti en sciences humaines et sociales*.

Ce tutoriel est également disponible dans un format permettant une meilleure lisibilité des scripts R à cette adresse : [https://rpubs.com/CorentinRoquebert/europresse](https://rpubs.com/CorentinRoquebert/europresse).





