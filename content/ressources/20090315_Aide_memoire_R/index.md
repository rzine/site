---
title: Aide mémoire R
summary: Référence des fonctions de R les plus courantes

authors:
- Mayeul Kauffman

publication_types: ["0"]


sources:
- "0"

thematics: ["0"]


update:
- "0"

languages:
- "0"

projects:
- divers


date: "2009-03-15T00:00:00Z"

tags:
- cheatsheet
- rbase

url_source: 'https://cran.r-project.org/doc/contrib/Kauffmann_aide_memoire_R.pdf'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Ce qui suit ne montre qu’une minuscule partie des fonctions de R. Ce document est en grande partie traduit de Tom Short, « R Reference Card », 12 juillet 2005 (domaine public).La « R Reference Card » inclut des éléments de [R for Beginners](http://r.developpez.com/tutoriels/r/debutants/debutants.pdf) d’Emmanuel Paradis (reproduit par Tom Short avec autorisation). En plus de la traduction de l'anglais vers le français, nous avons fait quelques ajouts et modifications, etenlevé environ un tiers des fonctions.






