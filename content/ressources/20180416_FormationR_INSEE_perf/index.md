---
title: Formation R perfectionnement

subtitle: Formation donnée à la Drees en Avril 2018
summary:  Formation donnée à la Drees en Avril 2018

authors:
- mchevalier

# Type de la publication 
publication_types: ["12","13"]


thematics: ["10","11","1","7"]


languages:
- "0"


sources:
- "4"

update:
- "0"

projects:
- spyrales

# Date that the page was published
date: "2018-04-16T00:00:00Z"
doi: ""

 
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

tags:
- apply
- do.call
- Reduce
- ggplot2
- foreign
- DREES
- INSEE




url_source: 'https://teaching.slmc.fr/perf/'


links:
- name: Diaporama
  url: https://teaching.slmc.fr/perf/presentation_handout.pdf
- name: Données
  url: https://teaching.slmc.fr/perf/donnees.zip


---

Tous les supports de la formation *R perfectionnement* donnée à la  Direction de la Recherche, des Études, de l'Évaluation et des Statistiques (Drees) les 16 et 17 avril 2018 :

- **Supports de présentation**
- **Ennoncé et correction** d'exercices
- **Données utilisées**


<br>

#### Conditions d'utilisation

Ces supports seront durablement disponibles à l’adresse http://r.slmc.fr et leur code source sur [github](https://github.com/martinchevalier/r_init). L’ensemble est librement réutilisable sous **© 2016-2020 Martin Chevalier** [CC BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/).




