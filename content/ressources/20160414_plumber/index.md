---
authors:
- Jeff Allen

date : "2016-04-14"

# TITRE
title : plumber

subtitle: An R package that converts your existing R code to a web API.
summary: An R package that converts your existing R code to a web API using a handful of special one-line comments.

# TAGS
tags :
- plumber
- API


# TYPE DE PUBLICATION
publication_types: ["6"]


thematics: ["6"]


languages :
- "1"




sources :
- "0"

update :
- "1"

projects :
- divers


url_source : 'https://www.rplumber.io/'
url_code : 'https://github.com/rstudio/plumber'

links:
- name : CRAN
  url: https://cran.r-project.org/web/packages/plumber/index.html


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


An R package that converts your existing R code to a web API using a handful of special one-line comments.




