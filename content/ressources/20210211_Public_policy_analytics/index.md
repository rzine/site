---
title: Public Policy Analytics
subtitle : Code & Context for Data Science in Government
summary: Code & Context for Data Science in Government

authors:
- Ken Steif

publication_types: ["5"]


sources:
- "0"

thematics: ["13","19","3"]


update:
- "0"

languages:
- "1"


projects:
- divers

date: "2021-02-11T00:00:00Z"

tags:
- transit oriented development
- TOD
- Urban Growth Boundary
- lancaster
- density
- Boundarie
- geospatial
- machine learning
- price
- model
- predict
- neighborhood
- autocorrelation
- rsik
- poisson
- ML


url_source: 'https://urbanspatial.github.io/PublicPolicyAnalytics/'
url_dataset: 'https://github.com/urbanSpatial/Public-Policy-Analytics-Landing'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Public Policy Analytics is a new book by Ken Steif, Ph.D that teaches at the intersection of data science and public policy. The book is available online and eventually, in print. Designed for students studying City Planning and related disciplines, the book teaches both code and context toward improved public-sector decision making. Readers can expect an introduction to R, geospatial data science, and machine learning, conveyed through real world use cases of data science in government.

All of the book's data is free and open source, compiled from across the web. Each chapter includes API calls that read data directly into R. However, for posterity, the [DATA](https://github.com/urbanSpatial/Public-Policy-Analytics-Landing/tree/master/DATA) folder on this repo has all the data, organized by chapter. The sections below provide a description of each dataset and the original source, when applicable.




