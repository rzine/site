---
title: GIS and mapping (Introduction)
subtitle : A lecture on the GIS and mapping ecosystem of R
summary: A lecture on the GIS and mapping ecosystem of R. Communication for the thematic school SIGR2021 


authors:
- rlovelace
- jnowosad

publication_types: ["12","13"]


sources:
- "9"

thematics: ["2","2"]


update:
- "0"

languages:
- "1"


projects:
- SIGR2021

date: "2021-06-30T00:00:00Z"

url_source: 'https://nowosad.github.io/SIGR2021/lecture/lecture.html#1'
url_code: 'https://github.com/Nowosad/SIGR2021/tree/master/lecture'

tags:
- SIGR2021
- sf
- spatial
- rspatial
- sp
- terra
- raster
- stars 
- tmap
- OSGeo
- PROJ
- GDAL
- GEOS
- s2geometry
- lwgeom
- vector
- raster
- units
- mapping
- cartogram
- rayshader
- mapsf
- leaflet
- mapview
- mapdeck
- cartogram
- CRS
- geocomputation



featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


Introduction of the GIS and mapping ecosystem of R at thematic school SIGR2021.



