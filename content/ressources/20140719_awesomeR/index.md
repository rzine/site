---
title: Awesome R
subtitle: Curated list of awesome R packages and tools
summary: A curated list of awesome R packages and tools. Inspired by [awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning)

authors:
- R community

# Type de la publication 
publication_types: ["0"]


sources :
- "0"

thematics: ["9"]


update :
- "1"





languages:
- "1"

projects:
- divers

# Date that the page was published
date: "2014-07-19T00:00:00Z"
doi: ""  

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

# Tagging your content helps users to discover similar content on your site. 
# Tags can improve search relevancy and are displayed after the page content and also in the Tag Cloud widget.
tags:
- package
- tool
- list

url_source: 'https://project-awesome.org/qinwf/awesome-R'
url_code: 'https://github.com/qinwf/awesome-R'

---


A curated list of awesome R packages and tools. Inspired by [awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning)


