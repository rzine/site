---
date: "2013-09-28T00:00:00Z"
authors:
- Pascal Cristofoli
- bgarnier
- tgiraud
- Elisabeth Morand

title: R à l'Usage des Sciences Sociales
subtitle: Séminaire sur la pratique de R en SHS
summary: RUSS propose une séance tous les deux mois sur la pratique de R en sciences sociales

tags: 
- ined
- russ
- ehess
- séminaire
- évènement

publication_types: ["2", "9"]

thematics: ["8"]

projects:
- rencontresr

sources:
- "7"

update:
- "1"

languages:
- "0"


url_source: 'https://russ.site.ined.fr/'

links:
- name: Vidéos
  url: https://www.canal-u.tv/chaines/ined/seminaire-russ-r-a-l-usage-des-sciences-sociales
- name: Liste de diffusion
  url: https://listes.ined.fr/subscribe/seminaire-russ@ined.fr

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


**L’objectif de ce séminaire est d’échanger autour du logiciel de statistique libre, gratuit et multiplateforme R.**

Il s’adresse aux praticiens impliqués dans le traitement quantitatif des données en sciences humaines et sociales (utilisateurs de données, chercheurs, ingénieurs ou étudiants) qu’ils aient ou non déjà utilisé le logiciel R.

Chaque séance est organisée autour de la présentation d’une expérience de traitement de données avec le logiciel (fonction spécifique et/ou packages). Le cadre de ces réunions est informel et les participants doivent se sentir libres d’intervenir afin de confronter leurs expériences.


## Infos pratiques

RUSS organise un séance tous les 2 mois sur le [Campus Condorcet](https://www.campus-condorcet.fr/).  
Pour s'inscrire à la liste de diffusion : [https://listes.ined.fr/subscribe/seminaire-russ@ined.fr](https://listes.ined.fr/subscribe/seminaire-russ@ined.fr)   
**Pour plus d'informations** : [***https://russ.site.ined.fr/***](https://russ.site.ined.fr/)


