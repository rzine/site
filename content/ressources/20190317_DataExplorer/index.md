---
title: DataExplorer 
subtitle:
summary: This R package aims to automate most of data handling and visualization

authors:
- Boxuan Cui 

projects:
- divers

sources :
- "0"

thematics: ["11","12","9"]


update :
- "1"

publication_types: ["6"]


languages:
- "1"


date: "2019-03-17T00:00:00Z"

tags:
- exploration
- report
- data handling
- visualization



url_source: 'https://boxuancui.github.io/DataExplorer/index.html'
url_code: 'https://github.com/boxuancui/DataExplorer/'

links:
- name : CRAN
  url: https://cran.r-project.org/web/packages/DataExplorer/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

[Exploratory Data Analysis (EDA)](https://en.wikipedia.org/wiki/Exploratory_data_analysis) is the initial and an important phase of data analysis/predictive modeling. During this process, analysts/modelers will have a first look of the data, and thus generate relevant hypotheses and decide next steps. However, the EDA process could be a hassle at times. This R package aims to automate most of data handling and visualization, so that users could focus on studying the data and extracting insights.
