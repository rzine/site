---
# AUTHOR(S) 
authors:
- frebaudo

# DATE de soumission à Rzine
date: "2019-11-15"

# TITRE
title: Module R’Stat1
subtitle: Analyse de variance… et la modélisation linéaire

# RESUME COURT - une phrase
summary: Formation IRD - Analyse de variance et modélisation linéaire

# TAGS
tags:
- cours
- statistiques
- descriptive
- ANOVA
- régréssion
- linèaire
- multiple
- polynomiale
- ANCOVA
- série temporelle
- GML

publication_types: ["12", "13"]


thematics: ["10","11","12","13","15"]


languages:
- "0"

sources:
- "0"

update:
- "1"

projects:
- divers

url_source: 'http://myrbookfr.netlify.com/myhtmls/france_montpellier_2019/r00_links#/'
url_code : 'https://github.com/frareb/coursDeR'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Support de **formation R'Stat1** organisée par le service Développement des talents et qualité de vie au travail de l'**IRD** en novembre 2019.


