---
title: Reproductible Corporate Publications with R
subtitle: with pagedown
summary: The pagedown package (talk slides)

authors:
- rlesur

publication_types: ["12"]


thematics: ["1"]


sources :
- "0"

update :
- "0"

languages:
- "1"


projects:
- divers

date: "2019-05-21T00:00:00Z"


tags:
- markdown
- pagedown
- css
- letter
- CV
- resume
- scientific article
- book
- template
- pages.js



url_source: 'https://r-project.ro/conference2019/presentations/Lesur2.pdf'




featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

