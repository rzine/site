---
title: knitr - Produire des documents dynamiques
summary: Communication réalisée dans le cadre d'un semin-R du MNHN

authors:
- Pascal Bessonneau 

# Type de la publication 
publication_types: ["12"]


sources :
- "0"

thematics: ["1"]


update :
- "0"




languages:
- "0"


projects:
- seminr

# Date that the page was published
date: "2013-05-24T00:00:00Z"
doi: ""

tags:
- knitr
- document
- latex
- sweave
- pdf
- HTML
- R2HTML
- xtable
- hook
- chunk
- semin-r


# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
# Name of review or book if the publication has been published in
# publication:
# publication_short:  

featured: true

image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'http://rug.mnhn.fr/semin-r/PDF/semin-R_knitr_PBessonneau_240513.pdf'
url_code: 'http://rug.mnhn.fr/semin-r/ZIP/semin-R_knitr_PBessonneau_240513_source_presentation.zip'



---


Communication réalisée dans le cadre d'un semin-R du MNHN



