---
title:  Handbook of Regression Modeling in People Analytics
subtitle: With Examples in R and Python 
summary: Online version of the book published to Chapman & Hall/CRC (later in 2021)

authors:
- Keith McNulty

publication_types: ["5"]


sources:
- "0"

thematics: ["12","13","3"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- descriptive statistics
- regression
- linear
- multiple
- model
- binomial
- logistic
- probabilistic
- multinomial
- proportional Odds
- mixed model
- python
- modeling
- people analytic


date: "2021-03-02T00:00:00Z"

url_source: 'http://peopleanalytics-regression-book.org/gitbook/'
url_code: 'https://github.com/keithmcnulty/peopleanalytics-regression-book'


links:
- name: bootstrap format
  url: http://peopleanalytics-regression-book.org/index.html

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This book is to be published by Chapman & Hall/CRC later in 2021. The online version of this book is free to read here (thanks to Chapman & Hall/CRC), and licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/) License.


