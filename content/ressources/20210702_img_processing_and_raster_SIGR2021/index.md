---
title: Image processing and all things raster
subtitle :  A workshop on image processing and raster analysis
summary: A workshop on image processing and raster analysis at thematic school SIGR2021 


authors:
- jnowosad

publication_types: ["12","13"]


sources:
- "9"

thematics: ["20","2"]


update:
- "0"

languages:
- "1"

projects:
- SIGR2021

date: "2021-07-02T00:00:00Z"

url_source: 'https://nowosad.github.io/SIGR2021/workshop2/workshop2.html#1'
url_code: 'https://github.com/Nowosad/SIGR2021/tree/master/workshop2'

tags:
- SIGR2021
- mapping
- maps
- sf
- terra
- stars
- tmap
- map algebra
- local
- focal
- zonal
- global
- raster calculator
- reclassification
- resampling
- projecting
- vector
- crop
- mask
- extraction
- rasterization
- vectorization
- autocorrelation
- predictions
- interpolation
- segmentation


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

A workshop on image processing and raster analysis at thematic school SIGR2021. 

