---
title: Mastering Software Development in R
subtitle: 
summary: This book is designed to be used in conjunction with the course sequence Mastering Software Development in R, available on Coursera

authors:
- Roger D. Peng
- Sean Kross
- Brooke Anderson

publication_types: ["5"]


thematics: ["10","11","2","5","7"]


sources :
- "0"

update :
- "1"

languages:
- "1"

projects:
- divers

date: "2017-08-15T00:00:00Z"


tags:
- theme
- grid
- package
- html
- mapping
- ggplot2
- devtools
- CRAN

url_source: 'https://bookdown.org/rdpeng/RProgDA/'
url_pdf: 'https://englianhu.files.wordpress.com/2018/10/mastering-software-development-in-r.pdf'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

This book is designed to be used in conjunction with the course sequence Mastering Software Development in R, available on Coursera. The book covers R software development for building data science tools. As the field of data science evolves, it has become clear that software development skills are essential for producing useful data science results and products. You will obtain rigorous training in the R language, including the skills for handling complex data, building R packages and developing custom data visualizations. You will learn modern software development practices to build tools that are highly reusable, modular, and suitable for use in a team-based environment or a community of developers.
