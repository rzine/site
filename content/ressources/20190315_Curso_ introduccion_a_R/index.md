---
# AUTHOR(S) 
authors:
- frebaudo

# DATE de soumission à Rzine
date: "2019-03-15"

# TITRE
title: Iniciación a R
subtitle: Curso realizado en el marco del Laboratorio Mixto Internacional BIO_INCA

# RESUME COURT - une phrase
summary: Curso de introducción a R, realizado en el marco del Laboratorio Mixto Internacional BIO_INCA

# TAGS
tags:
- Introducción
- Calculadora
- Gestion de proyecto
- Objetos
- Gráficos
- Editores
- Tipos
- Figuras
- Estadística
- Funciones
- Reportes
- Proyecto
- Datos
- Algo 

publication_types: ["12", "13"]


thematics: ["0","10","11","12","13"]





sources :
- "0"

update :
- "1"

languages:
- "2"

projects:
- divers

url_source: 'http://myrbooksp.netlify.com/myhtmls/ecuador_quito_2019/r00_links#/'
url_code: 'https://github.com/frareb/myRBook_SP/tree/master/myHtmls/Ecuador_Quito_2019'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Diapositivas utilizadas para una iniciación a la R en el marco del Laboratorio Mixto Internacional BIO_INCA. 
**Este material del curso se completa con el manual del mismo autor** : [***Aprender R***](https://myrbooksp.netlify.app/).

