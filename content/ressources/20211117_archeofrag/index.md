---
# Author(s)
authors:
- splutniak


# Date
date: "2021-11-18"


# Title
title: TSAR - Topological Study of Archaeological Refitting (JAS paper)


# Short summary
summary: Methods to analyse fragmented objects in archaeology using refitting relationships between fragments scattered in archaeological spatial units (e.g. stratigraphic layers)


# Tags
tags:
- archaeology 
- archéologie
- igraph
- network
- RGBL
- fragmented objects
- fragments
- graphs 
- fragmentation
- topology
- refitting


publication_types: ["10"]


sources:
- "0"

thematics: ["4", "18"]


update:
- "0"

languages:
- "1"


# Link
url_source: 'https://hal.archives-ouvertes.fr/hal-03419952/document'


links:
- name: HAL
  url: https://hal.archives-ouvertes.fr/hal-03419952

doi: "10.1016/j.jas.2021.105501"


# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---



#### The strength of parthood ties. Modelling spatial units and fragmented objects with the TSAR method — Topological Study of Archaeological Refitting

#### Abstract

Refitting and conjoinable pieces have long been used in archaeology to assess the consistency of discrete spatial units, such as layers, and to evaluate disturbance and post-depositional processes. The majority of current methods, despite their differences, rely on the count and proportion of refits within and between spatial units. Little attention is paid to the distribution and topology of the relationships between fragments, although this is now known to have significant effects on archaeological interpretation. This paper presents a new methodological approach for refitting studies. The TSAR approach (Topological Study of Archaeological Refitting) draws on concepts and methods from graph theory to model the network of connections observed between refitting fragments. Measures of cohesion and admixture of spatial units are defined using the structural properties of the sets of refitting relationships. To ensure reproducibility and reusability, the TSAR method is implemented as an R package, which also includes a simulator generating refitting fragments scattered in two spatial units. The advantages of the topological approach are discussed by comparing it to: (1) the results of a survey in which archaeologists were asked to rank examples of stratigraphic admixture; and (2) other computational methods. The approach is applied to simulated data, and empirical data from the Liang Abu rock shelter (East Borneo) are presented. Finally, the use of the TSAR simulation approach to test different scenarios of site formation processes is demonstrated.


