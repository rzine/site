---
# Author(s)
authors:
- Julie Lowndes
- Allison Horst


# Date
date: "2021-01-08"


# Title
title: R for Excel Users


# Short summary
summary: This course is for Excel users who want to add or integrate R and RStudio into their existing data analysis toolkit

# Tags
tags:
- excel
- rstudio
- github
- ggplot2
- dplyr
- tidyr
- filter
- join
- collaborating


publication_types: ["13", "5"]

sources:
- "0"

thematics: ["0", "10", "11", "1", "7", "8"]

update:
- "0"

languages:
- "1"


# Link
url_source: 'https://rstudio-conf-2020.github.io/r-for-excel/'
url_code: 'https://github.com/rstudio-conf-2020/r-for-excel'


links:
- name: slide
  url: https://docs.google.com/presentation/d/1w9cVzEsdbyZ6vay4UnoTvTWd9t-AO_x06HoVzgAiSFE/edit#slide=id.g7ca34c2539_2_0
- name: blog
  url: https://education.rstudio.com/blog/2020/02/conf20-r-excel



# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify

---

This course is for Excel users who want to add or integrate R and RStudio into their existing data analysis toolkit. It is a friendly intro to becoming a modern R user, full of tidyverse, RMarkdown, GitHub, collaboration & reproducibility.

This book is written to be used as a reference, to teach, or as self-paced learning. And also, awesomely, it’s created with the same tools and practices we will be talking about: R and RStudio — specifically [bookdown](https://bookdown.org/yihui/bookdown/) — and GitHub. It is being fine-tuned but the most recent version is always available:

This book: [https://rstudio-conf-2020.github.io/r-for-excel/](https://rstudio-conf-2020.github.io/r-for-excel/)    
Book GitHub repo: [https://github.com/rstudio-conf-2020/r-for-excel](https://github.com/rstudio-conf-2020/r-for-excel)    
Accompanying slides: [Google Slides](https://docs.google.com/presentation/d/1w9cVzEsdbyZ6vay4UnoTvTWd9t-AO_x06HoVzgAiSFE/edit#slide=id.g7ca34c2539_2_0)    
Blog: [https://education.rstudio.com/blog/2020/02/conf20-r-excel/](https://education.rstudio.com/blog/2020/02/conf20-r-excel/)    




