---
title: R weekly
summary: Site web collaboratif permettant l'ajout de contenu sur R (articles, blogs, tutoriels...).

authors:
- Batool Almarzouq
- Colin Fay
- Eric Nantz
- Jon Calder
- Jonathan Carroll
- Kelly N. Bodwin
- Miles McBain
- Robert Hickman
- Ryo Nakagawara
- Tony ElHabr
- Wolfram Qin

# Type de la publication 
publication_types: ["0","8"]


sources :
- "0"

thematics: ["8"]


update :
- "1"

languages:
- "1"


projects:
- divers

date: "2016-05-21T00:00:00Z"
doi: ""
 
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://rweekly.org/'
utl_code: 'https://github.com/rweekly/rweekly.org'
---

R-Weekly a été fondé le 20 mai 2016. R se développe très rapidement et de nombreux blogs, tutoriels et autres formats de ressources sortent chaque jour. R Weekly offre la possibilité de garder une trace de ces différentes productions dans la communauté R et tente de les rendre accessible à tous.



