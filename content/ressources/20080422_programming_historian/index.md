---
date: "2008-04-22T00:00:00Z"

title: The programming historian
summary: PH propose des tutoriels conviviaux, évalués par des pairs, qui aident les humanistes à apprendre un large éventail d'outils numériques.

tags: 
- project
- projet
- collection

authors:
- William J. Turkel
- Alan MacEachern

publication_types: ["7", "8", "10"]

thematics: ["10", "17", "18", "8", "9", "2"]

projects:
- programming_historian

sources:
- "10"

update:
- "1"

languages: ["1", "0"]

url_source: 'https://programminghistorian.org/'
url_code: 'https://github.com/programminghistorian/jekyll'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---



The programming historian publie des tutoriels évalués par des pairs qui permettent l'initiation à et l'apprentissage d'un large éventail d'outils numériques, de techniques et de flux de travail pour faciliter la recherche et l'enseignement en sciences humaines et sociales. Ce projet tente de créer une communauté internationale, diversifiée et inclusive de rédacteur(trice)s, d'auteur(e)s et de lecteur(trice)s. 

Pour en savoir plus et retrouver l'ensemble des leçons (**en 4 langues**), rendez-vous sur le site web : [https://programminghistorian.org/](https://programminghistorian.org/)
