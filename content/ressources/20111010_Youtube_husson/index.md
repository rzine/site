---
title: Chaîne YouTube de François Husson
summary: De nombreux tutoriels sur la statistique avec R 

authors:
- fhusson

publication_types: ["9"]


thematics: ["10","11","12","13"]



sources :
- "0"

update :
- "1"

languages:
- "0"

projects:
- divers
date: "2011-10-10T00:00:00Z"
doi: ""

tags:
- FactoMiner
- Factoshiny
- FactoInvestigate
- MissMDA
- PCA
- ACP
- MFA
- clustering
- RcmdR
- SensoMineR
- statistique


url_source: 'https://www.youtube.com/c/HussonFrancois/featured'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---


Chaîne Youtube de [François Husson](https://husson.github.io/), développeur de nombreux packages de référence en statistique ([FactoMineR](http://factominer.free.fr/), missMDA, Factoshiny, SensoMineR...) 

