---
title: R Reference Card 2.0
summary: From ***R for Beginners*** by permission of Emmanuel Paradis

authors:
- Matt Baggott

# Type de la publication 
publication_types: ["0"]


sources :
- "0"

thematics: ["0","10","11","12"]


update :
- "0"




languages:
- "1"

tags:
- base
- operator
- package
- indexing
- input
- output
- conversion
- selection
- manipulation
- creation
- reshaping
- distribution
- strings
- test
- statistics
- correlation
- variance
- plot
- date
- time
- graphics
- lattice
- function

projects:
- divers


# Date that the page was published
date: "2012-12-24T00:00:00Z"
doi: ""

# Date that the page was published. 
# You only need to specify this option if you wish to set date in the future but publish the page now, as is the case for publishing a journal article that is to appear in a journal etc.
# Name of review or book if the publication has been published in
# publication:
# publication_short:  

featured: true

image:
  caption: ""
  focal_point: "Center"
  preview_only: false
  

url_source: 'https://cran.r-project.org/doc/contrib/Baggott-refcard-v2.pdf'


---


Material from [*R for Beginners*](https://cran.r-project.org/doc/contrib/Paradis-rdebuts_fr.pdf) by permission of Emmanuel Paradis



