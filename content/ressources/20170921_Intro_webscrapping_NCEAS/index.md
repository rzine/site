---
title: Introduction to Web Scraping
subtitle: Learn how to get data from the World Wide Web using R
summary: Learn how to get data from the World Wide Web using R


authors:
- Julien Brun

publication_types: ["13"]


thematics: ["6"]





projects:
- divers

sources:
- "0"

update:
- "0"

languages:
- "1"

date: "2017-09-21T00:00:00Z"
doi: ""

tags:
- Rcurl
- libcurl
- httr
- rvest
- Rselenium
- utils
- HTTP
- HTML
- XML
- XPath




url_source: 'https://nceas.github.io/oss-lessons/data-liberation/intro-webscraping.html'
url_code: 'https://github.com/NCEAS/oss-lessons/tree/gh-pages/data-liberation'



# ###### NE PAS MODIFIER #####
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

**Course material used in a training course** ([**OSS 2017**](https://nceas.github.io/oss-2017/)) offered by the [**National Center for Ecological Analysis and Synthesis**](https://www.nceas.ucsb.edu/)
