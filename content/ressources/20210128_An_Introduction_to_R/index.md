---
title: An Introduction to R
summary: The aim of this book is to introduce you to using R, a powerful and flexible interactive environment for statistical computing and research


authors:
- Alex Douglas
- Deon Roos
- Francesca Mancini
- Ana Couto
- David Lusseau

publication_types: ["5","13"]


sources:
- "0"

thematics: ["0","1","10","11","12","13","7"]


update:
- "1"

languages:
- "1"

projects:
- divers

tags:
- Rstudio
- basics
- graphics
- markdow
- git


date: "2021-01-28T00:00:00Z"

url_source: 'https://intro2r.com/'
url_code: 'https://github.com/alexd106/Rbook'
url_pdf: 'https://intro2r.com/Rbook.pdf'

links:
- name: Course material
  url: https://alexd106.github.io/intro2R/index.html


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


The aim of this book is to introduce you to using R, a powerful and flexible interactive environment for statistical computing and research. R in itself is not difficult to learn, but as with learning any new language (spoken or computer) the initial learning curve can be a little steep and somewhat daunting. We have tried to simplify the content of this book as much as possible and have based it on our own personal experience of teaching (and learning) R over the last 15 years. It is not intended to cover everything there is to know about R - that would be an impossible task. Neither is it intended to be an introductory statistics course, although you will be using some simple statistics to highlight some of R’s capabilities. The main aim of this book is to help you climb the initial learning curve and provide you with the basic skills and experience (and confidence!) to enable you to further your experience in using R.





