---
title: R Installation Guide
summary: This resource provides step-by-step descriptions on how to install and run RStudio for *Spatial Modelling for Data Scientists* from your own computer

authors:
- Francisco Rowe
- Dani Arribas-Bel

publication_types: ["5"]


sources:
- "0"

thematics: ["0","7"]


update:
- "0"

languages:
- "1"

projects:
- divers

date: "2021-02-09T00:00:00Z"

tags:
- windows
- linux
- Mac
- MacOS
- Rstudio

url_source: 'https://gdsl-ul.github.io/r_install/'
url_code: 'https://github.com/GDSL-UL/r_install'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

This resource provides step-by-step descriptions on how to install and run RStudio for *Spatial Modelling for Data Scientists* from your own computer.



