---
# AUTHOR(S) 
authors:
- Yihui Xie

# DATE de soumission à Rzine
date: "2018-02-19"

# TITRE
title: knitr

subtitle: Elegant, flexible, and fast dynamic report generation with R
summary: Elegant, flexible, and fast dynamic report generation with R

# TAGS
tags:
- rmarkdown
- knitr
- chunk
- hook
- literate-programming 
- sweave 
- dynamic-documents 


# TYPE DE PUBLICATION
publication_types: ["6"]


thematics: ["1"]


languages:
- "1"




sources:
- "0"

update:
- "1"


projects:
- divers


url_source: 'https://yihui.org/knitr/'
url_code: 'https://github.com/yihui/knitr'

# publishDate: "2014-10-28T00:00:00Z"
# publication: Advanced R
# publication_short:  Advanced R
  
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

The **knitr package** was designed to be a transparent engine for dynamic report generation with R, solve some long-standing problems in Sweave, and combine features in other add-on packages into one package (**knitr** ≈ Sweave + cacheSweave + pgfSweave + weaver + `animation::saveLatex `+ `R2HTML::RweaveHTML` + `highlight::HighlightWeaveLatex` + 0.2 * brew + 0.1 * SweaveListingUtils + more).

- Transparency means that the user has full access to every piece of the input and output, e.g., `1 + 2` produces [1] `3` in an R terminal, and **knitr** can let the user decide whether to put `1 + 2` between `\begin{verbatim}` and `\end{verbatim}`, or `<div class="rsource">` and `</div>`, and put [1] `3` in `\begin{Routput}` and `\end{Routput}`; see the hooks page for details   
- **knitr** tries to be consistent with users’ expectations by running R code as if it were pasted in an R terminal, e.g., `qplot(x, y)` directly produces the plot (no need to `print()` it), and all the plots in a code chunk will be written to the output by default   
- Packages like **pgfSweave** and **cacheSweave** have added useful features to Sweave (high-quality tikz graphics and cache), and **knitr** has simplified the implementations   
- The design of **knitr** allows any input languages (e.g. R, Python and awk) and any output markup languages (e.g. LaTeX, HTML, Markdown, AsciiDoc, and reStructuredText)   

This package is developed on GitHub; for installation instructions and FAQ’s, see README. This website serves as the full documentation of **knitr**, and you can find the main manual, the graphics manual and other demos / examples here. For a more organized reference, see the knitr book.





