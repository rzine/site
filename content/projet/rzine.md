+++
title = "Le projet Rzine"
subtitle = "Encourager la production et favoriser la diffusion de documentation sur la pratique de R en SHS"
date = 2024-03-01
widget = "presentation"
weight = 10 # Order that this section will appear.
headless = true  # This file represents a page section.
active = false  # Activate this widget? true/false

hero_media = "Rzine_logo.png"
hero_media2 = "rzine_logo_bis.png"
+++


**Rzine est un projet de la <a href="http://cist.cnrs.fr/" target="_blank">Fédération de Recherche CIST</a> qui réunit une vingtaine d'unités et plusieurs disciplines autour des sciences territoriales. Son objectif est de contribuer à l'apprentissage et à la pratique de R en SHS, en favorisant le partage, la production et la diffusion de ressources**. Ce projet collaboratif et interdisciplinaire s'étend au-delà des équipes partenaires de la FR CIST et tente de répondre à un besoin commun à de nombreuses unités de recherche de l'INSHS.

##### R, un outil polyvalent et interdisciplinaire au service de la science ouverte et reproductible
C'est en 1992 que Robert Gentleman et Ross Ihaka de l'université d'Auckland se lancent dans le projet de développement du langage de programmation pour l'analyse de données R. Ils cherchent alors à développer un environnement pour le calcul scientifique plus performant que l'existant. Trente ans plus tard, R s'est imposé comme une référence en matière d'analyse de données. Sa communauté de développeur·euse et d'utilisateur·rice·s connaît une croissance impressionnante depuis plusieurs années. D'abord adopté par les sciences dures, R s'est peu à peu imposé comme une référence en sciences humaines et sociales à partir des années 2010. Aujourd'hui, la pratique de ce langage est enseignée à l'université dans de nombreuses disciplines (sociologie, économie, géographie, démographie, archéologie…) et son utilisation dépasse le seul domaine de la recherche.

R est gratuit, open-source et fonctionne sur les principaux systèmes d'exploitation. À la différence d'un logiciel « clic-bouton », chaque opération est facilement archivable, partageable et reproductible. Cet outil permet ainsi de s'inscrire pleinement dans le courant de la science ouverte et de la recherche reproductible, soutenu par le CNRS. Grâce à sa structure modulaire, le potentiel de développement de ce langage est immense car chaque utilisateur·rice peut mettre à disposition de tou·te·s des extensions supplémentaires appelées packages. Ainsi, l'ensemble des méthodes d'analyse et de représentation des données utilisées par les différentes disciplines de SHS sont exécutables en langage R.



<figure class="center">
  <img src="/site/img/figure_1.png" width="90%">
  
  <figcaption style="font-size:14px;">L'évolution du nombre de packages (extensions) mis à disposition au cours du temps traduit la popularité croissante du langage R.</figcaption>

</figure>



L'inscription du langage R comme un des projets du mouvement du logiciel libre, ainsi que le soutien financier d'acteurs majeurs du secteur privé assurent son développement, sa fiabilité et sa popularité à long terme. C'est dans ce contexte qu'un groupe d'ingénieur·e·s et de chercheur·euse·s a initié le projet Rzine pour contribuer au développement de la pratique de ce langage pour l'analyse de données, fédératrice pour les disciplines de sciences humaines et sociales.


##### Rzine.fr : favoriser la diffusion et le partage de connaissances sur la pratique de R en SHS
L'étude de la pratique de R et de sa communauté d'utilisateur·rice·s dans le monde francophone a permis de dresser un bilan positif en matière de ressources disponibles et de pluridisciplinarité. Si la communauté R francophone est indéniablement active et productive, plusieurs axes d'amélioration ont pu être recensés :   

- l'accessibilité aux ressources n'est pas toujours aisée pour un·e débutant·e;   
- la diffusion de ressources se limite parfois à un groupe d'utilisateur·rice·s, une structure ou une discipline;   
- deux grands types de ressources s'opposent: la documentation généraliste, orientée vers la technique et la documentation spécialisée dans un domaine, peu accessible et transposable pour les autres.   

Le site collaboratif rzine.fr a été développé à partir de ce constat. Il référence quatre grands types de contenus :

- des <a href="https://rzine.fr/publication/" target="_blank">ressources</a> en libre accès,   
- des <a href="https://rzine.fr/communaute/#projects" target="_blank">projets</a> qui favorisent la pratique de R,   
- des <a href="https://rzine.fr/communaute/#people" target="_blank">auteur·rice·s</a> de documentation ou de formations,  
- des <a href="https://rzine.fr/communaute/#posts" target="_blank">actualités</a> pour la communauté d'utilisateur·rice·s.  


**Rzine.fr s'efforce de dessiner les contours de la communauté R en SHS. Il donne aux débutant·e·s un accès simple à l'information et orienté vers une utilisation autonome. Il offre aux utilisateur·rice·s de niveau intermédiaire la possibilité d'étendre leurs pratiques et de s'ouvrir à d'autres méthodes parfois issues d'autres disciplines. Enfin, il s'agit d'un espace de diffusion pour les utilisateur·rice·s avancé·e·s, auteur·rice·s de développements ou de documentation.**

Rzine.fr est un <a href="https://bookdown.org/yihui/blogdown/hugo.html" target="_blank">blogdown</a> construit à partir du template <a href="https://themes.gohugo.io/themes/hugo-academic/" target="_blank">Hugo Academic</a>. Il s'agit d'un site statique sans réelle base de données : 
chaque publication, chaque auteur·rice, chaque actualité et chaque projet se formalise par un fichier markdown contenant des métadonnées (titre, date, résumé, auteur·rice...)



<figure class="center">
  <img src="/site/img/How_works_Rzine.png" width="100%">
  
  <figcaption style="font-size:14px;">Rzine.fr est un site statique Hugo qui permet le référencement de quatre types de contenus.</figcaption>

</figure>


Ce site repose ainsi sur des technologies qui facilitent son administration décentralisée et collaborative. À terme, une plus grande ouverture aux contributions externes est envisagée. L'un des futurs chantiers du projet sera la simplification et l'automatisation de la soumission de contenus.



#### La collection Rzine : valoriser et transmettre ses méthodes de travail avec R

Le projet Rzine a également pour objectif de contribuer à la science ouverte et à la pratique de la recherche reproductible. Pour cela, une <a href="https://rzine.fr/collection/#collection" target="_blank">collection de publications</a> open-source, ouverte à tou·te·s et soumise à une évaluation transparente par les paires vient d'être créée. L'objectif est d'offrir un espace de publication permettant le partage d'une méthode ou d'un type d'analyse reproductible, dans un périmètre pluridisciplinaire. Il ne s'agit pas d'articles scientifiques mais de supports méthodologiques ancrés dans une thématique, qui présentent du code de programmation de manière intelligible pour les différentes disciplines de SHS. Chaque publication fait l'objet d'une relecture thématique et technique, qui assure sa pérennité et son intérêt pour les différentes disciplines. Les publications acceptées sont éditées en format HTML, associées à un <a href="https://www.doi.org/" target="_blank">DOI</a> et référencées sur <a href="https://hal.archives-ouvertes.fr/RZINE" target="_blank">HAL</a>. Leur stockage et diffusion sont assurés par le site rzine.fr.

<figure class="center">
  <img src="/site/img/figure_3.png" width="100%">
  
  <figcaption style="font-size:14px;">Impression d'écran de la <a href="https://rzine.fr/docs/20191125_ironcurtain/index.html" target="_blank">première publication Rzine (Lambert, 2021)</a>, qui porte sur la réalisation d'une carte publiée plusieurs fois, et maintenant reproductible.</figcaption>

</figure>



##### Des publications au format *notebook*
Le choix du *notebook* comme format de publication de ces articles méthodologiques est inhérent au contenu qu'ils présentent. Parfois nommés « calepins électroniques » en français, les *notebooks* sont très utilisés en science des données, que ce soit pour la production de documentation, l'enseignement ou le travail collaboratif. Ils permettent de combiner des sections en langage naturel et des sections en langage informatique, et peuvent s'enregistrer sous différents formats. Le *notebook* permet ainsi d'appliquer le paradigme de la <a href="https://fr.wikipedia.org/wiki/Programmation_lettr%C3%A9e" target="_blank">programmation lettrée</a> préconisé par Donald Knuth dans les années 1970, qui est au cœur du concept de la collection Rzine.


<figure class="center">
  <img src="/site/img/figure_4.png" width="50%">
  
  <figcaption style="font-size:14px;">En permettant la combinaison de texte, de code et de ses résultats, le « notebook » permet de faire de la programmation lettrée.</figcaption>

</figure>






À l'image des travaux et de la coopération entre la <a href="https://www.huma-num.fr/" target="_blank">TGIR Huma-Num</a> et le <a href="https://www.crihn.org/" target="_blank">Centre de recherche interuniversitaire sur les humanités numériques</a> - CRIHN (université de Montréal), les humanités numériques s'intéressent particulièrement à l'écriture numérique et les initiatives d'innovation en matière de format de publication apparaissent peu à peu. La revue <a href="https://programminghistorian.org/" target="_blank">Programming Historian</a>, lancée lors de la <a href="https://fr.wikipedia.org/wiki/Digital_Humanities_conference" target="_blank">Digital Humanities conference</a> de 2012, fait partie des succès qui placent le *notebook* parmi les formats de publication de référence. Permettant l'intégration de données et de leur traitement à l'écriture scientifique, ce format semble idéal pour répondre aux objectifs de la collection Rzine.


<figure class="center">
  <img src="/site/img/figure_5.png" width="100%">
  
  <figcaption style="font-size:14px;">Impression d'écran d'un <a href="https://rzine.fr/docs/20200601_mletexier86_explo_spatiotemporel/index.html" target="_blank">« notebook » Rzine (Le Texier, 2021)</a>. Le texte, le code et son résultat se succèdent, dans une mise en page soignée facilitant la lecture.</figcaption>

</figure>


Bien que la pratique de la programmation lettrée avec R soit accessible pour un·e novice, l'utilisation d'un logiciel de gestion de versions décentralisé (Git) sur lequel repose l'ensemble du processus d'évaluation et de publication de la collection Rzine demande des connaissances plus approfondies. C'est pourquoi Rzine contribuera, par des formations et l'encadrement des auteur·rice·s de publication, à la montée en compétence collective et participative autour de la programmation lettrée et des systèmes de versionnage. Au-delà de sa vocation à offrir un espace de publication innovant, ce projet participe, à son échelle, au développement des humanités numériques en sciences humaines et sociales.

L'animation et l'évolution du projet Rzine repose aujourd'hui sur l'implication personnelle d'un petit groupe de personnes issues de différentes unités de recherche. Son avenir dépend de la communauté d'utilisateur·rices R en sciences humaines, sociales et territoriales qu'il réussira à rassembler et à impliquer dans cette aventure...





