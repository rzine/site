---
date: "2020-01-10T00:00:00Z"

title: Afrimapr project
subtitle: R building blocks for mapping
summary: Creating R building blocks and learning resources for making data-driven maps in Africa.


authors:
- Andy South
- Anelda van der Walt
- Laurie Baker
- Martyna Bogacz
- Mark Herringer
- Anne Treasure
- Paula Moraga
- Clinton Nkolokosa
- Julie-Anne Tangena 
- Thomas Mboa
- Margareth Gfrerer 
- rlovelace
- Ahmadou Dicko


tags: 
- fundation
- health
- africa
- spatial
- mappping
- projet

publication_types: ["8"]


sources:
- "1"

thematics: ["2"]


update:
- "1"

languages:
- "1"

sources: ["1"]

date: "2020-01-10T03:00:00Z"

url_source: 'https://afrimapr.github.io/afrimapr.website/'
url_code: 'https://github.com/afrimapr'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


afrimapr was funded through Wellcome Open Research and Wellcome Data for Science and Health between 2020 - 2021. The project is no longer actively being supported and the website and resources will no longer be updated. However, we will keep the website up for as long as possible and the GitHub repositories and Youtube channel will stay accessible. We hope you will find the news, information, and resources useful

