---
date: "2018-09-25T00:00:00Z"


title: R Toulouse

summary: Groupe d'utilisateur.rice.s R de Toulouse

tags: 
- séminaire
- formation
- conférence
- évènement
- groupe


image:
  caption: 
  focal_point: Smart  
---


R Toulouse est un Groupe d’utilisatrices et d’utilisateurs de R à Toulouse. Il est ouvert à tou·te·s, quelques soit le niveau, le domaine, le statut.

Site web : [https://r-toulouse.netlify.app/](https://r-toulouse.netlify.app/)     
Twitter : [@RUG_Toulouse](https://twitter.com/RUG_Toulouse)
