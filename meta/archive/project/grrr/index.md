---
date: "2018-09-24T00:00:00Z"

title: Grrr

summary: Groupe Slack (plateforme de discussion instantanée) francophone dédié aux échanges et à l’entraide autour de R. 

tags: 
- forum
- slack



image:
  caption: 
  focal_point: Smart  
---



[**Grrr**](https://app.slack.com/client/T9ML8RLMP) (“pour quand votre R fait Grrr”) est un groupe [**Slack**](https://r-grrr.slack.com) (plateforme de discussion instantanée) francophone **dédié aux échanges et à l’entraide autour de R**. Il est ouvert à tou.te.s et se veut accessible aux débutants. Vous pouvez même utiliser un pseudonyme si vous préférez.

Pour rejoindre la plateforme discussion, cliquez sur [ce lien](https://join.slack.com/t/r-grrr/shared_invite/zt-46utbgb9-uvo_bg5cbuxOV~H10YUX8w)



