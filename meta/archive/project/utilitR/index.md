---
date: "2021-05-20T00:00:00Z"

title: UtilitR

summary: Documentation collaborative sur l’usage de R, par l'INSEE

tags: 
- INSEE
- communauté
- réseau
- projet
- évènement

image:
  caption: 
  focal_point: Smart  
---

Le projet `utilitR` **vise à produire une documentation collaborative et open source sur** `R`, destinée en premier lieu aux agents de l’Insee. Ce projet est parti du constat qu’il est difficile d’apprendre à utiliser R pour de multiples raisons : multiplicité de packages faisant plus ou moins la même chose, abondance et éclatement de la documentation (souvent en anglais), difficultés supplémentaires pour effectuer des choix éclairés et adaptés à l’environnement informatique de travail…

Les contributeurs du projet `utilitR` se sont fixés pour objectif de rassembler dans un seul document tous les éléments utiles pour l’usage de R à l’Insee, en cherchant à couvrir la plupart des cas d’usage courants. Cette documentation a donc été élaborée en tenant compte du contexte de travail propre à l’Institut. Cette documentation peut évidemment contenir des éléments pertinents pour un usage de `R `en dehors de l’Insee, mais ce n’est pas sa finalité première.

Pour plus d'informations et découvrir la documentation, rendez-vous sur le [site web UtilitR](https://www.utilitr.org/).
