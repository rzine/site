---
date: "2020-06-28T00:00:00Z"
# authors: 
# - bgarnier
# - tgiraud

title: R à l'Usage des Sciences Sociales

summary: RUSS propose une séance tous les deux mois sur la pratique de R en sciences sociales

tags: 
- ined
- russ
- ehess
- séminaire
- évènement



# external_link: ""

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
links:
- name: Contact
  url: 'mailto:garnier@ined.fr'
  
# ##########################  
image:
  caption: 
  focal_point: Smart  
---



**L’objectif de ce séminaire est d’échanger autour du logiciel de statistique libre, gratuit et multiplateforme R.**

Il s’adresse aux praticiens impliqués dans le traitement quantitatif des données en sciences humaines et sociales (utilisateurs de données, chercheurs, ingénieurs ou étudiants) qu’ils aient ou non déjà utilisé le logiciel R.

Chaque séance est organisée autour de la présentation d’une expérience de traitement de données avec le logiciel (fonction spécifique et/ou packages). Le cadre de ces réunions est informel et les participants doivent se sentir libres d’intervenir afin de confronter leurs expériences.


## Infos pratiques

RUSS organise un séance tous les 2 mois sur le [Campus Condorcet](https://www.campus-condorcet.fr/).  
Pour s'inscrire à la liste de diffusion : [https://listes.ined.fr/subscribe/seminaire-russ@ined.fr](https://listes.ined.fr/subscribe/seminaire-russ@ined.fr)   
**Pour plus d'informations** : [***https://russ.site.ined.fr/***](https://russ.site.ined.fr/)


