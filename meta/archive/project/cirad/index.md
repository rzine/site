---
date: "2017-03-29T00:00:00Z"

title: Forum CIRAD

summary: Forum francophone d'échange autour du logiciel de calcul statistique R

tags: 
- forum
- Bayésien
- Bioinformatique
- Calcul distribué
- Graphisme et visualisation
- Optimisation
- Programmation
- Spatial
- Statistique


image:
  caption: 
  focal_point: Smart  
---


Ce site se présente comme **Un forum francophone d'échange autour du logiciel de calcul statistique R** dans le domaine de la recherche agronomique tropicale

Il constitue l'un des sites de **Groupe des utilisateurs du logiciel R** (RUGs) français.

Il permet de poser des questions (et d'avoir des réponses) au travers d'un forum très actif et de trouver des annonces, des fiches, des packages et fonctions.

Pour y accéder : [http://forums.cirad.fr/logiciel-R/index.php](http://forums.cirad.fr/logiciel-R/index.php)

