---
date: "2017-10-02T00:00:00Z"


title: R in Grenoble

summary: R user group in Grenoble


tags: 
- séminaire
- formation
- conférence
- évènement
- groupe


image:
  caption: 
  focal_point: Smart  
---


*R in Grenoble* est un Groupe d’utilisateur.trice.s Grenoblois (non-french speakers are all welcome).

Site web : [https://r-in-grenoble.github.io/index.html](https://r-in-grenoble.github.io/index.html)     



