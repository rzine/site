---
date: "2017-05-10T00:00:00Z"

title: R4ciam

summary: Groupe de travail de l'INRA


tags: 
- réseau
- groupe
- INRA


image:
  caption: 
  focal_point: Smart  
---


R4ciam est un groupe de travail de l'INRA dont l'activité s'inscrit dans le cadre du **CATI CaSciSDI** (pour Calcul Scientifique, Statistiques et Développements Informatiques). Un cati est une organisation qui regroupe des personnes exerçant un métier de l'informatique.

Le site de R4ciam a pour objectif d'aider la communauté d'utilisateurs du langage et environnement de calcul statistique R à obtenir plus facilement de l'information et de l'aide sur ce logiciel et à mutualiser les connaissances et pratiques quant à son utilisation dans toute sa diversité, de l'administration à la mise en oeuvre ou l'implémentation de méthodes mathématiques et statistiques.

Retrouvez des ressources référencées, des fiches techniques, des tutoriels et une FAQ sur le **site de R4ciam** : [https://informatique-mia.inrae.fr/r4ciam/](https://informatique-mia.inrae.fr/r4ciam/)