---
date: "2022-07-16T00:00:00Z"


title: R-Ladies Paris

summary: Groupe d'utilisateurices R de Paris

tags: 
- séminaire
- formation
- conférence
- évènement
- groupe


image:
  caption: 
  focal_point: Smart  
---


R-Ladies Paris est un réseau d'utilisatrices de R à Paris. Ce groupe est une section locale de R-Ladies Global, une communauté mondiale soutenant la non discrimination liée au genre parmi les utilisatrices de R.

Son objectif est de soutenir et supporter les utilisateurs de R qui sont sous-représentés et d'encourager l'apprentissage et la collaboration parmi les membres du réseau. Rladies Paris est ouvert à toute personne intéressée par la programmation en R, experte ou novice.

Pour rejoindre le groupe : [https://www.meetup.com/rladies-paris/](https://www.meetup.com/rladies-paris/)   
Chaîne Youtube : [https://www.youtube.com/@rladiesparis/featured](https://www.youtube.com/@rladiesparis/featured)   
Twitter : [https://twitter.com/RLadiesParis](https://twitter.com/RLadiesParis)   
GitHub : [https://github.com/R-Ladies-Paris](https://github.com/R-Ladies-Paris)   
