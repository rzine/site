---
date: "2019-04-22T00:00:00Z"

title: The programming historian
summary: PH propose des tutoriels conviviaux, évalués par des pairs, qui aident les humanistes à apprendre un large éventail d'outils numériques.

tags: 
- projet
- collection

image:
  caption: 
  focal_point: Smart  
---


The programming historian publie des tutoriels évalués par des pairs qui permettent l'initiation à et l'apprentissage d'un large éventail d'outils numériques, de techniques et de flux de travail pour faciliter la recherche et l'enseignement en sciences humaines et sociales. Ce projet tente de créer une communauté internationale, diversifiée et inclusive de rédacteur(trice)s, d'auteur(e)s et de lecteur(trice)s. 

Pour en savoir plus et retrouver l'ensemble des leçons (en 4 langues), rendez-vous sur le site web : [https://programminghistorian.org/](https://programminghistorian.org/)
