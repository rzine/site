---
date: "2021-07-02T00:00:00Z"

title: École thématique SIGR2021 
summary: École thématique CNRS "Sciences de l’information géographique reproductibles"


tags: 
- CNRS
- reproductible
- information géographique
- SIG
- RIATE
- sf
- raster
- terra
- stars
- télédétection
- analyse spatiale
- cartographie
- rmarkdown
- xarigan
- git
- évènement


image:
  caption: 
  focal_point: Smart  
---


L'école thématique SIGR2012 s'est déroulée début juillet 2021 à Saint-Pierre d’Oléron.

## Enjeux et objectifs

Les pratiques d’open science ont depuis plusieurs années gagné le domaine des sciences de l’information géographique. Les principaux programmes de recherche français et européens accordent une importance grandissante à l’ouverture, au partage des données et à la reproductibilité de la recherche. En géographie, le développement des méthodes d’analyses reproductibles a connu un fort essor qui se matérialise par le développement d’outils de traitements de l’information géographique et la pratique croissante d’outils de partage et de versionnement des protocoles d’analyse.

**L’école thématique vise à répondre aux problématiques de production, publication, diffusion ou valorisation de traitements de données géographiques dans une démarche de recherche reproductible.**

**En identifiant les principales avancées conceptuelles, méthodologiques et techniques du domaine via un focus sur les méthodes de traitement de l’information géographique, cette école a permis aux participants de s’initier à la mise en œuvre de protocoles de recherche ouverts et transparents avec le logiciel libre R.**

## Modalités pédagogiques

L’école thématique s’est articulée autour de cours magistraux, de séances de travail avec le logiciel R.

## Supports pédagogiques

**Vous pouvez retrouver l'ensemble des supports pédagogiques utilisés durant l'école sur le** [**site web dédié**](https://sigr2021.github.io/site/index.html)



