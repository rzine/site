---
date: "1978-06-06T00:00:00Z"
# authors: 
# - admin

title: O'Reilly Media

summary: Maison d'édition américaine spécialisée dans l'informatique

tags:
- collection
- édition

image:
  caption: 
  focal_point: Smart
---

O'Reilly Media (anciennement O'Reilly & Associates) est une maison d'édition américaine, fondée par Tim O'Reilly en 1978, et dont l'activité principale est la publication de livres concernant l'informatique. 

**Seules les publications en libre accès sont référencées sur Rzine**.
