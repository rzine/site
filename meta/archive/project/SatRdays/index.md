---
date: "2016-09-17T00:00:00Z"

title: SatRdays

summary: Conférences régionales pour soutenir la collaboration, la mise en réseau et l'innovation au sein de la communauté R

tags: 
- conference
- community
- évènement


image:
  caption: 
  focal_point: Smart  
---


**Les SatRDays sont des conférences régionales** soutenues par le [R consortium](https://www.r-consortium.org/) et organisées par la communauté pour soutenir la collaboration, la mise en réseau et l'innovation au sein de la communauté de la recherche

Plus d'informations : [https://satrdays.org/](https://satrdays.org/)


