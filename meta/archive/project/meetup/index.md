---
date: "2016-10-10T00:00:00Z"


title: Meetup R Nantes

summary: Groupe et rencontres d'utilisateurs R (Nantes)

tags: 
- séminaire
- formation
- conférence
- évènement


image:
  caption: 
  focal_point: Smart  
---


Ce groupe d'utilisateurs se rencontre régulièrement afin de partager leurs expériences avec le langage R et d'échanger à propos de tous ses possibles usages : **analyses statistiques**, **machine learning**, **projets big data**, **visualisation de données**, et bien plus encore !


Pour rejoindre le groupe ou suivre son activité : [https://www.meetup.com/fr-FR/Meetup-R-Nantes/](https://www.meetup.com/fr-FR/Meetup-R-Nantes/)


Pour consulter les documents relatifs aux différentes rencontres du "Meetup R Nantes" : [https://github.com/DACTA/MEETUP_R_NANTES](https://github.com/DACTA/MEETUP_R_NANTES)


Compte Twitter : [**@meetup_r_nantes**](https://twitter.com/meetup_r_nantes) .
