---
date: "2018-09-24T00:00:00Z"

title: École d’Été 2018 du LabEx DynamiTe

summary: Massive spatial data - challenges in acquisition, treatment and use for territories (ateliers R)

tags: 
- école
- évènement
- LabEx
- Dynamite
- formation




image:
  caption: 
  focal_point: Smart  
---



L’École d’Été 2018 du Laboratoire d’Excellence « Dynamiques Territoriales et Spatiales », co-organisée avec le LabEx Futurs Urbains, s’est déroulée
du lundi 24 au vendredi 28 septembre à la Villa Finaly de Florence (Italie).

Elle avait pour thème : « **Massive spatial data: challenges in acquisition, treatment and use for territories** »

Cette École d’Été était dédiée aux sources de données spatiales émergentes et hétérogènes. Son ambition était de proposer à ses participant-e-s :

I) un tour d’horizon de la façon dont ces données peuvent être utilisées pour **nourrir une recherche sur des dynamiques territoriales** ;

II) d’apprendre des savoir-faire (méthodes et outils) contemporains pour traiter ces données.

L’école etait destinée aux scientifiques ayant des compétences en programmation de niveau intermédiaire et familiers avec les méthodes quantitatives, et principalement aux jeunes chercheurs (étudiants en master / doctorants et postdoctorants). La semaine sera organisée comme suit :

Chaque matinée, un-e chercheur-e reconnu-e dans son domaine d’expertise présentera, lors d’une conférence plénière, les avancées permises par les nouvelles sources de données massives, les questions et problèmes soulevés, les méthodes de traitement, certains résultats importants, en plus des propres résultats de recherche du / de la conférencier/ère. Les cinq thématiques choisies pour les cinq matinées sont : **ville intelligente, mobilités et transport, géosciences et questions environnementales, réseaux sociaux et espace, enjeux politiques et citoyens du big data**.

Conférenciers et conférencières :

- **Marc BARTHELEMY**, Institute for Theoretical Physics (IPhT) – France
- **Marta C. GONZALEZ**, University of California, Berkeley – États-Unis
- **Catherine MORENCY**, Polytechnique Montréal – Canada
- **Remko UIJLENHOET**, Wageningen University – Pays-Bas
- **Taylor SHELTON**, Mississippi State University – États-Unis
- **Matthew ZOOK**, University of Kentucky – États-Unis

Les **après-midi seront consacrés à des ateliers en demi-groupe et réalisés sous R**, sur les méthodes et bibliothèques utiles pour effectuer les différentes tâches d’un projet de recherche empirique ou de science des données : collecte de données, retraitement de données, analyse spatiale, modélisation statistique, géovisualisation.

Les ateliers seront animés par :

- **Kim ANTUNEZ**, DREES – Paris
- **Etienne COME**, IFSTTAR – Champs/Marne
- **Clémentine COTTINEAU**, CNRS – Paris
- **Paul CHAPRON**, IGN – Saint-Mandé
- **Timothée GIRAUD**, CNRS – Paris
- **Fabien PFAENDER**, UTSEUS – Shanghai (Chine)
- **Sébastien REY-COYREHOURCQ**, CNRS – Rouen
- **Lise VAUDOR**, CNRS – Lyon

Comité d’organisation :

- **Florent LE NECHET**, Université Paris-Est
- **Thomas LOUAIL**, CNRS – Paris

**Site du LabEx** : [http://labex-dynamite.com/fr/](http://labex-dynamite.com/fr/)


