---
date: "2011-12-01T00:00:00Z"
# authors: 
# - admin

title: The R Series

summary: Série de livres sur R, proposée par la maison dédition **Taylor & Francis Group**.

tags:
- collection
- series
- édition


image:
  caption: 
  focal_point: Smart
---

Série de livres proposée par la maison d'édition **Taylor & Francis Group**.
La direction éditoriale est composée d'acteurs emblématiques de la communauté R.

La portée de la série est large, couvrant trois fils principaux:

- Applications de la R à des disciplines spécifiques telles que la biologie, l'épidémiologie, la génétique, l'ingénierie, la finance et **les sciences sociales**.

- Utilisation de R pour l'étude de sujets de méthodologie statistique, tels que la modélisation linéaire et mixte, les séries chronologiques, les méthodes bayésiennes et les données manquantes.

- Le développement de R, y compris la programmation, la construction de packages et les graphiques.

Les livres plairont aux programmeurs et développeurs de logiciels R, ainsi qu'aux statisticiens et analystes de données appliqués dans de nombreux domaines. Les livres présenteront des exemples travaillés détaillés et un code R entièrement intégré dans le texte, garantissant leur utilité pour les chercheurs, les praticiens et les étudiants.

**Seules les publications en libre accès sont référencées sur Rzine**.





