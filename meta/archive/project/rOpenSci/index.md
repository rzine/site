---
date: "2011-03-14T00:00:00Z"


title: rOpenSci

summary: Initiative à but non lucratif pour rendre reproductible la recherche de données scientifiques



tags: 
- reproductible
- community
- ROpenSci
- projet
 
image:
  caption: 
  focal_point: Smart
---


Initiative à but non lucratif fondée en 2011 par K. Ram, S. Chamberlain et C. Boettiger pour rendre reproductible la recherche de données scientifiques.
Pour cela rOpenSci participe à :

- la création d'outils et de packages R, qui réduisent le travail avec les sources de données scientifiques disponibles sur le web.

- la promotion d'une culture de partage des données et de logiciels réutilisables

Plus d'informations : [https://ropensci.org/](https://ropensci.org/)