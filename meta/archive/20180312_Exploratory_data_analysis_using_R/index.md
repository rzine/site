---
title: Exploratory data analysis using R
subtitle: 
summary: This book provides a classroom-tested introduction to exploratory data analysis (EDA) 

authors:
- Ronald K. Pearson

publication_types: ["6"]


thematics: ["11","12","13","17","10"]


sources:
- "0"

update:
- "0"

languages:
- "1"

projects:
- divers

date: "2018-03-12T00:00:00Z"




url_source: 'http://www.ru.ac.bd/wp-content/uploads/sites/25/2019/03/102_05_02_Pearson_Exploratory-data-analysis-using-R-2018.pdf'

featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false

---

Exploratory Data Analysis Using R provides a classroom-tested introduction to exploratory data analysis (EDA) and introduces the range of "interesting" – good, bad, and ugly – features that can be found in data, and why it is important to find them. It also introduces the mechanics of using R to explore and explain data.


The book begins with a detailed overview of data, exploratory analysis, and R, as well as graphics in R. It then explores working with external data, linear regression models, and crafting data stories. The second part of the book focuses on developing R programs, including good programming practices and examples, working with text data, and general predictive models. The book ends with a chapter on "keeping it all together" that includes managing the R installation, managing files, documenting, and an introduction to reproducible computing.


The book is designed for both advanced undergraduate, entry-level graduate students, and working professionals with little to no prior exposure to data analysis, modeling, statistics, or programming. it keeps the treatment relatively non-mathematical, even though data analysis is an inherently mathematical subject. Exercises are included at the end of most chapters, and an instructor's solution manual is available.


