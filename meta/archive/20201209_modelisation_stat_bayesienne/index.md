---
title: Introduction à la modélisation statistique bayésienne en R
summary: Contexte théorique & Travaux pratiques - Communication réalisée durant la [Semaine DATA-SHS 2020](https://www.univ-grenoble-alpes.fr/actualites/agenda/agenda-recherche/semaine-data-shs-2020-760705.kjsp)

authors:
- Ladislas Nalborczyk

publication_types: ["4"]


sources:
- "0"

thematics: ["13","14"]


update:
- "0"

languages:
- "0"

projects:
- divers

tags:
- probabilité
- inférence
- modélisation
- bayésienne
- brms
- loi normale
- prior
- vraisemblance
- distribution
- distribution
- Stan
- prédicteur
- incertitude
- multi-niveaux
- Shrinkage
- lme4


date: "2020-12-09T00:00:00Z"

url_source: 'https://www.barelysignificant.com/slides/data_shs_2020/data_shs_2020#1'


featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---

Communication réalisée durant la [Semaine DATA-SHS 2020](https://www.univ-grenoble-alpes.fr/actualites/agenda/agenda-recherche/semaine-data-shs-2020-760705.kjsp)

1) Contexte théorique    
- Interprétations du concept de probabilité    
- Inférence bayésienne   
- Modélisation statistique (bayésienne)    

2) Travaux pratiques    
- Présentation du package brms    
- Exemple #1 : Modèle de régression linéaire    
- Exemple #2 : Modèle de régression logistique    



