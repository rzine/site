+++
title = "Proposer une actualité"
subtitle = "Participer à l'ajout de contenu"
date = 2021-01-18
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50
+++


**Utilisez Rzine pour diffuser des actualités liées à la pratique de R en sciences humaines et sociales**. 

Un séminaire, une formation, un projet, un package...? **Soumettez votre actualité à** [**contact@rzine.fr**](mailto:contact@rzine.fr) 

Vous pouvez vous-même renseigner l'actualité et ses métadonnées associées. Pour cela, **un modèle de fichier et les instructions nécéssaires sont disponibles dans** [**ce document**](https://rzine.gitpages.huma-num.fr/add_news/). 


