---
title: Référencer un.e auteur.e sur Rzine
subtitle: Documentation et modèle de fichier
summary: Documentation (et modèle) pour référencer un.e auteur.e sur rzine.fr

authors:
- admin

projects:
- rzine

sources:
- "8"

publication_types:
- "2"

thematics :
- "9"


update :
- "1"


categories: ["documentation", "rzine"]  


date: "2019-09-01"

tags:
- documentation
- rzine
- contributeur
- auteur
- markdown

links:
- name: Templates
  url: https://gitlab.huma-num.fr/rzine/add_author/-/raw/master/username.zip

url_code: 'https://gitlab.huma-num.fr/rzine/add_author'

url_source: 'https://rzine.gitpages.huma-num.fr/add_author/'

featured: true


image:
  caption: 'Copyright'
  focal_point: "Center"
  preview_only: true

---

Ce document présente l'**ensemble des instructions à suivre pour référencer un.e auteur.e** sur la plateforme Rzine. Tous les éléments et informations à fournir y sont décrits. **Un modèle est mis à votre disposition** pour vous faciliter la tâche.


