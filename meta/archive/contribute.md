+++
title = "Ajouter du contenu"
subtitle = "Référencer une ressource, un.e auteur.e, un projet ou diffuser une actualité"
date = 2019-09-18
headless = true  # This file represents a page section.
active = false  # Activate this widget? true/false
weight = 40

+++


**Rzine permet le référencement de ressources, de projets, d'auteur.e.s, et offre également la possibilité d'ajouter des actualités sur la pratique de R en sciences humaines et sociales**.

Quelque soit le type contenu ajouté, la procédure est toujours la même. Il suffit de renseigner des métadonnées sur le contenu que vous souhaitez référencer dans un fichier markdown . 


### Référencer un ressource

Partagez et valorisez vos travaux et vos ressources documentaires en les référencant sur Rzine. De nombreux types de formats sont acceptés. La classification actuelle proposée est la suivante :

- <font color="#3f51b5"><b>Manuel</b></font><font size="3" color="#3f51b5"> ([exemple](https://rcarto.github.io/carto_avec_r/))</font>
- <font color="#3f51b5"><b>Fiche</b></font><font size="3" color="#3f51b5"> ([exemple](https://neocarto.github.io/ironcurtain/ironcurtain.html))</font>  
- <font color="#3f51b5"><b>Support de cours et de formation</b></font><font size="3" color="#3f51b5"> ([exemple](https://rcarto.gitpages.huma-num.fr/santelocal/))</font>     
- <font color="#3f51b5"><b>Article & billet</b></font><font size="3" color="#3f51b5"> ([exemple](https://quanti.hypotheses.org/1813))</font>   
- <font color="#3f51b5"><b>Package & application</b></font><font size="3" color="#3f51b5"> ([exemple](https://analytics.huma-num.fr/geographie-cites/ExploratR/))</font> 
- <font color="#3f51b5"><b>Site web & blog</b></font><font size="3" color="#3f51b5"> ([exemple](https://rgeomatic.hypotheses.org/))</font>      
- <font color="#3f51b5"><b>Diaporama</b></font><font size="3" color="#3f51b5"> ([exemple](https://hpecout.gitpages.huma-num.fr/R_presentation_FR/#/))</font>   
- <font color="#3f51b5"><b>Autre</b></font><font size="3" color="#3f51b5"> ([vidéo](https://www.youtube.com/user/VincentGouletIntroR/videos), [MOOC](https://www.fun-mooc.fr/courses/course-v1:ParisSaclay+71007+session14/about)...)</font> 


À l'exception des sites web, des applications shiny et des vidéos, **vous pouvez faire héberger vos publications (pdf ou html) et ses ressources associées (données, code source...) sur le serveur Rzine**.
  
**Chaque publication est soumise à un contrôle qualité avant tout référencement**. Il s'agit de vérifier que le contenu correspond aux règles et objectifs de la plateforme, et que son partage et sa valorisation se justifient. 

Si plusieurs publications ont été réalisées ou utilisées dans un même cadre (événement, groupe de travail, projet), **il est possibile de regrouper ces publications au sein d'un projet, également référencé sur Rzine** ([*exemple*](/site/project/parcoursr/)).  Contactez [@Rzine](mailto:contact@rzine.fr) si cela vous intéresse. 



<div class="alert alert-danger" role="alert">
L'ensemble des instructions à suivre et un modèle de fichier pour <b>soumettre le référencement d'une ressource</b> sont disponibles dans <a href="https://rzine.gitpages.huma-num.fr/add_publication/" target="_blank"><b>ce document</b></a>.
</div>

### Référencer un.e auteur.e


Il n'est pas obligatoire d'être référencé comme auteur sur Rzine pour y ajouter du contenu. Cependant, cela est fortement recommandé car au delà du partage de ressources, **Rzine souhaite favoriser la mise en réseau, le travail collaboratif et interdisciplinaire**. **Le référencement des auteurs est donc important**. 

**Référencer un.e auteur.e est simple**. Peu d'informations sont à fournir.   

<div class="alert alert-danger" role="alert">
L'ensemble des instructions à suivre et un modèle de fichier pour <b>référencer un.e auteur.e</b> sont disponibles dans <a href="https://rzine.gitpages.huma-num.fr/add_author/" target="_blank"><b> ce document</b></a>.
</div>


### Ajouter une actualité

**Séminaires, formations, projets, nouveaux packages... Utilisez la plateforme pour diffuser des actualités liées à la pratique de R en sciences humaines et sociales**. 

L'actualité et ses métadonnées doivent être balisées markdown.

<div class="alert alert-danger" role="alert">
L'ensemble des instructions à suivre et un modèle de fichier pour <b>diffuser une actualité</b> sont disponibles dans <a href="https://rzine.gitpages.huma-num.fr/add_news/" target="_blank"><b>ce document</b></a>.
</div>





