---
# the title of your publication
title: Référencer une ressource sur Rzine
subtitle: Documentation et modèle de fichier


# a one-sentence summary of the content on your page. The summary can be shown on the homepage and can also benefit your search engine ranking.
summary: Documentation (et modèle) pour soumettre le référencement d'une ressource sur rzine.fr

# Display the authors of the publication and link to their user profiles (if they exist).
authors:
- admin

# Project name associated ?
# Set the name if several publications can be grouped in one projet
# The existantig name projects are : 
# "Atelier INFTER", "Documentation diverse", "Ecole ete BIG DATA", 
# "Exercices CISTR", "Fiches CISTR", "Groupe fmr", "Manuels CISTR", "Ressource Web"
projects:
- rzine

sources:
- "8"


# Type de la publication 
# 0 = Manuel; 1 = Fiche technique; 2 = Fiche méthodologique; 3 = Fiche thématique;
# 4 = Cours et formation ; 5 = Exercice et atelier; 6 = Site Web et blog ; 7 =  Communication; 
# 8 = Vidéo; 9 = Annexe et script; 10 = Video
publication_types:
- "2"

thematics :
- "9"

update :
- "1"


# Categorizing your content helps users to discover similar content on your site. 
# Categories can improve search relevancy and display at the top of a page alongside a page’s metadata
categories: ["documentation", "rzine"]  


# Date that the page was published. 
# If using Git, enable enableGitInfo in config.toml to have the page modification date automatically updated, rather than manually specifying lastmod.
date: "2019-09-01"


# Tagging your content helps users to discover similar content on your site. 
# Tags can improve search relevancy and are displayed after the page content and also in the Tag Cloud widget.
tags:
- documentation
- rzine
- publication
- ressource
- markdown

# url of GIT publication (.html)
links:
- name: Templates
  url: https://gitlab.huma-num.fr/rzine/add_publication/-/raw/master/20191216_Publication_name.zip

# Lien Github/Gitlab ?
url_code: 'https://gitlab.huma-num.fr/rzine/add_publication'

url_source: 'https://rzine.gitpages.huma-num.fr/add_publication/'

  
# if the is an associated slide to this publication which is upload on the website
# Set the name of the folder where the md of the slide is saved.
# slides: example  


# #################### DON'T TOUCH ##########################
# by setting featured: true, a page can be displayed in the Featured widget. This is useful for sticky, announcement blog posts or selected publications etc. J'ai pas bien compris cet paramètre pour l'instant...
featured: true

# To display a featured image to represent your publication 
# To use, place an image named `featured.jpg/png` in your page's folder.
# set `placement` options to `1`  
# set `focal_point` options to  `Center`
# Set `preview_only` to `true` to just use the image for thumbnails. if not : `false`
# Set `caption` to precise the pitcure credit (with url), if necessary 
image:
  caption: 'Copyright'
  focal_point: "Center"
  preview_only: true
---

Ce document présente l'**ensemble des instructions à suivre pour soumettre le référencement d'une ressource** sur la pratique de R en sciences humaines et sociales, quel que soit son format et son contenu, sur la plateforme Rzine. Tous les éléments et informations à fournir y sont décrits. **Un modèle est mis à votre disposition** pour vous faciliter la tâche.


