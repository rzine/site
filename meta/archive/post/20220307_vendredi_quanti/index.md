---
date: "2022-03-07"
title: Séance "Vendredi Quanti" le 11 mars
summary: Les méthodes de correction de la non-réponse par imputation (avec R !), par Matthieu Marbac Lourdelle.
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---


Le vendredi **11 mars 2022 à 14h**, les *Vendredis Quanti* reçoivent **Matthieu Marbac Lourdelle** (enseignant-chercheur en statistique à l’ENSAI). **Il présentera le cadre général et quelques méthodes de correction de la non-réponse par imputation**. Un temps sera consacré à la mise en pratique de ces méthodes, à l’aide du logiciel R.

La séance sera en format hybride :
- En présentiel : salle de réunion de PACTE, au 1er étage de l’IEP Grenoble.
- En visio : [lien zoom](https://univ-grenoble-alpes-fr.zoom.us/j/91613024022?pwd=aFRTVWRwTVFyZGJweGtaZVVzcTZGdz09)

Si vous souhaitez manipuler les données pendant la séance, vous devez installer R et R Studio, ainsi que les extensions suivantes : `VIM`, `FactoMineR` et `missMDA`.
Pour ceux et celles qui démarrent avec R, vous pouvez consulter le [billet d’Anton Perdoncin sur le blog Quanti](https://quanti.hypotheses.org/1813) pour connaître la procédure à suivre.

Pour en savoir plus sur les *Vendredis Quanti*, rendez-vous sur [le blog associé](https://quantigre.hypotheses.org/).

 


