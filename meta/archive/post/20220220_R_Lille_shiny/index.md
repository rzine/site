---
date: "2022-02-21"
title: Séance conjointe RLille/RTunis sur Shiny le 24 février
summary: La séance intitulée « ***De la Création au Déploiement d'Applications `shiny` avec `golem`*** » sera animée par Margot Brard (ThinkR).
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

R Tunis et R Lille démarre leur collaboration pour l'organisation de *Meetups* conjoints avec une séance (francophone) intitulée  « ***De la Création au Déploiement d'Applications `shiny` avec `golem`*** », qui sera animée par **Margot Brard (ThinkR)**.

**Cette séance se déroulera le 24 février 2022 à 18 h 30 (CET/UTC+1) en ligne.** 

#### Résumé

Vous souhaitez construire une application Shiny en suivant les meilleures pratiques de développement ? Alors le package `golem` est ce qu’il vous faut. Grâce à `golem`, chaque application Shiny est un… package 📦 ! Cela vous permettra de faciliter la création de votre application, en automatisant les tâches ennuyantes répétitives par lesquelles vous devrez nécessairement passer. Cela vous permettra aussi de faciliter son déploiement et d’assurer sa durabilité, en développant un code documenté et testé.

#### Inscription
Pour assister au Meetup, veuillez remplir ce [court formulaire](https://tinyurl.com/y3avzb9s). Le lien Zoom sera envoyé par courriel après inscription et quelques jours avant le début du Meetup.



Plus d'information sur le [site de R Lille](https://rlille.fr/post/2022-01-24-golem/).








