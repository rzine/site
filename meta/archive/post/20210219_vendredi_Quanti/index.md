---
authors:
- admin

title: 'Prochain "Vendredi Quanti" en visio le 26 février'
subtitle: Atelier collectif autour des outils de travail collaboratif sur R
summary: La séance sera consacrée aux outils de travail collaboratif sur R 

date: "2021-02-18T00:00:00Z"

tags:
- Quanti
- PACTE
- séminaire
- atelier


draft: false
share: true
profile: false 
featured: false

image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
---


Les [*vendredis quanti*](https://quantigre.hypotheses.org/) sont des ateliers de discussion et de travail collaboratif autour des méthodes quantitatives dans les sciences sociales. Chaque atelier est introduit par une présentation autour de questions relatives à la collecte et à l’analyse des données quantitatives. La seconde partie de chaque séance est consacrée à une discussion libre au cours de laquelle chacun·e peut soumettre au débat un problème de traitement de données ou travailler sur ses données en bénéficiant des conseils des autres participant·e·s.

Les ateliers ont lieu deux fois par mois, les vendredi de 14h à 16h. **En temps normal, ils se déroulent en présentiel, à Grenoble**.

La prochaine séance se déroulera **exceptionnellement à distance le 26 février 2021**, et reste ouverte à tou.te.s. **Elle sera consacrée aux outils de travail collaboratif sur R**. Plusieurs outils seront évoqués : Gitlab/Github, NextCould, Live share, Jupyter et R server...


Pour plus d’informations, abonnez-vous à la [liste de diffusion](https://listes.univ-grenoble-alpes.fr/sympa/info/seminaire-vendredis-quanti).    
Consultez les activités des *Vendredis Quanti* sur [ce carnet Hypotheses](https://quantigre.hypotheses.org/).



 





