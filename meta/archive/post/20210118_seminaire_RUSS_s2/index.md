---
authors:
- admin

date: "2021-01-18T00:00:00Z"
draft: false
share: true  
profile: false 
 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2020-03-09T00:00:00Z"
projects: [RUSS]
subtitle: 'Visio conférence, le 5 février 2021 '
summary: Etre un•e utilisateur•rice de R résilient•e, par Maëlle Salmon
tags:
- séminaire
- RUSS

title: 'Séminaire RUSS - 5 février 2021'
---


La seconde séance (2021) du séminaire RUSS se déroulera au centre de colloques du campus Condorcet (salle 100) - Aubervilliers ou à distance :

**_Etre un•e utilisateur•rice de R résilient•e_** par Maëlle Salmon (Research Software Engineer).

Apprendre R est une excellente idée pour réaliser différentes tâches au travail. Cependant, même une fois passé·e au-delà de "Bonjour Monde", vous continuerez régulièrement à avoir des problèmes dans R... Triste constat ? Non, car vous apprendrez aussi à devenir un·e utilisateur·rice de R résilient·e !

Dans cette présentation je partagerai des astuces à cet effet : comment suivre les nouveautés de R sans se sentir submergé·e ; et comment et où demander de l’aide efficacement. J’espère que chacun·e repartira avec quelques clés de plus pour une utilisation joyeuse de R !

Rendez-vous sur le [site de RUSS](https://russ.site.ined.fr/fr/annee-2020-2021/vendredi-5-fevrier-2021/) pour l'inscription.





