---
authors:
- admin
categories:
- conférence
date: 2021-04-01T12:12:14-05:00
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
projects: [Rladies_montpellier]
subtitle: 'Première séance 2021 le 27 avril'
summary: Pour sa première séance de 2021, le groupe d'utilisatrices de Montpellier reçoit Maëlle Salmon, sur le thème *Être un·e utilisateur·rice de R résilient·e*
tags:
- conférence
- RLadies
title: 'R-Ladies Montpellier reprend du service'
---


A l'occasion de cette première séance 2021 des RLadies de Montpellier, nous avons la chance de recevoir [Maëlle Salmon](http://rzine.fr/authors/msalmon/) qui nous présentera : " Être un·e utilisateur·rice de R résilient·e "

##### Résumé :

Apprendre R est une excellente idée pour réaliser différentes tâches au travail. Cependant, même une fois passé·e au-delà de “Bonjour Monde”, vous continuerez régulièrement à avoir des problèmes dans R… Triste constat ? Non, car vous apprendrez aussi à devenir un·e utilisateur·rice de R résilient·e !
Dans cette présentation je partagerai des astuces à cet effet : comment suivre les nouveautés de R sans se sentir submergé·e ; et comment et où demander de l’aide efficacement. J’espère que chacun·e repartira avec quelques clés de plus pour une utilisation joyeuse de R !

##### L'oratrice :

Maëlle Salmon est actuellement Research Software Engineer chez rOpenSci. Mais ce n'est pas tout ! Maëlle est également très impliquée dans la communauté R puisqu'elle travaille sur beaucoup d'autres projets en tant que développeuse et/ou formatrice en R. Pour ne citer qu'eux : elle a notamment contribué à la création du livre "HTTP testing in R", est à l'origine du blog R-hub et assure la gestion du compte twitter R-Ladies Global. Nous sommes donc ravi·e·s de l'avoir parmi nous pour ce premier meet up de l'année 2021.
