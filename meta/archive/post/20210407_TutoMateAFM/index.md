---
authors:
- glecampion88
categories:
- conférence
date: 2021-04-03T14:00:00-16:00
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
projects: []
subtitle: 'Prochaine séance des Tuto@Mate'
summary: 'Analyse factorielle multiple (AFM) avec FactoMineR au Tuto@Mate, le 13 avril 14h avec Maëlle Amand'
tags:
- Tuto@Mate
- Mate SHS
title: 'Analyse factorielle multiple avec FactoMineR'
---


**Les tuto@mate présentent l’analyse factorielle multiple (AFM, Escofier & Pagès 1994) avec le package R FactoMineR (Josse & Husson 2008).**
 
 Cette approche a pour but de cibler de grandes tendances variationnelles au sein d’un jeu de données comprenant plusieurs groupes de variables (ex : groupe 1 « personnalité », variable 1 : « timidité », var. 2 : « résistance au stress », var. 3 : « sociabilité » …). Les variables peuvent être qualitatives, quantitatives ou les deux. Le nombre de variables par groupe peut varier et l’importance de chaque groupe est pondérée de façon à ce qu’ils aient le même poids. Cette approche est utilisée dans les études de comportement alimentaire (Ares et al. 2010), en écologie (Poulard et Léauté 2008), sensorimétrie (Stanimirova et al. 2011), sociolinguistique (Amand 2019) ou pour analyser les résultats de questionnaires d’enquête de satisfaction (Bécue-Bertaut et al. 2008). L’AFM permet, en outre, de hiérarchiser soit les variables, soit les groupes de variables en fonction de leur influence sur la variation générale. L’AFM met également en valeur les types de corrélation présents entre les variables. Combinée à une classification hiérarchique (Husson 2010), il est possible de construire des groupes d’individus basés sur les schémas variationnels dominants dégagés par l’AFM. Des graphiques accompagnent chaque tableau de diagnostique de l’AFM (grandes tendances, co-occurrence, groupes d’individus et individus médians ou atypiques), ce qui facilite l’interprétation.

Maelle Amand est maître de conférences en linguistique et phonologie anglaise à l’université de Limoges.  Sa recherche porte sur la variation sociolinguistique et la dialectométrie à l’aide d’analyses multivariées. Sa thèse (Amand 2019) propose une analyse sociophonétique de données patrimonialisées du parler de Newcastle en Angleterre à l’aide de l’analyse factorielle multiple. Elle propose régulièrement des ateliers R pour les écoles doctorales (Paris 7, Paris 3, Université de Turin).

