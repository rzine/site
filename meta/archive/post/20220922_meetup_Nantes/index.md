---
date: "2022-09-22"
title: Le Meetup R Nantes reprend du service
summary: La prochaine séance est prévue le 19 Octobre 2022, à Nantes
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

Après deux ans de somnolence, le [Meetup R Nantes](https://www.meetup.com/fr-FR/meetup-r-nantes/?_cookie-check=dhCSFWsdayMzUBja) est relancé sous l'impulsion de [Sébastien Rochette](https://thinkr.fr/equipe/sebastien-rochette/) (alias [@statnmap](https://mobile.twitter.com/StatnMap)).     
Un compte GitHub a été ouvert pour l'occasion : [https://github.com/r-nantes/meetups](https://github.com/r-nantes/meetups). 

Rendez-vous sur la [plateforme Meetup](https://meetu.ps/e/LpJNy/wkDMm/i) pour vous inscrire au groupe.

La prochaine rencontre est prévue le 19 Octobre (19h) à la salle de *The Link* à Nantes. La séance sera animée par Marion Louveaux (Opendatasoft) et Juliette Engelaere (DREAL Pays de la Loire) sur la thématique du Rmarkdown et de Quarto. 

Retrouvez plus de détails sur la [page Meetup consacrée à cette rencontre](https://www.meetup.com/fr-FR/meetup-r-nantes/events/288582124/).

