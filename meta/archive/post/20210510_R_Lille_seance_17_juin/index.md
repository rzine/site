---
date: "2021-05-10"
title: R-Lille, une séance prévue le 17 juin
subtitle: Reproducible Computation at Scale in R
summary: Reproducible Computation at Scale in R with {targets}, by Will Landau
authors: ["hpecout"]
projects: [RLille]
categories: []
tags: ["Reproducible ","targets","R-Lille"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

**Reproducible Computation at Scale in R with {targets}**, by Will Landau

The [{targets}](https://docs.ropensci.org/targets/) R package enhances the reproducibility, scale, and maintainability of data science projects in computationally intense fields such as machine learning, Bayesian data analysis, and statistical genomics. {targets} resolves the dependency structure of the analysis pipeline, skips tasks that are already up to date, executes the rest with optional distributed computing, and manages data storage. [{stantargets}](https://docs.ropensci.org/stantargets/) and similar packages extend {targets} to simplify pipeline construction for specialized use cases such as the validation of Bayesian models.

This event (in English) is organised by the R Lille user group.

More informations [here](https://www.meetup.com/fr-FR/R-Lille/events/277902715)


