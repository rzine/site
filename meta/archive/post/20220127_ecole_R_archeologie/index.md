---
date: "2022-01-27"
title: Une école d'été sur la manipulation de données archéologiques sous R en juillet 2022
summary: L'université de Strasbourg et l'Université de Kiel, en coopération avec l'Université Franco-Allemande, organise l'école d'été MOSAICdata.
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

L'Université de Strasbourg et l'Université de Kiel, en coopération avec l'Université Franco-Allemande, organise au mois de juillet prochain **une école d'été sur le thème de la manipulation de données archéologiques sous R**.

L'école d'été **MOSAICdata** durera six jours (**11-16 juillet 2022**), au cours desquels plusieurs méthodes de gestion et de traitement des données de différents domaines seront illustrées et mises en œuvre à l'aide du logiciel Rstudio. Les candidatures sont ouvertes aux chercheurs et jeunes chercheurs (doctorants et post-doctorants), ainsi qu'à tous les professionnels de l'archéologie. Bien qu'un intérêt pour le traitement et l'analyse des données soit attendu des participants, une connaissance approfondie de la méthodologie ou des logiciels n'est pas requise.

Vous trouverez plus d'informations ainsi que le programme de cette école d'été sur le site : [https://lscholtus.gitlab.io/mosaicdata](https://lscholtus.gitlab.io/mosaicdata)

**Les candidatures sont ouvertes jusqu'au 28 février 2022.**











