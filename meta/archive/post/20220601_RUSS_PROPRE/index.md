---
date: "2022-06-01"
title: Prochaine séance RUSS le 3 Juin 2022 (visio)
subtitle: PROcessus de Publications REproductibles avec R (la démarche PROPRE)
summary: PROcessus de Publications REproductibles avec R - la démarche PROPRE, par Juliette Engelaere-Lefebvre.
authors: ["hpecout"]
projects:
- RUSS
categories: []
tags: ["shs","RUSS","propre","reproductibilité", "publication"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

**La prochaine séance du séminaire R à l'Usage des Sciences Sociales (RUSS) aura lieu vendredi 3 juin 2022 de 9h30 à 11h30, en ligne**.

Intitulée "***PROcessus de Publications REproductibles avec R : la démarche PROPRE***", la séance sera animée par **Juliette ENGELAERE-LEFEBVRE** (Responsable du Centre de services de la donnée SCTE/CSD, Direction Régionale de l’Environnement, de l’Aménagement et du Logement Pays de Loire).

#### Résumé
Depuis 2017, le datalab de la DREAL Pays de la Loire a expérimenté de nouvelles méthodes de travail autour du Design Thinking et des méthodes Agiles pour réaliser ses publications statistiques.
En 2019, dans le prolongement des mutations plus centrées sur les besoins de ses clients, le datalab opère un pivot plus technique : d’une culture de la donnée il se tourne vers une culture logicielle et le formalise par la démarche PROPRE pour PROcessus de Publications REproductibles. Inspiré du Reproductible Analytical Pipeline du gouvernement britannique, cette approche utilise l’objet technique qu’est le paquet R comme véhicule de la publication statistique. Les développements sont versionnés, factorisés, documentés, testés et intégrés au sein d’une forge logicielle qui permet de mutualiser, rationaliser et sécuriser les productions. À l’écoute active des besoins de ses clients d’un côté et soucieux de l’opérationnalité en production de l’autre, le statisticien métamorphose son métier vers un continuum DataOps.
Cette présentation décrit cette (r)évolution, les principaux résultats et les conditions de sa réussite.

L'inscription est obligatoire. Retrouvez toutes les informations nécessaires sur le [site de RUSS](https://russ.site.ined.fr/fr/annee-2021-2022/processus-de-publications-reproductibles-avec-r-la-demarche-propre/). Inscrivez-vous à la [liste de diffusion](https://listes.ined.fr/subscribe/russ-sms) pour recevoir les annonces des séances du séminaire RUSS. 