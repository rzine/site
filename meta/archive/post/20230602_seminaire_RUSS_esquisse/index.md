---
authors:
- bgarnier
date: "2023-05-24T00:00:00Z"
draft: false
share: true  
profile: false 
 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2023-05-24T00:00:00Z"
projects: [RUSS]
title: 'Esquisse, moins tu sais coder plus tu vas rigoler'
subtitle: 'Une interface graphique pour la construction de graphique `ggplot2`'
summary: Dernière séance du séminaire RUSS 2022/2023 qui se déroulera en format hybride le 2 juin 2023 et portera sur le package `esquisse`, qui offre une interface graphique pour la construction de graphique `ggplot2`.
tags:
- séminaire
- RUSS
- esquisse
- ggplot2
- graphique

---

La prochaine séance du séminaire *R à l'Usage des Sciences Sociales* (RUSS) aura lieu **vendredi 2 juin 2023** de **9h30 à 11h30h**.

**"`Esquisse`, moins tu sais coder plus tu vas rigoler"**, présenté par Victor Perrier (dreamRs).

L’inscription est obligatoire pour venir assister à l’Ined en salle Sauvy ou recevoir le lien de connexion à la visio conférence. Le formulaire d’inscription est disponible sur [cette page](https://russ.site.ined.fr/fr/annee-2022-2023/esquisse/) (lien de connexion transmis avant le séance).


**résumé :**

Le package `esquisse` permet, via une interface graphique, de construire des graphiques avec `ggplot2` tout le générant le code associé vous permettant de recréer ces graphiques dans vos scripts.

Après une présentation des fonctionnalités du package, nous détaillerons quelques éléments clés de celui-ci : réutiliser esquisse dans vos propres applications, modules et fonctions shiny réutilisables (import de données, filtres, export de graphiques…), génération d’appel à des fonctions avec rlang, les addins RStudio, utiliser esquisse dans d’autres langues (12 implémentées).


**Retrouvez les séances précédentes avec présentations et vidéos sur le** [**site dédié**](https://russ.site.ined.fr/fr/annee-2022-2023/esquisse/).

Pour recevoir les annonces des séances du séminaire RUSS, inscrivez-vous à la [liste de diffusion](https://listes.ined.fr/subscribe/russ-sms).





