---
date: 2021-10-25T08:13:14-05:00
title: Analyse Spatiale Sous R, Avec L'Extension {sf}
subtitle: Evénement organisé par le groupe R Lille  
summary: Prochain événement Meetup organisé par R lille, le 18 Novembre à 13h (visioconférence).
authors: 
- hpecout
projects: [RLille]
categories: []
tags: 
- Meetup
- RUG
- analyse spatiale
- lille
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

#### Résumé

Bien qu’initialement non conçus spécifiquement pour l’analyse spatiale, un certain nombre de packages ont émergé sur le logiciel R.
Les trois principaux sont `rgdal` qui permettait d’importer des données spatiales (shapefile, …), `sp` qui fournit les outils pour analyser les données, les mettre en formes, etc., et `rgeos` qui permet les opérations de > SIG classique (calculs de distances, calculs de périmètre, intersection, etc.).

Le package `sf` pour "simple feature" est un package de simplification. Il permet d’exploiter tous les types de données spatiales que ce soit des analyses de points, des découpages administratifs ou encore des grilles. Grâce à sa panoplie rationalisée de fonction, il permet avec un minimum de fonctions d’importer/exporter, de manipuler/traiter les données et de les présenter de façon simple. Et tout cela en un seul package au lieu de trois auparavant!

À cela s’ajoute sa syntaxe claire et normée, ainsi toutes ses fonctions commençant par "st_" sont faciles à retrouver.

Si le format des données peut paraître complexe au premier abord, il interagit facilement avec d’autres packages. Ainsi, la sélection d’observation est facilitée avec le package `dplyr` et la visualisation des données est aisée via "ggplot".


**Cette session sera présentée par Aniss Louchez, doctorant en économie de la santé, et se déroulera le 18 Novembre à 13h (visioconférence)**.

**Plus d'informations et inscription sur le** [**Meetup R Lille**](https://www.meetup.com/fr-FR/R-Lille/events/281653108/).

