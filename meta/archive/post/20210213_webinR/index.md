---
authors:
- admin

title: 'analyse-R version webin-R'
subtitle: 'Un wébinaire hebdomadaire sur l’analyse d’enquêtes avec R et RStudio'
summary: Un wébinaire hebdomadaire sur l’analyse d’enquêtes avec R et RStudio

date: "2021-02-13T00:00:00Z"

tags:
- webinaire
- analyse-r


draft: false
share: true  
profile: false 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
---

Depuis Novembre 2020, [Joseph Larmarange](http://rzine.fr/authors/jlarmarange) propose un wébinaire hebdomadaire de deux heures (tous les jeudis de 17h-19h, CET heure de Paris) qui s’appuie sur les contenus de son site web [analyse-R](https://larmarange.github.io/analyse-R/) (Analyse d’enquêtes avec R et RStudio). 

#### Comment participer ?

[webin-R](https://larmarange.github.io/webin-R/) est diffusé en live et enregistré. Les vidéos des sessions précédentes sont accessibles sur le [site](https://larmarange.github.io/webin-R/seances.html) et directement sur la [chaîne Youtube associée](https://www.youtube.com/channel/UCF9ymC7Dpjwr0lge2QR_A5Q).

Les liens de connexion à la plateforme de diffusion sont diffusés par email. Pour s’inscrire à la liste de diffusion [webin-R](https://larmarange.github.io/webin-R/) et recevoir les liens de connexion hebdomadaires, il vous suffit d’envoyer un email avec pour objet "subscribe webin-R" à [sympa@listes.ird.fr](mailto:sympa@listes.ird.fr).
