---
authors:
- admin
categories:
- version
date: "2020-04-24T00:00:00Z"
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2020-04-24T00:00:00Z"
projects: []
subtitle: 'Nouvelle mise à jour majeure de R'
summary: Une nouvelle version de R comportant des mises à jour importantes est disponible
tags:
- 4.0.0
- R
- mise à jour
- version
title: 'Version R 4.0.0'
---


Retrouvez **l'ensemble des modifications et des nouveautés** apportées par la **version 4.0.0** de R sur le [**CRAN**](https://cran.r-project.org/doc/manuals/r-devel/NEWS.html).     

**La réinstallation de tous les packages est nécessaire !**





