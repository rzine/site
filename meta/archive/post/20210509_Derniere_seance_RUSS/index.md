---
date: "2021-05-09"
title: Dernière séance 2020/2021 du séminaire RUSS le 11 juin
subtitle: Reproductibilité des analyses avec R
summary: Panorama des solutions assurant la reproductibilité des analyses avec R, par Timothée Giraud
authors: ["hpecout"]
projects: [RUSS]
categories: []
tags: ["renv","projet","markdown","docker","git","reproductibilité"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

**Panorama des solutions assurant la reproductibilité des analyses avec R**, par Timothée Giraud (CNRS UMS-RIATE)

Le logiciel R est un outil de choix pour assurer la reproductibilité des traitements de données, sa nature même de langage de script est favorable à l’enregistrement explicites des procédures mobilisées dans les analyses.     
Utilisé en association avec d’autres briques logicielles telles que des packages dédiés (renv), les projets RStudio, le langage markdown, les conteneurs Docker ou les logiciels de gestion de version (git), R offre une variété de solutions pouvant assurer la reproductibilité des analyses. En fonction des options choisies cette reproductibilité peut être minimale ou très complète.

Retrouvez le formulaire d'inscription sur le [site de RUSS](https://russ.site.ined.fr/fr/annee-2020-2021/vendredi-11-juin-2021/)



