---
date: 2021-04-27T14:00:00-16:00
title: 'Ouverture des inscriptions à la conférence useR 2021'
subtitle: 'Inscriptions anticipées ouvertes depuis le 20 avril'
summary: 'Les inscriptions anticipées sont ouvertes depuis le 20 avril'
draft: false
authors:
- hpecout
projects: []
categories:
- conférence
tags: 
- conference
- useR
- virtual
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---


Dans ce contexte particulier, la conférence useR! 2021 se tiendra à distance.     
Les organisateurs proposent une session d'inscriptions anticipées, à des prix très abordables, en fonction des moyens de chacun.

Pour s'inscrire :  [https://user2021.r-project.org/participation/registration/](https://user2021.r-project.org/participation/registration/)

