---
authors:
- admin
categories:
- conférence
date: 2021-04-01T12:13:14-05:00
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
projects: [RLille]
subtitle: 'Reproductibilité et construction de package'
summary: 'Reproductibilité (15 avril) et construction de package (29 avril) au programme'
tags:
- conférence
- useR
title: 'R-Lille, deux séances prévues en avril'
---


**R-lille proposes deux nouvelles séances (en anglais)**, pour le mois d'avril :

- **15 Avril** - ***My Journey To Transparency And Reproducibility***, par Mickaël Canouil

In this talk, [Mickaël Canouil](https://m.canouil.fr) will chronicle his journey towards a more transparent and reproducible work environment. He will talk about the changes and improvements that he have implemented in his team, detailing the transition of infrastructure used when he arrived to an infrastructure using Docker and the {renv} R package.


- **29 Avril** - ***How To Build A Package With The "Rmd First" Method***, par Sébastien Rochette

You know how to build a Rmardown, you were said or would like to put your work in a R package, but you think this is too much work? You do not understand where to put what and when? What if writing a Rmd was the same as writing a package? Follow the "Rmd-first" method and let {fusen} guide you to build a documented and tested package, to ensure the sustainability and sharing of your work.


**Retrouvez toutes les activités du groupe d'utilisateurs lillois à ce** [**lien**](https://www.meetup.com/fr-FR/R-Lille)

