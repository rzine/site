---
authors:
- admin
categories:
- conférence
date: "2020-03-13T00:00:00Z"
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2020-03-13T00:00:00Z"
projects: [RencontresR]
subtitle: 'AgroParisTech, du 15 au 17 Juillet 2020'
summary: La 8ème éditions de **Rencontres** R aura lieu à **AgroParisTech** du **15 au 17 Juillet 2020**.**[REPORTÉ à 2021]**
tags:
- conférence
- Rencontres
- AgroParisTech
title: 'Rencontres R 2020'
---


Retrouvez **l'ensemble du programme** sur [**site de l'événement**](https://paris2020.rencontresr.fr/).    
**L'inscription** et les soumissions seront possibles **à partir du 4 mars** (tarif réduit jusqu'au 17 mai). 





