---
date: "2021-05-07"
title: Rencontre de R-Toulouse le jeudi 20 mai
subtitle: Package renv - Présentation et retour d’expérience
summary: Package renv - Présentation et retour d’expérience, par Elise Maigne (INRAE)
authors: ["hpecout"]
projects: [RToulouse]
categories: []
tags: ["renv","R-toulouse","reproductibilité"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---


**Package renv : présentation et retour d’expérience. Gérer ses versions de packages R, et de leurs dépendances (et dépendances des dépendances)**, par Elise Maigne (INRAE).

Le package R renv, développé par Kevin Ushey, permet de gérer les versions des packages R pour un projet : spécifier, figer et partager l’environnement R dans lequel on développe son code. Je montrerai le fonctionnement du package (ça fait quoi ?) et comment l’utiliser en pratique (je fais quoi ?), le tout illustré par des expériences personnelles (Elise Maigné elle fait comment ?).

Retrouvez les informations de connexion sur le [site de R-toulouse](https://r-toulouse.netlify.app/evenements/2021-04-27-rencontre-du-20-mai-2021/)





