---
title: "Les rencontres R 2021... Enfin !"

summary: Elles avaient été reportées en 2020... Elles se tiendront en 2021
authors: 
- admin
date: 2021-03-01T00:00:00-00:00
categories: ["rzine"]
projects: [RencontresR]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
---

Les rencontres R 2021 se dérouleront du 12 et 13 Juillet prochains.   

Un nombre réduit de participants (100) pourront y assister en présentiel, à AgroParisTech. La priorité sera donnée aux orateurs/oratrices invités, aux membres des comités d’organisation et de programme, ainsi qu’aux participants ayant une soumission à présenter. En contrepartie, l'inscription sera entièrement gratuite et la conférence pourra être suivie en visioconférence.

**Les inscriptions et les soumissions** sont ouvertes.  
**Retrouvez toutes les informations nécessaires sur le** [**site des rencontres R 2021**](https://paris2021.rencontresr.fr/).









