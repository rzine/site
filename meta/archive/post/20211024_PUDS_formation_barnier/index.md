---
date: 2021-10-24T08:13:14-05:00
title: Formation complémentR
subtitle: par Julien Barnier, formateur au langage et logiciel R  
summary: Formation avancée de Julien Barnier, proposée par la PUD de Strasbourg le 18 et 19 novembre 2021 en présentiel.
authors: 
- hpecout
projects: []
categories: []
tags: 
- formation
- MSH
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---



Cette formation est proposée par la [**Plateforme Universitaire de Données de Strasbourg (MISHA)**](https://www.misha.fr/pud-s). Elle sera animée par **Julien Barnier**, auteur de plusieurs supports de formation relatifs à cette thématique. Celui-ci présentera un usage dit "avancé" du logiciel pour mener à bien tous vos travaux de recherche.

**La formation est destinée aux doctorant·e·s, post-doctorant·e·s, ingénieur·e·s d'études ou de recherche, enseignant·e·s-chercheur·e·s issu·e·s des SHS ou d'autres disciplines, ayant déjà une expérience du langage R**. Les participant·e·s pourront suivre une séance de "remise à niveau" assurée par nos ingénieur·e·s quelques semaines avant le début de la formation.

**Le nombre de participants est limité à 12 personnes**. **L'inscription engage les participante·s à assister à l'intégralité de la formation**.

Le programme et le formulaire d'inscription sont disponibles sur le [site de la MISHA](https://www.misha.fr/plateformes/plateforme-universitaire-de-donnees/agenda-de-la-pud/evenement/formation-complementr). 




