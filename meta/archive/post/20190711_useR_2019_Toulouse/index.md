---
authors:
- admin
categories:
- conférence
date: 2019-06-01T21:13:14-05:00
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2019-06-01T00:00:00Z"
projects: []
subtitle: 'Conférence international pour les utilisateurs de R'
summary: Cette année, la conférence international useR à lieu en France, du 11 au 13 juillet à Toulouse
tags:
- conférence
- useR
title: 'useR! 2019 - Toulouse'
---

**La conférence international useR 2019 se déroulera à Toulouse, du 11 au 13 juillet.** Pour vous inscrire, rendez-vous sur le [site de l'événement](http://www.user2019.fr/)




