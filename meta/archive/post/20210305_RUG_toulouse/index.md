---
authors:
- admin

title: 'Prochaine rencontre du RUG Toulouse le 16 mars'
subtitle: Automatisation de la production de cartes en R 
summary: Automatisation de la production de cartes en R (visioconférence). 

date: "2021-03-05T00:00:00Z"

tags:
- RUG
- toulouse
- carte

projects: [RToulouse]

draft: false
share: true
profile: false 
featured: false

image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
---


La prochaine rencontre de R-Toulouse est prévue par visio le mardi 16 mars 2021 de 17h à 18h sur la plateforme de visioconférence Zoom.

#### Programme

**Typologie d’exploitations agricoles en région Grand Est : Automatisation de la production de cartes en R**, par *Michael Levi-Valensin* (Bureau des Méthodes et Information Statistiques, Complexe Agricole d’Auzeville).

Michael Levi-Valensin présentera un travail réalisé il y a un an à la DRAAF Grand Est pour le compte de la Chambre Régionale d’Agriculture, ce qui a donné lieu à un dossier mis en forme et automatisé avec des scripts R (pour les tableaux et graphiques).

Tous les informations de connections sont disponibles sur le [site web R Toulouse](https://r-toulouse.netlify.app/evenements/2021-03-02-rencontre-du-16-mars-2021/)



 





