---
authors:
- admin

title: 'Diaporama et vidéo de la rencontre R-Toulouse du 16 mars 2021'
subtitle: Automatisation de la production de cartes en R, par Michael Levi-Valensin 
summary: Automatisation de la production de cartes en R (visioconférence). 

date: "2021-03-18T00:00:00Z"

tags:
- RUG
- toulouse
- carte

projects: [RToulouse]
draft: false
share: true
profile: false 
featured: false

image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
---


Retrouvez le diaporama et un enregistrement vidéo de la visioconférence sur le [site de R-Toulouse](https://r-toulouse.netlify.app/evenements/2021-03-17-diaporama-et-video-de-la-rencontre-de-mars-2021/)


 





