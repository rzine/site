---
authors:
- hpecout

title: 'Séance "Vendredi Quanti" le 4 juin'
subtitle: Comment représenter graphiquement des analyses factorielles avec ggplot2 ?
summary: '"Comment représenter graphiquement des analyses factorielles avec ggplot2", par Anton Perdoncin'

date: "2021-06-02T00:00:00Z"

tags:
- Quanti
- PACTE
- séminaire
- atelier


draft: false
share: true
profile: false 
featured: false

image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
---


Cette séance consacrée à **la représentation graphique des analyses factorielles avec ggplot2** et animée par *Anton Perdoncin* se déroulera **le 4 juin, de 14h à 16h**.

Elle peut-être suivie sur Zoom, à l'adresse suivante :   
https://univ-grenoble-alpes-fr.zoom.us/j/91613024022?pwd=aFRTVWRwTVFyZGJweGtaZVVzcTZGdz09

Il sera également possible d'y assister en présentiel, à l'IEP de Grenoble (salle Domenach). La capacité d’accueil de la salle dans les conditions actuelles est de 21 places, il est donc nécessaire de s'inscrire :   
https://framadate.org/Z89uqyDTxww26Hgn

Cette séance s’appuiera sur un billet de blog disponible sur [Quanti](https://quanti.hypotheses.org/1871) (et [référencé sur Rzine](https://rzine.fr/publication/20200406_analyse_geom_quanti/)). Anton Perdoncin en présentera une mise en œuvre. Le script R accompagnant ce billet est téléchargeable [ici](https://webcloud.zaclys.com/index.php/s/4Nk6rMHUMyYZius). Il permet de générer les données utilisées pour le tutoriel, et les analyses, à partir d’un exemple de jeu de données « brut » de l’enquête Emploi qui est librement [téléchargeable sur le site de l’Insee](http://insee.fr/fr/statistiques/4191029#consulter).



 





