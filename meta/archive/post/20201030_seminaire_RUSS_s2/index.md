---
authors:
- hpecout

date: "2020-10-30T00:00:00Z"
draft: false
share: true  
profile: false 
 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2020-03-09T00:00:00Z"
projects: [RUSS]
subtitle: 'Visio conférence, le 4 décembre 2020 '
summary: la **première séance RUSS** (2020-2021) se déroulera **en visio conconférence** le **4 décembre**
tags:
- séminaire
- RUSS
- Introduction
- Rstudio
- reproductible

title: 'Séminaire RUSS - Séance 1 (2020/2021)'
---


En raison du contexte sanitaire, **la première séance 2020-2021 proposée par le séminaire RUSS se déroulera en visio conconférence** !

Cette séance intitulée **_Introduction à R pour la mise en oeuvre de chaines de traitements reproductibles_** sera présentée par Hugues Pecout (CNRS, FR CIST) et Ronan Ysebaert (UP, UMS RIATE), et se déroulera le **4 décembre** 2020 de **9h30 à 12h**.

Elle s'adresse aux personnes souhaitant s'initier au langage R et son environnement de développement intégré Rstudio.
Après une contextualisation de R dans le paysage de l'analyse de données puis une présentation des rudiments de ce langage de programmation, l'attention sera portée sur deux principaux atouts de R : polyvalence et reproductibilité. Les librairies de référence utiles pour couvrir l'intégralité d'une chaîne de traitement (de l'import de données à la valorisation des résultats) et réaliser différents types d'analyses (statistique, spatiale, textuelle, réseau...) seront présentées. Un exemple pratique et reproductible clôturera la présentation.

Rendez-vous sur le [site de RUSS](https://russ.site.ined.fr/fr/annee-2020-2021/04-12-20/) pour l'inscription (gratuite).





