---
date: 2021-09-23T08:13:14-05:00
title: Visualiser des données denquêtes avec ggplot2
subtitle: Atelier en distanciel proposé par la MSH Lyon St-Etienne
summary: Atelier en distanciel proposé par la MSH Lyon St-Etienne
authors:
- hpecout
projects: []
categories:
- atelier
tags:
- atelier
- MSH
- PANELS
- ggplot2
featured: false
draft: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

Cet atelier est proposé par la [MSH Lyon St-Etienne](https://www.msh-lse.fr/) dans le cadre du cycle [Visualisation(s) : outils, méthodes quantitatives et explorations graphiques](https://www.msh-lse.fr/visualisations/). Il s'agit d'un cycle d’ateliers qui s'inscrit dans le cadre de la plateforme technologique [PANELS](https://www.msh-lse.fr/services/panels/) de la MSH Lyon St-Etienne.

**Cet atelier** "***Visualiser des données d’enquêtes à l’aide de R et ggplot2***" **se déroulera en distanciel le 6 octobre 2021**, et sera animé par **Agathe Déan** (IE CNRS).

**Attention, il est uniquement ouvert à tous les personnels membres des laboratoires associés à la MSH Lyon St-Etienne !** 

**L'inscription est gratuite mais obligatoire** avant le 29 septembre 2021.

Pour plus d'informations, ou pour s'inscrire : https://www.msh-lse.fr/agenda/visualiser-donnees-denquetes-r-ggplot2/





