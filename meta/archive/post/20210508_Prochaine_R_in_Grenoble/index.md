---
date: "2021-05-08"
title: Prochaine session proposée par R in Grenoble le 25 mai sur Zoom
subtitle: Building dashboards in R/shiny
summary: Building dashboards in R/shiny (and improve them with logs and user feedback), by M. Kaufman & N. Ricci
authors: ["hpecout"]
projects: [RinGrenoble]
categories: []
tags: ["shiny","dashboard","R in Grenoble"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---


**Building dashboards in R/shiny (and improve them with logs and user feedback)**, by M. Kaufman & N. Ricci

The main goal of this seminar is to demonstrate how R/Shiny app developers can collect data from the visitors of their app (such as logs or feedback) to improve it. We will use as example the CaDyCo project, a dynamic and collaborative cartography project hosted by USMB and UGA.

Registration to the event on the [website of R in grenoble](https://r-in-grenoble.github.io/sessions.html)



