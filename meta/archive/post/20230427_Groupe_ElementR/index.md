---
authors:
- hpecout
date: "2023-04-28T00:00:00Z"
draft: false
share: true 
profile: false 
 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2023-03-29T00:00:00Z"
projects: [RUSS]
title: 'Nouvelle vague du Groupe ElementR'
subtitle: 'Le groupe d’utilisateur·rices relance ses activités'
summary: Le groupe d’utilisateur·rices pour le traitement de l'information géographique ElementR avec R relance ses activités


tags:
- séminaire
- elementR
- géographie
- analyse spatiale
- cartographie
- géomatique
- groupe


---

En septembre 2022, l'UMR géographie-Cités 8504 a relancé les activités de son groupe d'utilisateur·rices, pour le traitement de l'information géographique avec R.

Le périmètre du groupe ElementR s'est officiellement élargi à trois unités :

-   **UMR** [**Géographie-cités**](https://geographie-cites.cnrs.fr/) (8504)
-   **UMR** [**PRODIG**](https://www.prodig.cnrs.fr/) (8586)
-   **UAR** [**RIATE**](https://riate.cnrs.fr/) (2414)

Mais **les activités du groupe restent ouvertes aux membres d'unités de recherche localisées sur le [Campus Condorcet](https://www.campus-condorcet.fr/) : Ladyss, EHESS, INED...**. Des séances ElementR (cours, atelier, table ronde...) sont régulièrement organisés au centres des colloques de condorcet.

Après dix ans d'existence, le groupe ElementR poursuit ainsi son activité, pour l'apprentissage et de montée en compétence collective de la pratique du traitement de l'information géographique avec R.

<center>
<a href="https://elementr.netlify.app/" target="_blank" class="btn btn-info" role="button" aria-disabled="true"><i class="bi bi-cloud-arrow-down"></i> Site web ElementR</a>
</center>

</br>

Pour rejoindre le groupe, vous pouvez vous inscrire à la **liste de diffusion d'ElementR : [elementr\@services.cnrs.fr](https://listes.services.cnrs.fr/wws/subscribe/elementr?previous_action=info)**. L'inscription est restreinte aux membres d'unités localisées sur le Campus Condorcet et, sur demande, à tous les collègues de l'ESR qui traitent de l'information géographique avec R.




