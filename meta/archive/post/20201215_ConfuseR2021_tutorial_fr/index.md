---
authors:
- admin
categories:
- conférence
date: 2020-12-15T21:13:14-05:00
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
lastmod: "2020-12-15T00:00:00Z"
projects: []
title: 'The useR!2021 Conference'
subtitle: 'Soumettez un tutoriel en français'
summary: UseR2021 est à la recherche de tutoriels en français. Fermeture des soumissions le 5 février 2021.
tags:
- conférence
- useR
- tutorial
- submission

---


<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Vous avez des compétences en <a href="https://twitter.com/hashtag/RstatsFR?src=hash&amp;ref_src=twsrc%5Etfw">#RstatsFR</a> et vous aimeriez les partager avec un public mondial? <a href="https://twitter.com/hashtag/useR2021?src=hash&amp;ref_src=twsrc%5Etfw">#useR2021</a> est à la recherche de tutoriels en français! Proposez votre tutoriel: <a href="https://twitter.com/hashtag/dataviz?src=hash&amp;ref_src=twsrc%5Etfw">#dataviz</a>, bases de données, méthodes <a href="https://twitter.com/hashtag/stats?src=hash&amp;ref_src=twsrc%5Etfw">#stats</a> ou autres. Plus d&#39;infos: <a href="https://t.co/wEr8IFBAz0">https://t.co/wEr8IFBAz0</a></p>&mdash; useR! 2021 (@useR2021global) <a href="https://twitter.com/useR2021global/status/1340960395247448066?ref_src=twsrc%5Etfw">December 21, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


