---
date: "2022-04-14"
title: Les inscriptions à l'école d'été Quantilille 2022 sont ouvertes
summary: la formation se déroulera dans les locaux de l’Institut d’études politiques de Lille et aura lieu du 20 au 24 juin
authors: ["vjurie"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

Organisée par le Ceraps —Centre de recherches administratives, politiques et sociales, UMR 8026— la formation se déroulera dans les locaux de l’Institut d’études politiques de Lille. Elle aura lieu du 20 au 24 juin.

L'école d'été **Quantilille** durera cinq jours (**20-24 juin 2022**) et propose deux modules :
- [Pratiques de la recherche quantitative en sciences sociales](https://ceraps.univ-lille.fr/nc/detail-event/programme-2022-1/#m1) : concevoir, manipuler, décrire, inférer organisé par Pierre Blavier, Samuel Coavoux, Anton Perdoncin & Thomas Soubiran
- [Machine learning en sciences sociales](https://ceraps.univ-lille.fr/nc/detail-event/programme-2022-1/#m2) : classifier, visualiser, prédire  organisé par Julien Boelaert & Thomas Soubiran

L'école vise à permettre aux chercheurs, enseignants-chercheurs et futurs chercheurs d’approfondir leurs connaissances en matière de traitement des données quantitatives, soit en les formant aux techniques traditionnelles, soit en les initiant à des méthodes plus avancées.   
Elle entend de plus restituer ces techniques quantitatives dans leur environnement scientifique national et international afin de permettre une insertion plus aisée dans des réseaux européens et/ou pluridisciplinaires.   
Elle vise enfin à favoriser les échanges scientifiques entre les participants français et étrangers, mais aussi entre participants et intervenants. Plus généralement, aider à la constitution ou au renforcement de réseaux scientifiques dans la communauté française et internationale.   

Vous trouverez plus d'informations ainsi que le programme de cette école d'été sur le site : [https://ceraps.univ-lille.fr/quantilille/](https://ceraps.univ-lille.fr/quantilille/)

**L'inscription est payante et s'adresse prioritairement aux agent·e·s du CNRS, les candidatures sont ouvertes jusqu'au 25 avril 2022.**
