---
title: Les Rencontres R 2021 en vidéo
subtitle: Bilan et liens vers les vidéos des présentations
summary: Bilan et liens vers les vidéos des présentations
authors:
- hpecout
date: 2021-08-16T08:13:14-05:00
projects:
- RencontresR
categories:
- conférence
tags: 
- conférence
- Rencontres R
featured: false
draft: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

Les **Rencontres R 2021** se sont déroulées du 12 et 13 Juillet 2021 en semi-présentielle dans les murs d’AgroParisTech.    
Pour plus d'informations, consultez le [site web des Rencontres R 2021](https://paris2021.rencontresr.fr/).

Les vidéos des interventions (la plupart) ont été mises en lignes sur [une chaîne Youtube dédiée](https://www.youtube.com/channel/UC_1MHEuO3GNd2nEG_fam0jQ/videos).











