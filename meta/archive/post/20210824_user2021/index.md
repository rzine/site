---
date: 2021-08-24T08:13:14-05:00
title: Les vidéos de la conférence useR!2021
subtitle: Des enregistrements mis à disposition sur la chaîne Youtube du R Consortium
summary: Quatre playlists ouvertes sur la chaîne Youtube du R Consortium
authors: 
- hpecout
categories:
- conférence
tags: 
- conférence
- UseR!
featured: false
draft: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

La conférence UseR!2021 s'est déroulée du 5 au 9 juillet 2021.    

De nombreux enregistrements ont été mis à disposition sur différentes playlists ([Keynotes](https://lnkd.in/gJwjB54e), [Regulars Talks](https://lnkd.in/g9XbY-Xs), [Tutorials](https://lnkd.in/gTePvB6b), [Elevator Pitches](https://lnkd.in/g8jfYNa5)) de la [chaîne Youtube du R Consortium](https://www.youtube.com/channel/UC_R5smHVXRYGhZYDJsnXTwg).

Retrouvez l'ensemble des liens vers ces différentes playlists sur le [site internet de la conférence UseR!2021](https://user2021.r-project.org/recordings/).







