---
date: "2022-03-31"
title: Prochaine séance RUSS le 1er avril 2022 (9h30-12h)
subtitle: Covid-19 - dataviz et vulgarisation à partir de données publiques
summary: Covid-19 - dataviz et vulgarisation à partir de données publiques, par Florence Débarre.
authors: ["hpecout"]
projects:
- RUSS
categories: []
tags: ["shs","RUSS","dataviz","vulgarisation"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---


**Covid-19 : dataviz et vulgarisation à partir de données publiques**, par Florence Débarre, chargée de recherche CNRS à l’Institut d’écologie et des sciences de l’environnement de Paris (iEES Paris).

Au cours de cet exposé, différents travaux de visualisation de données publiques réalisés au cours de la pandémie de Covid-19 à des fins de vulgarisation et élaborés à partir de données publiques seront présentés. Les sélections de sujets et de jeux de données seront abordés ainsi que les outils utilisés (R et ses dérivés), et les choix de représentations. Enfin, les écueils de communication grand public seront évoqués.

Cette séance se déroulera le **1er avril 2022, de 9h30 à 12h en visioconférence**.

L'inscription est obligatoire, retrouvez toutes les informations nécessaires sur le [site de RUSS](https://russ.site.ined.fr/fr/annee-2021-2022/vendredi-1-avril/).



