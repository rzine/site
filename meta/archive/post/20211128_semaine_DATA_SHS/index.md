---
date: "2021-11-28"
title: Semaine Data-SHS 2021
subtitle: De nombreux ateliers R durant cette semaine organisée par l'IR PROGEDO
summary: R est à l'honneur pour ce millésime 2021, une quinzaine d'ateliers se dérouleront dans toute la France durant cette semaine organisée par l'IR PROGEDO
authors: [hpecout]
projects: []

tags: 
- progedo
- shs
- séminaire
- atelier
- PUD

featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

Pour la **3eme année consécutive**, le réseau des plateformes universitaires de données, coordonnées par [l’IR PROGEDO](http://www.progedo.fr/) avec l’appui du [RnMSH](https://www.msh-reseau.fr/), organise sa **semaine DATA-SHS**. Cette semaine mobilise les communautés locales d’utilisateurs pour **mettre en valeur les ressources qu’offrent les données quantitatives en sciences humaines et sociales**.

Cette année, 13 PUD participent pour la plupart dans un format hybride, et **de nombreuses formations/ateliers R sont au programme**... Il est encore temps de s'inscrire ! **Récapitulatif :**

[**Plateforme Universitaire de Données de Dijon**](https://progedo.hypotheses.org/1776) (PUD-D) :   
- 7 déc. (9h-12h) - *Statistiques avancées et régressions multivariées avec le logiciel R* (1/2)   
- 9 déc. (9h-12h) - *Statistiques avancées et régressions multivariées avec le logiciel R* (2/2)   

[**Plateforme Universitaire de Données d’Aix-Marseille**](https://progedo.hypotheses.org/1454) (PUD-AMU) :   
- 9 déc. (14h): *Visualisation graphique de mes données avec R* (présentiel)   

[**Plateforme Universitaire de Données de Nantes**](https://progedo.hypotheses.org/1481) (PROGEDO-Loire) :    
- 10 déc. (14h-16h) - *Un logiciel pour tout faire ? A la découverte du logiciel R pour l’analyse des données : atouts, limites, interface et principes de fonctionnement*   

[**Plateforme Universitaire de Données de Lille**](https://progedo.hypotheses.org/1648) (PUD-L) :    
- 7 déc. (10h-17h30) - *Prise en main et premiers traitements d’une base de données sous R*   
- 9 déc. (9h-17h) - *Visualisation de données : bases et pratique en R et ggplot2*   
- 10 déc. (9h-17h) - *Régression linéaire : méthode et pratique sous R*   

[**Plateforme Universitaire de Données de Lyon-Saint-Etienne**](https://progedo.hypotheses.org/1724) (PUD-PANELS) : 
- 9 déc. - *Glitter makes SPARQL : glitter, un package R pour explorer et collecter des données du web sémantique*  
- 10 déc. - *R et espace : introduction à la pratique de l’analyse spatiale sous R*    

[**Plateforme Universitaire de Données Grenoble-Alpes**](https://progedo.hypotheses.org/1735) (PUD-GA) :    
- 6 déc. (14h) - *Traiter facilement ses données de A à Z avec R : prétraitement, statistiques descriptives &    inférentielles, modélisation et présentation des résultats avec RMarkdown* (1/2)  
- 7 déc. (9h-12h) - *Traiter facilement ses données de A à Z avec R : prétraitement, statistiques descriptives &    inférentielles, modélisation et présentation des résultats avec RMarkdown* (2/2)
- 9 déc. (14h-16h) - *Introduction à R avec les données de la vague 1 (enquête VICO)*   

[**Plateforme Universitaire de Données de Nanterre**](https://progedo.hypotheses.org/1626) (PUD-N) :   
- 8 déc. (13h30h-16h) - *Quelques exemples de traitement de données avec R*   

[**Plateforme Universitaire de Données de Toulouse**](https://progedo.hypotheses.org/1698) (PUD-T) :    
- 9 déc. (14h17h) - *Prise en main et premières explorations des données VICO avec le logiciel R*    

[**Plateforme Universitaire de Données de Strasbourg**](https://progedo.hypotheses.org/1909) (PUD-S) :     
- 8 déc. (14h-17h) - *Analyse de séquence sous R : une introduction et premières analyses*   
- 10 déc. (14h-17h) - *Analyse des correspondances sur R : mise en oeuvre et retour aux données*   

[**Plateformes Universitaires de Données de Poitiers**](https://progedo.hypotheses.org/1989) (PUD-UP):     
- 8 déc. (14h-17h) - *L’IA, application avec le logiciel R*   

**Retrouvez toutes les informations sur le** [**site web de l'IR PROGEDO**](https://progedo.hypotheses.org/1714). 

