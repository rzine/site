---
date: "2022-03-03"
title: Un Tuto@Mate sur le package {Rainette} le 15 mars
summary: Présentation de {Rainette}, un paquet R pour mettre œuvre la méthode de classification textuelle de Reinert.
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

Les Tuto@Mate reçoivent le mardi 15 mars 2022 de 14h00 à 16h00 [Julien Barnier](https://www.centre-max-weber.fr/Julien-Barnier) pour présenter `Rainette`.

`Rainette` est une extension pour R permettant de mettre en œuvre la méthode de classification textuelle de Reinert, déjà présente dans d’autres logiciels comme Alceste ou Iramuteq. Cette méthode exploratoire permet de déterminer, à partir d’un corpus textuel (réponses à des questions ouvertes, tweets, discours, entretiens…), des groupes de documents ou de segments de documents en fonction des termes qui les composent.

Retrouvez plus d'informations sur cette séance sur le site de MATE-SHS : 
https://mate-shs.cnrs.fr/actions/tutomate/tuto42-rainette-julien-barnier/


