---
authors:
- bgarnier
date: "2023-03-29T00:00:00Z"
draft: false
share: true  
profile: false 
 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2023-03-29T00:00:00Z"
projects: [RUSS]
title: 'Glitter makes SPARQL '
subtitle: '`glitter`, un package R pour explorer et collecter des données du web sémantique'
summary: Troisième séance du séminaire RUSS 2022/2023 qui se déroulera en format hybride le 7 avril 2023 et portera sur le package `glitter`, pour l'exploration et la collecte des données du web sémantique.
tags:
- séminaire
- RUSS
- glitter
- sparql
- web sémantique

---

La prochaine séance du séminaire *R à l'Usage des Sciences Sociales* (RUSS) aura lieu **vendredi 7 avril 2023** de **9h30 à 12h**.


**Glitter makes SPARQL : glitter, un package R pour explorer et collecter des données du web sémantique**, présenté par **Lise Vaudor** (CNRS, UMR 5600-EVS (Environnement Ville Société), plateforme ISIG (Ingéniérie Spatiale, Images et Géomatique).

L'**inscription est obligatoire** pour venir assister à l'Ined en salle Sauvy ou recevoir le lien de connexion à la visio conférence. Le formulaire d'inscription est disponible **sur** [**cette page**](https://russ.site.ined.fr/fr/annee-2022-2023/glitter-makes-sparql-glitter-un-package-r-pour-explorer-et-collecter-des-donnees-du-web-semantique/) (lien de connexion transmis avant le séance).

**Résumé :**   
La **collecte de données du web sémantique**, formalisées selon le modèle RDF, nécessite l'**élaboration de requêtes dans le langage dédié SPARQL**. Ce langage, qui est aux données du web sémantique ce que SQL est aux bases de données relationnelles, a ainsi un objectif très spécifique et demeure assez méconnu des utilisateurs de données. Au contraire, R est un **langage de programmation généraliste** puisqu'il permet de gérer de nombreux aspects de la chaîne de traitements de données, depuis leur recueil jusqu'à leur valorisation (par des modèles, graphiques, cartes, rapports, applications, etc.).

Le **package glitter** permet aux **utilisateurs de R sans connaissance préalable de SPARQL** (analystes de données, chercheurs, étudiants) d'**explorer et collecter les données du web sémantique**. En effet, il permet à l'utilisateur de générer des requêtes SPARQL, de les envoyer aux points d'accès de son choix, et de recueillir les données correspondantes. Ces étapes sont ainsi intégrées à l'environnement R dans lequel l'utilisateur peut également réaliser les étapes d'analyse et de valorisation des données, dans une chaîne de traitement reproductible.

Dans cette présentation, **les principales fonctionnalités du package glitter seront illustrées à partir d'exemples**. Le package, quoique toujours en développement, est fonctionnel et documenté et peut être installé par les participants qui souhaitent le tester en suivant les instructions décrites sur [cette page](https://github.com/lvaudor/glitter).


**Retrouvez les séances précédentes avec présentations et vidéos sur le** [**site dédié**](https://russ.site.ined.fr/fr/annee-2022-2023/glitter-makes-sparql-glitter-un-package-r-pour-explorer-et-collecter-des-donnees-du-web-semantique/#r29318).

Pour recevoir les annonces des séances du séminaire RUSS, inscrivez-vous à la [liste de diffusion](https://listes.ined.fr/subscribe/russ-sms).





