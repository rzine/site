---
title: "Ouverture des inscriptions à la conférence useR! 2022"
subtitle: Comme en 2021 la conférence se tiendra intégralement à distance
summary: Comme en 2021 la conférence se tiendra intégralement à distance
authors: 
- admin
date: 2022-04-12
categories: ["rzine"]
projects: [RencontresR]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
---

Les rencontres useR! 2022 se dérouleront en ligne du 20 au 23 juin

Les inscriptions sont ouvertes depuis le 30 mars. Il est nécessaire de consulter le programme et les tutoriels proposés afin de préciser vos choix lors de votre inscription. 

Retrouvez toutes les informations nécessaires sur le site de la conférence [**useR! 2022**](https://user2022.r-project.org/).   
Pour s'inscrire : [https://user2022.r-project.org/participate/registration/](https://user2022.r-project.org/participate/registration/)


