---
authors:
- admin
categories:
- formation
date: 2019-09-20T00:00:00Z
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2019-09-20T00:00:00Z"
projects: []
subtitle: 'Prévue en 2021 (institut des systèmes complexes)'
summary: Formation organisée par la FR CIST et l'axe Humanités numériques de l'UMR géographie-cités. **[REPORTÉ]**
tags:
- formation
- git
title: 'Formation initiation au GIT'
---



<div class="alert alert-danger" role="alert">
Afin de permettre à ceux et celles qui le souhaiteraient de prendre part aux actions de lutte contre les projets de réforme portés par le gouvernement, <b>la formation Git a été reportée jusqu'à nouvel ordre.</b>   
</div>
<br>
**Le CIST et l'axe humanité numérique de l'UMR géographie-cités organisent une journée d'initiation à l'utilisation de GIT. Cette formation s'adresse à un public débutant, désireux d'utiliser ce genre d'outil...**

## Lieu & date

La formation se déroulera le (*à définir*), de 9h à 17h30, à l'[Institut des systèmes complexes de paris Ile-de-France](https://iscpif.fr/)

## Prérequis

Les participants devront être munis de leur ordinateur personnel et posséder un compte Gitlab Humanum.
Pour demander l'ouverture d'un compte GitLab Huma-Num, contactez **assistance@huma-num.fr**

## Inscription

**Si cette formation vous interresse**, [**contactez-nous**](mailto:hugues.pecout@cnrs.fr)




