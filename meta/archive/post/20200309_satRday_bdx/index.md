---
authors:
- admin
categories:
- conférence
date: "2020-03-09T00:00:00Z"
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2020-03-09T00:00:00Z"
projects: [SatRdays]
subtitle: 'Bordeaux, le 16 mai 2020 '
summary: La prochaine conférence **satRdays** *France* se déroulera le **16 mai 2020** à **Bordeaux**. **[REPORTÉ]**
tags:
- SatuRdays
title: 'SatRdays - France'
---



Co-organisé par des membres de l'UMR PASSAGES, de la caisse des dépôts et de l'IRSTEA, **le prochain SatRdays** (*france*) **se déroulera le 16 mai 2020 à Bordeaux**.

Retrouvez **l'ensemble du programme** sur [**site de l'événement**](https://bordeaux2020.satrdays.org/).    
**L'inscription est gratuite**. Elle sera ouverte du 9 Mars au 15 Avril 2020.





