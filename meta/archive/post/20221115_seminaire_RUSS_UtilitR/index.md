---
authors:
- hpecout
date: "2022-11-09T00:00:00Z"
draft: false
share: true  
profile: false 
 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2022-11-09T00:00:00Z"
projects: [RUSS]
title: 'Première séance du séminaire RUSS 2022-2023'
subtitle: 'Présentation du projet [UtilitR](https://www.utilitr.org/)'
summary: La séance se déroulera en format hybride le 2 décembre 2022 et portera sur la présentation du projet [UtilitR](https://www.utilitr.org/).
tags:
- séminaire
- RUSS
- UtilitR
- Documentation
- INSEE

---

**La première séance 2022-2023 proposée par le séminaire RUSS se déroulera en format hybride** !

Cette séance intitulée **_utilitR : une documentation collaborative et ouverte sur l’usage de R pour l’analyse de données_** sera présentée par Olivier Meslin (Insee) et Lino Galiana (Insee), et se déroulera le **2 décembre** 2022 de **14h à 16h30**.

**Le projet [utilitR](https://www.utilitr.org/) vise à produire une documentation collaborative et open source sur R.**
Ce projet est parti du constat qu’il est difficile d’apprendre à utiliser R pour de multiples raisons : multiplicité de packages faisant plus ou moins la même chose, abondance et éclatement de la documentation (souvent en anglais), difficultés supplémentaires pour effectuer des choix éclairés et adaptés à certaines contraintes informatiques…
Les contributeurs du projet utilitR se sont ainsi fixés pour objectif de rassembler dans un seul document tous les éléments utiles pour l’usage de R en cherchant à couvrir la plupart des cas d’usage courants. La documentation a vocation à servir de point d’entrée aux tâches standards d’analyse de données et renvoyer vers des ressources pertinentes pour les usages avancés. Elle a vocation à satisfaire un public large, qu’il s’agisse de débutants ou d’utilisateurs expérimentés ayant besoin d’une aide-mémoire sur une tâche précise. 

Pour correspondre au mieux à des cas d’usages réels dans le domaine de l’analyse de données en sciences sociales, les exemples présents dans la documentation s’appuient tous sur des données diffusées en open-data par l’Insee et sont entièrement reproductibles.

Rendez-vous sur le [site de RUSS](https://russ.site.ined.fr/fr/annee-2022-2023/2-decembre-2022/) pour l'inscription (gratuite mais obligatoire).





