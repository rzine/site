---
authors:
- hpecout

date: 2020-06-09T21:13:14-05:00
draft: false
share: true  
profile: false  

featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true

title: 'Le catalogue des API INSEE se renforce'
subtitle: 'Une version bêta pour les *Données locales*'
summary: Une version bêta de l'API pour les *Données locales* est désormais disponible. Un exemple d'utilisation sous R est mis à disposition...

tags:
- INSEE
- API
- données
- locales
---


L'INSEE travail désormais sur l'**API** ***Données locales*** qui **permet d’accéder aux données localisées à la commune**, diffusées sur insee.fr dans la rubrique '**chiffres détaillés**' (ou ‘chiffres clés’ pour les populations légales).

Les statistiques sont disponibles pour tous les zonages géographiques allant de la commune à la France entière, c’est-à-dire pour les zonages administratifs (communes, arrondissements municipaux, arrondissements, intercommunalités/EPCI, départements, régions et France), ainsi que pour les zonages d’étude (zones d'emploi, aires urbaines, unités urbaines).

**Une version 0.1 (bêta) est déjà disponible**. Et Qui dit API, dit **R compatible** !

**L'INSEE fournie d'ailleurs un [exemple de script R](https://api.insee.fr/catalogue/site/themes/wso2/subthemes/insee/templates/api/documentation/download.jag?tenant=carbon.super&resourceUrl=/registry/resource/_system/governance/apimgt/applicationdata/provider/insee/DonneesLocales/V0.1/documentation/files/fct_acces_api.R) contenant des fonctions permettant d’importer les résultats.** 

**N'attendons pas la V.1 pour essayer !**

L'ensemble du catalogue des API de l'INSEE est disponible à ce [lien](https://api.insee.fr/catalogue/)

