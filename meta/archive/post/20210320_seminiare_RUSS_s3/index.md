---
authors:
- admin

date: "2021-03-20T00:00:00Z"
draft: false
share: true  
profile: false 
 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
subtitle: 'Séance 3 - Vendredi 2 avril (9h30-12h)'
summary: Séminaire RUSS - 2 avril 2021 (9h30-12h), par Luc-Olivier Hervé
tags:
- séminaire
- RUSS
projects: [RUSS]
title: 'RUSS - Visualiser et catégoriser des trajectoires'
---

**Visualiser et catégoriser des trajectoires : panorama des méthodes existantes, application de l’analyse de séquences par appariement optimal et démonstration d’une nouvelle application dédiée** (***ViCaTraj***)

L’intervention sera structurée comme suit :

- Présentation des différentes méthodes pour analyser des données longitudinales/calendaires/biographiques (dresser des typologies) : modèles économétriques ; analyse harmonique quantitative ; recodage disjonctif puis analyses multivariées ; cartes de kohonen ; analyse de séquences par appariement optimal (l’oma)
- Présentation de l’oma : origines, applications, étapes de mise en place
- Illustration d’application de l’oma sur des données relatives à des dispositifs d’aides départementaux (RSA notamment)
- Démonstration de l’application ViCaTraj, application R shiny développée en collaboration avec la MRIE et le Département de l’Isère.
- Pistes d’amélioration de *ViCaTraj*
- Echanges avec la salle sur la méthode et l’application

Rendez-vous sur le [site de RUSS](https://russ.site.ined.fr/fr/annee-2020-2021/vendredi-2-avril-2021/) pour l'inscription.





