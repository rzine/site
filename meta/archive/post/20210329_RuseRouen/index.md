---
authors:
- admin
categories:
- conférence
date: 2021-03-29T21:13:14-05:00
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
projects: []
subtitle: 'Atelier de lancement le 2 avril 2021'
summary: Un atelier de lancement est prévu en ligne le vendredi 2 avril, sur la manipulation et représentation des données Open Street Map
tags:
- group
- Ruser
title: "Un nouveau groupe d'utilisateurs à Rouen"
---

Un nouveau groupe d'utilisateur francophone vient de naître à Rouen, à l'initiative d'étudiants du [MASTER GAED](http://lsh.univ-rouen.fr/master-geographie-amenagement-environnement-et-developpement-traitement-information-pour-amenagement-et-developpement-543119.kjsp) de l'université de Rouen Normandie.   

Un **atelier de lancement est prévu (en ligne) le vendredi 2 avril 2021 de 18h à 20H** Le thème de ce premier atelier porte sur les **manipulations et les représentations de données Open Street Map** avec R.

Rejoignez la visioconférence à ce lien : [https://t.co/LIuNvTBe0j?amp=1](https://t.co/LIuNvTBe0j?amp=1)

Pour plus d'informations : 
- Twitter du groupe : [@RouenUsers](https://twitter.com/RouenUsers)
- Mail : [r.users.rouen@gmail.com](mailto:r.users.rouen@gmail.com)
