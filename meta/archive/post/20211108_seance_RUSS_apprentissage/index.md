---
date: "2021-11-08"
title: Prochaine séance RUSS le 3 décembre 2021 (9h30-12h)
subtitle: Retours d’expérience sur l’apprentissage, l’utilisation et l’assistance sur R en SHS
summary: Retours d’expérience sur l’apprentissage, l’utilisation et l’assistance sur R, par Judith Gourmelin, Elisabeth Morand et Bénédicte Garnier
authors: ["rysebaert"]
projects:
- RUSS
categories: []
tags: ["shs","RUSS","apprentissage","assistance"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

**Pour un usage réflexif de R dans sa pratique des sciences sociales - Retours d’expérience sur l’apprentissage, l’utilisation et l’assistance sur R**, par Judith Gourmelin (sociologue indépendante), accompagnée d'Elisabeth Morand et de Bénédicte Garnier (Ingénieures INED).
 

Après un présentation générale de R et son environnement, nous montrerons sa polyvalence au service des sciences sociales à partir de cas concrets et mettrons en avant l’avantage acquis après investissement dans cet outil. Nous tenterons par la suite d’identifier les freins au bon usage R et proposerons des esquisses de solution incitant à un apprentissage progressif et structuré de R, permettant de relier les vertus de cet apprentissage technique à l’analyse scientifique.

Retrouvez le formulaire d'inscription sur le [site de RUSS](https://russ.site.ined.fr/fr/annee-2021-2022/vendredi-3-decembre-2021/)



