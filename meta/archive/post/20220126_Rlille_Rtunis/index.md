---
date: "2022-01-26"
title: Collaboration de R Lille & R Tunis
summary: Les deux groupes d'utilisateurs collaborent pour l'organisation de *Meetups*. Le premier se déroulera le 24 Février 2022.
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

R Tunis et R Lille ont annoncé leur collaboration pour l'organisation de *Meetups* conjoints.

La première séance (francophone), intitulée : « ***De la Création au Déploiement d'Applications `shiny` avec `golem`*** » se déroulera en distanciel le 24 février à 18h30 et sera animée par Margot Brard (ThinkR).

Plus d'informations et inscription :  [https://www.meetup.com/fr-FR/R-Lille/](https://www.meetup.com/fr-FR/R-Lille/events/283419297/)

Bravo pour cette initiative !












