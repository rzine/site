---
authors:
- admin
categories:
- conférence
date: 2020-06-24T21:13:14-05:00
draft: false
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2020-06-24T00:00:00Z"
projects: []
subtitle: 'A global, virtual conference (5-7 July)'
summary: La conférence internationale useR!, prévue du **5 au 7 juillet 2021**, sera entièrement virtuelle
tags:
- conference
- useR
- virtual
title: 'The useR!2021 Conference'
---

useR! conferences are non-profit conferences organized by community volunteers for the community, supported by the R Foundation. Attendees include R developers and users who are data scientists, business intelligence specialists, analysts, statisticians from academia and industry, and students. The useR! 2021 conference will be the first R conference that is global by design, both in audience and leadership. Leveraging a diversity of experiences and backgrounds helps us to make the conference accessible and inclusive in as many ways as possible and to grow the global community of R users giving new talents access to this amazing ecosystem. Being virtual makes the conference more accessible to minoritized individuals and we strive to leverage that potential. We pay special attention to the needs of people with a disability to ensure that they can attend and contribute to the conference as conveniently as possible. Going fully virtual and global allows us to reimagine what an R conference can offer to presenters and attendees from across the globe and from diverse backgrounds.


Visit the website : [https://user2021.r-project.org/](https://user2021.r-project.org/)


