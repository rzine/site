---
date: "2022-01-24"
title: Un collection pour le partage de méthodes reproductibles avec R !
summary: Rzine lance sa collection de *notebook*, soumise à comité de lecture.
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

**Le comité éditorial Rzine a le plaisir de vous annoncer une nouvelle étape dans le projet : le lancement de [sa collection de publications](https://rzine.fr/collection/#collection)**. Cet espace de publications (open source, ouvertes à tout·es et soumises à évaluation) accueille des articles méthodologiques à visée pratique, présentant de façon didactique une ou des méthodes d'analyse en SHS, réalisées avec R et reproductibles.

Une des caractéristiques majeures de cette collection est son format de publication : le *notebook* (Rmarkdown). Ce format permet de combiner langages naturel et informatique et ainsi d'appliquer le paradigme de la programmation lettrée, au cœur du concept de la collection Rzine.

Quatre articles, appelés « fiches Rzine », ont déjà été publiés dans le cadre de la mise en place du processus de publication :

- Le Campion G., juin 2021, « [Analyse des corrélations avec easystats. Guide pratique avec R](https://rzine.fr/publication_rzine/20201127_glecampion_initiation_aux_correlations/) », Rzine, « Collection Rzine ».
- Le Texier M., juin 2021, « [Exploration spatio-temporelle d’objets, géographiques ponctuels. L’exemple du patrimoine architectural toulousain](https://rzine.fr/publication_rzine/20200601_mletexier86_explo_spatiotemporel/) », Rzine, « Collection Rzine ».
- Ysebaert R., Grasland C., octobre 2021, « [Analyse territoriale, multiscalaire. Application à la concentration de l’emploi dans la métropole du Grand Paris](https://rzine.fr/publication_rzine/20211101_ysebaert_grasland_mta/) », Rzine, « Collection Rzine ».
- Lambert N., octobre 2021, « [Le nouveau rideau de fer : un exemple de carte en 2,5D](https://rzine.fr/publication_rzine/20191125_ironcurtain/) », Rzine, « Collection Rzine ».


Si vous êtes intéressé·e par la publication, [un guide est disponible en ligne](https://rzine-reviews.github.io/documentation/) pour vous accompagner dans la production et la soumission d’une fiche Rzine.

Chaque fiche est soumise à comité de lecture. Une fois publiée, son stockage et sa diffusion pérenne sont assurés par les éditeur·rice·s (dépôt d’un DOI, référencement sur HAL, stockage et diffusion de la fiche et de son code source).

Au-delà de sa vocation à offrir un espace de publication innovant, l’objectif de cette collection est de contribuer, par de la documentation, l’encadrement des auteur·e·s de publication, voire la formation, à la montée en compétence collective et participative autour de la programmation lettrée et des systèmes de versionnage en science humaine et sociales.











