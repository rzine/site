---
authors:
- admin

date: "2021-01-15T00:00:00Z"
draft: false
share: true  
profile: false 
 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2020-03-09T00:00:00Z"
projects: [RinGrenoble]
subtitle: 'Version control and git'
summary: La 5e séance de travail du groupe *R in Grenoble* aura lieu le 28 janvier 2021, par visio-conférence
tags:
- séminaire
- git
- version
- reproductible

title: 'R in grenoble - Working session on git'
---


D
The fifth working session of the Grenoble R community will be about "Version control and git", by P. Jedynak and M. Rolland. Due to COVID-19 restrictions, this session will be held by ZOOM, from 17:00 to 18:00.

More informations : [https://r-in-grenoble.github.io/index.html](https://r-in-grenoble.github.io/index.html)






