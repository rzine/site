---
title: "Les Rencontres R 2023"

summary: Les 9èmes Rencontres R auront lieux à Avignon du 21 au 23 juin 2023
authors: 
- tgiraud
date: 2023-02-06T00:00:00-00:00
categories: ["rzine"]
projects: [RencontresR]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
---

Les 9èmes Rencontres R auront lieux à Avignon du 21 au 23 juin 2023.    

Les Rencontres R, portées par la Société Française de Statistique (SFdS), ont pour objectif d'offrir à la communauté francophone un lieu d'échange et de partage d'idées sur l'usage du langage R toutes disciplines confondues.  

L'édition 2023 est co-organisée par INRAE et Avignon Université. Elle s'adresse aussi bien aux débutants qu'aux utilisateurs confirmés et expérimentés issus de tous les secteurs d'activités.  




**Les inscriptions et les soumissions** sont ouvertes.  
**Retrouvez toutes les informations nécessaires sur le** [**site des rencontres R 2023**](https://rr2023.sciencesconf.org/).









