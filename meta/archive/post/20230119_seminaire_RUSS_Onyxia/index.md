---
authors:
- bgarnier
date: "2023-01-19T00:00:00Z"
draft: false
share: true  
profile: false 
 
featured: false
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
lastmod: "2023-01-19T00:00:00Z"
projects: [RUSS]
title: 'Onyxia, une plateforme de traitement de données moderne'
subtitle: 'Seconde séance du séminaire RUSS 2022-2023'
summary: La seconde séance du séminaire RUSS 2022/2023 se déroulera en format hybride le 3 février 2023 et portera sur la présentation du projet opensource [Onyxia](https://www.onyxia.sh/), développé par l’Insee.
tags:
- séminaire
- RUSS
- Onyxia
- SSPCloud 
- traitement de données
- calcul
- reproductibilité
- INSEE

---

La prochaine séance du séminaire *R à l'Usage des Sciences Sociales* (RUSS) aura lieu **vendredi 3 février 2023** de **9h30 à 12h**.

**Onyxia : Une plateforme de traitement de données moderne**, présentée par Frédéric Comte (Insee), Shiraz Adamaly (Insee) et Romain Avouac (Insee).

Inscription obligatoire pour venir assister à l'Ined en salle Sauvy ou recevoir le lien de connexion à la visio conférence [ici](https://russ.site.ined.fr/fr/annee-2022-2023/3-fevrier-2023/) (lien de connexion transmis avant le séance).

**Résumé :**   
Le projet opensource [Onyxia](https://www.onyxia.sh/), développé par l’Insee, permet de rendre accessible à des utilisateurs statisticiens aux compétences hétérogènes en informatique de nombreux moyens de traitement de données.
Ce projet est parti du constat que beaucoup d’entre eux travaillent les données directement sur leur poste de travail ce qui a des mauvaises propriétés :
- une puissance limitée
- une reproductibilité faible
- une sécurité des données rendue difficile avec leur dispersion

En s’appuyant sur cette technologie, l’Insee propose une plateforme en ligne nommée [SSPCloud](https://datalab.sspcloud.fr/home) permettant à tout agent de l’état et de nombreuses écoles et universités de se former ou de traiter des données ouvertes.

**Retrouvez les séances précédentes avec présentations et vidéos sur le** [**site dédié**](https://russ.site.ined.fr/fr/).

Pour recevoir les annonces des séances du séminaire RUSS, inscrivez-vous à la [liste de diffusion](https://listes.ined.fr/subscribe/russ-sms).





