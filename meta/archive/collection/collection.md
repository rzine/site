+++
title = "Collection Rzine"
subtitle = "Rmarkdown, Git et licences libres"
date = 2020-06-30
widget = "collection"
weight = 10 # Order that this section will appear.
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false

hero_media = "Rzine_logo.png"
+++

**La <a href="https://rzine.fr/publication_rzine/" target="_blank">Collection Rzine</a> offre un espace de publication pour des travaux présentant une ou des méthodes d'analyse avec R en sciences humaines et sociales de façon reproductible et didactique afin d'en faciliter leur application pratique**. Elle a pour but de favoriser :  

- la valorisation d'analyses faisant appel à des méthodes innovantes ou restées confidentielles afin de consolider la formation méthodologique en sciences humaines et sociales, en offrant un cadre de publication ouvert, gratuit et visible (recension sur <a href="https://hal.archives-ouvertes.fr/RZINE" target="_blank">HAL</a> et création d'un <a href="https://www.doi.org/" target="_blank">DOI</a>),   

- les échanges entre disciplines scientifiques et avec des acteurs non académiques, en prêtant une attention particulière à l'intelligibilité des contenus (texte, méthode et code source),

- le développement de protocoles d'analyse robustes, en proposant une procédure d'évaluation interactive entre deux évaluateurs désignés par le comité éditorial et l'auteur·rice, et en proposant une discussion de la publication ouverte aux membres de la communauté intéressés durant la phase d'évaluation,

- la montée en compétence collective et participative autour de la programmation lettrée avec R et des systèmes de versionnage.


**La collection Rzine est ouverte à tout·e·s. Les publications sont soumises à des relectures publiques. Actuellement, un seul format de publication est proposé : la « fiche ».**  


<figure class="center">
  <img src="/site/img/exo.png" width="100%">
  
  <figcaption style="font-size:14px;">Le modèle de mise en page (HTML) d'une fiche est fourni par le package Rzine : <a href="https://gitlab.huma-num.fr/rzine/package" target="_blank">https://gitlab.huma-num.fr/rzine/package</a>.</figcaption>

</figure>

Dans le cadre de la mise en place du processus de publication, quatre fiches Rzine ont déjà été publiées et servent de références éditoriales :

- <a href="/docs/20211101_ysebaert_grasland_MTA/index.html" target="_blank">Analyse territoriale multiscalaire</a>, *Ronan Ysebaert* et *Claude Grasland*.
- <a href="/docs/20191125_ironcurtain/index.html" target="_blank">Le nouveau rideau de fer : un exemple de carte en 2,5D</a>, *Nicolas Lambert*.
- <a href="/docs/20200526_glecampion_initiation_aux_correlations/index.html" target="_blank">Analyse des corrélations avec easystats</a>, *Grégoire Le Campion*.
- <a href="/docs/20200601_mletexier86_explo_spatiotemporel/index.html" target="_blank">Exploration spatio-temporelle d’objets géographiques ponctuels</a>, *Marion Le Texier*.


L'intégralité des fiches Rzine répond à des conditions d'utilisation peu restrictives :
- Les publications (HTML) sont soumises à la licence <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr" target="_blank">CC BY-SA 4.0</a>,      
- Les codes sources, mis à disposition sur un dépôt GitHub, sont soumis à la licence <a href="https://opensource.org/licenses/mit-license.php" target="_blank">MIT</a>,     
- Les données, obligatoirement libres d'utilisation, sont soumises à leur licence d'origine.  

</br>

**Pour connaître l'ensemble des règles de rédaction et de soumission d'une fiche Rzine, vous pouvez consulter ce document :** « <a href="https://rzine-reviews.github.io/documentation/" target="_blank"><b>Publier une fiche Rzine</b></a> ».

