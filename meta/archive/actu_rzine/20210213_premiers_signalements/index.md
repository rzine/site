---
title: "Premiers signalements de ressources"
summary: Des nouvelles ressources signalées ont été référencées.
authors: 
- admin
date: 2021-02-13T08:13:14-05:00
categories: ["rzine"]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
---

Une dizaine de ressources supplémentaires a été référencé grâce à des signalements d'utilisateurs.
Merci à ces premiers contributeurs, et tout particulièrement à ceux qui nous ont signalé des publications en français !











