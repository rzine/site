---
title: "Le lancement officiel de Rzine.fr approche !"
authors: 
- admin
date: 2020-12-24T21:13:14-05:00
categories: ["rzine"]
featured: true
image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
summary: La version 0.0.9 est en ligne... Joyeux Noël !
---


Après cette année 2020 un peu spéciale, vous n'avez même pas eu de cadeau de Noël pour vous remonter le moral ? Ne vous inquiétez-pas, Rzine sera là en 2021 pour vous réconforter ! 

Après plusieurs mois de préparation, **rzine.fr est bientôt opérationnel !**

Nous espérons que ce site collaboratif vous accompagnera dans votre pratique de R en 2021...








