---
date: "2023-05-01"
title: Une nouvelle fiche Rzine publiée
summary: '"Caractérisation des formes du relief à l’échelle de bassins-versants", Theureaux  O., Passy P., Feuillet T. et Birre  D.'
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

Le périmètre de la collection Rzine s'étend au délà des SHS. Ce projet porté par la [FR CIST](https://cist.cnrs.fr/) est ouvert à l'ensemble des disciplines des sciences territoriales. La géophysique a donc sa place dans la collection Rzine et le fait savoir.

[Olivier Theureaux](https://rzine.fr/authors/otheureaux/), [Paul Passy](https://rzine.fr/authors/ppassy/), [Thierry Feuillet](https://rzine.fr/authors/tfeuillet/) et [Déborah Birre](https://rzine.fr/authors/dbirre/) nous partagent une **méthode de caractèrisation des formes du relief à l'échelle de bassins-versants, via l'algorithme Geomorphons.** [**Plus d'info ?**](https://rzine.fr/publication_rzine/20230425_geomorphon/)

<center>
<a href="/docs/20230425_geomorphon/index.html" target="_blank" class="btn btn-info" role="button" aria-disabled="true"><i class="bi bi-cloud-arrow-down"></i> Consulter la fiche</a>
</center>

</br>

Nous remercions les auteurs pour leur partage, leur confiance et pour leur patience...















