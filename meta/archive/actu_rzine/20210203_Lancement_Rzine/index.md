---
title: "Naissance officielle de rzine.fr"
authors: 
- admin
date: 2021-02-03T08:13:14-05:00
categories: ["rzine"]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
summary: Après plusieurs mois de développement et de référencement de contenu, rzine est enfin prêt !
---

Après plusieurs mois de développement et de référencement de contenu, rzine est enfin prêt.
À ce jour, 210 ressources, 20 initiatives et 40 auteur.e.s sont référencés sur le site. 

Il s'agit d'une plateforme collaborative. N'hésitez pas à participer à l'[ajout de contenu](http://rzine.fr/communaute/#signal_ressource) !











