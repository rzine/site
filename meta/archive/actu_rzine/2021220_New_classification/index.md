---
date: "2021-12-24"
title: Mise à jour complète des ressources référencées
summary: La liste des ressources et leur classification ont été mises à jour.
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

**La liste des** [**ressources référencées**](https://rzine.fr/publication/) **a entièrement été mise à jour**. Certaines ressources ne répondant pas ou plus aux critères de référencement ont été supprimées. De nouvelles ressources seront prochainement ajoutées.

**Un travail important a été réalisé sur la classification des ressources par type et par thématique**, et **les outils de filtre ont été améliorés**. 

Les [ressources](https://rzine.fr/publication/) sont ainsi mieux renseignées et référencées. Il sera plus simple de les retrouver !










