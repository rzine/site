---
date: "2022-05-20"
title: Valorisation du projet Rzine
summary: Retrouvez les diaporamas de plusieurs communications sur Rzine et les notebooks réalisées au cours de ce dernières semaines. 
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

Rzine a fait l'objet de plusieurs communications au cours de ce printemps 2022. Les diaporamas utilisés sont consultables en ligne :

-  12 Avril 2022, [**Les Midis de Géotéca**](http://geoteca.u-paris.fr/actualites/midis-de-geoteca/) : ["***Présentation du projet Rzine***"](https://rzine.gitpages.huma-num.fr/communications/projet_rzine/#/).     

-  17 mai 2022, [**Journées Annuelles du réseau MATE-SHS**](https://ja-mate2022.sciencesconf.org/) : ["***Le Notebook et la programmation lettrée***"](https://huguespecout.github.io/notebook_mateshs/#/).

-  17 mai 2022, [**Journées Annuelles du réseau MATE-SHS**](https://ja-mate2022.sciencesconf.org/) : ["***Présentation du projet Rzine***"](https://rzine.gitpages.huma-num.fr/communications/projet_rzine/#/).

- 18 mai 2022, [**Colloque Humanistica**](https://humanistica2022.sciencesconf.org/) (table ronde intitulée "*Programming Historian en français, Rzine, ... & Tutos : Faire communauté autour des dispositifs et des formats alternatifs du savoir*") : ["***Présentation du projet Rzine***"](https://huguespecout.github.io/Rzine_Humanistica/#/).   










