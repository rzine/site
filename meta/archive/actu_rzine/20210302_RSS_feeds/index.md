---
title: "Plusieurs flux RSS disponibles"
summary: Suivez les actualités, et l'ajout de contenu récent
authors: 
- admin
date: 2021-03-01T00:00:00-00:00
categories: ["rzine"]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
---

Quatre flux RSS sont disponibles pour suivre les actualités et l'ajout de contenu récent :    

- Actualités communautaires : [http://rzine.fr/post/index.xml](http://rzine.fr/post/index.xml)    
- Actualités Rzine.fr : [http://rzine.fr/actu_rzine/index.xml](http://rzine.fr/actu_rzine/index.xml)    
- Publications Rzine : [http://rzine.fr/publication_rzine/index.xml](http://rzine.fr/publication_rzine/index.xml)    
- Ressources diverses : [http://rzine.fr/publication/index.xml](http://rzine.fr/publication/index.xml)     













