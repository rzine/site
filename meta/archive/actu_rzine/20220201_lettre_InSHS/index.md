---
date: "2022-02-01"
title: Rzine mis à l'honneur par l'InSHS
summary: Présentation du projet Rzine dans la lettre de l'InSHS n°75, de Janvier 2022.
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---


Le projet Rzine est mis à l'honneur dans la dernière [lettre de l'InSHS n°75](https://www.inshs.cnrs.fr/sites/institut_inshs/files/download-file/lettre_infoINSHS_75.pdf) (Janvier 2022). L'article de présentation du projet Rzine est également directement consultable à [ce lien](/docs/20220201_lettre_InSHS/rzine.pdf). 

Nous remercions le service de communication de l'Institut des sciences humaines et sociales du CNRS pour cette valorisation.










