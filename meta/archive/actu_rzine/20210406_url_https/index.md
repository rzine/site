---
title: "L'adresse de Rzine est sécurisée !"
summary: Accédez désormais au site avec le protocole sécurisé https 
authors: 
- admin
date: 2021-04-05T00:00:00-00:00
categories: ["rzine"]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
---

Depuis début avril, Rzine est accessible à l'adresse https://rzine.fr












