---
title: "Développement de Rzine.fr"
authors: 
- admin
date: 2019-09-15T21:13:14-05:00
categories: ["rzine"]
featured: true
image:
  caption: ''
  focal_point: ""
  preview_only: true
summary: Initié par un groupe interdisciplinaire d’ingénieur.e.s et de chercheur.e.s, le projet Rzine a notamment pour objectif de proposer un espace de référencement de documentation et de favoriser leur diffusion dans un périmètre pluridisciplinaire large
---



Initié par un groupe interdisciplinaire d’ingénieur.e.s et de chercheur.e.s, le projet Rzine a notamment pour objectif de proposer un espace stockage pérenne aux auteur.e.s de documentation de qualité et de favoriser leur diffusion dans un périmètre pluridisciplinaire large.

C’est pour répondre à cette objectif que **le développement de cette plateforme de stockage et de diffusion a démarré en septembre 2019**. Dans un premier temps, le site en développement est stocké et déployé depuis le Gitlab Huma-num proposé par le CNRS. **Il sera officiellement mis en ligne à l’adresse rzine.fr dans le courant de l’année 2020**.






