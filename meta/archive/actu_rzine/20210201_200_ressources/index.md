---
title: "Plus de 200 ressources référencées"
authors: 
- admin
date: 2021-01-30T08:13:14-05:00
categories: ["rzine"]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
summary: Articles, manuels, blogs, exercices, vidéos... Il y a du choix !
---

**Plus de 200 ressources (articles, livres, blogs, tutoriels, cours, vidéo...) sont dorénavant référencées sur Rzine**.
Il est possible de signaler une ressource afin qu'elle soit ajoutée sur le site en contactant les administrateurs du site à l'adresse **contact@rzine.fr**











