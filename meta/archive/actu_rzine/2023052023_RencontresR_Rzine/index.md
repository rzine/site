---
date: "2023-05-25"
title: Rzine aux Rencontres R 2023
summary: 'Le projet sera présenté aux Rencontres R 2023, qui se dérouleront à Avignon du 21 au 23 juin'
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---



Les Rencontres R, portées par la Société Française de Statistique ([SFdS](https://www.sfds.asso.fr/)), ont pour objectif d'offrir à la communauté francophone un lieu d'échange et de partage d'idées sur l'usage du langage R toutes disciplines confondues. Des membres Rzine seront présents à l'édition 2023, co-organisée par l'[INRAE](https://www.inrae.fr/) et [Avignon Université](https://univ-avignon.fr/), pour présenter le projet et la collection de publication Rzine à la communauté.

Consulter le diaporama de présentation du projet Rzine : [ici](https://rzine.gitpages.huma-num.fr/communications/rr2023/#/title-slide)

Retrouvez le [programme complet des Rencontres (pdf)](https://rr2023.sciencesconf.org/data/pages/programme_3.pdf), et plus d'informations sur le [site dédié](https://rr2023.sciencesconf.org/).





