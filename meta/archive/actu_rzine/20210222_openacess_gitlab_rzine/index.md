---
title: "Ouverture du code source de rzine.fr"
summary: Le dépôt Gitlab qui héberge le code source désormais accessible. 
authors: 
- admin
date: 2021-02-22T08:13:14-05:00
categories: ["rzine"]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
---

Rzine est produit avec R. Il s'agit d'un [blogdown](https://github.com/rstudio/blogdown) construit à partir du template [Hugo](https://gohugo.io/) [Academic](https://themes.gohugo.io/academic/).
Le code source est hébergé sur une instance [GitLab](https://about.gitlab.com/) mis à disposition par le [TGIR Huma-Num](https://www.huma-num.fr/) .

Ce code est soumis à la licence [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), c'est à dire complétement libre de droits.

**Code source de rzine.fr** : [https://gitlab.huma-num.fr/rzine/site](https://gitlab.huma-num.fr/rzine/site)











