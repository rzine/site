---
date: "2021-05-10"
title: Un atelier sur Rzine organisé par le FabPart Lab
subtitle: Documenter les pratiques de R en SHS
summary: Le 10 mai, les membres du projet Rzine ont été conviés par le FabPart Lab pour présenter le projet
authors: ["hpecout"]
projects: []
categories: []
tags: ["FabPart Lab"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

Le [FabPart Lab](https://fplab.parisnanterre.fr/#) a invité les membres du projet Rzine dans le cadre d'un [atelier pratique qui s'est tenu le 10 mai](https://fplab.parisnanterre.fr/ateliers/rzine_10052021.html), pour présenter les choix qui ont précédé la réalisation de Rzine : comment le projet s’est structuré, comment la communauté est mise à contribution, quelles catégories et quels principes pour l’organisation des connaissances, quelles pratiques de collaboration du protocole git et de la plateforme Gitlab etc...

Nous remercions les organisateurs pour cette invitation qui aura permis aux membres du projet de présenter Rzine pour la toute première fois !





