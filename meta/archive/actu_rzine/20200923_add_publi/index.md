---
title: "Une centaine de ressources déjà référencées"
authors: 
- admin
date: 2020-09-13T21:13:14-05:00
categories: ["rzine"]
featured: false
image:
  caption: ''
  focal_point: ""
  preview_only: true
summary: Rzine.fr n'est pas encore prêt... mais de nombreuses ressources y sont déjà référencées
---


Bien que le site soit toujours dans sa phase de développement, il est d'ores et déjà accessible à l'adresse rzine.fr.

**Plus d'une centaine de ressources on déjà été référencées sur la plateforme**. N'hésitez pas à les consulter !








