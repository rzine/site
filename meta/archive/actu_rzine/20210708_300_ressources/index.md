---
date: "2021-07-09"
title: Plus de 300 ressources référencées
summary: Vous avez prévu de vous mettre à R à la rentrée ? Votre livre d'été se trouve ici...
authors: ["hpecout"]
featured: false
image:
  caption: ""
  focal_point: ""
  placement: 1
  preview_only: true
---

**Plus de 300 ressources (articles, livres, blogs, tutoriels, cours, vidéo...) sont dorénavant référencées sur Rzine**.

N'hésitez pas à consulter les [dernières ressources référencées](https://rzine.fr/publication/), complètes et en français :

- **Cours et tutoriels proposés par l’axe sciences sociales computationnelles du CREST-CNRS**
- **Supports des ateliers de l'école thématique SIGR2021** portant sur les "Sciences de l’information géographique reproductibles"

Il est possible de signaler une ressource afin qu'elle soit ajoutée sur le site en contactant les administrateurs du site à l'adresse **contact@rzine.fr**







