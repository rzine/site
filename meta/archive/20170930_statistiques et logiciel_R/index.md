---
title: Statistiques et logiciel R
subtitle: Apprenez à analyser vos données
summary: Blog sur l'utilisation de R pour les statistiques.

authors:
- Claire Della Vedova

# Type de la publication 
publication_types: ["8"]


sources :
- "0"

thematics: ["10","11","12","13","1","7"]


update :
- "1"

languages:
- "0"

projects:
- divers

date: "2017-09-30T00:00:00Z"
doi: ""
 
featured: true
image:
  caption: "© Claire Della Vedova"
  focal_point: "Center"
  preview_only: false
  
url_source: 'https://statistique-et-logiciel-r.com/'

---

Blog de Claire Della Vedova (ingénieure en biostatistiques) qui partage ses connaissances et son utilisation des librairies R et de leurs fonctions.

