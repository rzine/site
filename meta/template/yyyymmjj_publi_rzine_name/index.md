---
# Author(s)
authors:
- auteur1
- auteur2

# Date
date: "yyyy-mm-dd"

# Title
title: xxxxxxxxxxxxxxxxxxxxxxx

# Short summary
summary: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

# Indiquer au minimum 1 thématique (numéro)
# cf. fchier metadata_list.txt dans le répertoire "meta" pour les numéros
# Si plusieurs sources -> sources: ["x", "x", "x"]
thematics: ["x"]

# Ne pas modifier
publication_types: ["2"]

# Ne pas modifier
projects:
- rzine

# Ne pas modifier
sources:
- "8"

# Tags
tags:
- xxx
- xx xxx
- xxxx x
- xx xxx
- xxx
- xxx
- xxxx


# Ne pas modifier
update :
- "0"

# Ne pas modifier (pour l'instant !)
languages:
- "0"

# Link
url_source: '/docs/xxxxxxxx/index.html'
url_code: 'https://github.com/rzine-reviews/xxxxxxxx'
url_dataset: '/docs/xxxxxxxx/xxxxx.zip'

links:
- name: HAL
  url: https://hal.archives-ouvertes.fr/hal-xxxxxxxx

# Image credit
image:
  caption: ''
  focal_point: "Center" # Do not modify
  preview_only: false # Do not modify
---


**Auteur** : [Prénom Nom](https://rzine.fr/authors/codename/), [Prénom Nom](https://rzine.fr/authors/codename/)    
**Evaluateur.rice.s** : [Prénom Nom](https://rzine.fr/authors/codename/), [Prénom Nom](https://rzine.fr/authors/codename/)    
**Editeur.rice.s** : [Prénom Nom](https://rzine.fr/authors/codename/), [Prénom Nom](https://rzine.fr/authors/codename)     


#### Résumé 

Cette fiche présente .....


#### Citation

<div style="font-size:15px;">
Ronan Y, Grasland C (2021). <i>"TITRE</i>, doi: 10.48645/HJVQ-YP94, URL: https://rzine.fr/publication_rzine/Nom_repertoire/.
</div>

<p class="d-inline-flex" style="gap:12px;"><a href="https://doi.org/10.48645/HJVQ-YP94"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://zenodo.org/badge/DOI/10.48645/HJVQ-YP94.svg" alt="DOI:10.48645/HJVQ-YP94"></a><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img style="gap:12px;margin-top:15px;margin-bottom:15px;padding:0;" src="https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg" alt="License: CC BY-SA 4.0"></a></p>

