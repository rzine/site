---
# code name de l'auteur (permet le lien avec les ressources, les actu...)
# Ce code doit être identique au nom du repertaoire auteur ! 
# Il sera a renseigner dans les publications ou actus dont liées à cet.te aut.eur.rice
authors:
- uunamed

# Nom affiché à l'écran 
name: Prénom Nom

role: Statut de l'auteur.rice
email: 'xxxx@xxxx.xx'

# Si un seul ou deux rattachements, supprimer le ou les tirets (2 lignes) en trop
organizations:
- name: Affiliation 1 (ex : FR CIST)
  url: "Lien vers le site de la FR CIST"
- name: Affiliation 2 (ex : CNRS)
  url: "Lien vers le site du CNRS"
- name: Affiliation 3 (ex : entreprise)
  url: "Lien vers le site de l'entreprise

# Bouton "social" sur la fiche auteur.rice
# Supprimer tirets (3 lignes) non nécessaires
# Ne modifier que la valeur de la variable "link:" pour les autres !
social:
# Mail
- icon: envelope
  icon_pack: fas
  link: 'mailto:xxxxxxxx@xxx.xx'
# Twitter
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/xxxxxx
# Site personnel
- icon: blog
  icon_pack: fa
  link: https://www.xxxxx.xx/
# Chaîne youtube
- icon: youtube
  icon_pack: fab
  link: https://www.youtube.com/xxxxxxx
# Github
- icon: github
  icon_pack: fab
  link: https://github.com/b-rodrigues
# Gitlab
- icon: gitlab
  icon_pack: fab
  link: https://xxxxxx
# Orcid
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/xxxx-xxxx-xxxx-xxxx
# figshare
- icon: figshare
  icon_pack: ai
  link: https://figshare.com/authors/xxxxxxx/xxxxxx
# Goggle scholar
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.fr/xxxxxxx
# Linkedin
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/xxxxxxxxx/

# Ne jamais mettre TRUE
superuser: false

# Ne garder que le ou les groupes souhaités (cf site web)
user_groups:
- Direction
- Édition
- Comité éditorial
- Publications Rzine
- Autres
---


