---
# MERCI DE SUPPRIMER LES COMMENTAIRES, LE DEPOT du site EST PUBLIC donc tout le monde peut les voir...

# Bien respecter cette mise en forme !
# Attention, Si la date est plus récente que la date du serveur qui deploy Rzine, vous ne verrez pas apparaitre la ressource en ligne ! 
# Essayer de trouver la date de création '(ou dernière mise à jour la ces échéant) de la ressource :
date: "2021-06-09T00:00:00Z"

# Titre de la ressource (si possible sur une seule ligne à l'affichage !)
# NE PAS UTILISER ":" à l'intérieur du titre 
title: xxxxxxxxxxxxxxxxxx

# Phrase qui résume explicitement le contenu de la ressource (visible uniquement dans la liste des ressources)
# Eviter plus de 170 character (la phrase n'est plus visible au delà...)
# NE PAS UTILISER ":"
summary: xxx xxx  xxxxx xxxx  xxxxx xxx

# Sous-titre, uniquement visible sur la fiche de la ressource 
# NE PAS UTILISER ":"
subtitle : xxxxxxxxxxxxxxxxx


# Indiquer le codename OU le vrai prénom et nom de l'auteur.rice si celui-ci n'est pas référencé dans Rzine
# Si qu'un seul auteur, supprimer les tirets supplémentaires
authors:
- xxxxxxx
- xxxxxxx
- xxxxxxx

# Indiquer au minimum 1 type de publication (numéro)
# cf. fchier metadata_list.txt dans le répertoire "meta"
# Si plusieurs type -> publication_types: ["x", "x", "x"]
publication_types: ["x"]

# Indiquer la source (ex : rOpenSci, rzine, russ...)
# cf. fchier metadata_list.txt dans le répertoire "meta" pour les numéros
# Si plusieurs sources -> sources: ["x", "x", "x"]
sources:
- "x"

# Indiquer au minimum 1 thématique (numéro)
# cf. fchier metadata_list.txt dans le répertoire "meta" pour les numéros
# Si plusieurs sources -> sources: ["x", "x", "x"]
thematics: ["x"]

# Ressource susceptible d'être mise à jour ?
# "0" - Ne sera mise à jour
# "1" - Suceptible d'être mise à jour
update:
- "0"

# Langue utilisée ?
# Si ressource en deux langues = construire 2 repertoires, un dans chaque langue !
# 0 - français
# 1 - anglais
# 2 - Espagnol
languages:
- "0"

# Rattaché à un projet référencé sur Rzine ?
# Si non, supprimer cette métadonnée
# Si oui, indiquer le nom du reperoire du projet ciblé dans "content/project" :
# utilitR
# stateoftheR
# spyrales
# SIGR2021
# seminr
# SatRdays
# RUSS
# RToulouse
# r_soc
# rseries
# rOpenSci
# RLille
# Rladies_montpellier
# RinGrenoble
# RencontresR
# R4ciam
# programming_historian
# parcoursr
# oreilly
# meetup
# grrr
# elementr
# dynamite
# cirad
# afrimapr
projects:
- RUSS

# Existence d'un DOI ?
# Si non, supprimer les lignes
# si oui, l'indiquez ici :
doi: "XX.XXXX/XXXXXX"

# Lien vers la ressource (OBLIGATOIRE) :
url_source: 'https://rcarto.github.io/spectre-reproductibilite-r/#1'
# Lien vers le code source de la ressource :
url_code: 'https://github.com/rCarto/spectre-reproductibilite-r'
# Lien vers la ressource en format pdf (si existe) : 
url_pdf: 'http://xxxxxxxxxxxxxxxxxxx'
# Lien vers un jeu de données utilisé par la ressource : 
url_dataset: 'http://xxxxxxxxxxxxxxx'

# Boutons/liens supplémentaires ?
# Quelques exemple déjà réalisés :
links:
- name: tutoriel
  url: https://www.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
- name: CRAN
  url: https://www.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
- name: Vignettes
  url: https://www.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
- name: Partie 2
  url: https://www.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
- name: Cheatsheet
  url: https://www.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
- name: Ressources associées
  url: https://www.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
- name: Video (partie 1)
  url: https://www.canal-u.tv/video/ined/reproductibilite_des_analyses_avec_r_partie_1_recherche_reproductible.62281
- name: Video (partie 2)
  url: https://www.canal-u.tv/video/ined/reproductibilite_des_analyses_avec_r_partie_2_gerer_ses_packages.62285
- name: Video (partie 3)
  url: https://www.canal-u.tv/video/ined/reproductibilite_des_analyses_avec_r_partie_3_utiliser_des_containers.62289

# No rules... mais c'est important !
# Autant de mot que vous voulez
# Attention, casse, accent et espace pris en considération dans l'outil de recherche 
tags:
- Motclef1
- mot clef 2
- expression1
- expression 2
- ...

# Ne pas modifier
featured: true
image:
  caption: ""
  focal_point: "Center"
  preview_only: false
---


#### Rédiger en markdown 

*Pas de titre level 1, 2 et 3. Uniquement level 4 ou plus*

**Texte/resumé de la ressource** qui sera affiché sur la page de la ressource.
*Vous pouvez mettre des [liens]() mais pas d'image SVP !*

Souvent je copie/colle l'introduction ou la préface, avec quelques ajustements.






